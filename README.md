# EXERCITIUM #

Colección de ejercicios resueltos propuestos en el blog [Exercitium](http://www.glc.us.es/~jalonso/exercitium/):

> El principal objetivo de este blog es servir de complemento a la asignatura [I1M](http://www.cs.us.es/~jalonso/cursos/i1m/) (Informática de 1º del Grado en Matemáticas),

> Cada día, de lunes a viernes, se propone a las 7:00 un ejercicio.

> Los ejercicios están clasificados por niveles: inicial, medio y avanzado.

> El nivel aparece al final de cada ejercicio.