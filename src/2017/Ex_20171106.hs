{-# LANGUAGE UnicodeSyntax #-}

module Ex_20171106 where

{-

Números completos
=================

Las descomposiciones de un número n son las parejas de números (x,y) tales que x >= y y la suma de las cuatro operaciones básicas (suma, producto, resta (el mayor menos el menor) y cociente (el mayor entre el menor)) es el número n. Por ejemplo, (8,2) es una descomposición de 36 ya que

   (8 + 2) + (8 - 2) + (8 * 2) + (8 / 2) = 36

Un número es completo si tiene alguna descomposición como las anteriores. Por ejemplo, el 36 es completo pero el 21 no lo es.

Definir las siguientes funciones

   descomposiciones :: Integer -> [(Integer,Integer)]
   completos        :: [Integer]

tales que

    (descomposiciones n) es la lista de las descomposiones de n. Por ejemplo,

     descomposiciones 12   ==  [(3,1)]
     descomposiciones 16   ==  [(3,3),(4,1)]
     descomposiciones 36   ==  [(5,5),(8,2),(9,1)]
     descomposiciones 288  ==  [(22,11),(40,5),(54,3),(64,2),(72,1)]
     descomposiciones 21   ==  []

    completos es la lista de los números completos. Por ejemplo,

     take 15 completos  ==  [4,8,9,12,16,18,20,24,25,27,28,32,36,40,44]
        ==  261


-}


-- Equivalencia:
--   (a + b) + (a - b) + (a * b) + (a / b) == (a/b) (b+1)²
-- por tanto:
-- a = n * b / (b+1)²

-- import           Data.Numbers.Primes


raiz :: Integer -> Integer
raiz = floor . sqrt .fromIntegral

descomposiciones :: Integer -> [(Integer,Integer)]
descomposiciones n = [(f b, b-1) | b <- [inicio, inicio-1..2]
                                 , n `mod` b^2 == 0]
  where
    inicio = raiz n
    f x = (x-1) * (n `div` x^2)

esCompleto :: Integer -> Bool
esCompleto n = any (n `esDivisiblePorCuadrado`) [ini,ini-1..2]
  where
    ini = raiz n
    esDivisiblePorCuadrado x a = x `mod` a^2 == 0


completos :: [Integer]
completos = filter esCompleto [1..]
