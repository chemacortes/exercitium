module Ex_20170124 where

{-

La sucesión "Mira y di"
=======================

La sucesión "Mira y di" (en inglés, [Look-and-Say][1]) es una sucesión de
números naturales en donde cada término se obtiene agrupando las cifras iguales
del anterior y recitándolas. Por ejemplo, si x(0) = 1 se lee como "un uno" y por
tanto x(1) = 11. Análogamente,

   x(1) = 11     ("dos unos                          --> "21")
   x(2) = 21     ("un dos un uno                     --> "1211")
   x(3) = 1211   ("un uno un dos dos unos            --> "111221")
   x(4) = 111221 ("tres unos dos doses un uno        --> "312211")
   x(5) = 312211 ("un tres un uno dos doses dos unos --> "13112221")

Definir la función

​​​  ​sucMiraYDi :: Integer -> [Integer]

tal que (sucMiraYDi n) es la sucesión "Mira y di" cuyo primer término es n. Por
ejemplo,

   λ> take 9 (​sucMiraYDi 1)
   [1,11,21,1211,111221,312211,13112221,1113213211,31131211131221]
   λ> take 5 (​sucMiraYDi 2017)
   [2017,12101117,111211103117,31123110132117,1321121321101113122117]
   λ> take 7 (sucMiraYDi 22)
   [22,22,22,22,22,22,22]
   λ> (sucMiraYDi 1) !! 11
   3113112221232112111312211312113211
   λ> (sucMiraYDi 1) !! 12
   1321132132111213122112311311222113111221131221

Independientemente del término inicial x(0) elegido (con la única salvedad del
22), la sucesión diverge y la razón entre el número de cifras de x(n) y el de
x(n-1) tiende a un valor fijo que es la constante de Conway λ ≈ 1.303577269. Por
ejemplo, para x(0) = 1, las razones son

   2, 1, 2, 1.5, 1, 1.3333333333333333, 1.25, 1.4, 1.4285714285714286, 1.3

Definir la función

   aproximacionConway :: Integer -> Double -> Int

tal que (aproximacionConway n e) es el menor k tal que la diferencia entre la
[constante de Conway][2] y la razón entre el número de cifras de x(k) x(k-1) es,
en valor absoluto, menor que e. Por ejemplo,

   aproximacionConway 1 0.3     ==  3
   aproximacionConway 1 0.1     ==  5
   aproximacionConway 1 0.01    ==  9
   aproximacionConway 1 0.001   ==  24
   aproximacionConway 1 0.0001  ==  43
   aproximacionConway 2 0.0001  ==  47

Nota: Este ejercicio ha sido propuesto por Elías Guisado.

[1]: https://en.wikipedia.org/wiki/Look-and-say_sequence "Look-and-say sequence"
[2]: https://es.wikipedia.org/wiki/Constante_de_Conway "Constante de Conway"

-}

import Data.List (group)

sucMiraYDi :: Integer -> [Integer]
sucMiraYDi = sucMiraYDi3

sucMiraYDi1 :: Integer -> [Integer]
sucMiraYDi1 = iterate (read . aux)
  where
    aux n = concat [ (show . length) xs ++ take 1 xs | xs <- (group.show) n ]


sucMiraYDi2 :: Integer -> [Integer]
sucMiraYDi2 = iterate (read . concatMap miraYdi <$> group . show)
  where
    miraYdi = (++) <$> show . length <*> take 1

sucMiraYDi3 :: Integer -> [Integer]
sucMiraYDi3 = fmap read . sucMiraYDiX . show

sucMiraYDiX :: String -> [String]
sucMiraYDiX = iterate (concatMap miraYdi . group)
  where
    miraYdi = (++) <$> show . length <*> take 1


cteConway :: Double
cteConway =  1.303577269

aproximacionConway :: Integer -> Double -> Int
aproximacionConway n e = length . takeWhile (>=e)
                       $ zipWith diff xs (tail xs)
  where
    xs = fmap length (sucMiraYDiX (show n))
    diff x y = abs (cteConway - (fromIntegral y / fromIntegral x))
