{-# LANGUAGE UnicodeSyntax #-}

module Ex_20171120 where

{-

Número de divisores
===================

Definir la función

   numeroDivisores :: Integer -> Integer

tal que (numeroDivisores x) es el número de divisores de x. Por ejemplo,

   numeroDivisores 12  ==  6
   numeroDivisores 25  ==  3
   length (show (numeroDivisores (product (take 20000 primes)))) == 6021


-}

import           Data.List
import           Data.Numbers.Primes

numeroDivisores :: Integer -> Integer
numeroDivisores = product . fmap (1+) . factores
  where
    factores = map genericLength . group . primeFactors


numeroDivisores2 :: Integer -> Integer
numeroDivisores2 = product . fmap ((1+).genericLength)
                 <$> group . primeFactors
