module Ex_20170130 where

{-

Máximo producto de pares en la lista
====================================

Definir la función

  maximoProducto :: [Int] -> Maybe Int

tal que (maximoProducto xs) es el mayor elemento de xs que se puede escribir
como producto de dos elementos distintos de xs o Nothing, en el caso de que
ningún elemento de xs se pueda escribir como producto de dos elementos
distintos de xs. Por ejemplo,

  maximoProducto [10, 3, 5, 30, 35]       ==  Just 30
  maximoProducto [10, 2, 2, 4, 30, 35]    ==  Just 4
  maximoProducto [17, 2, 1, 35, 30]       ==  Just 35
  maximoProducto [2, 5, 7, 8]             ==  Nothing
  maximoProducto [10, 2, 4, 30, 35]       ==  Nothing
  maximoProducto [1+2^n | n <- [1..10^6]] ==  Just 4611686018427387905

En el primer ejemplo, 30 es el producto de 10 y 3; en el segundo, 4 es el
producto de 2 y 2 y en el tercero, 35 es el producto de 1 y 35.

-}

import Data.List
import Data.Maybe

-- 1ª definición (poco eficiente)
maximoProducto1 :: [Int] -> Maybe Int
maximoProducto1 = aux . sortBy (flip compare)
  where aux xs = listToMaybe $ xs \\ ((*) <$> xs <*> xs)

-- 2ª definición
maximoProducto2 :: [Int] -> Maybe Int
maximoProducto2 xs | 1 `elem` xs  = Just (maximum xs)
                   | otherwise    = fmap head . find esProducto
                                  $ tails (sortBy (flip compare) xs)

esProducto :: [Int] -> Bool
esProducto []      = False
esProducto (x:xs)  = not.null
                   $ [y | (y:ys) <- tails xs
                        , x `mod` y == 0
                        , x `div` y `elem` ys ]
