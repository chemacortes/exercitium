module Ex_20170111 where

{-

Sucesión de capicúas
====================

Definir las funciones

   capicuas        :: [Integer]
   posicionCapicua :: Integer -> Integer

tales que

    capicuas es la sucesión de los números capicúas. Por ejemplo,

   λ> take 45 capicuas
   [0,1,2,3,4,5,6,7,8,9,11,22,33,44,55,66,77,88,99,101,111,121,131,
    141,151,161,171,181,191,202,212,222,232,242,252,262,272,282,292,
    303,313,323,333,343,353]
   λ> capicuas !! (10^5)
   900010009

    (posicionCapicua x) es la posición del número capicúa x en la sucesión de los capicúas. Por ejemplo,

   λ> posicionCapicua 353
   44
   λ> posicionCapicua 900010009
   100000
   λ> let xs = show (123^30)
   λ> posicionCapicua (read (xs ++ reverse xs))
   1497912859868342793044999075260564303046944727069807798026337448
   λ> posicionCapicua (read (xs ++ "7" ++ reverse xs))
   5979128598683427930449990752605643030469447270698077980263374496

-}


capicuas :: [Integer]
capicuas = capicuas2

capicuas1 :: [Integer]
capicuas1 = 0 : [ read xs | xs <- concat (tail xss), head xs /= '0']
  where
    wrap xs = [ (c:x) ++ [c] | c <- ['0'..'9'], x <- xs]
    xss = [""] : map show [0..9] : map wrap xss


capicuas2 :: [Integer]
capicuas2 = 0 : map read (capicuasAux [1..9])

capicuasAux :: [Integer] -> [String]
capicuasAux xs =  map duplica1 xs'
               ++ map duplica2 xs'
               ++ capicuasAux [head xs * 10 .. last xs * 10 + 9]
  where
    xs' = map show xs
    duplica1 = (++) <$> id <*> tail.reverse
    duplica2 = (++) <$> id <*> reverse


posicionCapicua :: Integer -> Integer
posicionCapicua n = (read . take (m+r)) (show n) - 1 + 10^m
   where (m,r) = (length . show) n `divMod` 2
