module Ex_20170125 where

{-

Árboles continuos
=================

Los árboles binarios se pueden representar con el de tipo de dato algebraico

   data Arbol a = H
                | N a (Arbol a) (Arbol a)
     deriving Show

Por ejemplo, los árboles

       3                7
      / \              / \
     2   4            5   8
    / \   \          / \   \
   1   3   5        6   4   10

se representan por

   ej1, ej2 :: Arbol Int
   ej1 = N 3 (N 2 (N 1 H H) (N 3 H H)) (N 4 H (N 5 H H))
   ej2 = N 7 (N 5 (N 6 H H) (N 4 H H)) (N 8 H (N 10 H H))

Un árbol binario es continuo si el valor absoluto de la diferencia de los
elementos adyacentes es 1. Por ejemplo, el árbol ej1 es continuo ya que el valor
absoluto de sus pares de elementos adyacentes son

   |3-2| = |2-1| = |2-3| = |3-4| = |4-5| = 1

En cambio, el ej2 no lo es ya que |8-10| ≠ 1.

Definir la función

   esContinuo :: (Num a, Eq a) => Arbol a -> Bool

tal que (esContinuo x) se verifica si el árbol x es continuo. Por ejemplo,

   esContinuo ej1  ==  True
   esContinuo ej2  ==  False

-}


data Arbol a = H
             | N a (Arbol a) (Arbol a)
             deriving Show


ej1, ej2 :: Arbol Int
ej1 = N 3 (N 2 (N 1 H H) (N 3 H H)) (N 4 H (N 5 H H))
ej2 = N 7 (N 5 (N 6 H H) (N 4 H H)) (N 8 H (N 10 H H))

esContinuo :: (Num a, Eq a) => Arbol a -> Bool
esContinuo H          = True
esContinuo (N x i d)  = and $ [f x, esContinuo] <*> [i,d]
  where
    f _ H         = True
    f y (N z _ _) = abs (y-z) == 1
