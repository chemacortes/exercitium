module Ex_20170119 where

{-

La conjetura de Rodolfo
=======================

El pasado 1 de enero, Claudio Meller publicó el artículo [La conjetura de Rodolfo][1]
que afirma que

    > Todos los números naturales se pueden números pueden expresarse como la suma
    > de un capicúa y un capicúa especial (siendo los capicúas especiales los
    > números que al quitarles los ceros finales son capicúas; por ejemplo,
    > 32300, 50500 y 78987).

Definir las funciones

   descomposiciones               :: Integer -> [(Integer, Integer)]
   contraejemplosConjeturaRodolfo :: [Integer]

tales que

    (descomposiciones x) es la lista de las descomposiciones de x como la suma
    de un capicúa y un capicúa especial. Por ejemplo,

     descomposiciones 1980  ==  [(99,1881),(979,1001)]
     descomposiciones 2016  ==  [(575,1441),(606,1410)]
     descomposiciones 1971  ==  [(161,1810),(1771,200),(1881,90)]

    contraejemplosConjeturaRodolfo es la lista de contraejemplos de la conjetura
    de Rodolfo; es decir, de los números que no pueden expresarse com la suma de
    un capicúa y un capicúa especial. Por ejemplo,

     λ> take 12 contraejemplosConjeturaRodolfo
     [1200,1220,1240,1250,1260,1270,1280,1290,1300,1330,1350,1360]
     λ> take 12 (dropWhile (< 2000) contraejemplosConjeturaRodolfo)
     [3020,3240,3350,3460,3570,3680,3920,4030,4250,4360,4470,4580]

[1]: http://simplementenumeros.blogspot.com.es/2017/01/1472-la-conjetura-de-rodolfo.html

-}

import Data.Tuple (swap)
import Data.List (nubBy)

import Ex_20170111 (capicuas)

descomposiciones :: Integer -> [(Integer, Integer)]
descomposiciones n =  nubBy f [(a, n-a) | a <- takeWhile (<= n) capicuas
                                        , esCapicuaEspecial (n-a)]
  where f t u = t==u || t==swap u

contraejemplosConjeturaRodolfo :: [Integer]
contraejemplosConjeturaRodolfo = [i | i <- [1..], null (descomposiciones i)]

esCapicua :: Integer -> Bool
esCapicua = (==) <$> id <*> reverse <$> show

esCapicuaEspecial :: Integer -> Bool
esCapicuaEspecial = (==) <$> id <*> reverse
                         <$> dropWhile (=='0').reverse.show
