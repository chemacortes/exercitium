{-# LANGUAGE UnicodeSyntax #-}

module Ex_20171114 where

{-

Mayor número equidigital
========================

Definir la función

   mayorEquidigital :: Integer -> Integer

tal que (mayorEquidigital x) es el mayor número que se puede contruir con los dígitos de x. Por ejemplo,

   mayorEquidigital 13112017  ==  73211110
   mayorEquidigital2 (2^100)  ==  9987776666655443322222211000000

-}

import           Data.List (sort)

mayorEquidigital :: Integer -> Integer
mayorEquidigital = read . reverse . sort . show
