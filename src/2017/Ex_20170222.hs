module Ex_20170222 where

{-

Cálculo de pi mediante la fracción continua de Lange
====================================================

En 1999, L.J. Lange publicó el artículo [An elegant new continued fraction for π][1].

[1]: http://www.maa.org/sites/default/files/pdf/pubs/amm_supplements/Monthly_Reference_11.pdf

En el primer teorema del artículo se demuestra la siguiente expresión de π mediante una fracción continua

![Calculo_de_pi_mediante_la_fraccion_continua_de_Lange](Ex_20170222-Calculo_de_pi_mediante_la_fraccion_continua_de_Lange.png)

La primeras aproximaciones son

   a(1) = 3+1                = 4.0
   a(2) = 3+(1/(6+9))        = 3.066666666666667
   a(3) = 3+(1/(6+9/(6+25))) = 3.158974358974359

Definir las funciones

   aproximacionPi :: Int -> Double
   grafica        :: [Int] -> IO ()

tales que

    (aproximacionPi n) es la n-ésima aproximación de pi con la fracción continua de Lange. Por ejemplo,

     aproximacionPi 1     ==  4.0
     aproximacionPi 2     ==  3.066666666666667
     aproximacionPi 3     ==  3.158974358974359
     aproximacionPi 10    ==  3.141287132741557
     aproximacionPi 100   ==  3.141592398533554
     aproximacionPi 1000  ==  3.1415926533392926

    (grafica xs) dibuja la gráfica de las k-ésimas aproximaciones de pi donde k toma los valores de la lista xs. Por ejemplo, (grafica [1..10]) dibuja
    Calculo_de_pi_mediante_la_fraccion_continua_de_Lange_2
    (grafica [10..100]) dibuja
    Calculo_de_pi_mediante_la_fraccion_continua_de_Lange_3
    y (grafica [100..200]) dibuja
    Calculo_de_pi_mediante_la_fraccion_continua_de_Lange_4

Nota: Este ejercicio ha sido propuesto por Antonio Morales.

-}

import Graphics.Gnuplot.Simple

aproximacionPi :: Int -> Double
aproximacionPi n = (-3) + foldr (\x acc -> 6 + fromIntegral x / acc) 1 xs
  where
    xs = map (^2) [1,3..2*n-1]

grafica :: [Int] -> IO ()
grafica xs =
    plotList [Key Nothing]
             [(k,aproximacionPi k) | k <- xs]
