module Ex_20170201 where

{-

Subrayado de un carácter
========================

Definir el procedimiento

   subraya :: String -> Char -> IO ()

tal que (subraya cs c) escribe la cadena cs y debajo otra subrayando las
ocurrencias de c. Por ejemplo,

   λ> subraya "Salamanca es castellana" 'a'
   Salamanca es castellana
    ^ ^ ^  ^     ^     ^ ^
   λ> subraya "Salamanca es castellana" 'n'
   Salamanca es castellana
         ^              ^
   λ> subraya "Salamanca es castellana" ' '
   Salamanca es castellana
            ^  ^

-}

subraya :: String -> Char -> IO ()
subraya cs c = do putStrLn cs
                  putStrLn [if x==c then '^' else ' ' | x <- cs]
