module Ex_20170112 where

{-

Sumas de tres capicúas
======================

Definir la función

   sumas3Capicuas  :: Integer -> [(Integer, Integer, Integer)]

tales que (sumas3Capicuas x) es la lista de las descomposiciones de x como suma
de tres capicúas (con los sumandos no decrecientes). Por ejemplo,

   sumas3Capicuas 0  ==  [(0,0,0)]
   sumas3Capicuas 1  ==  [(0,0,1)]
   sumas3Capicuas 2  ==  [(0,0,2),(0,1,1)]
   sumas3Capicuas 3  ==  [(0,0,3),(0,1,2),(1,1,1)]
   sumas3Capicuas 4  ==  [(0,0,4),(0,1,3),(0,2,2),(1,1,2)]
   length (sumas3Capicuas 17)      ==  17
   length (sumas3Capicuas 2017)    ==  47
   length (sumas3Capicuas 999999)  ==  15266

Comprobar con QuickCheck que todo número natural se puede escribir como suma de
tres capicúas.

-}

import Test.QuickCheck
import Ex_20170111 (capicuas)


sumas3Capicuas :: Integer -> [(Integer, Integer, Integer)]
sumas3Capicuas n = [ (a,b,c) | a <- xs
                             , b <- intervalo (a, (n-a) `div` 2) xs
                             , let c = n - a - b
                             , esCapicua c ]
  where xs = takeWhile (<= n) capicuas
        intervalo (a,b) = takeWhile (<= b) . dropWhile (<a)
        esCapicua = (==) <$> id <*> reverse <$> show


prop_triocapicuas :: Positive Integer -> Bool
prop_triocapicuas = not.null.sumas3Capicuas . getPositive
