{-# LANGUAGE UnicodeSyntax #-}

module Ex_20171109 where

{-

Biparticiones de un número
==========================

Definir la función

   biparticiones :: Integer -> [(Integer,Integer)]

tal que (biparticiones n) es la lista de pares de números formados por las primeras cifras de n y las restantes. Por ejemplo,

   biparticiones  2025  ==  [(202,5),(20,25),(2,25)]
   biparticiones 10000  ==  [(1000,0),(100,0),(10,0),(1,0)]

-}

import           Data.List

biparticiones :: Integer -> [(Integer,Integer)]
biparticiones n = reverse $ zip ys zs
    where
        xs = show n
        ys = (fmap read.init.tail.inits) xs :: [Integer]
        zs = (fmap read.init.tail.tails) xs :: [Integer]


biparticiones2 :: Integer -> [(Integer,Integer)]
biparticiones2 n =
    do
        i <- [p-1,p-2..1]
        let (x,y) = splitAt i xs
        return (read x, read y)
    where
        xs = show n
        p = length xs

biparticiones3 :: Integer -> [(Integer,Integer)]
biparticiones3 n =
    [(read x,read y)|i <- [p-1,p-2..1], let (x,y) = splitAt i xs ]
    where
        xs = show n
        p = length xs

biparticiones4 :: Integer -> [(Integer,Integer)]
biparticiones4 n =
  [quotRem n (10^x) | x <- [1..length (show n) -1]]
