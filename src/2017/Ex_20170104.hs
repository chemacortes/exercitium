module Ex_20170104 where

{-

Familias de números con algún dígito en común
=============================================

Una familia de números es una lista de números tal que todos tienen la misma
cantidad de dígitos y, además, dichos números tienen al menos un dígito común.

Por ejemplo, los números 72, 32, 25 y 22 pertenecen a la misma familia ya que
son números de dos dígitos y todos tienen el dígito 2, mientras que los números
123, 245 y 568 no pertenecen a la misma familia, ya que no hay un dígito que
aparezca en los tres números.

Definir la función

   esFamilia :: [Integer] -> Bool

tal que (esFamilia ns) se verifica si ns es una familia de números. Por ejemplo,

   esFamilia [72, 32, 25, 22]  ==  True
   esFamilia [123,245,568]     ==  False
   esFamilia [72, 32, 25, 223] ==  False

-}

import Data.List
import Control.Monad (liftM2)


esFamilia :: [Integer] -> Bool
esFamilia xs =  length (nub ls) == 1
             && (not . null . foldl1 intersect) ds
  where (ds, ls) = unzip [ (nub ys, length ys) | ys <- map show xs ]


esFamilia2 :: [Integer] -> Bool
esFamilia2 = liftM2 (&&) f g . map show
  where f = ((==1) . length) . nub . map length
        g = (not . null) . foldl1 intersect

esFamilia3 :: [Integer] -> Bool
esFamilia3 = (&&) <$> f <*> g <$> fmap show
  where f = ((==1) . length) . nub . map length
        g = (not . null) . foldl1 intersect
