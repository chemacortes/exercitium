{-# LANGUAGE UnicodeSyntax #-}

module Ex_20171127 where

{-

Conjunto de relaciones binarias entre dos conjuntos
===================================================

Una [relación binaria][1] entre dos conjuntos A y B se puede representar mediante un conjunto de pares (a,b) tales que a ∈ A y b ∈ B. Por ejemplo, la relación < entre A = {1,5,3} y B = {0,2,4} se representa por {(1,2),(1,4),(3,4)}.

[1]: https://en.wikipedia.org/wiki/Binary_relation "Relación Binaria"

Definir las funciones

   relaciones  :: [a] -> [b] -> [[(a,b)]]
   nRelaciones :: [a] -> [b] -> Integer

tales que

    (relaciones xs ys) es el conjunto de las relaciones del conjunto xs en el conjunto ys. Por ejemplo,

     λ> relaciones [1] [2]
     [[],[(1,2)]]
     λ> relaciones [1] [2,4]
     [[],[(1,2)],[(1,4)],[(1,2),(1,4)]]
     λ> relaciones [1,3] [2]
     [[],[(1,2)],[(3,2)],[(1,2),(3,2)]]
     λ> relaciones [1,3] [2,4]
     [[],[(1,2)],[(1,4)],[(1,2),(1,4)],[(3,2)],[(1,2),(3,2)],
      [(1,4),(3,2)],[(1,2),(1,4),(3,2)],[(3,4)],[(1,2),(3,4)],
      [(1,4),(3,4)],[(1,2),(1,4),(3,4)],[(3,2),(3,4)],
      [(1,2),(3,2),(3,4)],[(1,4),(3,2),(3,4)],
      [(1,2),(1,4),(3,2),(3,4)]]
     λ> relaciones [] []
     [[]]
     λ> relaciones [] [2]
     [[]]
     λ> relaciones [1] []
     [[]]

    (nRelaciones xs ys) es el número de relaciones del conjunto xs en el conjunto ys. Por ejemplo,

     nRelaciones [1,2] [4,5]    ==  16
     nRelaciones [1,2] [4,5,6]  ==  64
     nRelaciones [0..9] [0..9]  ==  1267650600228229401496703205376

-}

import           Data.List (genericLength, subsequences)

relaciones  :: [a] -> [b] -> [[(a,b)]]
relaciones xs ys = subsequences $ (,) <$> xs <*> ys

nRelaciones :: [a] -> [b] -> Integer
nRelaciones xs ys = genericLength $ relaciones xs ys

nRelaciones2 :: [a] -> [b] -> Integer
nRelaciones2 xs ys = 2^ (length xs * length ys)
