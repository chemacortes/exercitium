module Ex_20170108 where

{-

Caminos desde la raíz en un árbol binario
=========================================

Los árboles binarios se pueden representar con el de tipo de dato algebraico

   data Arbol = H
              | N Int Arbol Arbol
     deriving Show

Por ejemplo, los árboles

       3                7
      / \              / \
     2   4            5   8
    / \   \          /   / \
   1   7   5        6   4   9

se representan por

   ej1, ej2 :: Arbol
   ej1 = N 3 (N 2 (N 1 H H) (N 7 H H)) (N 4 H (N 5 H H))
   ej2 = N 7 (N 5 (N 6 H H) H) (N 8 (N 4 H H) (N 9 H H))

Definir la función

   caminosDesdeRaiz :: Arbol -> [[Int]]

tal que (caminosDesdeRaiz a) es la lista de las caminosDesdeRaiz desde la raíz
de a hasta cualquiera de sus nodos. Por ejemplo.

   λ> caminosDesdeRaiz ej1
   [[3],[3,2],[3,2,1],[3,2,7],[3,4],[3,4,5]]
   λ> caminosDesdeRaiz ej2
   [[7],[7,5],[7,5,6],[7,8],[7,8,4],[7,8,9]]

-}

data Arbol = H
           | N Int Arbol Arbol
  deriving Show

ej1, ej2 :: Arbol
ej1 = N 3 (N 2 (N 1 H H) (N 7 H H)) (N 4 H (N 5 H H))
ej2 = N 7 (N 5 (N 6 H H) H) (N 8 (N 4 H H) (N 9 H H))


caminosDesdeRaiz1 :: Arbol -> [[Int]]
caminosDesdeRaiz1 H          = []
caminosDesdeRaiz1 (N x i d)  =
  [x] : fmap (x:) (caminosDesdeRaiz1 i ++ caminosDesdeRaiz1 d)
