module Ex_20170127 where

{-

Suma minimal de productos de pares de elementos consecutivos
============================================================

Al permutar los elementos de la lista [1,2,3,4] se obtienen los siguientes
valores de la suma de pares de elementos consecutivos:

    10, por ejemplo con [1,4,2,3] ya que 1*4+2*3 = 10
    11, por ejemplo con [1,3,2,4] ya que 1*3+2*4 = 11
    14, por ejemplo con [1,2,3,4] ya que 1*2+3*4 = 14

Por tanto, la mínima suma de los productos de elementos consecutivos en las
permutaciones de [1,2,3,4] es 10 y una permutación con dicha suma es [1,4,2,3].

Definir las funciones

   minimaSumaProductos  :: (Num a, Ord a) => [a] -> a
   permutacionMinimal   :: (Num a, Ord a) => [a] -> [a]

tales que

    (minimaSumaProductos xs) es la mínima suma de los productos de elementos
    consecutivos en las permutaciones de lista xs, suponiendo que xs tiene un
    número par de elementos. Por ejemplo,

     minimaSumaProductos [1..4]             ==  10
     minimaSumaProductos [3,2,5,7,1,9,6]    ==  34
     minimaSumaProductos [9,2,8,4,5,7,6,0]  ==  74
     minimaSumaProductos [1,2,1,4,0,5,6,0]  ==  6

    (permutacionMinimal xs) es una permutación de xs cuya suma de productos de
    elementos consecutivos de xs es la mínima suma de los productos de elementos
    consecutivos en las permutaciones de lista xs, suponiendo que xs tiene un
    número par de elementos. Por ejemplo,

     permutacionMinimal [1..4]             ==  [1,4,3,2]
     permutacionMinimal [3,2,5,7,1,9,6]    ==  [1,9,2,7,3,6]
     permutacionMinimal [9,2,8,4,5,7,6,0]  ==  [0,9,2,8,4,7,5,6]
     permutacionMinimal [1,2,1,4,0,5,6,0]  ==  [0,6,0,5,1,4,1,2]

-}

import           Data.Function (on)
import           Data.List


minimaSumaProductos :: (Num a, Ord a) => [a] -> a
minimaSumaProductos = sumaProductos . permutacionMinimal

sumaProductos :: Num a => [a] -> a
sumaProductos (x:y:xs) = x*y + sumaProductos xs
sumaProductos _        = 0

permutacionMinimal :: (Num a, Ord a) => [a] -> [a]
permutacionMinimal = permutacionMinimal3


-- 1ª definición
-- =============

minimaSumaProductos1 :: (Num a, Ord a) => [a] -> a
minimaSumaProductos1 = sumaProductos . permutacionMinimal1

permutacionMinimal1 :: (Num a, Ord a) => [a] -> [a]
permutacionMinimal1 = minimumBy (compare `on` sumaProductos) . permutations

-- 2ª definición (sólo listas de longitud par)
-- =============

minimaSumaProductos2 :: (Num a, Ord a) => [a] -> a
minimaSumaProductos2 = sumaProductos . permutacionMinimal2

permutacionMinimal2 :: (Num a, Ord a) => [a] -> [a]
permutacionMinimal2 xs =
  intercala ys (reverse zs)
  where n = length xs
        (ys,zs) = splitAt (n `div` 2) (sort xs)

intercala :: [a] -> [a] -> [a]
intercala xs ys =
  concat [[x,y] | (x,y) <- zip xs ys]

-- 3ª definición
-- =============

minimaSumaProductos3 :: (Num a, Ord a) => [a] -> a
minimaSumaProductos3 = sumaProductos . permutacionMinimal3

permutacionMinimal3 :: (Num a, Ord a) => [a] -> [a]
permutacionMinimal3 [] = []
permutacionMinimal3 xs = take (length xs')
                      $ concat [[x,y] | (x,y) <- zip xs' (reverse xs')]
  where
    -- xs' elimina el mayor elemento cuando tiene longitud impar
    xs' | (odd.length) xs = init (sort xs)
        | otherwise       = sort xs
        | otherwise       = sort xs
