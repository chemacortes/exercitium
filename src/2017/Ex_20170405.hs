module Ex_20170405 where

{-

Agrupamiento según valores
==========================

Definir la función

   agrupa :: Ord c => (a -> c) -> [a] -> Map c [a]

tal que (agrupa f xs) es el diccionario obtenido agrupando los elementos de xs según sus valores mediante la función f. Por ejemplo,

   ghci> agrupa length ["hoy", "ayer", "ana", "cosa"]
   fromList [(3,["hoy","ana"]),(4,["ayer","cosa"])]
   ghci> agrupa head ["claro", "ayer", "ana", "cosa"]
   fromList [('a',["ayer","ana"]),('c',["claro","cosa"])]

-}

import qualified Data.Map.Strict as Map

agrupa :: Ord c => (a -> c) -> [a] -> Map c [a]
agrupa f = foldl g empty
  where g m x =
