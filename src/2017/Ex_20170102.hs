module Ex_20170102 where

{-

Terminaciones de Fibonacci
==========================

Definir la sucesión

   sucFinalesFib :: [(Integer,Integer)]

cuyo elementos son los pares (n,x) donde x es el primer número de la sucesión de
Fibonacci cuya terminación es n. Por ejemplo,

   λ> take 6 sucFinalesFib
   [(0,0),(1,1),(5,5),(25,75025),(29,514229),(41,165580141)]
   λ> head [(n,x) | (n,x) <- sucFinalesFib, n > 200]
   (245,712011255569818855923257924200496343807632829750245)
   λ> head [n | (n,_) <- sucFinalesFib, n > 10^4]
   10945


-}

import Data.List (isSuffixOf)

sucFinalesFib :: [(Integer,Integer)]
sucFinalesFib = (filter esTerminacion . zip [0..]) fibs
  where esTerminacion (n,x) = show n `isSuffixOf` show x

fibs :: [Integer]
fibs = 0 : scanl (+) 1 fibs
