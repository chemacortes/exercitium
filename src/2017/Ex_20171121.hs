{-# LANGUAGE UnicodeSyntax #-}

module Ex_20171121 where

{-

Punto de inflexión
==================

Definir la función

   inflexion :: Ord a => [a] -> Maybe a

tal que (inflexion xs) es el primer elemento de la lista en donde se cambia de creciente a decreciente o de decreciente a creciente y Nothing si no se cambia. Por ejemplo,

   inflexion [2,2,3,5,4,6]    ==  Just 4
   inflexion [9,8,6,7,10,10]  ==  Just 7
   inflexion [2,2,3,5]        ==  Nothing
   inflexion [5,3,2,2]        ==  Nothing

-}

import           Data.List  (find)
import           Data.Maybe (fromJust, isJust)

inflexion :: Ord a => [a] -> Maybe a
inflexion = inflexion6


-- Definición 1

inflexion1 :: Ord a => [a] -> Maybe a
inflexion1 (x:y:xs) | x == y = inflexion1 (y:xs)
                   | otherwise = inflexionCond (x<y) (y:xs)
inflexion1 _ = Nothing

inflexionCond :: Ord a => Bool -> [a] -> Maybe a
inflexionCond cond (x:y:xs) | (x<=y) == cond = inflexionCond cond (y:xs)
                            | otherwise = Just y
inflexionCond _ _ = Nothing


-- Definición 1

inflexion2 :: Ord a => [a] -> Maybe a
inflexion2 [] = Nothing
inflexion2 xs | length ps <= 1 = Nothing
              | null qs        = Nothing
              | otherwise      = Just $ snd (head qs)
  where
    -- estricta (creciente o decreciente)
    ps = filter (uncurry (/=)) $ zip xs (tail xs)
    -- ps = [(x,y) | (x,y) <- zip xs (tail xs), x/=y]
    -- buscamos punto de inflexión
    qs = dropWhile (tendencia (head ps)) (tail ps)
    tendencia (x,y) | x<y = uncurry (<)
                    | otherwise = uncurry (>)


-- Definición 4

inflexion4 :: Ord a => [a] -> Maybe a
inflexion4 [] = Nothing
inflexion4 xs | length ps <= 1 = Nothing
              | otherwise      = res
  where
    -- estricta (creciente o decreciente)
    ps = filter (uncurry (/=)) $ zip xs (tail xs)
    -- ps = [(x,y) | (x,y) <- zip xs (tail xs), x/=y]
    -- buscamos punto de inflexión
    res = Just snd <*> find (tendenciaInv (head ps)) (tail ps)
    tendenciaInv (x,y) | x<y = uncurry (>)
                       | otherwise = uncurry (<)


-- Definición 1

inflexion5 :: Ord a => [a] -> Maybe a
inflexion5 [] = Nothing
inflexion5 xs = Just snd <*> find ((f==).uncurry compare) ps
  where
    ps = dropWhile (uncurry (==)) $ zip xs (tail xs)
    f = uncurry (flip compare) $ head ps


-- Definición 6 (la mejor)

inflexion6 :: Ord a => [a] -> Maybe a
inflexion6 []  = Nothing
inflexion6 [_] = Nothing
inflexion6 (x:y:xs) | x==y      = inflexion6 (y:xs)
                    | x < y     = busca (>) (y:xs)
                    | otherwise = busca (<) (y:xs)

busca :: Ord a => (a -> a -> Bool) -> [a] -> Maybe a
busca f xs = snd <$> find (uncurry f) (zip xs (tail xs))



----------
-- Definición 3
-- Versión oficial

inflexion3 :: Ord  a => [a] -> Maybe a
inflexion3 (x:y:xs)
  | x == y    = inflexion3 $ y:xs
  | x < y     = buscaMenor $ y:xs
  | otherwise = buscaMayor $ y:xs
inflexion3 _  = Nothing

buscaMenor :: Ord a => [a] -> Maybe a
buscaMenor (y:xs)
  | isJust busca = Just (snd (fromJust busca))
  | otherwise    = Nothing
  where busca = find parDecreciente (zip (y:xs) xs)

buscaMayor :: Ord a => [a] -> Maybe a
buscaMayor (y:xs)
  | isJust busca = Just (snd (fromJust busca))
  | otherwise    = Nothing
  where busca = find parCreciente (zip (y:xs) xs)

parCreciente :: Ord a => (a,a) -> Bool
parCreciente (a,b) = a < b

parDecreciente :: Ord a => (a,a) -> Bool
parDecreciente (a,b) = a > b
