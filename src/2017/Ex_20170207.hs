module Ex_20170207 where

{-

Prefijo con suma acotada
========================

Definir la función

   prefijoAcotado :: (Num a, Ord a) => a -> [a] -> [a]

tal que (prefijoAcotado x ys) es el mayor prefijo de ys cuya suma es menor que x.
Por ejemplo,

   prefijoAcotado 10 [3,2,5,7]  ==  [3,2]
   prefijoAcotado 10 [1..]      ==  [1,2,3]

-}

import Data.List

prefijoAcotado :: (Num a, Ord a) => a -> [a] -> [a]
prefijoAcotado x = last . takeWhile ((x>).sum) . inits
