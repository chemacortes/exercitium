module Ex_20170223 where

{-

Cálculo de pi mediante los métodos de Gregory-Leibniz y de Beeler
=================================================================

La fórmula de Gregory-Leibniz para calcular pi es

![Calculo_de_pi_mediante_los_metodos_de_Gregory-Leibniz_y_de_Beeler_1](Ex_20170223-Calculo_de_pi_mediante_los_metodos_de_Gregory-Leibniz_y_de_Beeler_1.png)

y la de Beeler es

![Calculo_de_pi_mediante_los_metodos_de_Gregory-Leibniz_y_de_Beeler_2](Ex_20170223-Calculo_de_pi_mediante_los_metodos_de_Gregory-Leibniz_y_de_Beeler_2.png)

Definir las funciones

   aproximaPiGL     :: Int -> Double
   aproximaPiBeeler :: Int -> Double
   graficas         :: [Int] -> IO ()

tales que

    (aproximaPiGL n) es la aproximación de pi con los primeros n términos de la fórmula de Gregory-Leibniz. Por ejemplo,

     aproximaPiGL 1       ==  4.0
     aproximaPiGL 2       ==  2.666666666666667
     aproximaPiGL 3       ==  3.466666666666667
     aproximaPiGL 10      ==  3.0418396189294032
     aproximaPiGL 100     ==  3.1315929035585537
     aproximaPiGL 1000    ==  3.140592653839794
     aproximaPiGL 10000   ==  3.1414926535900345
     aproximaPiGL 100000  ==  3.1415826535897198

    (aproximaPiBeeler n) es la aproximación de pi con los primeros n términos de la fórmula de Beeler. Por ejemplo,

     aproximaPiBeeler 1   ==  2.0
     aproximaPiBeeler 2   ==  2.6666666666666665
     aproximaPiBeeler 3   ==  2.933333333333333
     aproximaPiBeeler 10  ==  3.140578169680337
     aproximaPiBeeler 60  ==  3.141592653589793
     pi                   ==  3.141592653589793

    (graficas xs) dibuja la gráfica de las k-ésimas aproximaciones de pi, donde k toma los valores de la lista xs, con las fórmulas de Gregory-Leibniz y de Beeler. Por ejemplo, (graficas [1..25]) dibuja

    ![Calculo_de_pi_mediante_los_metodos_de_Gregory-Leibniz_y_de_Beeler_3](Ex_20170223-Calculo_de_pi_mediante_los_metodos_de_Gregory-Leibniz_y_de_Beeler_3.png)

    donde la línea morada corresponde a la aproximación de Gregory-Leibniz y la verde a la de Beeler.

Nota: Este ejercicio ha sido propuesto por Enrique Naranjo.

-}

import Graphics.Gnuplot.Simple


aproximaPiGL :: Int -> Double
aproximaPiGL n = 4 * sum (take n (zipWith (/) (cycle [1.0,-1.0]) [1,3..]))

aproximaPiBeeler :: Int -> Double
aproximaPiBeeler n = 2 * foldr (\x acc -> 1 + x*acc) 1 xs
  where
    xs = take (n-1) $ zipWith (/) [1..] [3,5..]

graficas :: [Int] -> IO ()
graficas xs =
    plotLists [Key Nothing]
             [[(k,aproximaPiGL k)     | k <- xs],
              [(k,aproximaPiBeeler k) | k <- xs]]
