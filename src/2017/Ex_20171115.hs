{-# LANGUAGE UnicodeSyntax #-}

module Ex_20171115 where

{-

Números dígito potenciales
==========================

Un número entero x es dígito potencial de orden n si x es la suma de los dígitos de x elevados a n. Por ejemplo,

    153 es un dígito potencial de orden 3 ya que 153 = 1^3+5^3+3^3
    4150 es un dígito potencial de orden 5 ya que 4150 = 4^5+1^5+5^5+0^5

Un número x es dígito auto potencial si es un dígito potencial de orden n, donde n es el número de dígitos de n. Por ejemplo, 153 es un número dígito auto potencial.

Definir las funciones

   digitosPotencialesOrden :: Integer -> [Integer]
   digitosAutoPotenciales  :: [Integer]

tales que

    (digitosPotencialesOrden n) es la lista de los números dígito potenciales de orden n. Por ejemplo,

     take 6 (digitosPotencialesOrden 3)  ==  [0,1,153,370,371,407]
     take 5 (digitosPotencialesOrden 4)  ==  [0,1,1634,8208,9474]
     take 8 (digitosPotencialesOrden 5)  ==  [0,1,4150,4151,54748,92727,93084,194979]
     take 3 (digitosPotencialesOrden 6)  ==  [0,1,548834]

    digitosAutoPotenciales es la lista de los números dígito auto potenciales. Por ejemplo,

     λ> take 20 digitosAutoPotenciales
     [0,1,2,3,4,5,6,7,8,9,153,370,371,407,1634,8208,9474,54748,92727,93084]

-}

import           Data.List (genericLength)

esDigitoPotencial :: Integer -> Integer -> Bool
esDigitoPotencial n x = x == sum [read [c] ^ n|c <- show x]

digitosPotencialesOrden :: Integer -> [Integer]
digitosPotencialesOrden = digitosPotencialesOrden2

digitosPotencialesOrden1 :: Integer -> [Integer]
digitosPotencialesOrden1 n = filter (esDigitoPotencial n) [0..]

digitosPotencialesOrden2 :: Integer -> [Integer]
digitosPotencialesOrden2 = flip filter [0..] . esDigitoPotencial


esDigitoAutoPotencial :: Integer -> Bool
esDigitoAutoPotencial x = esDigitoPotencial (genericLength (show x)) x

esDigitoAutoPotencial2 :: Integer -> Bool
esDigitoAutoPotencial2 = esDigitoPotencial =<< genericLength . show

digitosAutoPotenciales :: [Integer]
digitosAutoPotenciales = filter esDigitoAutoPotencial [0..]

digitosAutoPotenciales2 :: [Integer]
digitosAutoPotenciales2 =
  0: concat [[x | x <- [10^k..10^(k+1)-1], esDigitoPotencial (k+1) x]
            | k <- [0..]]
