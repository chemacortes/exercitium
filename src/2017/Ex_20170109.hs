module Ex_20170109 where

{-

Sumas y restas alternativas
===========================

Definir la función

   sumasYrestas :: Num a => [a] -> a

tal que (sumasYrestas xs) es el resultado de alternativamente los elementos de
xs. Por ejemplo,

   sumasYrestas [3,2,4,1,7] = 3 - 2 + 4 - 1 + 7
                            = 11

Otros ejemplos,

   sumasYrestas [3,2,4]              ==  5
   sumasYrestas [3,2,4,1]            ==  4
   sumasYrestas [3,2,4,1,7]          ==  11
   sumasYrestas (replicate (10^6) 1) ==  0

-}

sumasYrestas :: Num a => [a] -> a
sumasYrestas = sumasYrestas4


sumasYrestas1 :: Num a => [a] -> a
sumasYrestas1 = sum . zipWith (*) (cycle [1,-1])

sumasYrestas2 :: Num a => [a] -> a
sumasYrestas2 []        = 0
sumasYrestas2 [x]       = x
sumasYrestas2 (x:y:xs)  = x-y + sumasYrestas2 xs

sumasYrestas3 :: Num a => [a] -> a
sumasYrestas3 = suma 0 1
  where suma acc _ []     = acc
        suma acc 1 (x:xs) = suma (acc+x) 0 xs
        suma acc _ (x:xs) = suma (acc-x) 1 xs

sumasYrestas4 :: Num a => [a] -> a
sumasYrestas4 = foldr1 (-)
