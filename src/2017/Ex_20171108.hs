{-# LANGUAGE UnicodeSyntax #-}

module Ex_20171108 where

{-

Producto cartesiano de una familia de conjuntos
===============================================

Definir la función

   producto :: [[a]] -> [[a]]

tal que (producto xss) es el producto cartesiano de los conjuntos xss. Por ejemplo,

   ghci> producto [[1,3],[2,5]]
   [[1,2],[1,5],[3,2],[3,5]]
   ghci> producto [[1,3],[2,5],[6,4]]
   [[1,2,6],[1,2,4],[1,5,6],[1,5,4],[3,2,6],[3,2,4],[3,5,6],[3,5,4]]
   ghci> producto [[1,3,5],[2,4]]
   [[1,2],[1,4],[3,2],[3,4],[5,2],[5,4]]
   ghci> producto []
   [[]]

Comprobar con QuickCheck que para toda lista de listas de números enteros, xss, se verifica que el número de elementos de (producto xss) es igual al producto de los números de elementos de cada una de las listas de xss.

Nota. Al hacer la comprobación limitar el tamaño de las pruebas como se indica a continuación

   quickCheckWith (stdArgs {maxSize=9}) prop_producto

-}

import           Test.QuickCheck


producto :: [[a]] -> [[a]]
producto []       = [[]]
producto (xs:xss) = [x:zs | x <- xs, zs <- producto xss]

producto2 :: [[a]] -> [[a]]
producto2 []       = [[]]
producto2 (xs:xss) = (:) <$> xs <*> producto2 xss

producto3 :: [[a]] -> [[a]]
producto3 = foldr ((<*>) . ((:)<$>) ) [[]]


prop_producto :: [[a]] -> Bool
prop_producto xss =
    (length . producto) xss == (product . fmap length) xss

prop_producto2 :: [[a]] -> Bool
prop_producto2 =
    (==) <$> length . producto <*> product . fmap length
