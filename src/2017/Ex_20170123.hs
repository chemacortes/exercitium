module Ex_20170123 where

{-

Cadena de primos
================

La lista de los primeros números primos es

   [2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59,61,67,71]

Los primeros elementos de la cadena obtenida concatenado los números primos es

   "23571113171923293137414347535961677173798389971011"

Definir la función

   primoEnPosicion :: Int -> Integer

tal que (primoEnPosicion n) es el número primo que tiene algún dígito en la
posición n de la cadena obtenida concatenado los números primos. Por ejemplo,

   primoEnPosicion 0       ==  2
   primoEnPosicion 1       ==  3
   primoEnPosicion 4       ==  11
   primoEnPosicion 5       ==  11
   primoEnPosicion 6       ==  13
   primoEnPosicion 1022    ==  2011
   primoEnPosicion 1023    ==  2017
   primoEnPosicion 1026    ==  2017
   primoEnPosicion 1027    ==  2027
   primoEnPosicion (10^7)  ==  21242357

-}

import Data.Numbers.Primes (primes)

primoEnPosicion :: Int -> Integer
primoEnPosicion n = head [ p | (x,p) <- ps, x > n ]

ps :: [(Int, Integer)]
ps = scanl (\(acc,_) p -> (acc+(length.show)p,p)) (0,2) primes


primoEnPosicion1 :: Int -> Integer
primoEnPosicion1 n = head [ p | (p,x) <- zip primes xs
                             , x > n ]
  where xs = scanl1 (+) $ map (length.show) primes

primoEnPosicion2 :: Int -> Integer
primoEnPosicion2 n = aux n primes
  where aux n (x:xs) | n <= m     = x
                     | otherwise  = aux (n-m) xs
          where m = (length.show) x

primoEnPosicion3 :: Int -> Integer
primoEnPosicion3 n = cadena primes !! n
  where cadena (x:xs) = replicate ((length.show) x) x ++ cadena xs

primoEnPosicion4 :: Int -> Integer
primoEnPosicion4 n = xs !! n
  where xs = concatMap (\x -> replicate ((length.show) x) x) primes

primoEnPosicion5 :: Int -> Integer
primoEnPosicion5 n = xs !! n
  where xs = concatMap (replicate <$> length.show <*> id) primes

{-
primoEnPosicion6 :: Int -> Integer
primoEnPosicion6 n = xs !! n
  where xs = concatMap (replicate <$> length.show <* ) primes
-}
