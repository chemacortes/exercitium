{-# LANGUAGE UnicodeSyntax #-}

module Ex_20171218 where

{-

Cadenas opuestas
================

La opuesta de una cadena de letras es la cadena obtenida cambiando las minúsculas por mayúsculas y las minúsculas por mayúsculas. Por ejemplo, la opuesta de “SeViLLa” es “sEvIllA”.

Definir la función

   esOpuesta :: String -> String -> Bool

tal que (esOpuesta s1 s2) se verifica si las cadenas de letras s1 y s2 son opuestas. Por ejemplo,

   esOpuesta "ab" "AB"      `== True
   esOpuesta "aB" "Ab"      `== True
   esOpuesta "aBcd" "AbCD"  `== True
   esOpuesta "aBcde" "AbCD" `== False
   esOpuesta "AB" "Ab"      `== False
   esOpuesta "" ""          `== True

-}

import           Data.Char

esOpuesta :: String -> String -> Bool
esOpuesta = esOpuesta1

esOpuesta1 :: String -> String -> Bool
esOpuesta1 s1 = (s1 ==) . fmap opuesta
  where
    opuesta c | isLower c = toUpper c
              | otherwise = toLower c
