module Ex_20170105 where

{-

Sucesión de cuadrados reducidos
===============================

La sucesión de cuadrados de orden n definida a partir de un número x se forma
iniciándola en x y, para cada término z el siguiente es el número formado por
los n primeros dígitos del cuadrado de z. Por ejemplo, para n = 4 y x = 1111,
el primer término de la sucesión es 1111, el segundo es 1234 (ya que
1111^2 = 1234321) y el tercero es 1522 (ya que 1234^2 = 1522756).

Definir la función

   sucCuadrados :: Int -> Integer -> [Integer]

tal que (sucCuadrados n x) es la sucesión de cuadrados de orden n definida a
partir de x. Por ejemplo,

   λ> take 10 (sucCuadrados 4 1111)
   [1111,1234,1522,2316,5363,2876,8271,6840,4678,2188]
   λ> take 10 (sucCuadrados 3 457)
   [457,208,432,186,345,119,141,198,392,153]
   λ> take 20 (sucCuadrados 2 55)
   [55,30,90,81,65,42,17,28,78,60,36,12,14,19,36,12,14,19,36,12]

-}

sucCuadrados :: Int -> Integer -> [Integer]
sucCuadrados n = iterate (nDigitos n . (^2))
  where nDigitos m = read . take m . show
