module Ex_20170126 (
  maxPotDivFact
  ) where

{-

Máxima potencia que divide al factorial
=======================================

La máxima potencia de 2 que divide al factorial de 5 es 3, ya que 5! = 120, 120
es divisible por 2^3 y no lo es por 2^4.

Definir la función

   maxPotDivFact :: Integer -> Integer -> Integer

tal que (maxPotDivFact p n), para cada primo p, es el mayor k tal que p^k divide
al factorial de n. Por ejemplo,

   maxPotDivFact 2 5       ==  3
   maxPotDivFact 3 6       ==  2
   maxPotDivFact 2 10      ==  8
   maxPotDivFact 3 10      ==  4
   maxPotDivFact 2 (10^2)  ==  97
   maxPotDivFact 2 (10^3)  ==  994
   maxPotDivFact 2 (10^4)  ==  9995
   maxPotDivFact 2 (10^5)  ==  99994
   maxPotDivFact 2 (10^6)  ==  999993
   maxPotDivFact 3 (10^5)  ==  49995
   maxPotDivFact 3 (10^6)  ==  499993
   maxPotDivFact 7 (10^5)  ==  16662
   maxPotDivFact 7 (10^6)  ==  166664
   length (show (maxPotDivFact 2 (10^20000)))  ==  20000

-}

import Data.List
import Data.Numbers.Primes (primeFactors)

-- Fórmula de Porlignac
--  https://es.wikipedia.org/wiki/F%C3%B3rmula_de_De_Polignac
maxPotDivFact :: Integer -> Integer -> Integer
maxPotDivFact p = sum  . takeWhile (>0) . tail . iterate (`div` p)

-- Otras soluciones
maxPotDivFact1 :: Integer -> Integer -> Integer
maxPotDivFact1 p n = genericLength . filter (==p) $ concatMap primeFactors [2..n]

maxPotDivFact2 :: Integer -> Integer -> Integer
maxPotDivFact2 p n = genericLength $ concatMap (filter (==p) . primeFactors) [2..n]

maxPotDivFact3 :: Integer -> Integer -> Integer
maxPotDivFact3 p n = sum $ map (genericLength . unfoldr (divisiblePor p)) [2..n]

divisiblePor :: Integer -> Integer -> Maybe (Integer, Integer)
divisiblePor n m | r == 0    = Just (d,d)
                 | otherwise = Nothing
  where (d,r) = divMod m n

maxPotDivFact4 :: Integer -> Integer -> Integer
maxPotDivFact4 p n = sum . fmap genericLength
                   . takeWhile (not.null)
                   $ iterate f [2..n]
  where
    f xs = [x `div` p | x <- xs, x `mod` p == 0]

maxPotDivFact5 :: Integer -> Integer -> Integer
maxPotDivFact5 _ 1 = 0
maxPotDivFact5 p n | n `mod` p /= 0 = maxPotDivFact5 p (n-1)
                   | otherwise      = nDiv n + maxPotDivFact5 p (n-1)
                      where nDiv = genericLength . filter (==p) . primeFactors

maxPotDivFact6 :: Integer -> Integer -> Integer
maxPotDivFact6 p n = genericLength $ takeWhile ((==0).mod n) qs
  where
    qs = (p^) <$> [1..]
