{-# OPTIONS_GHC -fno-warn-incomplete-patterns #-}

module Ex_20170203 where

{-

Sucesión de Cantor de números innombrables
==========================================

Un número es esInnombrable si es divisible por 7 o alguno de sus dígitos es un 7. Un juego infantil consiste en contar saltándose los números innombrables:

   1 2 3 4 5 6 ( ) 8 9 10 11 12 13 ( ) 15 16 ( ) 18 ...

La sucesión de Cantor se obtiene llenando los huecos de la sucesión anterior
como se indica a continuación:

  1 2 3 4 5 6 (1) 8 9 10 11 12 13 (2) 15 16 (3) 18 19 20 (4) 22 23
  24 25 26 (5) (6) 29 30 31 32 33 34 (1) 36 (8) 38 39 40 41  (9) 43
  44 45 46 (10) 48 (11) 50 51 52 53 54 55 (12) (13) 58 59 60 61 62
  (2) 64 65 66 (15) 68 69 (16) (3) (18) (19) (20) (4) (22) (23) (24)
  (25) 80 81 82 83 (26) 85 86 (5) 88 89 90 (6) 92 93 94 95 96 (29)
  (30) 99 100

Definir la sucesión

   sucCantor :: [Integer]

cuyos elementos son los términos de la sucesión de Cantor. Por ejemplo,

   λ> take 100 sucCantor
   [1,2,3,4,5,6, 1 ,8,9,10,11,12,13, 2, 15,16, 3, 18,19,20, 4,
    22,23,24,25,26, 5 , 6 ,29,30,31,32,33,34, 1 ,36 , 8 ,38,39,
    40,41, 9 ,43,44,45,46, 10 ,48, 11 ,50,51,52,53,54,55 , 12 ,
    13, 58,59,60,61,62, 2 ,64,65,66, 15 ,68,69, 16 , 3 , 18, 19,
    20, 4, 22, 23, 24 ,25 ,80,81,82,83, 26 ,85,86, 5 ,88,89,90,
    6, 92,93,94,95,96, 29, 30 ,99,100]

   λ> sucCantor !! (5+10^6)
   544480

   λ> sucCantor !! (6+10^6)
   266086

-}

esInnombrable :: Integer -> Bool
esInnombrable x = '7' `elem` show x || x `mod` 7 == 0

sucCantor :: [Integer]
sucCantor =  1 : f [2..] sucCantor
  where
    f (x:xs) (c:cs) | esInnombrable x = c : f xs cs
                    | otherwise       = x : f xs (c:cs)

sucCantor1 :: [Integer]
sucCantor1 =  1 : f 2 sucCantor1
  where
    f n (c:cs) | esInnombrable n = c : f (n+1) cs
               | otherwise       = n : f (n+1) (c:cs)

sucCantor2 :: [Integer]
sucCantor2 =  1 : f sucCantor2 [2..]
  where
    f (c:cs) xs = ys ++ c : f cs zs
      where (ys,_:zs) = break esInnombrable xs


sucCantor3 :: [Integer]
sucCantor3 =  1 : f sucCantor3 [2..]
  where
    f cs xs = ys ++ g cs zs
      where (ys,zs) = break esInnombrable xs
    g cs xs = cs1 ++ f cs2 zs
      where (ys,zs) = span esInnombrable xs
            (cs1,cs2) = splitAt (length ys) cs


sucCantor4 :: [Integer]
sucCantor4 = map fst $ iterate f (1,(2,sucCantor4))
  where f (_,(n,c:cs)) | esInnombrable n = (c,(n+1,cs))
                       | otherwise       = (n,(n+1,c:cs))

sucCantor5 :: [Integer]
sucCantor5 = map head $ iterate f (1:2:sucCantor5)
  where f (_:n:c:cs) | esInnombrable n = c:n+1:cs
                     | otherwise       = n:n+1:c:cs

sucCantor6 :: [Integer]
sucCantor6 = 1 : f 2 zs
  where
    zs = zip sucCantor6 (filter esInnombrable [1..])
    f k ys@((x,y):xs) | k == y     = x : f (k+1) xs
                      | otherwise  = k : f (k+1) ys
