module Ex_20170216 where

{-

Distribución de dígitos de pi
=============================

Se pueden generar los dígitos de Pi, como se explica en el artículo [Unbounded
spigot algorithms for the digits of pi][1], con la función digitosPi definida por

   digitosPi :: [Integer]
   digitosPi = g(1,0,1,1,3,3) where
     g (q,r,t,k,n,l) =
       if 4*q+r-t < n*t
       then n : g (10*q, 10*(r-n*t), t, k, div (10*(3*q+r)) t - 10*n, l)
       else g (q*k, (2*q+r)*l, t*l, k+1, div (q*(7*k+2)+r*l) (t*l), l+2)

Por ejemplo,

   λ> take 25 digitosPi
   [3,1,4,1,5,9,2,6,5,3,5,8,9,7,9,3,2,3,8,4,6,2,6,4,3]

La distribución de los primeros 25 dígitos de pi es [0,2,3,5,3,3,3,1,2,3] ya que
el 0 no aparece, el 1 ocurre 2 veces, el 3 ocurre 3 veces, el 4 ocurre 5 veces, …

Usando digitosPi, definir las siguientes funciones

   distribucionDigitosPi :: Int -> [Int]
   frecuenciaDigitosPi   :: Int -> [Double]

tales que

    (distribucionDigitosPi n) es la distribución de los n primeros dígitos de
    pi. Por ejemplo,

     λ> distribucionDigitosPi 10
     [0,2,1,2,1,2,1,0,0,1]
     λ> distribucionDigitosPi 100
     [8,8,12,12,10,8,9,8,12,13]
     λ> distribucionDigitosPi 1000
     [93,116,103,103,93,97,94,95,101,105]
     λ> distribucionDigitosPi 5000
     [466,531,496,460,508,525,513,488,492,521]

    (frecuenciaDigitosPi n) es la frecuencia de los n primeros dígitos de pi.
    Por ejemplo,

   λ> frecuenciaDigitosPi 10
   [0.0,20.0,10.0,20.0,10.0,20.0,10.0,0.0,0.0,10.0]
   λ> frecuenciaDigitosPi 100
   [8.0,8.0,12.0,12.0,10.0,8.0,9.0,8.0,12.0,13.0]
   λ> frecuenciaDigitosPi 1000
   [9.3,11.6,10.3,10.3,9.3,9.7,9.4,9.5,10.1,10.5]
   λ> frecuenciaDigitosPi 5000
   [9.32,10.62,9.92,9.2,10.16,10.5,10.26,9.76,9.84,10.42]

[1]: bit.ly/2kn0Kh2

-}

import Data.List

digitosPi :: [Integer]
digitosPi = g(1,0,1,1,3,3) where
 g (q,r,t,k,n,l) =
   if 4*q+r-t < n*t
   then n : g (10*q, 10*(r-n*t), t, k, div (10*(3*q+r)) t - 10*n, l)
   else g (q*k, (2*q+r)*l, t*l, k+1, div (q*(7*k+2)+r*l) (t*l), l+2)


distribucionDigitosPi1 :: Int -> [Int]
distribucionDigitosPi1 n = map length . group . sort $ take n digitosPi

distribucionDigitosPi :: Int -> [Int]
distribucionDigitosPi n = [ length $ filter (==k) digitos | k <- [0..9]]
  where
    digitos = take n digitosPi


frecuenciaDigitosPi :: Int -> [Double]
frecuenciaDigitosPi n =
  [100 * fromIntegral x / fromIntegral n | x <- distribucionDigitosPi n]
