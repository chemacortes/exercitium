module Ex_20170206 where

{-

Aplicaciones de operaciones
===========================

Definir la función

   aplicaciones :: [a -> b -> c] -> [a] -> [b] -> [c]

tal que (aplicaciones os xs ys) es la lista obtenida aplicando las operaciones
de os a los elementos de xs es ys. Por ejemplo,

   λ> aplicaciones [(+),(*)] [1,2] [5,8]
   [6,9,7,10,5,8,10,16]
   λ> aplicaciones [(+),(*)] [1,2] [5]
   [6,7,5,10]
   λ> aplicaciones [(<),(>)] ["ab","c"] ["def","c"]
   [True,True,True,False,False,False,False,False]
   λ> import Data.List
   λ> aplicaciones [(++),intersect] ["ab","c"] ["bd","cf"]
   ["abbd","abcf","cbd","ccf","b","","","c"]

-}

aplicaciones1 :: [a -> b -> c] -> [a] -> [b] -> [c]
aplicaciones1 fs xs ys = [f x y| f <- fs,  x <- xs, y <-ys]

aplicaciones :: [a -> b -> c] -> [a] -> [b] -> [c]
aplicaciones os xs ys = os <*> xs <*> ys
