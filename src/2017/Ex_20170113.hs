{-# OPTIONS_GHC -fno-warn-incomplete-patterns #-}

module Ex_20170113 where

{-

Suma ordenada de listas infinitas ordenadas
===========================================

Definir la función

   sumaOrdenada :: [Integer] -> [Integer] -> [Integer]

tal que (sumaOrdenada xs ys) es la suma ordenada de las listas infinitas
crecientes xs e ys. Por ejemplo,

   λ> take 15 (sumaOrdenada [5,10..] [7,14..])
   [12,17,19,22,24,26,27,29,31,32,33,34,36,37,38]
   λ> take 15 (sumaOrdenada [2^n | n <- [0..]] [3^n | n <- [0..]])
   [2,3,4,5,7,9,10,11,13,17,19,25,28,29,31]

-}

sumaOrdenada :: [Integer] -> [Integer] -> [Integer]
sumaOrdenada (x:xs) (y:ys) =
    x+y : map (+x) ys `mezcla` map (+y) xs `mezcla` sumaOrdenada ys xs

mezcla :: Ord a => [a] -> [a] -> [a]
mezcla (x:xs) (y:ys) | x == y    = x : mezcla xs ys
                     | x < y     = x : mezcla xs (y:ys)
                     | otherwise = y : mezcla (x:xs) ys


sumaOrdenada2 :: [Integer] -> [Integer] -> [Integer]
sumaOrdenada2 xs ys = mezclaTodas [map (+x) ys | x <- xs]

mezclaTodas :: Ord a => [[a]] -> [a]
mezclaTodas = foldr1 xmezcla
  where xmezcla (x:xs) ys = x : mezcla xs ys
