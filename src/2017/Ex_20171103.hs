{-# LANGUAGE UnicodeSyntax #-}

module Ex_20171103 where

{-

Números libres de cuadrados
===========================

Un número es libre de cuadrados si no es divisible el cuadrado de ningún entero mayor que 1. Por ejemplo, 70 es libre de cuadrado porque sólo es divisible por 1, 2, 5, 7 y 70; en cambio, 40 no es libre de cuadrados porque es divisible por 2^2.

Definir la función

   libreDeCuadrados :: Integer -> Bool

tal que (libreDeCuadrados x) se verifica si x es libre de cuadrados. Por ejemplo,

   libreDeCuadrados 70                    ==  True
   libreDeCuadrados 40                    ==  False
   libreDeCuadrados 510510                ==  True
   libreDeCuadrados (((10^10)^10)^10)     ==  False

Otro ejemplo,

   λ> filter (not . libreDeCuadrados) [1..50]
   [4,8,9,12,16,18,20,24,25,27,28,32,36,40,44,45,48,49,50]

-}

import           Data.Numbers.Primes (primeFactors, primes)


libreDeCuadrados :: Integer -> Bool
libreDeCuadrados n = not $ any esDivisiblePor xs
  where
    esDivisiblePor x = n `mod` x == 0
    xs = takeWhile (<=n) [p*p| p <- primes]

libreDeCuadrados2 :: Integer -> Bool
libreDeCuadrados2 n = and $ zipWith (/=) xs (tail xs)
  where xs = primeFactors n
