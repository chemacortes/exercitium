module Ex_20170215 where

{-

Búsqueda en los dígitos de pi
=============================

El fichero Digitos_de_pi.txt contiene el número pi con un millón de decimales;
es decir,

   3.1415926535897932384626433832 ... 83996346460422090106105779458151

Definir la función

   posicion :: String -> IO (Maybe Int)

tal que (posicion n) es (Just k) si k es la posición de n en la sucesión formada
por un millón dígitos decimales del número pi y Nothing si n no ocurre en dicha
sucesión. Por ejemplo,

   λ> posicion "15"
   Just 3
   λ> posicion "2017"
   Just 8897
   λ> posicion "022017"
   Just 382052
   λ> posicion "14022017"
   Nothing
   λ> posicion "999999"
   Just 762
   λ> posicion "458151"
   Just 999995

Nota. Se puede comprobar la función mediante [The pi-search page][1] o [Pi search
engine][2].

[1]: http://www.angio.net/pi/piquery.html
[2]: http://www.subidiom.com/pi

-}

import Data.List

posicion :: String -> IO (Maybe Int)
posicion n = do _:xs <- readFile "src/2017/Ex_20170215-Digitos_de_pi.txt"
                return $ findIndex (n `isPrefixOf`) (tails xs)
