{-# LANGUAGE UnicodeSyntax #-}

module Ex_20171116 where

{-

Suma de divisores
=================

Definir las funciones

   divisores     :: Integer -> [Integer]
   sumaDivisores :: Integer -> Integer

tales que

    (divisores x) es la lista de los divisores de x. Por ejemplo,

     divisores 12  ==  [1,2,3,4,6,12]
     divisores 25  ==  [1,5,25]
     length (divisores (product [1..12]))  ==  792
     length (divisores 131535436245601)    ==  32

    (sumaDivisores x) es la suma de los divisores de x. Por ejemplo,

     sumaDivisores 12  ==  28
     sumaDivisores 25  ==  31
     sumaDivisores (product [1..12])  ==  2217441408
     sumaDivisores 131535436245601    ==  132534784471040

-}

import           Data.List
import           Data.Numbers.Primes


divisores :: Integer -> [Integer]
divisores = divisores1

divisores1 :: Integer -> [Integer]
divisores1 = nub . map product . subsequences . primeFactors

sumaDivisores :: Integer -> Integer
sumaDivisores = sumaDivisores2

sumaDivisores1 :: Integer -> Integer
sumaDivisores1 = sum . divisores


-- 2ª definición de sumaDivisores
-- ==============================

-- Si la descomposición de x en factores primos es
--    x = p(1)^e(1) . p(2)^e(2) . .... . p(n)^e(n)
-- entonces la suma de los divisores de x es
--    p(1)^(e(1)+1) - 1     p(2)^(e(2)+1) - 1       p(n)^(e(2)+1) - 1
--   ------------------- . ------------------- ... -------------------
--        p(1)-1                p(2)-1                  p(n)-1
-- Ver la demostración en http://bit.ly/2zUXZPc

sumaDivisores2 :: Integer -> Integer
sumaDivisores2 x =
  product [(p^(e+1)-1) `div` (p-1) | (p,e) <- basesYexponentes x]

-- (basesYexponentes x) son las bases y exponentes de la descomposición
-- prima de x. Por ejemplo,
--    basesYexponentes 18000 == [(2,4),(3,2),(5,3)]
basesYexponentes :: Integer -> [(Integer,Integer)]
basesYexponentes x =
  map primeroYlongitud (group (primeFactors x))

-- (primeroYlongitus) xs es el par formado por el primer elemento de xs
-- y la longitud de xs. Por ejemplo,
--    primeroYlongitud [3,2,5,7] == (3,4)
primeroYlongitud :: [a] -> (a,Integer)
primeroYlongitud (x:xs) =
  (x, 1 + genericLength xs)
