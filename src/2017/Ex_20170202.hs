module Ex_20170202 where

{-

Particiones de una lista
========================

Definir la función

   particiones :: [a] -> [[[a]]]

tal que (particiones xs) es la lista de las particiones de xs en segmentos de
elementos consecutivos. Por ejemplo,

   λ> particiones [1..3]
   [[[1],[2],[3]],[[1],[2,3]],[[1,2],[3]],[[1,2,3]]]
   λ> mapM_ print (particiones "abcd")
   ["a","b","c","d"]
   ["a","b","cd"]
   ["a","bc","d"]
   ["a","bcd"]
   ["ab","c","d"]
   ["ab","cd"]
   ["abc","d"]
   ["abcd"]
   λ> length (particiones [1..22])
   2097152

Comprobar con QuickCheck que la concatenación de cada uno de los elementos de
(particiones xs) es igual a xs.

Nota: En la comprobación usar ejemplos pequeños como se indica a
continuación

   quickCheckWith (stdArgs {maxSize=10}) prop_particiones

-}

import Data.List
import Test.QuickCheck

particiones1 :: [a] -> [[[a]]]
particiones1 [] = [[]]
particiones1 xs = concat [map (ys:) (particiones zs) | (ys,zs) <- tail (zip (inits xs) (tails xs))]

particiones :: [a] -> [[[a]]]
particiones [] = [[]]
particiones xs = concat.tail $ (zipWith f <$> inits <*> tails) xs
  where
    f ys zs = (ys:) <$> particiones zs


prop_particiones :: Eq a => [a] -> Bool
prop_particiones xs = all ((xs==).concat) (particiones xs)
