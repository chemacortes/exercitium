module Ex_20170200 where

{-

Cálculo de pi usando la fórmula de Vieta
========================================

La fórmula de Vieta para el cálculo de pi es la siguiente

$\pi = 2 \times \dfrac 2 {\sqrt 2} \times \dfrac 2 {\sqrt {2 + \sqrt 2} } \times \dfrac 2 {\sqrt {2 + \sqrt {2 + \sqrt 2} } } \times \dfrac 2 {\sqrt {2 + \sqrt {2 + \sqrt {2 + \sqrt 2 } } } } \times \cdots$

Definir las funciones

   aproximacionPi :: Int -> Double
   errorPi :: Double -> Int

tales que

    (aproximacionPi n) es la aproximación de pi usando n factores de la fórmula
    de Vieta. Por ejemplo,

     aproximacionPi  5  ==  3.140331156954753
     aproximacionPi 10  ==  3.1415914215112
     aproximacionPi 15  ==  3.141592652386592
     aproximacionPi 20  ==  3.1415926535886207
     aproximacionPi 25  ==  3.141592653589795

    (errorPi x) es el menor número de factores de la fórmula de Vieta necesarios
    para obtener pi con un error menor que x. Por ejemplo,

     errorPi 0.1        ==  2
     errorPi 0.01       ==  4
     errorPi 0.001      ==  6
     errorPi 0.0001     ==  7
     errorPi 1e-4       ==  7
     errorPi 1e-14      ==  24
     pi                 ==  3.141592653589793
     aproximacionPi 24  ==  3.1415926535897913


[1]: https://proofwiki.org/wiki/Vieta's_Formula_for_Pi

-}

sucVieta :: [Double]
sucVieta =  (2/) <$> 1 : iterate (\x -> sqrt(2+x)) (sqrt 2)

aproximacionPi :: Int -> Double
aproximacionPi n = product $ take (n+1) sucVieta

aproximacionPi2 :: Int -> Double
aproximacionPi2 n =  product (2 : take n (map (2/) xs))
  where xs = sqrt 2 : [sqrt (2 + x) | x <- xs]

errorPi :: Double -> Int
errorPi = errorPi2

errorPi1 :: Double -> Int
errorPi1 x = length $ takeWhile (\d -> abs (pi - aproximacionPi d) >= x) [0..]

errorPi2 :: Double -> Int
errorPi2 x = length $ takeWhile aux (scanl1 (*) sucVieta)
  where
    aux z = abs (pi - z) >= x

errorPi3 :: Double -> Int
errorPi3 x = until aceptable (+1) 1
  where aceptable n = abs (pi - aproximacionPi n) < x
