module Ex_20170209 where

{-

Máximo común divisor de x e y veces n
=====================================

Definir las funciones

   repite :: Int -> Integer -> Integer
   mcdR   :: Integer -> Int -> Int -> Integer

tales que

    (repite x n) es el número obtenido repitiendo x veces el número n. Por ejemplo.

     repite 3 123  ==  123123123

    (mcdR n x y) es el máximo común divisor de los números obtenidos repitiendo
    x veces e y veces el número n. Por ejemplo.

     mcdR 123 2 3                     ==  123
     mcdR 4 4 6                       ==  44
     mcdR 2017 (10^1000) (2+10^1000)  ==  20172017

-}

repite :: Int -> Integer -> Integer
repite x = read . concat . replicate x . show


mcdR1 :: Integer -> Int -> Int -> Integer
mcdR1 n x y = gcd (repite x n) (repite y n)

mcdR :: Integer -> Int -> Int -> Integer
mcdR n x y = repite (gcd x y) n

mcdR2 :: Integer -> Int -> Int -> Integer
mcdR2 n = ((`repite` n) .). gcd
