{-# OPTIONS_GHC -fno-warn-incomplete-patterns #-}

module Ex_20170213 where

{-

Cálculo de pi usando el producto de Wallis
==========================================

El producto de Wallis es una expresión, descubierta por John Wallis en 1655,
para representar el valor de π y que establece que:

    π     2     2     4     4     6     6     8     8
   --- = --- · --- · --- · --- · --- · --- · --- · --- ···
    2     1     3     3     5     5     7     7     9

Definir las funciones

   factoresWallis  :: [Rational]
   productosWallis :: [Rational]
   aproximacionPi  :: Int -> Double
   errorPi         :: Double -> Int

tales que

    factoresWallis es la sucesión de los factores del productos de Wallis. Por
    ejemplo,

     λ> take 10 factoresWallis
     [2 % 1,2 % 3,4 % 3,4 % 5,6 % 5,6 % 7,8 % 7,8 % 9,10 % 9,10 % 11]

    productosWallis es la sucesión de los productos de los primeros factores de
    Wallis. Por ejemplo,

     λ> take 7 productosWallis
     [2 % 1,4 % 3,16 % 9,64 % 45,128 % 75,256 % 175,2048 % 1225]

    (aproximacionPi n) es la aproximación de pi obtenida multiplicando los n
    primeros factores de Wallis. Por ejemplo,

     aproximacionPi 20     ==  3.2137849402931895
     aproximacionPi 200    ==  3.1493784731686008
     aproximacionPi 2000   ==  3.142377365093878
     aproximacionPi 20000  ==  3.141671186534396

    (errorPi x) es el menor número de factores de Wallis necesarios para obtener
    pi con un error menor que x. Por ejemplo,

     errorPi 0.1     ==  14
     errorPi 0.01    ==  155
     errorPi 0.001   ==  1569
     errorPi 0.0001  ==  15707

-}

import Data.Ratio

factoresWallis ::  [Rational]
factoresWallis = factoresWallis5

factoresWallis1  :: [Rational]
factoresWallis1 = zipWith (%) pares nones
  where
    pares = concatMap (replicate 2) [2,4..]
    nones = 1 : concatMap (replicate 2) [3,5..]

factoresWallis2 ::  [Rational]
factoresWallis2 = map (uncurry (%)) zs
  where
    zs = iterate (\(a,b) -> (b+1,a+1)) (2,1)

factoresWallis3 ::  [Rational]
factoresWallis3 = iterate (\x -> (1 + denominator x) % (1 + numerator x)) 2

factoresWallis4 ::  [Rational]
factoresWallis4 = iterate f 2
  where
    f = (%) <$> (1+).denominator <*> (1+).numerator

factoresWallis5 :: [Rational]
factoresWallis5 = aux1 [1..]
  where
    aux1 (x:y:xs) = y%x : aux2 (y:xs)
    aux2 (x:y:xs) = x%y : aux1 (y:xs)

factoresWallis6 ::  [Rational]
factoresWallis6 = [x % (y+i) | (x,y) <- zip [2,4..] [1,3..], i <- [0,2]]

factoresWallis7 :: [Rational]
factoresWallis7 =
  concat [[y%(y-1),  y%(y+1)] | x <- [1..], let y = 2*x]


productosWallis :: [Rational]
productosWallis = scanl1 (*) factoresWallis


aproximacionPi :: Int -> Double
aproximacionPi = aproximacionPi2

aproximacionPi1  :: Int -> Double
aproximacionPi1 n = fromRational $ 2 * product (take (n+1) factoresWallis)

aproximacionPi2  :: Int -> Double
aproximacionPi2 n = 2 * fromRational (num % den)
  where
    pares = concatMap (replicate 2) [2,4..]
    nones = 1 : concatMap (replicate 2) [3,5..]
    num = product $ take (n+1) pares
    den = product $ take (n+1) nones

aproximacionPi3 :: Int -> Double
aproximacionPi3 = fromRational . (2*) . (productosWallis!!)


errorPi :: Double -> Int
errorPi = errorPi2

errorPi1 :: Double -> Int
errorPi1 x = head [n | n <- [1..], abs (pi - aproximacionPi n) < x]

errorPi2 :: Double -> Int
errorPi2 x = length $ takeWhile f productosWallis
  where
    f z =  abs (pi - 2 * fromRational z) >= x
