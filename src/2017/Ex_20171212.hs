{-# LANGUAGE UnicodeSyntax #-}

module Ex_20171212 where

{-

Reconocimiento de recorridos correctos
======================================

Se usará la misma representación del ejercicio anterior para las subidas y bajadas en el autobús; es decir, una lista de pares donde los primeros elementos es el número de viajeros que suben y los segundo es el de los que bajan.

Un recorrido es correcto si en cada bajada tanto el número de viajeros que suben como los que bajan son positivos, el número de viajeros en el autobús no puede ser mayor que su capacidad y el número de viajeros que bajan no puede ser mayor que el número de viajeros en el autobús. Se supone que en la primera parada el autobús no tiene viajeros.

Definir la función

   recorridoCorrecto :: Int -> [(Int,Int)] -> Bool

tal que (recorridoCorrecto n ps) se verifica si ps es un recorrido correcto en un autobús cuya capacidad es n. Por ejemplo,

  recorridoCorrecto 20 [(3,0),(9,1),(4,10),(12,2),(6,1)]  ==  True
  recorridoCorrecto 15 [(3,0),(9,1),(4,10),(12,2),(6,1)]  ==  False
  recorridoCorrecto 15 [(3,2),(9,1),(4,10),(12,2),(6,1)]  ==  False
  recorridoCorrecto 15 [(3,0),(2,7),(4,10),(12,2),(6,1)]  ==  False

el segundo ejemplo es incorrecto porque en la última para se supera la capacidad del autobús; el tercero, porque en la primera para no hay viajeros en el autobús que se puedan bajar y el cuarto, porque en la 2ª parada el autobús tiene 3 viajeros por lo que es imposible que se bajen 7.

-}


recorridoCorrecto :: Int -> [(Int,Int)] -> Bool
recorridoCorrecto m = (>= 0) . foldl aux 0
    where
        aux acc (s,b) | comprobacion acc (s,b) = acc + s - b
                      | otherwise = -1

        comprobacion n (s, b) =
            s >= 0 && b >= 0    -- número de viajeros que suben y bajan son positivos
            && n + s - b <= m   -- número de viajeros en el autobús no mayor que su capacidad
            && b <= n           -- número de viajeros que bajan no mayor que el número de viajeros en el autobús
