{-# LANGUAGE UnicodeSyntax #-}

module Ex_20171221 where

{-

Sumable sin vecinos
===================

En la lista [3,2,5,7,4] el número 12 se puede escribir como una suma de elementos de la lista sin incluir sus vecinos (ya que es la suma de 3, 5 y 4); en cambio, 14 no lo es (porque es la suma de 3, 7 y 4, pero 7 y 4 son vecinos).

Definir la función

   esSumableSinVecinos :: [Int] -> Int -> Bool

tal que (esSumableSinVecinos xs n) se verifica si n se puede escribir como una suma de elementos de xs que no incluye a ninguno de sus vecinos. Por ejemplo,

   esSumableSinVecinos [3,2,5,7,4] 12  ==  True
   esSumableSinVecinos [3,2,5,7,4] 9   ==  True
   esSumableSinVecinos [3,2,5,7,4] 6   ==  True
   esSumableSinVecinos [3,2,5,7,4] 14  ==  False
   esSumableSinVecinos [3,2,5,7,4] 1   ==  False

-}


esSumableSinVecinos :: [Int] -> Int -> Bool
esSumableSinVecinos [] _        = False
esSumableSinVecinos [x] n       = x == n
esSumableSinVecinos (x:y:xs) n  | n <= 0 = False
                                | n == x = True
                                | otherwise =  esSumableSinVecinos xs (n-x)
                                            || esSumableSinVecinos (y:xs) n
