{-# OPTIONS_GHC -fno-warn-incomplete-patterns #-}

module Ex_20170116 where

{-

Inversa del factorial
=====================

Definir la función

   inversaFactorial :: Integer -> Maybe Integer

tal que (inversaFactorial x) es (Just n) si el factorial de n es x y es Nothing
si no existe ningún número n tal que el factorial de n es x. Por ejemplo,

   inversaFactorial 24  ==  Just 4
   inversaFactorial 25  ==  Nothing
   inversaFactorial (product [1..10000]) == Just 10000
   inversaFactorial (1 + product [1..10000]) == Nothing

-}


inversaFactorial :: Integer -> Maybe Integer
inversaFactorial = inversaFactorial2

inversaFactorial1 :: Integer -> Maybe Integer
inversaFactorial1 = aux [1..]
  where aux (x:xs) n | x == n         = Just x
                     | n `mod` x /= 0 = Nothing
                     | otherwise      = aux xs (n `div` x)

inversaFactorial2 :: Integer -> Maybe Integer
inversaFactorial2 = aux 1
  where aux p n | p == n         = Just p
                | n `mod` p /= 0 = Nothing
                | otherwise      = aux (p+1) (n `div` p)

inversaFactorial3 :: Integer -> Maybe Integer
inversaFactorial3 = aux 1
  where aux p n | p == n    = Just p
                | r /= 0    = Nothing
                | otherwise = aux (p+1) d
            where (d,r) = n `divMod` p
