{-# LANGUAGE UnicodeSyntax #-}

module Ex_20171113 where

{-

Números oblongos
================

Un número oblongo es un número que es el producto de dos números naturales consecutivos; es decir, n es un número oblongo si existe un número natural x tal que n = x(x+1). Por ejemplo, 42 es un número oblongo porque 42 = 6 x 7.

Definir las funciones

   esOblongo :: Integer -> Bool
   oblongos  :: [Integer]

tales que

    (esOblongo n) se verifica si n es oblongo. Por ejemplo,

     esOblongo 42               ==  True
     esOblongo 40               ==  False
     esOblongo 100000010000000  ==  True

    oblongos es la suceción de los números oblongos. Por ejemplo,

     take 15 oblongos   == [0,2,6,12,20,30,42,56,72,90,110,132,156,182,210]
     oblongos !! 50     == 2550
     oblongos !! (10^7) == 100000010000000

-}

esOblongo :: Integer -> Bool
esOblongo = esOblongo1

esOblongo1 :: Integer -> Bool
esOblongo1 n = n == x * (x+1)
  where x = floor $ sqrt (fromIntegral n)

oblongos  :: [Integer]
oblongos = zipWith (*) [0..] [1..]
