module Ex_20170103 where

{-

Estratificación de un árbol
===========================

Los árboles se pueden representar mediante el siguiente tipo de datos

   data Arbol a = N a [Arbol a]
     deriving Show

Por ejemplo, los árboles

     1         1             1
    / \       / \           / \
   8   3     8   3         8   3
       |        /|\       /|\  |
       4       4 5 6     4 5 6 7

se representan por

   ej1, ej2, ej3 :: Arbol Int
   ej1 = N 1 [N 8 [],N 3 [N 4 []]]
   ej2 = N 1 [N 8 [], N 3 [N 4 [], N 5 [], N 6 []]]
   ej3 = N 1 [N 8 [N 4 [], N 5 [], N 6 []], N 3 [N 7 []]]

Un estrato de un árbol es la lista de nodos que se encuentran al mismo nivel de
profundidad. Por ejemplo, los estratos del árbol ej1 son [1], [8,3] y [4].

Definir la función

   estratos :: Arbol a -> [[a]]

tal que (estratos x) es la lista de los estratos del árbol x. Por ejemplo,

   estratos ej1 == [[1],[8,3],[4]]
   estratos ej2 == [[1],[8,3],[4,5,6]]
   estratos ej3 == [[1],[8,3],[4,5,6,7]]


-}

import Data.List


data Arbol a = N a [Arbol a]
  deriving Show

ej1, ej2, ej3 :: Arbol Int
ej1 = N 1 [N 8 [],N 3 [N 4 []]]
ej2 = N 1 [N 8 [], N 3 [N 4 [], N 5 [], N 6 []]]
ej3 = N 1 [N 8 [N 4 [], N 5 [], N 6 []], N 3 [N 7 []]]

estratos :: Arbol a -> [[a]]
estratos = estratos3

estratos1 :: Arbol a -> [[a]]
estratos1 xs = unfoldr estrato1 [xs]

estrato1 :: [Arbol a] -> Maybe ([a], [Arbol a])
estrato1 [] = Nothing
estrato1 xs = Just (roots, concat leafs)
  where (roots, leafs) = unzip [(r,as) | (N r as) <- xs ]

estratos2 :: Arbol a -> [[a]]
estratos2 x = estrato2 [x]

estrato2 :: [Arbol a] -> [[a]]
estrato2 []  = []
estrato2 xss = roots : estrato2 (concat leafs)
  where (roots, leafs) = unzip [(r,as) | (N r as) <- xss ]

estratos3 :: Arbol a -> [[a]]
estratos3 (N x xs) = [x] : (map concat . transpose . map estratos3) xs

-- solución oficial
estratos4 :: Arbol a -> [[a]]
estratos4 x = takeWhile (not . null) [estrato n x | n <- [0..]]

-- (estrato n x) es el estrato de nivel n del árbol x. Por ejemplo,
--    estrato 0 ej1  ==  [1]
--    estrato 1 ej1  ==  [8,3]
--    estrato 2 ej1  ==  [4]
--    estrato 4 ej1  ==  []
estrato :: Int -> Arbol a ->  [a]
estrato 0 (N x _)  = [x]
estrato n (N _ xs) = concatMap (estrato (n-1)) xs
