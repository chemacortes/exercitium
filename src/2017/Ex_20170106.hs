module Ex_20170106 where

{-

Números dorados
===============

Los dígitos del número 2375 se pueden separar en dos grupos ([7,2] y [5,3])
tales que los correspondientes números (72 y 53) tienen el mismo número de
dígitos y, además, 72^2 – 53^2 es el número original (es decir, 72^2 – 53^2 = 2375).

Un número x es dorado si tiene un número par de dígitos y con sus dígitos se
pueden formar dos números a, b de igual longitud tales que b^2 – a^2 = x. Por
ejemplo, 2375 es dorado (con b = 72 y a = 53).

Definir la función

   esDorado :: Integer -> Bool

tales que (esDorado x) se verifica si x es un número dorado. Por
ejemplo,

   λ> esDorado 2375
   True
   λ> take 5 [x | x <- [1..], esDorado x]
   [48,1023,1404,2325,2375]

-}

import Data.List

esDorado :: Integer -> Bool
esDorado x =
  even (length (show x)) &&
  or [b^2 - a^2 == x | (a,b) <- particionesNumero x]

-- (particiones xs) es la lista de las formas de dividir xs en dos
-- partes de igual longitud (se supone que xs tiene un número par de
-- elementos). Por ejemplo,
--    λ> particiones "abcd"
--    [("ab","cd"),("ba","cd"),("cb","ad"),("bc","ad"),("ca","bd"),
--     ("ac","bd"),("dc","ba"),("cd","ba"),("cb","da"),("db","ca"),
--     ("bd","ca"),("bc","da"),("da","bc"),("ad","bc"),("ab","dc"),
--     ("db","ac"),("bd","ac"),("ba","dc"),("da","cb"),("ad","cb"),
--     ("ac","db"),("dc","ab"),("cd","ab"),("ca","db")]
particiones :: [a] -> [([a],[a])]
particiones xs =
  [splitAt m ys | ys <- permutations xs]
  where m = length xs `div` 2

-- (particionesNumero n) es la lista de las formas de dividir n en dos
-- partes de igual longitud (se supone que n tiene un número par de
-- dígitos). Por ejemplo,
--    λ> particionesNumero 1234
--    [(12,34),(21,34),(32,14),(23,14),(31,24),(13,24),(43,21),(34,21),
--     (32,41),(42,31),(24,31),(23,41),(41,23),(14,23),(12,43),(42,13),
--     (24,13),(21,43),(41,32),(14,32),(13,42),(43,12),(34,12),(31,42)]
particionesNumero :: Integer -> [(Integer,Integer)]
particionesNumero n =
  [(read xs,read ys) | (xs,ys) <- particiones (show n)]



esDorado1 :: Integer -> Bool
esDorado1 n = even m && n `elem` cs
  where xs = show n
        m = length xs
        cs = do ws <- combinaciones (m `div` 2) xs
                zs <- permutations (xs\\ws)
                ys <- permutations ws
                return $ abs (read ys ^2 - read zs ^2)

-- combinaciones de N elementos tomadas de k en k
combinaciones :: Int -> [a] -> [[a]]
combinaciones 0 _      = [[]]
combinaciones _ []     = []
combinaciones n (x:xs) = map (x:) (combinaciones (n-1) xs) ++ combinaciones n xs

esDorado2 :: Integer -> Bool
esDorado2 x = even (length z) &&
              x `elem` (map (f.g.splitAt (div (length z) 2)) (permutations z))
          where z = show x
                f (x,y) = x^2-y^2
                g (x,y) = (read x,read y)

esDorado3 :: Integer -> Bool
esDorado3 n =  r == 0 && n `elem` map (f . splitAt d) (permutations z)
  where z = show n
        (d,r) = length z `divMod` 2
        f (x,y) = abs (read x ^2 - read y ^2)
