module Ex_20170117 where

{-

Sumas de dos capicúas
=====================

Definir las funciones

   sumas2Capicuas  :: Integer -> [(Integer, Integer)]
   noSuma2Capicuas :: [Integer]

tales que

    (sumas2Capicuas x) es la lista de las descomposiciones de x como suma de dos
    capicúas (con el primer sumando menor o igual que el segundo). Por ejemplo,

      sumas2Capicuas 17  == [(6,11),(8,9)]
      sumas2Capicuas 187 == [(6,181),(66,121),(88,99)]
      sumas2Capicuas 165 == [(4,161),(44,121),(66,99),(77,88)]
      sumas2Capicuas 382 == [(9,373),(191,191)]
      sumas2Capicuas 151 == [(0,151)]
      sumas2Capicuas 201 == []

    noSuma2Capicuas es la sucesión de los números que no se pueden escribir como
    suma de dos capicúas. Por ejemplo,

      λ> take 15 noSuma2Capicuas
      [21,32,43,54,65,76,87,98,201,1031,1041,1042,1051,1052,1053]
      λ> noSuma2Capicuas !! 3000
      19941

-}

import Ex_20170111 (capicuas)

esCapicua :: Integer -> Bool
esCapicua = (==) <$> id <*> reverse <$> show

sumas2Capicuas  :: Integer -> [(Integer, Integer)]
sumas2Capicuas n = [ (x,n-x) | x <- takeWhile (<= n `div` 2) capicuas
                             , esCapicua (n-x)]

noSuma2Capicuas :: [Integer]
noSuma2Capicuas = filter (null . sumas2Capicuas) [1..]
