{-# LANGUAGE UnicodeSyntax #-}

module Ex_20171211 where

{-

Número de viajeros en el autobús
================================

Un autobús inicia su recorrido con 0 viajeros. El número de viajeros que se suben y bajan en cada parada se representa por un par `(x,y)` donde `x` es el número de las que suben e `y` el de las que bajan. Un recorrido del autobús se representa por una lista de pares representando los números de viajeros que suben o bajan en cada parada.

Definir la función

   nViajerosEnBus :: [(Int, Int)] -> Int

tal que (nViajerosEnBus ps) es el número de viajeros en el autobús tras el recorrido ps. Por ejemplo,

  nViajerosEnBus []                                        ==  0
  nViajerosEnBus [(10,0),(3,5),(5,8)]                      ==  5
  nViajerosEnBus [(3,0),(9,1),(4,10),(12,2),(6,1),(7,10)]  ==  17
  nViajerosEnBus [(3,0),(9,1),(4,8),(12,2),(6,1),(7,8)]    ==  21

-}


nViajerosEnBus :: [(Int, Int)] -> Int
nViajerosEnBus xs = sum [x-y | (x,y) <- xs]

nViajerosEnBus2 :: [(Int, Int)] -> Int
nViajerosEnBus2 = sum . fmap (uncurry (-))
