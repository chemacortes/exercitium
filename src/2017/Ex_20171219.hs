{-# LANGUAGE UnicodeSyntax #-}

module Ex_20171219 where

{-

Vecino en lista circular
========================

En la lista circular [3,2,5,7,9]

    el vecino izquierdo de 5 es 2 y su vecino derecho es 7,
    el vecino izquierdo de 9 es 7 y su vecino derecho es 3,
    el vecino izquierdo de 3 es 9 y su vecino derecho es 2,
    el elemento 4 no tiene vecinos (porque no está en la lista).

Para indicar las direcciones se define el tipo de datos

   data Direccion = I | D deriving Eq

Definir la función

   vecino :: Eq a => Direccion -> [a] -> a -> Maybe a

tal que (vecino d xs x) es el vecino de x en la lista de elementos distintos xs según la dirección d. Por ejemplo,

   vecino I [3,2,5,7,9] 5  ==  Just 2
   vecino D [3,2,5,7,9] 5  ==  Just 7
   vecino I [3,2,5,7,9] 9  ==  Just 7
   vecino D [3,2,5,7,9] 9  ==  Just 3
   vecino I [3,2,5,7,9] 3  ==  Just 9
   vecino D [3,2,5,7,9] 3  ==  Just 2
   vecino I [3,2,5,7,9] 4  ==  Nothing
   vecino D [3,2,5,7,9] 4  ==  Nothing

-}


data Direccion = I | D deriving Eq


vecino :: Eq a => Direccion -> [a] -> a -> Maybe a
vecino = vecino1

vecino1 :: Eq a => Direccion -> [a] -> a -> Maybe a
vecino1 _ [] _ = Nothing
vecino1 d (x:xs) y | d == D    = lookup y (zip (x:xs) despl)
                   | otherwise = lookup y (zip despl (x:xs))
  where
    despl = xs ++ [x]

rotL :: [a] -> [a]
rotL = (++) <$> tail <*> pure . head

vecino2 :: Eq a => Direccion -> [a] -> a -> Maybe a
vecino2 _ [] = const Nothing
vecino2 I xs = flip lookup $ zip (rotL xs) xs
vecino2 D xs = flip lookup $ zip xs (rotL xs)


rotL2 :: [a] -> [a]
rotL2 []     = []
rotL2 (x:xs) = xs ++ [x]

vecino3 :: Eq a => Direccion -> [a] -> a -> Maybe a
vecino3 d = compose d <$> id <*> rotL2
  where
    compose I = (flip lookup .) . flip zip
    compose D = (flip lookup .) . zip
