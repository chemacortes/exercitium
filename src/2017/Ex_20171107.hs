{-# LANGUAGE UnicodeSyntax #-}

module Ex_20171107 where

{-

Primos consecutivos equidistantes
=================================

Definir la función

   primosEquidistantes :: Integer -> [(Integer,Integer)]

tal que (primosEquidistantes k) es la lista de los pares de primos consecutivos cuya diferencia es k. Por ejemplo,

   take 3 (primosEquidistantes 2)     ==  [(3,5),(5,7),(11,13)]
   take 3 (primosEquidistantes 4)     ==  [(7,11),(13,17),(19,23)]
   take 3 (primosEquidistantes 6)     ==  [(23,29),(31,37),(47,53)]
   take 3 (primosEquidistantes 8)     ==  [(89,97),(359,367),(389,397)]
   (primosEquidistantes 30) !! 14000  ==  (6557303,6557333)

-}

import           Data.Numbers.Primes

primosEquidistantes :: Integer -> [(Integer, Integer)]
primosEquidistantes n = [(x,y) | (x,y) <- zip primes (tail primes), y-x == n]

primosEquidistantes2 :: Integer -> [(Integer, Integer)]
primosEquidistantes2 n = aux primes
    where aux (x:y:xs) | y-x==n     = (x,y): aux (y:xs)
                       | otherwise  = aux (y:xs)
          aux _ = []


primosEquidistantes3 :: Integer -> [(Integer, Integer)]
primosEquidistantes3 n = aux 2 primes
  where aux a (x:xs) | x-a==n     = (a,x): aux x xs
                     | otherwise  = aux x xs
        aux _ _ = []
