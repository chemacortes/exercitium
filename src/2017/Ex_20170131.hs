module Ex_20170131 where

{-

Eliminación de triplicados
==========================

Definir la función

   sinTriplicados :: Eq a => [a] -> [a]

tal que (sinTriplicados xs) es la lista obtenida dejando en xs sólo las dos
primeras ocurrencias de cada uno de sus elementos. Por ejemplo,

   sinTriplicados "aaabcbccdbabdcd"  ==  "aabcbcdd"
   sinTriplicados "xxxxx"            ==  "xx"
   sinTriplicados "abcabc"           ==  "abcabc"
   sinTriplicados "abcdabcaba"       ==  "abcdabc"
   sinTriplicados "abacbadcba"       ==  "abacbdc"
   sinTriplicados "aaabcbccdbabdcd"  ==  "aabcbcdd"
   sinTriplicados (show (5^4^3))     ==  "54210108624757363989"
   sinTriplicados (show (8^8^8))     ==  "60145207536139279488"


-}

import Data.List

-- 1ª Definición
--  'vs' lista de elementos visitados dos veces
--  'as' lista resultado
sinTriplicados1 :: Eq a => [a] -> [a]
sinTriplicados1 = f [] []
  where
    f _  as [] = reverse as
    f vs as (x:xs) | x `elem` vs = f vs as xs
                   | x `elem` as = f (x:vs) (x:as) xs
                   | otherwise   = f vs (x:as) xs

-- 2ª Definición
sinTriplicados2 :: Eq a => [a] -> [a]
sinTriplicados2 = reverse . f []
  where
    count x = length.filter (==x)
    f as [] = as
    f as (x:xs) | count x as < 2 = f (x:as) xs
                | otherwise      = f as (filter (/=x) xs)

-- 3ª Definición: Algoritmo progresivo
sinTriplicados3 :: Eq a => [a] -> [a]
sinTriplicados3 xs = f (nub xs) [] xs
  where
    f _  as [] = reverse as
    f vs as (y:ys) | y `elem` vs && y `elem` as = f (delete y vs) (y:as) (delete y ys)
                   | y `elem` vs                = f vs (y:as) ys
                   | otherwise                  = f vs as ys


-- 4ª Definición: Optimizada y general
sinTriplicados :: Eq a => [a] -> [a]
sinTriplicados = soloKDuplicados 2


soloKDuplicados :: Eq a => Int -> [a] -> [a]
soloKDuplicados k xs = f [] (concat . replicate k $ nub xs) xs
  where
    f as ds@(_:_) (y:ys) | y `elem` ds  = f (y:as) (delete y ds) ys
                         | otherwise    = f as ds ys
    f as _ _  = reverse as
