{-# LANGUAGE UnicodeSyntax #-}

module Ex_20180430 where

{-

Las sucesiones de Loomis
========================

La sucesión de Loomis generada por un número entero positivo x es la sucesión cuyos términos se definen por

    f(0) es x
    f(n) es la suma de f(n-1) y el producto de los dígitos no nulos de f(n-1)

Los primeros términos de las primeras sucesiones de Loomis son

    Generada por 1: 1, 2, 4, 8, 16, 22, 26, 38, 62, 74, 102, 104, 108, 116, 122, …
    Generada por 2: 2, 4, 8, 16, 22, 26, 38, 62, 74, 102, 104, 108, 116, 122, 126, …
    Generada por 3: 3, 6, 12, 14, 18, 26, 38, 62, 74, 102, 104, 108, 116, 122, 126, …
    Generada por 4: 4, 8, 16, 22, 26, 38, 62, 74, 102, 104, 108, 116, 122, 126, 138, …
    Generada por 5: 5, 10, 11, 12, 14, 18, 26, 38, 62, 74, 102, 104, 108, 116, 122, …

Se observa que a partir de un término todas coinciden con la generada por 1. Dicho término se llama el punto de convergencia. Por ejemplo,

    la generada por 2 converge a 2
    la generada por 3 converge a 26
    la generada por 4 converge a 4
    la generada por 5 converge a 26

Definir las siguientes funciones

   sucLoomis           :: Integer -> [Integer]
   convergencia        :: Integer -> Integer
   graficaConvergencia :: [Integer] -> IO ()

tales que

    (sucLoomis x) es la sucesión de Loomis generada por x. Por ejemplo,

     λ> take 15 (sucLoomis 1)
     [1,2,4,8,16,22,26,38,62,74,102,104,108,116,122]
     λ> take 15 (sucLoomis 2)
     [2,4,8,16,22,26,38,62,74,102,104,108,116,122,126]
     λ> take 15 (sucLoomis 3)
     [3,6,12,14,18,26,38,62,74,102,104,108,116,122,126]
     λ> take 15 (sucLoomis 4)
     [4,8,16,22,26,38,62,74,102,104,108,116,122,126,138]
     λ> take 15 (sucLoomis 5)
     [5,10,11,12,14,18,26,38,62,74,102,104,108,116,122]
     λ> take 15 (sucLoomis 20)
     [20,22,26,38,62,74,102,104,108,116,122,126,138,162,174]
     λ> take 15 (sucLoomis 100)
     [100,101,102,104,108,116,122,126,138,162,174,202,206,218,234]

    (convergencia x) es el término de convergencia de la sucesioń de Loomis generada por x xon la geerada por 1. Por ejemplo,

     convergencia  2  ==  2
     convergencia  3  ==  26
     convergencia  4  ==  4
     convergencia 17  ==  38
     convergencia 19  ==  102
     convergencia 43  ==  162
     convergencia 27  ==  202
     convergencia 58  ==  474
     convergencia 63  ==  150056
     convergencia 81  ==  150056
     convergencia 89  ==  150056

    (graficaConvergencia xs) dibuja la gráfica de los términos de convergencia de las sucesiones de Loomis generadas por los elementos de xs. Por ejemplo, (graficaConvergencia [1..50]) dibuja

    ![Las_sucesiones_de_Loomis_1](Ex_20180430-Las_sucesiones_de_Loomis_1.png)

    y graficaConvergencia ([1..148] \\ [63,81,89,137]) dibuja

    ![Las_sucesiones_de_Loomis_2](x_20180430-Las_sucesiones_de_Loomis_2.png)

-}

import           Data.Char
import           Graphics.Gnuplot.Simple

sucLoomis :: Integer -> [Integer]
sucLoomis = sucLoomis3

sucLoomis1 :: Integer -> [Integer]
sucLoomis1 = iterate aux
  where aux x = x + product [ read [c] | c <- show x, c /= '0' ]

sucLoomis3 :: Integer -> [Integer]
sucLoomis3 = iterate
  ((+) <*> product . map (toInteger . digitToInt) . filter (/= '0') . show)


convergencia :: Integer -> Integer
convergencia = aux (sucLoomis 1) . sucLoomis
 where
  aux (x : xs) (y : ys) | x == y    = x
                        | x < y     = aux xs (y : ys)
                        | otherwise = aux (x : xs) ys
  aux _ _ = undefined

graficaConvergencia :: [Integer] -> IO ()
graficaConvergencia xs = plotList
  [Key Nothing, Title "Convergencia de sucesiones de Loomis"]
  [ (x, convergencia x) | x <- xs ]
