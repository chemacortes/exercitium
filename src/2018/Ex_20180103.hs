{-# LANGUAGE UnicodeSyntax #-}

module Ex_20180103 where

{-

Ofertas 3 por 2
===============

En una tienda tienen la “oferta 3 por 2″ de forma que cada cliente que elige 3 artículos obtiene el más barato de forma gratuita. Por ejemplo, si los precios de los artículos elegidos por un cliente son 10, 2, 4, 5 euros pagará 19 euros si agrupa los artículos en (10,2,4) y (5) o pagará 17 si lo agupa en (5,10,4) y (2).

Definir la función

   minimoConOferta :: [Int] -> Int

tal que (minimoConOferta xs) es lo mínimo que pagará el cliente si los precios de la compra son xs; es decir, lo que pagará agrupando los artículos de forma óptima para aplicar la oferta 3 por 2. Por ejemplo,

   minimoConOferta [10,2,4,5]     ==  17
   minimoConOferta [3,2,3,2]      ==  8
   minimoConOferta [6,4,5,5,5,5]  ==  21

-}

import           Data.List (sortBy)

minimoConOferta :: [Int] -> Int
minimoConOferta = minimoConOferta2


minimoConOferta1 :: [Int] -> Int
minimoConOferta1 xs = sum [x | (x,i) <- zip xs' [1..], i `mod` 3 /= 0]
  where
    xs' = sortBy (flip compare) xs


minimoConOferta2 :: [Int] -> Int
minimoConOferta2 xs = aux $ sortBy (flip compare) xs
  where
    aux [] = 0
    aux ys = sum (take 2 ys) + aux (drop 3 ys)
