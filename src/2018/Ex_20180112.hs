{-# LANGUAGE UnicodeSyntax #-}

module Ex_20180112 where

{-

Números malvados y odiosos
==================

Un número malvado es un número natural cuya expresión en base 2 (binaria) contiene un número par de unos.

Un número odioso es un número natural cuya expresión en base 2 (binaria) contiene un número impar de unos.

Podemos representar los números malvados y odiosos mediante el siguiente tipo de dato

  data MalvadoOdioso = Malvado | Odioso deriving Show

Definir la función

  malvadoOdioso :: Integer -> MalvadoOdioso

tal que (malvadoOdioso n) devuelve el tipo de número que es n. Por ejemplo,

   λ> malvadoOdioso 11
   Odioso
   λ> malvadoOdioso 12
   Malvado
   λ> malvadoOdioso3 (10^20000000)
   Odioso
   λ> malvadoOdioso3 (1+10^20000000)
   Malvado

Nota: Este ejercicio ha sido propuesto por Ángel Ruiz Campos.

-}

import           Data.Bits (popCount)

data MalvadoOdioso = Malvado | Odioso deriving Show

malvadoOdioso :: Integer -> MalvadoOdioso
malvadoOdioso n | even (popCount n) = Malvado
                | otherwise         = Odioso

cambio :: MalvadoOdioso -> MalvadoOdioso
cambio Malvado = Odioso
cambio Odioso  = Malvado

malvadoOdioso2 :: Integer -> MalvadoOdioso
malvadoOdioso2 n | r == 0     = malvadoOdioso d
                 | otherwise  = cambio $ malvadoOdioso d
  where
    (d,r) = divMod n 2
