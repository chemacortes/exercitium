{-# LANGUAGE UnicodeSyntax #-}

module Ex_20180130 where

{-

Relaciones arbóreas
===================

Como se explica en el ejercicio "Relación definida por un árbol", cada árbol binario define una relación binaria donde un elemento x está relacionado con y si x es el padre de y.

Una relación binaria es arbórea si

    hay exactamente un elemento que no tiene ningún (la raíz del árbol) y
    todos los elementos tienen dos hijos (los nodos internos) o ninguno (las hojas del árbol).

Definir la función

   arborea :: Eq a => [(a,a)] -> Bool

tal que (arborea r) se verifica si la relación r es arbórea. Por ejemplo,

   arborea [(10,8),(8,3),(8,5),(10,2),(2,2),(2,0)]  ==  True
   arborea [(8,3),(8,5),(10,2),(2,2),(2,0)]         ==  False
   arborea [(10,8),(8,3),(8,5),(10,2),(8,2),(2,0)]  ==  False

-}

import           Data.List (elemIndices, nub, (\\))

count :: Eq a => a -> [a] -> Int
count i = length . elemIndices i

arborea :: Eq a => [(a,a)] -> Bool
arborea xs =  length (nub padres \\ nub hijos) == 1
           && and [count x padres == 2 | x <- nub padres]
  where
    (padres, hijos) = unzip xs
