{-# LANGUAGE UnicodeSyntax #-}

module Ex_20180313 where

{-

Suma de las alturas de los nodos de un árbol
============================================

Las árboles binarios se pueden representar con el siguiente tipo

   data Arbol a = H
                | N a (Arbol a) (Arbol a)
     deriving Show

Por ejemplo, el árbol

          1
         / \
        2   3
       / \
      4   5

se representa por

   ej1 :: Arbol Int
   ej1 = N 1 (N 2 (N 4 H H) (N 5 H H)) (N 3 H H)

La altura de cada elemento del árbol es la máxima distancia a las hojas en su rama. Por ejemplo, en el árbol anterior, la altura de 1 es 3, la de 2 es 2, la de 3 es 1, la de 4 es 1 y la de 5 es 1.

Definir la función

   sumaAlturas :: Arbol t -> Int

tal que (sumaAlturas a) es la suma de las alturas de los elementos de a. Por ejemplo,

   λ> sumaAlturas (N 1 (N 2 (N 4 H H) (N 5 H H)) (N 3 H H))
   8
   λ> sumaAlturas (N 1 (N 2 (N 4 H H) H) (N 3 H H))
   7
   λ> sumaAlturas (N 1 (N 2 (N 4 H H) H) (N 2 (N 4 H H) (N 5 H H)))
   10

-}


data Arbol a = H
             | N a (Arbol a) (Arbol a)
  deriving Show

ej1 :: Arbol Int
ej1 = N 1 (N 2 (N 4 H H) (N 5 H H)) (N 3 H H)


sumaAlturas :: Arbol t -> Int
sumaAlturas H         = 0
sumaAlturas (N _ a b) = 1 + max izq der + sumaAlturas a + sumaAlturas b
 where
  izq = altura a
  der = altura b

altura :: Arbol t -> Int
altura H         = 0
altura (N _ a b) = 1 + max (altura a) (altura b)


sumaAlturas2 :: Arbol t -> Int
sumaAlturas2 = suma . f

data ArbolData = ArbolData { alt::Int, suma::Int}
  deriving Show

f :: Arbol t -> ArbolData
f H         = ArbolData 0 0
f (N _ a b) = ArbolData maxAlt (maxAlt + suma fa + suma fb)
 where
  fa     = f a
  fb     = f b
  maxAlt = 1 + max (alt fa) (alt fb)


sumaAlturas3 :: Arbol t -> Int
sumaAlturas3 = sum . alturas

alturas :: Arbol t -> [Int]
alturas H         = [0]
alturas (N _ a b) = (1 + max x y) : xs ++ ys
 where
  xs@(x : _) = alturas a
  ys@(y : _) = alturas b
