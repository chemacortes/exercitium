{-# LANGUAGE UnicodeSyntax #-}

module Ex_20180122 where

{-

Complemento potencial
=====================

El complemento potencial de un número entero positivo `x` es el menor número `y` tal que el producto de `x` por `y` es un una potencia perfecta. Por ejemplo,

    el complemento potencial de 12 es 3 ya que 12 y 24 no son potencias perfectas pero 36 sí lo es;
    el complemento potencial de 54 es 4 ya que 54, 108 y 162 no son potencias perfectas pero 216 = 6^3 sí lo es.

Definir las funciones

   complemento                 :: Integer -> Integer
   graficaComplementoPotencial :: Integer -> IO ()

tales que

    (complemento x) es el complemento potencial de x; por ejemplo,

     complemento 12     ==  3
     complemento 54     ==  4
     complemento 720    ==  5
     complemento 24000  ==  9
     complemento 2018   ==  2018

    (graficaComplementoPotencial n) dibuja la gráfica de los complementos potenciales de los n primeros números enteros positivos. Por ejemplo, (graficaComplementoPotencial 100) dibuja


    ![Complemento_potencial_100](Ex_20180122-Complemento_potencial_100.png)


    y (graficaComplementoPotencial 500) dibuja


    ![Complemento_potencial_500](Ex_20180122-Complemento_potencial_500.png)

Comprobar con QuickCheck que (complemento x) es menor o igual que x.

-}

import           Data.List
import           Data.Numbers.Primes     (primeFactors)
import           Graphics.Gnuplot.Simple
import           Test.QuickCheck


-- Calcula el número que hay que suma a `m` para alcanzar un múltiplo de `n`
complementoMCM :: Int -> Int -> Int
complementoMCM n m | m `mod` n == 0 = 0
                   | otherwise      = n - m `mod` n

complemento :: Integer -> Integer
complemento n =
  minimum [ product [b^complementoMCM p k | (b,k) <- fs] | p <- ms]
  where
    fs = [(head xs, length xs) | xs <- (group . primeFactors) n]
    ms = nub $ 2 : (filter (>1) . map snd) fs

graficaComplementoPotencial :: Integer -> IO ()
graficaComplementoPotencial n =
  plotList [
    Title ("(graficaComplementoPotencial " ++ show n ++")"),
    Key Nothing]
     (map complemento [1..n])


prop_complemento :: Positive Integer -> Bool
prop_complemento (Positive n) = complemento n <= n
