{-# LANGUAGE UnicodeSyntax #-}

module Ex_20180508 where

{-

Números construidos con los dígitos de un conjunto dado
=======================================================

Definir las siguientes funciones

   numerosCon      :: [Integer] -> [Integer]
   numeroDeDigitos :: [Integer] -> Integer -> Int

tales que

    (numerosCon ds) es la lista de los números que se pueden construir con los dígitos de ds (cuyos elementos son distintos elementos del 1 al 9) . Por ejemplo,

     λ> take 22 (numerosCon [1,4,6,9])
     [1,4,6,9,11,14,16,19,41,44,46,49,61,64,66,69,91,94,96,99,111,114]
     λ> take 15 (numerosCon [4,6,9])
     [4,6,9,44,46,49,64,66,69,94,96,99,444,446,449]
     λ> take 15 (numerosCon [6,9])
     [6,9,66,69,96,99,666,669,696,699,966,969,996,999,6666]

    (numeroDeDigitos ds k) es el número de dígitos que tiene el k-ésimo elemento (empezando a contar en 0) de la sucesión (numerosCon ds). Por ejemplo,

     numeroDeDigitos [1,4,6,9]   3  ==  1
     numeroDeDigitos [1,4,6,9]   6  ==  2
     numeroDeDigitos [1,4,6,9]  22  ==  3
     numeroDeDigitos [4,6,9]    15  ==  3
     numeroDeDigitos [6,9]      15  ==  4
     numeroDeDigitos [1,4,6,9] (10^(10^5))  ==  166097
     numeroDeDigitos   [4,6,9] (10^(10^5))  ==  209590
     numeroDeDigitos     [6,9] (10^(10^5))  ==  332192

-}

-- FIXME: falta cotejo con la solución oficial

import Data.List (genericIndex)


numerosCon :: [Integer] -> [Integer]
numerosCon xs = map read $ concat zss
 where
  xss = map show xs
  zss = xss : [ [ x : s | [x] <- xss, s <- ys ] | ys <- zss ]

numeroDeDigitos1 :: [Integer] -> Integer -> Int
numeroDeDigitos1 ds k = length . show $ genericIndex (numerosCon ds) k



--numeroDeDigitos :: [Integer] -> Integer -> Int
