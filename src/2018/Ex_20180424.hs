{-# LANGUAGE UnicodeSyntax #-}

module Ex_20180424
where

{-

Sumas de subconjuntos
==================

Definir la función

   sumasSubconjuntos :: Set Int -> Set Int

tal que (sumasSubconjuntos xs) es el conjunto de las sumas de cada uno de los subconjuntos de xs. Por ejemplo,

   λ> sumasSubconjuntos (fromList [3,2,5])
   fromList [0,2,3,5,7,8,10]
   λ> length (sumasSubconjuntos (fromList [-40,-39..40]))
   1641

-}


-- FIXME: falta cotejo con la solución oficial

import           Data.Set                      as S
import           Data.List                     as L

sumasSubconjuntos :: Set Int -> Set Int
sumasSubconjuntos = sumasSubconjuntos3

sumasSubconjuntos1 :: Set Int -> Set Int
sumasSubconjuntos1 = fromList . L.map sum . subsequences . elems

sumasSubconjuntos2 :: Set Int -> Set Int
sumasSubconjuntos2 = S.map sum . powerSet

sumasSubconjuntos3 :: Set Int -> Set Int
sumasSubconjuntos3 xs | S.null xs = singleton 0
                      | otherwise = zs `S.union` S.map (y +) zs
 where
  (y, ys) = S.deleteFindMin xs
  zs      = sumasSubconjuntos3 ys

sumasSubconjuntos4 :: Set Int -> Set Int
sumasSubconjuntos4 xs
  | S.null xs = singleton 0
  | otherwise = (S.union <$> id <*> S.map (y +)) $ sumasSubconjuntos4 ys
  where (y, ys) = S.deleteFindMin xs
