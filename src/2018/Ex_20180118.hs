{-# LANGUAGE UnicodeSyntax #-}

module Ex_20180118 where

{-

Escalada hasta un primo
=======================

Este ejercicio está basado en el artículo [La conjetura de la “escalada hasta un primo”][1] publicado esta semana por Miguel Ángel Morales en su blog Gaussianos.

La conjetura de escalada hasta un primo trata, propuesta por John Horton Conway, es sencilla de plantear, pero primero vamos a ver qué es eso de escalar hasta un primo. Tomamos un número cualquiera y lo descomponemos en factores primos (colocados en orden ascendente). Si el número era primo, ya hemos acabado; si no era primo, construimos el número formado por los factores primos y los exponentes de los mismos colocados tal cual salen en la factorización. Con el número obtenido hacemos lo mismo que antes. La escalada finaliza cuando obtengamos un número primo. Por ejemplo, para obtener la escalada prima de 1400, como no es primo, se factoriza (obteniéndose 2^3 * 5^2 * 7) y se unen bases y exponentes (obteniéndose 23527). Con el 23527 se repite el proceso obteniéndose la factorización (7 * 3361) y su unión (73361). Como el 73361 es primo, termina la escalada. Por tanto, la escalada de 1400 es [1400,23527,73361].

La conjetura de Conway sobre “escalada hasta un primo” dice que todo número natural mayor o igual que 2 termina su escalada en un número primo.

Definir las funciones

   escaladaPrima                :: Integer -> [Integer]
   longitudEscaladaPrima        :: Integer -> Integer
   longitudEscaladaPrimaAcotada :: Integer -> Integer -> Integer
   graficaEscalada              :: Integer -> Integer -> IO ()

tales que

    (escaladaPrima n) es la escalada prima de n. Por ejemplo,

     λ> escaladaPrima 1400
     [1400,23527,73361]
     λ> escaladaPrima 333
     [333,3237,31383,3211317,3217139151,39722974813]
     λ> take 10 (escaladaPrima 20)
     [20,225,3252,223271,297699,399233,715623,3263907,32347303,160720129]
     λ> take 3 (escaladaPrima 13532385396179)
     [13532385396179,13532385396179,13532385396179]
     λ> take 2 (escaladaPrima 45214884853168941713016664887087462487)
     [45214884853168941713016664887087462487,13532385396179]

    (longitudEscaladaPrima n) es la longitud de la escalada prima de n. Por ejemplo,

     longitudEscaladaPrima 1400  ==  3
     longitudEscaladaPrima 333   ==  6

    (longitudEscaladaPrimaAcotada n k) es el mínimo entre la longitud de la escalada prima de n y k. Por ejemplo,

     longitudEscaladaPrimaAcotada 333 10  ==  6
     longitudEscaladaPrimaAcotada 333 4   ==  4
     longitudEscaladaPrimaAcotada 20 4    ==  4

    (graficaEscalada n k) dibuja la gráfica de (longitudEscaladaPrimaAcotada x k) para x entre 2 y n. Por ejemplo, (graficaEscalada 120 15) dibuja


![Escalada hasta un primo](Ex_20180118-Escalada_hasta_un_primo.png)

[1]: https://www.gaussianos.com/la-conjetura-de-la-escalada-hasta-un-primo/

-}

import           Data.List
import           Data.Numbers.Primes
import           Graphics.Gnuplot.Simple


escaladaPrima :: Integer -> [Integer]
escaladaPrima n | isPrime n = [n]
                | otherwise = n : escaladaPrima (aux n)
  where
    aux x = (read . concatMap show . filter (/=1))
       [f xs | xs <- (group.primeFactors) x
             , f <- [head, genericLength]]

longitudEscaladaPrima :: Integer -> Integer
longitudEscaladaPrima = genericLength . escaladaPrima

longitudEscaladaPrimaAcotada :: Integer -> Integer -> Integer
longitudEscaladaPrimaAcotada x k =
  k `min` (genericLength . genericTake k . escaladaPrima) x

graficaEscalada :: Integer -> Integer -> IO ()
graficaEscalada n k =
  plotList
  [Title $ "graficaEscalada " ++ show n ++ " " ++ show k,
   XLabel "Numeros",
   YLabel "Longitud de escalada",
   Key Nothing]
  (map (`longitudEscaladaPrimaAcotada` k) [2..n])
