{-# LANGUAGE UnicodeSyntax #-}

module Ex_20180207 where

{-

Huecos binarios
===============

Los huecos binarios de un número natural n son las listas de cer0 entre dos unos en la representación binaria de n. Por ejemplo, puesto que la representación binaria de 20 es 10100 tiene dos hecos binarios de longitudes 1 y 2. La longitud del mayor hueco binario de 529 es 4 ya que la representación binaria de 529 es 1000010001.

Definir las funciones

   longMayorHuecoBinario        :: Int -> Int
   graficaLongMayorHuecoBinario :: Int -> IO ()

tales que

    (longMayorHuecoBinario n) es la longitud del mayor hueco binario de n. Por ejemplo,

     longMayorHuecoBinario 20    ==  2
     longMayorHuecoBinario 529   ==  4
     longMayorHuecoBinario 2018  ==  3

    (graficaLongMayorHuecoBinario n) dibuja la gráfica de las longitudes de los mayores huecos binarios de los n primeros números naturales. Por ejemplo, (graficaLongMayorHuecoBinario 200) dibuja

    ![Huecos_binarios_200.png](Ex_20180207-Huecos_binarios_200.png)

-}

import           Graphics.Gnuplot.Simple

longMayorHuecoBinario        :: Int -> Int
longMayorHuecoBinario = maximum . huecosBinarios 0 []

huecosBinarios :: Int -> [Int] -> Int -> [Int]
huecosBinarios r xs 0 = r:xs
huecosBinarios r xs n | even n = huecosBinarios (r+1) xs (n `div` 2)
                      | otherwise = huecosBinarios 0 (r:xs) ((n-1) `div` 2)


graficaLongMayorHuecoBinario :: Int -> IO ()
graficaLongMayorHuecoBinario n =
  plotList [ Key Nothing
           , Title ("graficaLongMayorHuecoBinario " ++ show n)
--           , PNG ("Huecos_binarios_" ++ show n ++ ".png")
           ]
           [longMayorHuecoBinario k | k <- [0..n-1]]
