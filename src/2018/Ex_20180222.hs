{-# LANGUAGE UnicodeSyntax #-}

module Ex_20180222 where

{-

Menor potencia de 2 que comienza por n
======================================

Definir las funciones

   menorPotencia            :: Integer -> (Integer,Integer)
   graficaMenoresExponentes :: Integer -> IO ()

tales que

    (menorPotencia n) es el par (k,m) donde m es la menor potencia de 2 que empieza por n y k es su exponentes (es decir, 2^k = m). Por ejemplo,

     menorPotencia 3             ==  (5,32)
     menorPotencia 7             ==  (46,70368744177664)
     fst (menorPotencia 982)     ==  3973
     fst (menorPotencia 32627)   ==  28557
     fst (menorPotencia 158426)  ==  40000

    (graficaMenoresExponentes n) dibuja la gráfica de los exponentes de 2 en las menores potencias de los n primeros números enteros positivos. Por ejemplo, (graficaMenoresExponentes 200) dibuja

  ![Menor_potencia_de_2_que_comienza_por_n](Ex_20180222-Menor_potencia_de_2_que_comienza_por_n.png)

-}

import           Data.List               (isPrefixOf)
import           Graphics.Gnuplot.Simple


esPrefijoDe :: Integer -> Integer -> Bool
esPrefijoDe n m = show n `isPrefixOf` show m

menorPotencia :: Integer -> (Integer,Integer)
menorPotencia n = head [(k,m) | k <- [0..]
                              , let m = 2^k
                              , n `esPrefijoDe` m]

menorPotencia2 :: Integer -> (Integer,Integer)
menorPotencia2 n = until ((n `esPrefijoDe`) . snd)
                         (\(k,m) -> (k+1,m*2)) (0,1)

menorPotencia3 :: Integer -> (Integer,Integer)
menorPotencia3 n = head [(k,m) | (k,m) <- zip [0..] (iterate (*2) 1)
                               , n `esPrefijoDe` m]

menorPotencia4 :: Integer -> (Integer,Integer)
menorPotencia4 n = head $
  filter ((n `esPrefijoDe`) . snd)
         (zip [0..] (iterate (*2) 1))



graficaMenoresExponentes :: Integer -> IO ()
graficaMenoresExponentes n =
  plotList [ Key Nothing ]
           (map (fst.menorPotencia) [1..n])
