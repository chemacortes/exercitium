{-# LANGUAGE UnicodeSyntax #-}

module Ex_20180123 where

{-

Sumas parciales de Juzuk
========================

En 1939 Dov Juzuk extendió el método de Nicómaco del cálculo de los cubos. La extensión se basaba en los siguientes pasos:

    se comienza con la lista de todos los enteros positivos

     [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, ...

    se agrupan tomando el primer elemento, los dos siguientes, los tres
    siguientes, etc.

     [[1], [2, 3], [4, 5, 6], [7, 8, 9, 10], [11, 12, 13, 14, 15], ...

    se seleccionan los elementos en posiciones pares

     [[1],         [4, 5, 6],                [11, 12, 13, 14, 15], ...

    se suman los elementos de cada grupo

     [1,           15,                       65,                   ...

    se calculan las sumas acumuladas

     [1,           16,                       81,                   ...

Las sumas obtenidas son las cuantas potencias de los números enteros positivos.

Definir las funciones

   listasParcialesJuzuk :: [a] -> [[a]]
   sumasParcialesJuzuk  :: [Integer] -> [Integer]

tal que

    (listasParcialesJuzuk xs) es lalista de ls listas parciales de Juzuk; es decir, la selección de los elementos en posiciones pares de la agrupación de los elementos de xs tomando el primer elemento, los dos siguientes, los tres siguientes, etc. Por ejemplo,

     λ> take 4 (listasParcialesJuzuk [1..])
     [[1],[4,5,6],[11,12,13,14,15],[22,23,24,25,26,27,28]]
     λ> take 4 (listasParcialesJuzuk [1,3..])
     [[1],[7,9,11],[21,23,25,27,29],[43,45,47,49,51,53,55]]

    (sumasParcialesJuzuk xs) es la lista de las sumas acumuladas de los elementos de las listas de Juzuk generadas por xs. Por ejemplo,

     take 4 (sumasParcialesJuzuk [1..])  ==  [1,16,81,256]
     take 4 (sumasParcialesJuzuk [1,3..])  ==  [1,28,153,496]

Comprobar con QuickChek que, para todo entero positivo n,

    el elemento de (sumasParcialesJuzuk [1..]) en la posición (n-1) es n^4.
    el elemento de (sumasParcialesJuzuk [1,3..]) en la posición (n-1) es n^2*(2*n^2 - 1).
    el elemento de (sumasParcialesJuzuk [1,5..]) en la posición (n-1) es 4*n^4-3*n^2.
    el elemento de (sumasParcialesJuzuk [2,3..]) en la posición (n-1) es n^2*(n^2+1).


-}

import           Data.List
import           Test.QuickCheck

listasParcialesJuzuk :: [a] -> [[a]]
listasParcialesJuzuk xs = aux xs [1..]
  where
    aux xs (k:p:ks) = take k xs : aux (drop (k+p) xs) ks

sumasParcialesJuzuk  :: [Integer] -> [Integer]
sumasParcialesJuzuk = scanl1 (+) . map sum . listasParcialesJuzuk

sumasParcialesJuzuk2  :: [Integer] -> [Integer]
sumasParcialesJuzuk2 = scanl (\acc xs -> acc + sum xs) 0 . listasParcialesJuzuk



prop_sumasParcialesJuzuk1 :: Positive Integer -> Bool
prop_sumasParcialesJuzuk1 (Positive n) =
  genericIndex (sumasParcialesJuzuk [1..]) (n-1) == n^4

prop_sumasParcialesJuzuk2 :: Positive Integer -> Bool
prop_sumasParcialesJuzuk2 (Positive n) =
  genericIndex (sumasParcialesJuzuk [1,3..]) (n-1) == n^2*(2*n^2-1)

prop_sumasParcialesJuzuk3 :: Positive Integer -> Bool
prop_sumasParcialesJuzuk3 (Positive n) =
  genericIndex (sumasParcialesJuzuk [1,5..]) (n-1) == 4*n^4-3*n^2

prop_sumasParcialesJuzuk4 :: Positive Integer -> Bool
prop_sumasParcialesJuzuk4 (Positive n) =
  genericIndex (sumasParcialesJuzuk [2,3..]) (n-1) == n^2*(n^2+1)
