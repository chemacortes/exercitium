{-# LANGUAGE UnicodeSyntax #-}

module Ex_20180126 where

{-

Relación definida por un árbol
==============================

Los árboles binarios con valores en las hojas y en los nodos se definen por

   data Arbol a = H a
                | N a (Arbol a) (Arbol a)
     deriving (Eq, Show)

Por ejemplo, el árbol

         10
        /  \
       /    \
      8      2
     / \    / \
    3   5  2   0

se pueden representar por

   ejArbol :: Arbol Int
   ejArbol = N 10 (N 8 (H 3) (H 5))
                  (N 2 (H 2) (H 0))

Un árbol binario define una relación binaria donde un elemento `x` está relacionado con `y` si `x` es el padre de `y`. Por ejemplo, la relación definida por el árbol anterior es [(10,8),(8,3),(8,5),(10,2),(2,2),(2,0)].

Definir la función

   relacionDelArbol :: Arbol a -> [(a,a)]

tal que (relacionDelArbol a) es la relación binaria definida por el árbol a. Por ejemplo,

   λ> relacionDelArbol ejArbol
   [(10,8),(8,3),(8,5),(10,2),(2,2),(2,0)]
   λ> relacionDelArbol (N 10 (H 8) (N 2 (H 2) (H 0)))
   [(10,8),(10,2),(2,2),(2,0)]
   λ> relacionDelArbol (N 10 (N 8 (H 3) (H 5)) (H 2))
   [(10,8),(8,3),(8,5),(10,2)]
   λ> relacionDelArbol (H 10)
   []

-}

data Arbol a = H a
             | N a (Arbol a) (Arbol a)
  deriving (Eq, Show)


ejArbol :: Arbol Int
ejArbol = N 10 (N 8 (H 3) (H 5))
               (N 2 (H 2) (H 0))



relacionDelArbol :: Arbol a -> [(a,a)]
relacionDelArbol (H _) = []
relacionDelArbol (N a i d) =  (a, raiz i) : relacionDelArbol i
                           ++ (a, raiz d) : relacionDelArbol d
  where
    raiz (H x)     = x
    raiz (N x _ _) = x
