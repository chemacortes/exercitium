{-# LANGUAGE UnicodeSyntax #-}

module Ex_20180215 where

{-

Números que no son cuadrados
============================

Definir las funciones

  noCuadrados :: [Integer]
  graficaNoCuadrados :: Integer -> IO ()

tales que

  noCuadrados es la lista de los números naturales que no son cuadrados. Por ejemplo,

    λ> take 25 noCuadrados
    [2,3,5,6,7,8,10,11,12,13,14,15,17,18,19,20,21,22,23,24,26,27,28,29,30]

  (graficaNoCuadrados n) dibuja las diferencias entre los n primeros elementos de noCuadrados y sus posiciones. Por ejemplo, (graficaNoCuadrados 300) dibuja

  ![Numeros_que_no_son_cuadrados_300](Ex_20180215-Numeros_que_no_son_cuadrados_300.png)

  (graficaNoCuadrados 3000) dibuja

  ![Numeros_que_no_son_cuadrados_3000](Ex_20180215-Numeros_que_no_son_cuadrados_3000.png)

  (graficaNoCuadrados 30000) dibuja

  ![Numeros_que_no_son_cuadrados_30000](Ex_20180215-Numeros_que_no_son_cuadrados_30000.png)

Comprobar con QuickCheck que el término de noCuadrados en la posición n-1 es (n + floor(1/2 + sqrt(n))).

-}


import           Data.List
import           Graphics.Gnuplot.Simple
import           Test.QuickCheck


noCuadrados :: [Integer]
noCuadrados = mezcla [2..] [x^2 | x <- [2..]]
  where
    mezcla (x:xs) (y:ys) | x == y    = mezcla xs ys
                         | otherwise = x : mezcla xs (y:ys)

noCuadrados2 :: [Integer]
noCuadrados2 = concatMap entreCuadrados [2..]
  where
    entreCuadrados n = [(n-1)^2+1..n^2-1]

noCuadrados3 :: [Integer]
noCuadrados3 = aux 2 [1..]
  where aux n (_:xs) = ys ++ aux (n+2) zs
          where (ys,zs) = splitAt n xs

graficaNoCuadrados :: Integer -> IO ()
graficaNoCuadrados n =
  plotList [Key Nothing]
    (zipWith (-) (genericTake n noCuadrados) [0..])

prop_noCuadrados :: Positive Integer -> Bool
prop_noCuadrados (Positive n) =
  genericIndex noCuadrados (n-1)  == (n + floor (1/2 + sqrt(fromIntegral n)))
