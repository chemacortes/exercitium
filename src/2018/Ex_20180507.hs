{-# LANGUAGE UnicodeSyntax #-}

module Ex_20180507 where

{-

Número de triangulaciones de un polígono
========================================

Una triangulación de un polígono es una división del área en un conjunto de triángulos, de forma que la unión de todos ellos es igual al polígono original, y cualquier par de triángulos es disjunto o comparte únicamente un vértice o un lado. En el caso de polígonos convexos, la cantidad de triangulaciones posibles depende únicamente del número de vértices del polígono.

Si llamamos T(n) al número de triangulaciones de un polígono de n vértices, se verifica la siguiente relación de recurrencia:

    T(2) = 1
    T(n) = T(2)*T(n-1) + T(3)*T(n-2) + ... + T(n-1)*T(2)

Definir la función

   numeroTriangulaciones :: Integer -> Integer

tal que (numeroTriangulaciones n) es el número de triangulaciones de un polígono convexo de n vértices. Por ejemplo,

   numeroTriangulaciones 3  == 1
   numeroTriangulaciones 5  == 5
   numeroTriangulaciones 6  == 14
   numeroTriangulaciones 7  == 42
   numeroTriangulaciones 50 == 131327898242169365477991900
   length (show (numeroTriangulaciones   800)) ==  476
   length (show (numeroTriangulaciones  1000)) ==  597
   length (show (numeroTriangulaciones 10000)) == 6014

-}


-- FIXME: falta cotejo con la solución oficial

import           Data.List                      ( genericIndex )


numeroTriangulaciones :: Integer -> Integer
numeroTriangulaciones = numeroTriangulaciones1

numeroTriangulaciones1 :: Integer -> Integer
numeroTriangulaciones1 2 = 1
numeroTriangulaciones1 n = sum
  [ numeroTriangulaciones1 i * numeroTriangulaciones1 (n - i + 1)
  | i <- [2 .. n - 1]
  ]


-- Versión memorizada

mT :: [Integer]
mT = 0 : 0 : map numeroTriangulaciones2 [2 ..]

numeroTriangulaciones2 :: Integer -> Integer
numeroTriangulaciones2 2 = 1
numeroTriangulaciones2 n =
  sum [ genericIndex mT i * genericIndex mT (n - i + 1) | i <- [2 .. n - 1] ]

