{-# LANGUAGE UnicodeSyntax #-}

module Ex_20180109 where

{-

Enumeración de los números enteros
==================================

Definir la sucesión

   enteros :: [Int]

tal que sus elementos son los números enteros comenzando en el 0 e intercalando los positivos y los negativos. Por ejemplo,

   ghci> take 23 enteros
   [0,1,-1,2,-2,3,-3,4,-4,5,-5,6,-6,7,-7,8,-8,9,-9,10,-10,11,-11]

Comprobar con QuickCheck que el n-ésimo término de la sucesión es
(1-(2*n+1)*(-1)^n)/4.

Nota. En la comprobación usar

   quickCheckWith (stdArgs {maxSize=7}) prop_enteros

-}

import           Control.Applicative ((<**>))
import           Test.QuickCheck

enteros :: [Int]
enteros = enteros3

enteros1 :: [Int]
enteros1 = concat [[x,y] | (x,y) <- zip [0,-1..] [1..]]

enteros2 :: [Int]
enteros2 = 0 : concat [[x,-x] | x <- [1..]]

enteros3 :: [Int]
enteros3 = 0 : concatMap (\x -> [x,-x]) [1..]

-- Soluciones oficiales
-- 4ª definición
enteros4 :: [Int]
enteros4 = iterate (\i -> if i > 0 then -i else 1-i) 0

-- 5ª definición
enteros5 :: [Int]
enteros5 = 0 : [f x | x <- [1..], f <- [id, negate]]

-- 6ª definición
enteros6 :: [Int]
enteros6 = 0 : ([1..] <**> [id, negate])

prop_enteros :: Positive Int -> Bool
prop_enteros (Positive n) = enteros!!n == (1-(2*n+1)*(-1)^n) `div` 4
