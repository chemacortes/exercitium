{-# LANGUAGE UnicodeSyntax #-}

module Ex_20180501 where

{-

Polinomio digital
=================

Definir la función

   polinomioDigital :: Int -> Polinomio Int

tal que (polinomioDigital n) es el polinomio cuyos coeficientes son los dígitos de n. Por ejemplo,

   λ> polinomioDigital 5703
   5*x^3 + 7*x^2 + 3

Nota: Este ejercicio debe realizarse usando únicamente las funciones de la librería I1M.Pol que se encuentra aquí y se describe aquí.

[1]: http://www.cs.us.es/~jalonso/cursos/i1m/codigos/
[2]: http://www.cs.us.es/~jalonso/cursos/i1m/doc/Tipos_abstractos_de_datos.html#el-tad-de-los-polinomios-i1m.pol

-}

import I1M.Pol

polinomioDigital1 :: Int -> Polinomio Int
polinomioDigital1 n = foldr (\(r,c) res -> consPol r c res) polCero zs
  where
    digitos1 x = [read [c] | c <- show x]
    zs = zip [0..] (reverse (digitos1 n))


polinomioDigital2 :: Int -> Polinomio Int
polinomioDigital2 n = foldr ($) polCero fs
  where
    digitos1 = [read [c] | c <- show n]
    fs = zipWith consPol [0..] (reverse digitos1)

-- Solución oficial:

polinomioDigital :: Int -> Polinomio Int
polinomioDigital = creaPolDensa . digitos

-- (digitos n) es la lista de las dígitos de n. Por ejemplo,
--    dígitos 142857  ==  [1,4,2,8,5,7]
digitos :: Int -> [Int]
digitos n = [read [x]| x <- show n]

-- (creaPolDensa xs) es el polinomio cuya representación densa es
-- xs. Por ejemplo,
--    creaPolDensa [7,0,0,4,0,3]  ==  7*x^5 + 4*x^2 + 3
creaPolDensa :: (Num a, Eq a) => [a] -> Polinomio a
creaPolDensa []     = polCero
creaPolDensa (x:xs) = consPol (length xs) x (creaPolDensa xs)