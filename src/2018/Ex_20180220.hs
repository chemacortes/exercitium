{-# LANGUAGE UnicodeSyntax #-}

module Ex_20180220 where

{-

Representación reducida de matrices dispersas
=============================================

Una representación reducida de una matriz dispersa es una lista de listas donde cada una de las listas representa una fila de la matriz mediante listas de pares correspondientes a las snúmeros de columnas con valores no nulos de la matriz. Por ejemplo, la representacioń reducida de la matriz

   ( 0 0 4 )
   ( 0 5 0 )
   ( 0 0 0 )

es [[(3,4)],[(2,5)],[]].

Definir la función

   reducida :: (Num a, Eq a) => Matrix a -> [[(Int,a)]]

tal que (reducida p) es la representación reducida de la matriz p. Por ejemplo,

   reducida (fromList 3 3 [0,0,4,0,5,0,0,0,0])  ==  [[(3,4)],[(2,5)],[]]
   reducida (identity 3)  ==  [[(1,1)],[(2,1)],[(3,1)]]
   reducida (zero 9 3)    ==  [[],[],[],[],[],[],[],[],[]]
   reducida (zero 9 4)    ==  [[],[],[],[],[],[],[],[],[]]

-}

import           Data.Matrix


reducida :: (Num a, Eq a) => Matrix a -> [[(Int,a)]]
reducida = map aux . toLists
  where
    aux xs = [(i,x) | (i,x) <- zip [1..] xs, x /= 0]
