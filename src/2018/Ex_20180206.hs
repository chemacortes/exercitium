{-# LANGUAGE UnicodeSyntax #-}

module Ex_20180206 where

{-

División equitativa
===================

Definir la función

   divisionEquitativa :: [Int] -> Maybe ([Int],[Int])

tal que (divisionEquitativa xs) determina si la lista de números enteros positivos xs se puede dividir en dos partes (sin reordenar sus elementos) con la misma suma. Si es posible, su valor es el par formado por las dos partes. Si no lo es, su valor es Nothing. Por ejemplo,

   divisionEquitativa [1,2,3,4,5,15]  ==  Just ([1,2,3,4,5],[15])
   divisionEquitativa [15,1,2,3,4,5]  ==  Just ([15],[1,2,3,4,5])
   divisionEquitativa [1,2,3,4,7,15]  ==  Nothing
   divisionEquitativa [1,2,3,4,15,5]  ==  Nothing



-}

import           Control.Monad (guard)
import           Data.List     (find, inits, tails)
import           Data.Maybe    (listToMaybe)


divisionEquitativa :: [Int] -> Maybe ([Int],[Int])
divisionEquitativa xs | odd s = Nothing
                      | otherwise = find (\(ys,_) -> sum ys == s `div` 2) (zip (inits xs) (tails xs))
  where
    s = sum xs

divisionEquitativa2 :: [Int] -> Maybe ([Int],[Int])
divisionEquitativa2 = find ((==) . sum . fst <*> sum . snd)
                    . (zip . inits <*> tails)

divisionEquitativa3 :: [Int] -> Maybe ([Int],[Int])
divisionEquitativa3 xs =
  listToMaybe
    [(ys, zs) | (ys,zs) <- zip (inits xs) (tails xs)
              , sum ys == sum zs ]
