{-# LANGUAGE UnicodeSyntax #-}

module Ex_20180101 where

{-

De hexadecimal a decimal
========================

El sistema hexadecimal http://bit.ly/2Em3aaQ es el sistema de numeración posicional que tiene como base el 16.

En principio, dado que el sistema usual de numeración es de base decimal y, por ello, sólo se dispone de diez dígitos, se adoptó la convención de usar las seis primeras letras del alfabeto latino para suplir los dígitos que nos faltan. El conjunto de símbolos es el siguiente: {0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F}. En ocasiones se emplean letras minúsculas en lugar de mayúsculas. Se debe notar que A = 10, B = 11, C = 12, D = 13, E = 14 y F = 15.

Como en cualquier sistema de numeración posicional, el valor numérico de cada dígito es alterado dependiendo de su posición en la cadena de dígitos, quedando multiplicado por una cierta potencia de la base del sistema, que en este caso es 16. Por ejemplo, el valor decimal del número hexadecimal 3E0A es

     3×16^3 + E×16^2 + 0×16^1 + A×16^0
   = 3×4096 + 14×256 + 0×16   + 10×1
   = 15882

Definir la función

   hexAdec :: String -> Int

tal que (hexAdec cs) es el valor decimal del número hexadecimal representado meiante la cadena cs. Por ejemplo,

   hexAdec "3E0A"   == 15882
   hexAdec "ABCDEF" == 11259375
   hexAdec "FEDCBA" == 16702650

-}

import           Data.List  (elemIndex)
import           Data.Maybe (fromJust)

carAdec :: Char -> Int
carAdec = fromJust . flip elemIndex "0123456789ABCDEF"


hexAdec :: String -> Int
hexAdec = foldl (\acc c -> acc * 16 + carAdec c) 0


hexAdec3 :: String -> Int
hexAdec3 = read . ("0x" ++)
