{-# LANGUAGE UnicodeSyntax #-}

module Ex_20180118b where

  import Data.List (genericLength, group)
  import Data.Numbers.Primes
  import Graphics.Gnuplot.Simple

  -- Definición de escaladaPrima
  escaladaPrima :: Integer -> [Integer]
  escaladaPrima n
    | isPrime n = [n]
    | otherwise = n : escaladaPrima (siguiente n)

  -- (siguiente n) es el siguiente de n en la escalada hacia un primo. Por
  -- ejemplo,
  --    siguiente 1400  ==  23527
  siguiente :: Integer -> Integer
  siguiente = paresAnumero . factorizacion

  -- (factorizacion n) es la factorizacion prima de n. Por ejemplo,
  --    factorizacion 1400  ==  [(2,3),(5,2),(7,1)]
  factorizacion :: Integer -> [(Integer,Integer)]
  factorizacion n =
    [(x,genericLength xs) | xs@(x:_) <- group (primeFactors n)]

  -- (paAnumero p) es el número obtenido pegando las dos componentes de p
  -- (si la segunda es mayor que 1) o sólo la primera componente, en caso
  -- contrario. Por ejemplo,
  --    parAnumero (7,1)  ==  7
  --    parAnumero (23,5)  ==  235
  parAnumero :: (Integer,Integer) -> Integer
  parAnumero (n,1) = n
  parAnumero (n,k) = read (show n ++ show k)

  -- (paresA numeros ps) es el número obtenido aplicando parAnumero a cada
  -- uno de los pares de ps. Por ejemplo,
  --    paresAnumero [(2,3),(5,2),(7,1)]  ==  23527
  paresAnumero :: [(Integer,Integer)] -> Integer
  paresAnumero = read . concatMap (show . parAnumero)

  -- Definición de longitudEscaladaPrima
  longitudEscaladaPrima :: Integer -> Integer
  longitudEscaladaPrima n
    | isPrime n = 1
    | otherwise = 1 + longitudEscaladaPrima (siguiente n)

  -- Definición de longitudEscaladaPrimaAcotada
  longitudEscaladaPrimaAcotada :: Integer -> Integer -> Integer
  longitudEscaladaPrimaAcotada _ 0 = 0
  longitudEscaladaPrimaAcotada n k
    | isPrime n = 1
    | otherwise = 1 + longitudEscaladaPrimaAcotada (siguiente n) (k-1)

  -- Definición de graficaEscalada
  graficaEscalada :: Integer -> Integer -> IO ()
  graficaEscalada n k =
    plotList [ Key Nothing
             , PNG "Escalada_hasta_un_primo.png"
             , Title ("graficaEscalada " ++ show n ++ " " ++ show k)
             , XLabel "Numeros"
             , YLabel "Longitud de escalada"
             ]
             [(x,longitudEscaladaPrimaAcotada x k) | x <- [2..n]]
