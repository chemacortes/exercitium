{-# LANGUAGE UnicodeSyntax #-}

module Ex_20180320 where

{-

Particiones primas
==================

Una partición prima de un número natural n es un conjunto de primos cuya suma es n. Por ejemplo, el número 7 tiene 3 particiones primas ya que

   7 = 7 = 5 + 2 = 3 + 2 + 2

Definir la función

   particiones :: Int -> [[Int]]

tal que (particiones n) es el conjunto de las particiones primas de n. Por ejemplo,

   particiones 7             ==  [[7],[5,2],[3,2,2]]
   particiones 8             ==  [[5,3],[3,3,2],[2,2,2,2]]
   particiones 9             ==  [[7,2],[5,2,2],[3,3,3],[3,2,2,2]]
   length (particiones 100)  ==  40899

-}

import Data.Numbers.Primes
import Data.Array          (Array, (!), array)


particiones :: Int -> [[Int]]
particiones = particiones1

particiones1 :: Int -> [[Int]]
particiones1 0 = [[]]
particiones1 n = [p:xs | p <- ps
                      , xs <- particiones (n-p)
                      , [p] >= take 1 xs]
  where
    ps = (reverse . takeWhile (<=n)) primes


particiones2 :: Int -> [[Int]]
particiones2 n = aux n ps
  where
    ps = (reverse . takeWhile (<=n)) primes


aux :: Int -> [Int] -> [[Int]]
aux 0 _  = [[]]
aux 1 _  = []
aux n ps = [ x:xs | x <- ps
                  , xs <- aux (n-x) (f x)]
  where
    f a = filter (min a (n-a) >=) ps


-- solución oficial (con programación dinámica)
-- ============================================

particiones3 :: Int -> [[Int]]
particiones3 n = (vectorParticiones n) ! n

-- (vectorParticiones n) es el vector con índices de 0 a n tal que el
-- valor del índice k es la lista de las particiones primas de k. Por
-- ejemplo,
--    λ> mapM_ print (elems (vectorParticiones 9))
--    [[]]
--    []
--    [[2]]
--    [[3]]
--    [[2,2]]
--    [[5],[3,2]]
--    [[3,3],[2,2,2]]
--    [[7],[5,2],[3,2,2]]
--    [[5,3],[3,3,2],[2,2,2,2]]
--    [[7,2],[5,2,2],[3,3,3],[3,2,2,2]]
--    λ> elems (vectorParticiones 9) == map particiones1 [0..9]
--    True
vectorParticiones :: Int -> Array Int [[Int]]
vectorParticiones n = v where
  v = array (0,n) [(i,f i) | i <- [0..n]]
    where f 0 = [[]]
          f m = [x:y | x <- xs,
                       y <- v ! (m-x),
                       [x] >= take 1 y]
            where xs = reverse (takeWhile (<= m) primes)
