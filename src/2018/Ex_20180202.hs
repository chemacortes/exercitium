{-# LANGUAGE UnicodeSyntax #-}

module Ex_20180202 where

{-

Dígitos iniciales
==================

Definir las funciones

   digitosIniciales        :: [Int]
   graficaDigitosIniciales :: Int -> IO ()

tales que

    digitosIniciales es la lista de los dígitos iniciales de los números naturales. Por ejemplo,

     λ> take 100 digitosIniciales
     [0,1,2,3,4,5,6,7,8,9,1,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,2,
      3,3,3,3,3,3,3,3,3,3,4,4,4,4,4,4,4,4,4,4,5,5,5,5,5,5,5,5,5,5,
      6,6,6,6,6,6,6,6,6,6,7,7,7,7,7,7,7,7,7,7,8,8,8,8,8,8,8,8,8,8,
      9,9,9,9,9,9,9,9,9,9]

    (graficaDigitosIniciales n) dibuja la gráfica de los primeros n términos de la sucesión digitosIniciales. Por ejemplo, (graficaDigitosIniciales 100) dibuja

    ![Digitos_iniciales_100](Ex_20180202-Digitos_iniciales_100.png)

    y (graficaDigitosIniciales 1000) dibuja

    ![Digitos_iniciales_1000](Ex_20180202-Digitos_iniciales_1000.png)

-}

import           Data.Char               (digitToInt)
import           Graphics.Gnuplot.Simple


digitosIniciales :: [Int]
digitosIniciales = digitosIniciales5


digitosIniciales1 :: [Int]
digitosIniciales1 = map (digitToInt.head.show) [0..]

digitosIniciales2 :: [Int]
digitosIniciales2 = 0 : concat [replicate (10^k) i | k <- [0..], i <- [1..9]]

digitosIniciales3 :: [Int]
digitosIniciales3 = 0 : concat (replicate . (10^) <$> [0..] <*> [1..9])

digitosIniciales4 :: [Int]
digitosIniciales4 = 0 : concat [replicate k n | k <- iterate (10*) 1, n <- [1..9]]

digitosIniciales5 :: [Int]
digitosIniciales5 = 0 : concat (replicate <$> iterate (10*) 1 <*> [1..9])

digitosIniciales6 :: [Int]
digitosIniciales6 = map (fromInteger . until (< 10) (`div` 10)) [0..]

digitosIniciales7 :: [Int]
digitosIniciales7 =
  0 : concat [replicate k x | k <- [10^n | n <- [0..]]
                            , x <- [1..9]]



graficaDigitosIniciales :: Int -> IO ()
graficaDigitosIniciales n =
  plotList [Key Nothing
          -- , PNG ("digitosIniciales"  ++ show n ++ ".png")
           , Title ("graficaDigitosiniciales " ++ show n)
           ]
           (take n digitosIniciales)
