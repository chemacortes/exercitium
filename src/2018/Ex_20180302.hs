{-# LANGUAGE UnicodeSyntax #-}

module Ex_20180302 where

{-

Matrices centro simétricas
==========================

Una matriz centro simétrica es una matriz cuadrada que es simétrica respecto de su centro. Por ejemplo, de las siguientes matrices, las dos primeras son simétricas y las otras no lo son

   (1 2)   (1 2 3)   (1 2 3)   (1 2 3)
   (2 1)   (4 5 4)   (4 5 4)   (4 5 4)
           (3 2 1)   (3 2 2)

Definir la función

   esCentroSimetrica :: Eq t => Array (Int,Int) t -> Bool

tal que (esCentroSimetrica a) se verifica si la matriz a es centro simétrica. Por ejemplo,

   λ> esCentroSimetrica (listArray ((1,1),(2,2)) [1,2, 2,1])
   True
   λ> esCentroSimetrica (listArray ((1,1),(3,3)) [1,2,3, 4,5,4, 3,2,1])
   True
   λ> esCentroSimetrica (listArray ((1,1),(3,3)) [1,2,3, 4,5,4, 3,2,2])
   False
   λ> esCentroSimetrica (listArray ((1,1),(2,3)) [1,2,3, 4,5,4])
   False

-}

import           Data.Array


esCentroSimetrica :: Eq t => Array (Int,Int) t -> Bool
esCentroSimetrica m = esCuadrada m
                    && ((==) <$> id <*> reverse) (elems m)


esCuadrada :: Array (Int, Int) t -> Bool
esCuadrada m = nfilas-i == ncols-j
  where
    ((i,j),(nfilas,ncols)) = bounds m
