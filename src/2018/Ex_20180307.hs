{-# LANGUAGE UnicodeSyntax #-}

module Ex_20180307 where

{-

Suma de las sumas de los cuadrados de los divisores
===================================================

La suma de las sumas de los cuadrados de los divisores de los 6 primeros números enteros positivos es

     1² + (1²+2²) + (1²+3²) + (1²+2²+4²) + (1²+5²) + (1²+2²+3²+6²)
   = 1  + 5       + 10      + 21         + 26      + 50
   = 113

Definir la función

   sumaSumasCuadradosDivisores :: Integer -> Integer

tal que (sumaSumasCuadradosDivisores n) es la suma de las sumas de los cuadrados de los divisores de los n primeros números enteros positivos. Por ejemplo,

   sumaSumasCuadradosDivisores 6       ==  113
   sumaSumasCuadradosDivisores (10^6)  ==  400686363385965077

-}

-- 1² + (1²+2²) + (1²+3²) + (1²+2²+4²) + (1²+5²) + (1²+2²+3²+6²)
-- 1² +  1²     +  1²     +  1²        +  1²        1²
--          2²            +     2²               +     2²
--                    3²                         +        3²
--                                 4²
--                                           5²
--                                                           6²

sumaSumasCuadradosDivisores :: Integer -> Integer
sumaSumasCuadradosDivisores n = sum[(div n x)*(x^2) | x <- [1..n]]
