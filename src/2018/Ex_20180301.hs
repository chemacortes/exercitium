{-# LANGUAGE UnicodeSyntax #-}

module Ex_20180301 where

{-

Nodos con máxima suma de hijos
==============================

Los árboles se pueden representar mediante el siguiente tipo de datos

   data Arbol a = N a [Arbol a]
                  deriving Show

Por ejemplo, los árboles

     1               3
    / \             /|\
   2   3           / | \
       |          5  4  7
       4          |    /|\
                  6   2 8 6

se representan por

   ej1, ej2 :: Arbol Int
   ej1 = N 1 [N 2 [], N 3 [N 4 []]]
   ej2 = N 3 [N 5 [N 6 []],
              N 4 [],
              N 7 [N 2 [], N 8 [], N 6 []]]

Definir la función

   nodosSumaMaxima :: (Num t, Ord t) => Arbol t -> [t]

tal que (nodosSumaMaxima a) es la lista de los nodos del árbol `a` cuyos hijos tienen máxima suma. Por ejemplo,

   nodosSumaMaxima ej1  ==  [1]
   nodosSumaMaxima ej2  ==  [7,3]

-}

data Arbol a = N a [Arbol a]
               deriving Show


ej1, ej2 :: Arbol Int
ej1 = N 1 [N 2 [], N 3 [N 4 []]]
ej2 = N 3 [N 5 [N 6 []],
          N 4 [],
          N 7 [N 2 [], N 8 [], N 6 []]]

nodosSumaMaxima :: (Num t, Ord t) => Arbol t -> [t]
nodosSumaMaxima = snd . foldl aux (0,[]) . sumasNodos
  where
    aux (m,res) (x, s) | m > s      = (m, res)
                       | m == s     = (m, x:res)
                       | otherwise  = (s, [x])


-- Lista de nodos y la suma de sus hijos
sumasNodos :: (Num a, Ord a) => Arbol a -> [(a,a)]
sumasNodos (N x []) = [(x, 0)]
sumasNodos (N x xs) = (x, sum [y | (N y _) <- xs]) : concatMap sumasNodos xs
