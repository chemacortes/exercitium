{-# LANGUAGE UnicodeSyntax #-}

module Ex_20180119 where

{-

Terna pitagórica a partir de un lado
====================================

Una terna pitagórica con primer lado x es una terna (x,y,z) tal que x^2 + y^2 = z^2. Por ejemplo, las ternas pitagóricas con primer lado 16 son (16,12,20), (16,30,34) y (16,63,65).

Definir las funciones

   ternasPitagoricas      :: Integer -> [(Integer,Integer,Integer)]
   mayorTernaPitagorica   :: Integer -> (Integer,Integer,Integer)
   graficaMayorHipotenusa :: Integer -> IO ()

tales que

    (ternasPitgoricas x) es la lista de las ternas pitagóricas con primer lado x. Por ejemplo,

     ternasPitagoricas 16 == [(16,12,20),(16,30,34),(16,63,65)]
     ternasPitagoricas 20 == [(20,15,25),(20,21,29),(20,48,52),(20,99,101)]
     ternasPitagoricas 25 == [(25,60,65),(25,312,313)]
     ternasPitagoricas 26 == [(26,168,170)]

    (mayorTernaPitagorica x) es la mayor de las ternas pitagóricas con primer lado x. Por ejemplo,

     mayorTernaPitagorica 16     ==  (16,63,65)
     mayorTernaPitagorica 20     ==  (20,99,101)
     mayorTernaPitagorica 25     ==  (25,312,313)
     mayorTernaPitagorica 26     ==  (26,168,170)
     mayorTernaPitagorica 2018   ==  (2018,1018080,1018082)
     mayorTernaPitagorica 2019   ==  (2019,2038180,2038181)

    (graficaMayorHipotenusa n) dibuja la gráfica de las sucesión de las mayores hipotenusas de las ternas pitagóricas con primer lado x, para x entre 3 y n. Por ejemplo, (graficaMayorHipotenusa 100) dibuja

    ![Terna pitagórica a partir de un lado](Ex_20180119-Terna_pitagorica_a_partir_de_un_lado.png)

-}

import           Graphics.Gnuplot.Simple


raizEnesima :: Integer -> Integer -> Integer
raizEnesima x n = round $ fromIntegral x ** (1/fromIntegral n)

raizCuadrada :: Integer -> Integer
raizCuadrada = (`raizEnesima` 2)

ternasPitagoricas :: Integer -> [(Integer, Integer, Integer)]
ternasPitagoricas a = [(a,b,c) | c <- [a+1..(a^2+1) `div` 2]
                               , let b = raizCuadrada (c^2 - a^2)
                               , a^2 + b^2 == c^2]

mayorTernaPitagorica :: Integer -> (Integer,Integer,Integer)
mayorTernaPitagorica a = head [(a,b,c) | b <- [bMax,bMax-1..1]
                                       , let c = raizCuadrada (a^2 + b^2)
                                       , a^2 + b^2 == c^2]
  where
    bMax = a^2 - 1 `div` 2


-- Se supone que x > 2. Se consideran dos casos:
--
-- Primer caso: Supongamos que x es par. Entonces x^2 > 4 y es divisible
-- por 4. Por tanto, existe un y tal que x^2 = 4*y + 4; luego,
--    x^2 + y^2 = 4*y + 4 + y^2
--              = (y + 2)^2
-- La terna es (x,y,y+2) donde y = (x^2 - 4) / 4.
--
-- Segundo caso: Supongamos que x es impar. Entonces x^2 es impar. Por
-- tanto, existe un y tal que x^2 = 2*y + 1; luego,
--    x^2 + y^2 = 2*y + 1 + y^2
--              = (y+1)^2
-- La terna es (x,y,y+1) donde y = (x^2 - 1) / 2.

mayorTernaPitagorica3 :: Integer -> (Integer,Integer,Integer)
mayorTernaPitagorica3 x
  | even x    = (x, y1, y1 + 2)
  | otherwise = (x, y2, y2 + 1)
    where y1 = (x^2 - 4) `div` 4
          y2 = (x^2 - 1) `div` 2

graficaMayorHipotenusa :: Integer -> IO ()
graficaMayorHipotenusa n =
  plotList [
    Title "Grafica Mayor Hipotenusa",
    XLabel "Lado de partida",
    YLabel "Mayor hipotenusa",
    Key Nothing]
    [c | a <- [3..n], (_,_,c) <- [mayorTernaPitagorica a]]
