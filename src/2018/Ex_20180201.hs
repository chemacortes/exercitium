{-# LANGUAGE UnicodeSyntax #-}

module Ex_20180201 where

{-

Exponentes de Hamming
=====================

Los números de Hamming forman una sucesión estrictamente creciente de números que cumplen las siguientes condiciones:

    El número 1 está en la sucesión.
    Si x está en la sucesión, entonces 2x, 3x y 5x también están.
    Ningún otro número está en la sucesión.

Los primeros números de Hamming son 1, 2, 3, 4, 5, 6, 8, 9, 10, 12, 15, 16, …

Los exponentes de un número de Hamming n es una terna (x,y,z) tal que n = 2^x*3^y*5^z. Por ejemplo, los exponentes de 600 son (3,1,2) ya que 600 = 2^x*3^1*5^z.

Definir la sucesión

   sucExponentesHamming :: [(Int,Int,Int)]

cuyos elementos son los exponentes de los números de Hamming. Por ejemplo,

   λ> take 21 sucExponentesHamming
   [(0,0,0),(1,0,0),(0,1,0),(2,0,0),(0,0,1),(1,1,0),(3,0,0),
    (0,2,0),(1,0,1),(2,1,0),(0,1,1),(4,0,0),(1,2,0),(2,0,1),
    (3,1,0),(0,0,2),(0,3,0),(1,1,1),(5,0,0),(2,2,0),(3,0,1)]
   λ> sucExponentesHamming !! (5*10^5)
   (74,82,7)

-}

import           Data.Function (on)
import           Data.List     (minimumBy)

-- Cálculo del número de Hamming a partir de los tres exponentes
hamming :: (Int, Int, Int) -> Integer
hamming (a,b,c) = 2^a + 3^b + 5^c

-- Sucesión de Hamming
sucHamming :: [Integer]
sucHamming = map hamming sucExponentesHamming

-- Sucesión de los exponentes de Hamming
sucExponentesHamming :: [(Int,Int,Int)]
sucExponentesHamming =
  (0,0,0) : mezcla [(x+1,y,z) | (x,y,z) <- sucExponentesHamming]
                   [(x,y+1,z) | (x,y,z) <- sucExponentesHamming]
                   [(x,y,z+1) | (x,y,z) <- sucExponentesHamming]

mezcla :: [(Int,Int,Int)] -> [(Int,Int,Int)] -> [(Int,Int,Int)] -> [(Int,Int,Int)]
mezcla (x:xs) (y:ys) (z:zs) = m : mezcla xs' ys' zs'
  where
    m = minimumBy (compare `on` hamming) [x,y,z]
    xs' = if m==x then xs else x:xs
    ys' = if m==y then ys else y:ys
    zs' = if m==z then zs else z:zs
