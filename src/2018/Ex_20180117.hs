{-# LANGUAGE UnicodeSyntax #-}

module Ex_20180117 where

{-

Números como diferencias de potencias
=====================================

El número 45 se puede escribir de tres formas como diferencia de los cuadrados de dos números naturales:

   45 =  7^2 -  2^2
      =  9^2 -  6^2
      = 23^2 - 22^2

Definir la función

   diferencias :: Integer -> Integer -> [(Integer,Integer)]

tal que (diferencias x n) es la lista de pares tales que la diferencia de sus potencias n-ésima es x. Por ejemplo,

   diferencias 45 2    ==  [(7,2),(9,6),(23,22)]
   diferencias 50 2    ==  []
   diferencias 19 3    ==  [(3,2)]
   diferencias 8 3     ==  [(2,0)]
   diferencias 15 4    ==  [(2,1)]
   diferencias 4035 2  ==  [(142,127),(406,401),(674,671),(2018,2017)]
   head (diferencias 1161480105172255454401 5)  ==  (123456,123455)

-}


raizEnesima :: Integer -> Integer -> Integer
raizEnesima x n = round (fromIntegral x ** (1/fromIntegral n))


diferencias :: Integer -> Integer -> [(Integer,Integer)]
diferencias x n = [(a,b) | a <- [aMin..aMax+1]
                         , let c = a^n - x
                         , let b = raizEnesima c n
                         , b^n == c ]
  where
    aMin = raizEnesima (x-1) n
    aMax = raizEnesima (x `div` 2) (n-1)


diferencias3 :: Integer -> Integer -> [(Integer,Integer)]
diferencias3 x n =
  [(a,b) | a <- [0..x]
         , a^n >= x
         , b <- raizEntera n (a^n - x)]

raizEntera :: Integer -> Integer -> [Integer]
raizEntera n x
  | y^n == x  = [y]
  | otherwise = []
  where y = head [k | k <- [0..], k^n >= x]

diferencias4 :: Integer -> Integer -> [(Integer,Integer)]
diferencias4 x n =
  [(a,b) | a <- [0..x]
         , a^n >= x
         , b <- raizEntera n (a^n - x)]
  where potencias = [(k,k^n) | k <- [0..]]
        raizEntera n x
          | yn == x   = [y]
          | otherwise = []
          where (y,yn) = head [(k,kn) | (k,kn) <- potencias, kn >= x]
