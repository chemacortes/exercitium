{-# LANGUAGE UnicodeSyntax #-}

module Ex_20180305 where

{-

Cruce de listas
===============

Definir la función

   cruce :: Eq a => [a] -> [a] -> [[a]]

tal que (cruce xs ys) es la lista de las listas obtenidas uniendo las listas de xs sin un elemento con las de ys sin un elemento. Por ejemplo,

   ghci> cruce [1,5,3] [2,4]
   [[5,3,4],[5,3,2],[1,3,4],[1,3,2],[1,5,4],[1,5,2]]
   ghci> cruce [1,5,3] [2,4,6]
   [[5,3,4,6],[5,3,2,6],[5,3,2,4],[1,3,4,6],[1,3,2,6],
    [1,3,2,4],[1,5,4,6],[1,5,2,6],[1,5,2,4]]

Comprobar con QuickCheck que el número de elementos de (cruce xs ys) es el producto de los números de elementos de xs y de ys.

-}

import           Data.List       (delete)
import           Test.QuickCheck

cruce :: Eq a => [a] -> [a] -> [[a]]
cruce xs ys = [ delete x xs ++ delete y ys | x <- xs, y <- ys]


prop_cruce :: Eq a => [a] -> [a] -> Bool
prop_cruce xs ys = length (cruce xs ys) == length xs * length ys
