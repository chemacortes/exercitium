{-# LANGUAGE UnicodeSyntax #-}

module Ex_20180504 where

{-

Sucesiones suaves
=================

Una sucesión es suave si valor absoluto de la diferencia de sus términos consecutivos es 1.

Definir la función

   suaves :: Int -> [[Int]]

tal que (suaves n) es la lista de las sucesiones suaves de longitud n cuyo último término es 0. Por ejemplo,

   suaves 2  ==  [[1,0],[-1,0]]
   suaves 3  ==  [[2,1,0],[0,1,0],[0,-1,0],[-2,-1,0]]

-}


suaves :: Int -> [[Int]]
suaves 0 = []
suaves 1 = [[0]]
suaves n = [ (x + i) : xs | xs@(x : _) <- suaves (n - 1), i <- [1, -1] ]
