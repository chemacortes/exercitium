{-# LANGUAGE UnicodeSyntax #-}

module Ex_20180503 where

{-

Máximos de expresiones aritméticas
==================================

Las expresiones aritméticas se pueden definir usando el siguiente tipo de datos

   data Expr = N Int
             | X
             | S Expr Expr
             | R Expr Expr
             | P Expr Expr
             | E Expr Int
             deriving (Eq, Show)

Por ejemplo, la expresión

   3*x - (x+2)^7

se puede definir por

   R (P (N 3) X) (E (S X (N 2)) 7)

Definir la función

   maximo :: Expr -> [Int] -> (Int,[Int])

tal que (maximo e xs) es el par formado por el máximo valor de la expresión e para los puntos de xs y en qué puntos alcanza el máximo. Por ejemplo,

   λ> maximo (E (S (N 10) (P (R (N 1) X) X)) 2) [-3..3]
   (100,[0,1])

-}

import           Data.List                      ( maximum )

data Expr = N Int
  | X
  | S Expr Expr
  | R Expr Expr
  | P Expr Expr
  | E Expr Int
  deriving (Eq, Show)


eval :: Expr -> Int -> Int
eval (N n)   _ = n
eval X       x = x
eval (S a b) x = eval a x + eval b x
eval (R a b) x = eval a x - eval b x
eval (P a b) x = eval a x * eval b x
eval (E a d) x = eval a x ^ d

maximo1 :: Expr -> [Int] -> (Int, [Int])
maximo1 _ [] = undefined
maximo1 e xs = (m, [ x | (x, p) <- zip xs ys, p == m ])
 where
  ys = map (eval e) xs
  m  = maximum ys

maximo :: Expr -> [Int] -> (Int, [Int])
maximo e = foldr aux (minBound :: Int, [])
 where
  aux x (m, ms) | m == v    = (m, x : ms)
                | m < v     = (v, [x])
                | otherwise = (m, ms)
    where v = eval e x
