{-# LANGUAGE UnicodeSyntax #-}

module Ex_20180319 where

{-

La sucesión de Sylvester
========================

La sucesión de Sylvester es la sucesión que comienza en 2 y sus restantes términos se obtienen multiplicando los anteriores y sumándole 1.

Definir las funciones

   sylvester        :: Integer -> Integer
   graficaSylvester :: Integer -> Integer -> IO ()

tales que

    (sylvester n) es el n-ésimo término de la sucesión de Sylvester. Por ejemplo,

     λ> [sylvester n | n <- [0..7]]
     [2,3,7,43,1807,3263443,10650056950807,113423713055421844361000443]
     λ> length (show (sylvester 25))
     6830085

    (graficaSylvester d n) dibuja la gráfica de los `d` últimos dígitos de los `n` primeros términos de la sucesión de Sylvester. Por ejemplo,

        (graficaSylvester 3 30) dibuja
  ![La_sucesion_de_Sylvester_(3,30)](Ex_20180318-La_sucesion_de_Sylvester_330.png)

        (graficaSylvester 4 30) dibuja

  ![La_sucesion_de_Sylvester_(4,30)](Ex_20180318-La_sucesion_de_Sylvester_430.png)

        (graficaSylvester 5 30) dibuja
  ![La_sucesion_de_Sylvester_(5,30)](Ex_20180318-La_sucesion_de_Sylvester_530.png)


-}

import Data.List (genericIndex)
import Graphics.Gnuplot.Simple
import Data.Array              ((!), array)

sylvester :: Integer -> Integer
sylvester = sylvester2

sylvester1 :: Integer -> Integer
sylvester1 0 = 2
sylvester1 n = 1 + product [sylvester1 x | x <- [0..n-1]]

sylvester2 :: Integer -> Integer
sylvester2 = genericIndex xs
  where
    xs = iterate (\x -> x^2 - x + 1) 2

-- solución con programación dinámica
-- ==================================

sylvester3 :: Integer -> Integer
sylvester3 n = v ! n where
  v = array (0,n) [(i,f i) | i <- [0..n]]
  f 0 = 2
  f m = 1 + product [v!k | k <- [0..m-1]]


graficaSylvester :: Integer -> Integer -> IO ()
graficaSylvester d n =
  plotList [ Key Nothing ]
           [sylvester k `mod` (10^d) | k <- [0..n]]
