{-# LANGUAGE UnicodeSyntax #-}

module Ex_20180223 where

{-

Ceros con los n primeros números
================================

Los números del 1 al 3 se pueden escribir de dos formas, con el signo más o menos entre ellos, tales que su suma sea 0:

    1+2-3 = 0
   -1-2+3 = 0

Definir la función

   ceros :: Int -> [[Int]]

tal que (ceros n) son las posibles formas de obtener cero sumando los números del 1 al n, con el signo más o menos entre ellos. Por ejemplo,

   ceros 3           ==  [[1,2,-3],[-1,-2,3]]
   ceros 4           ==  [[1,-2,-3,4],[-1,2,3,-4]]
   ceros 5           ==  []
   length (ceros 7)  ==  8
   take 3 (ceros 7)  ==  [[1,2,-3,4,-5,-6,7],[1,2,-3,-4,5,6,-7],[1,-2,3,4,-5,6,-7]]

-}

ceros :: Int -> [[Int]]
ceros = ceros4

ceros1 :: Int -> [[Int]]
ceros1 n = [xs | xs <- combinaciones [1..n], sum xs == 0]

combinaciones :: [Int] -> [[Int]]
combinaciones [] = [[]]
combinaciones (x:xs) = fmap (x:) xss ++ fmap (negate x:) xss
  where
    xss = combinaciones xs


combinaciones2 :: [Int] -> [[Int]]
combinaciones2 []     = [[]]
combinaciones2 (x:xs) = [f ys | f <- [(x:),(negate x:)], ys <- combinaciones2 xs]

combinaciones3 :: [Int] -> [[Int]]
combinaciones3  = foldr (\x -> zipWith ($) [(x:), (negate x:)]) [[]]


-- solución algo mejor
ceros2 :: Int -> [[Int]]
ceros2 = filter ((==0).sum) . mapM (\x -> [x, -x]) . enumFromTo 1

-- optimización (aunque el resultado no mantiene el orden)
ceros3 :: Int -> [[Int]]
ceros3 = filter ((==1).abs.head)
       . map (\xs -> -sum xs:xs)
       . mapM (\x -> [x, -x])
       . enumFromTo 2

-- optimización ordenada
ceros4 :: Int -> [[Int]]
ceros4 n = filter ((==n).abs.last)
         . map (\xs -> xs ++ [-sum xs])
         $ mapM (\x -> [x, -x]) [1..n-1]

-- solución oficial
ceros5 :: Integer -> [[Integer]]
ceros5 = map fst
       . filter ((==0) . snd)
       . sumas

-- (sumas n) es la lista de las candidatos con los n primeros números y
-- sus sumas. Por ejemplo,
--    λ> sumas 2
--    [([1,2],3),([-1,2],1),([1,-2],-1),([-1,-2],-3)]
--    λ> sumas 3
--    [([1,2,3],6),([-1,2,3],4),([1,-2,3],2),([-1,-2,3],0),([1,2,-3],0),
--     ([-1,2,-3],-2),([1,-2,-3],-4),([-1,-2,-3],-6)]
sumas :: Integer -> [([Integer],Integer)]
sumas = foldr aux [([], 0)]
      . enumFromTo 1
  where
    aux n = concatMap (\(ns', s') -> [(n : ns', s' + n), (-n : ns', s' - n)])
