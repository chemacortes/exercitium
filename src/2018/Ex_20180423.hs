{-# LANGUAGE UnicodeSyntax #-}

module Ex_20180423
where

{-

Subsucesiones crecientes de elementos consecutivos
==================================================

Definir la función

   subsucesiones :: [Integer] -> [[Integer]]

tal que (subsucesiones xs) es la lista de las subsucesiones crecientes de elementos consecutivos de xs. Por ejemplo,

   subsucesiones [1,0,1,2,3,0,4,5]  == [[1],[0,1,2,3],[0,4,5]]
   subsucesiones [5,6,1,3,2,7]      == [[5,6],[1,3],[2,7]]
   subsucesiones [2,3,3,4,5]        == [[2,3],[3,4,5]]
   subsucesiones [7,6,5,4]          == [[7],[6],[5],[4]]

-}

subsucesiones :: [Integer] -> [[Integer]]
subsucesiones []  = []
subsucesiones [x] = [[x]]
subsucesiones (x : y : zs) | x < y     = (x : us) : vss
                           | otherwise = [x] : p
  where p@(us : vss) = subsucesiones (y : zs)
