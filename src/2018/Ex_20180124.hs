{-# LANGUAGE UnicodeSyntax #-}

module Ex_20180124 where

{-

Sucesión de dígitos 0 y 1 alternados
==================

Los primeros términos de la sucesión de los dígitos 0 y 1 alternados son

   0
   1
   10
   101
   1010
   10101
   101010
   1010101
   10101010
   101010101

Definir la lista

   sucAlternada :: [Integer]

tal que sus elementos son los términos de la sucesión de los dígitos 0 y 1 alternados. Por ejemplo,

   λ> take 10 sucAlternada
   [0,1,10,101,1010,10101,101010,1010101,10101010,101010101]
   λ> length (show (sucAlternada !! (2*10^6)))
   2000000

-}

import           Data.List


sucAlternada :: [Integer]
sucAlternada = map read
             $ (tail.inits.cycle) "01"
