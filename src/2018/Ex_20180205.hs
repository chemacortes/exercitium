{-# LANGUAGE UnicodeSyntax #-}

module Ex_20180205 where

{-

Celdas interiores de una retícula
=================================

Las celdas de una retícula cuadrada se numeran consecutivamente. Por ejemplo, la numeración de la retícula cuadrada de lado 4 es

   1, 2, 3, 4
   5, 6, 7, 8
   9,10,11,12
  13,14,15,16

Los números de sus celdas interiores son 6,7,10,11.

Definir la función

   interiores :: Int -> [Int]

tal que (interiores n) es la lista de los números de las celdas interiores de la retícula cuadrada de lado n. Por ejemplo,

   interiores 4  == [6,7,10,11]
   interiores 5  == [7,8,9,12,13,14,17,18,19]
   interiores 6  == [8,9,10,11,14,15,16,17,20,21,22,23,26,27,28,29]
   interiores 2  == []
   length (interiores 2018)  == 4064256

Comprobar con QuickCheck que el número de celdas interiores de la retícula cuadrada de lado n, con n > 1, es (n-2)^2.

-}

import           Data.List.Split (divvy)
import           Test.QuickCheck

interiores :: Int -> [Int]
interiores n = [i | i <- [n..n*n-n], i `mod` n > 1]

interiores3 :: Int -> [Int]
interiores3 n = concat (divvy (n-2) n [n+2..n*(n-1)-1])

--

interiores1 :: Int -> [Int]
interiores1 n = concat [[k..k+n-3] | k <- [n+2,2*n+2..n^2-2*n+2]]

interiores2 :: Int -> [Int]
interiores2 n = concat [[k..k+n-3] | k <- take (n-2) (iterate (n+) (n+2))]

interiores4 :: Int -> [Int]
interiores4 n = concat [take (n-2) [k..] | k <- take (n-2) (iterate (n+) (n+2))]



prop_interiores :: Positive Int -> Property
prop_interiores (Positive n) = n > 1 ==>
  length (interiores n) == (n-2)^2
