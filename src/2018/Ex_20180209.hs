{-# LANGUAGE UnicodeSyntax #-}

module Ex_20180209 where

{-

Períodos de Fibonacci
=====================

Los primeros términos de la sucesión de Fibonacci son

   0,1,1,2,3,5,8,13,21,34,55,89,144,233,377,610

Al calcular sus restos módulo 3 se obtiene

   0,1,1,2,0,2,2,1, 0,1,1,2,0,2,2,1

Se observa que es periódica y su período es

   0,1,1,2,0,2,2,1

Definir las funciones

   fibsMod                   :: Integer -> [Integer]
   periodoFibMod             :: Integer -> [Integer]
   longPeriodosFibMod        :: [Int]
   graficaLongPeriodosFibMod :: Int -> IO ()

tales que

    (fibsMod n) es la lista de los términos de la sucesión de Fibonacci módulo n. Por ejemplo,

     λ> take 24 (fibsMod 3)
     [0,1,1,2,0,2,2,1, 0,1,1,2,0,2,2,1, 0,1,1,2,0,2,2,1]
     λ> take 24 (fibsMod 4)
     [0,1,1,2,3,1, 0,1,1,2,3,1, 0,1,1,2,3,1, 0,1,1,2,3,1]

    (periodoFibMod n) es la parte perioica de la sucesión de Fibonacci módulo n. Por ejemplo,

     periodoFibMod 3  ==  [0,1,1,2,0,2,2,1]
     periodoFibMod 4  ==  [0,1,1,2,3,1]
     periodoFibMod 7  ==  [0,1,1,2,3,5,1,6,0,6,6,5,4,2,6,1]

    longPeriodosFibMod es la sucesión de las longitudes de los períodos de las sucesiones de Fibonacci módulo n, para n > 0. Por ejemplo,

     λ> take 20 longPeriodosFibMod
     [1,3,8,6,20,24,16,12,24,60,10,24,28,48,40,24,36,24,18,60]

    (graficaLongPeriodosFibMod n) dibuja la gráfica de los n primeros términos de la sucesión longPeriodosFibMod. Por ejemplo, (graficaLongPeriodosFibMod 300) dibuja

  ![Periodos_de_Fibonacci 300](Ex_20180209-Periodos_de_Fibonacci-300.png)



-}

import           Control.Monad
import           Data.List

import           Graphics.Gnuplot.Simple


fibs :: [Integer]
fibs = 0 : scanl (+) 1 fibs

fibsMod :: Integer -> [Integer]
fibsMod n = map (`mod` n) fibs

periodoFibMod :: Integer -> [Integer]
periodoFibMod n = head [xs | (xs,ys) <- zs, xs `isPrefixOf` ys ]
  where
    zs = tail $ liftM2 zip inits tails (fibsMod n)


longPeriodosFibMod :: [Int]
longPeriodosFibMod = map (length . periodoFibMod) [1..]

graficaLongPeriodosFibMod :: Int -> IO ()
graficaLongPeriodosFibMod n =
  plotList [ Key Nothing
           , Title ("graficaLongPeriodosFibMod " ++ show n ) ]
           (take n longPeriodosFibMod)
