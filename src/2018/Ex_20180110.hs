{-# LANGUAGE UnicodeSyntax #-}

module Ex_20180110 where

{-

Números somirp
==============

Un número omirp es un número primo que forma un primo distinto al invertir el orden de sus dígitos.

Definir las funciones

  esOmirp            :: Integer -> Bool
  omirps             :: [Integer]
  nOmirpsIntermedios :: Int -> Int

tales que

    (esOmirp n) se verifica si n es un número omirp. Por ejemplo,

     esOmirp 13      ==  True
     esOmirp 11      ==  False
     esOmirp 112207  ==  True

    omirps es la lista de los números omirps. Por ejemplo,

     take 15 omirps  ==  [13,17,31,37,71,73,79,97,107,113]
     omirps !! 2000  ==  112207

    (nOmirpsIntermedios n) es la cantidad de números omirps entre el n-ésimo número omirp y el obtenido al invertir el orden de sus dígitos. Por ejemplo,

     nOmirpsIntermedios 2000  ==  4750

Nota: Este ejercicio ha sido propuesto por Ángel Ruiz Campos.

-}


import           Data.Numbers.Primes

esOmirp :: Integer -> Bool
esOmirp n = n /= inv && isPrime inv
  where
      inv = (read . reverse . show) n

omirps :: [Integer]
omirps = filter esOmirp primes


nOmirpsIntermedios :: Int -> Int
nOmirpsIntermedios n = length . takeWhile (<b) $ dropWhile (<=a) omirps
    where
        x = omirps!!n
        inv = (read . reverse . show) x
        a = min x inv
        b = max x inv


nOmirpsIntermedios2 :: Int -> Int
nOmirpsIntermedios2 n | x < inv   = length . takeWhile (<inv) $ drop (n+1) omirps
                      | otherwise = length . dropWhile (<inv) $ take (n-1) omirps
    where
        x = omirps!!n
        inv = (read . reverse . show) x


nOmirpsIntermedios3 :: Int -> Int
nOmirpsIntermedios3 n = abs (n - length (takeWhile (<inv) omirps)) - 1
    where
        inv = (read . reverse . show) (omirps!!n)
