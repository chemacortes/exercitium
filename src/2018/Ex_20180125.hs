{-# LANGUAGE UnicodeSyntax #-}

module Ex_20180125 where

{-

Sucesión de Lichtenberg
=======================

La sucesión de Lichtenberg esta formada por la representación decimal de los números binarios de la sucesión de dígitos 0 y 1 alternados Los primeros términos de ambas sucesiones son

   Alternada ..... Lichtenberg
   0 ....................... 0
   1 ....................... 1
   10 ...................... 2
   101 ..................... 5
   1010 ................... 10
   10101 .................. 21
   101010 ................. 42
   1010101 ................ 85
   10101010 .............. 170
   101010101 ............. 341
   1010101010 ............ 682
   10101010101 .......... 1365
   101010101010 ......... 2730

Definir las funciones

   lichtenberg        :: [Integer]
   graficaLichtenberg :: Int -> IO ()

tales que

    lichtenberg es la lista cuyos elementos son los términos de la sucesión de Lichtenberg. Por ejemplo,

     λ> take 17 lichtenberg
     [0,1,2,5,10,21,42,85,170,341,682,1365,2730,5461,10922,21845,43690]

    (graficaLichtenberg n) dibuja la gráfica del número de dígitos de los n primeros términos de la sucesión de Lichtenberg. Por ejemlo, (graficaLichtenberg 100) dibuja

    ![Sucesion_de_Lichtenberg](Ex_20180125-Sucesion_de_Lichtenberg.png)

Comprobar con QuickCheck que todos los términos de la sucesión de Lichtenberg, a partir del 4º, son números compuestos.

-}

import           Data.List
import           Data.Numbers.Primes
import           Graphics.Gnuplot.Simple
import           Test.QuickCheck


lichtenberg :: [Integer]
lichtenberg = lichtenberg3

lichtenberg1        :: [Integer]
lichtenberg1 = 0 : [f x | (f,x) <- zip fs lichtenberg]
  where
    fs = cycle [(1+).(2*), (2*)]



lichtenberg2 :: [Integer]
lichtenberg2 = map aux sucAlternada
  where
    aux = foldl (\acc s -> if s == '1' then acc*2 + 1 else acc*2 ) 0

sucAlternada :: [String]
sucAlternada = (tail.inits.cycle) "01"


lichtenberg3 :: [Integer]
lichtenberg3 = scanl1 (\acc c -> 2*acc+c) (cycle [0,1])



lichtenbergDigitos :: [Int]
lichtenbergDigitos = map (length.show) lichtenberg

graficaLichtenberg :: Int -> IO ()
graficaLichtenberg n =
  plotList [Key Nothing
          -- , PNG ("Sucesion_de_Lichtenberg_"  ++ show n ++ ".png")
           , Title "Numero de digitos de la sucencion de Lichtenberg"
           ]
           (take n lichtenbergDigitos)

prop_lichtenberg :: Positive Int -> Property
prop_lichtenberg (Positive n) = n > 4 ==>
  (not.isPrime) (lichtenberg!!n)
