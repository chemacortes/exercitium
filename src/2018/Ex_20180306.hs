{-# LANGUAGE UnicodeSyntax #-}
module Ex_20180306 where


{-

Suma de los dígitos de las repeticiones de un número
====================================================

Dados dos números naturales n y x, su suma reducida se obtiene a partir del número obtenido repitiendo n veces el x sumando sus dígitos hasta obtener un número con sólo un dígito. Por ejemplo, si n es 3 y x es 24 las transformaciones son

   242424 ==> 18 ==> 9

Análogamente, si n es 4 y x es 7988 las transformaciones son

   7988798879887988 ==> 128 ==> 11 ==> 2

Definir las funciones

   sumaReducidaDigitosRepeticiones :: Integer -> Integer -> Integer
   grafica                         :: Integer -> IO ()

tales que

    (sumaReducidaDigitosRepeticiones n x) es la suma reducida de n repeticiones de x. Por ejemplo

     sumaReducidaDigitosRepeticiones 3 24                    ==  9
     sumaReducidaDigitosRepeticiones 4 7988                  == 2
     sumaReducidaDigitosRepeticiones (12^(10^7)) (12^(10^7)) == 9

    (grafica n) dibuja la gráfica de los n primeros elementos de la sucesión cuyo elementos k-ésimo es (sumaReducidaDigitosRepeticiones k k). Por ejemplo, (grafica 50) dibuja

  ![Suma_de_los_digitos_de_las_repeticiones_de_un_numero50](Ex_20180306-Suma_de_los_digitos_de_las_repeticiones_de_un_numero50.png)


-}

import           Graphics.Gnuplot.Simple

sumaReducidaDigitosRepeticiones :: Integer -> Integer -> Integer
sumaReducidaDigitosRepeticiones = sumaReducidaDigitosRepeticiones2

sumaReducidaDigitosRepeticiones1 :: Integer -> Integer -> Integer
sumaReducidaDigitosRepeticiones1 n x = sumaReducidaDigitos $ x * sumaReducidaDigitos n


sumaReducidaDigitos :: Integer -> Integer
sumaReducidaDigitos n | n < 10 = n
                      | otherwise = sumaReducidaDigitos (sum [read [c] | c <- show n])


sumaReducidaDigitosRepeticiones2 :: Integer -> Integer -> Integer
sumaReducidaDigitosRepeticiones2 n x = if m == 0 then 9 else m
  where
    m = (n `mod` 9) * (x `mod` 9) `mod` 9


grafica :: Integer -> IO ()
grafica n =
  plotList [Key Nothing]
           [sumaReducidaDigitosRepeticiones k k | k <- [1..n]]
