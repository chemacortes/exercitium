{-# LANGUAGE UnicodeSyntax #-}

module Ex_20180228 where

{-

Números cuyos factoriales son divisibles por x pero no por y
============================================================

Hay 3 números (el 2, 3 y 4) cuyos factoriales son divisibles por 2 pero no por 5. Análogamente, hay números 5 (el 5, 6, 7, 8, 9) cuyos factoriales son divisibles por 15 pero no por 25.

Definir la función

   nNumerosConFactorialesDivisibles :: Integer -> Integer -> Integer

tal que (nNumerosConFactorialesDivisibles x y) es la cantidad de números cuyo factorial es divisible por x pero no por y. Por ejemplo,

  nNumerosConFactorialesDivisibles 2   5     ==  3
  nNumerosConFactorialesDivisibles 15  25    ==  5
  nNumerosConFactorialesDivisibles 100 2000  ==  5

-}


nNumerosConFactorialesDivisibles :: Integer -> Integer -> Integer
nNumerosConFactorialesDivisibles = nNumerosConFactorialesDivisibles1

nNumerosConFactorialesDivisibles1 :: Integer -> Integer -> Integer
nNumerosConFactorialesDivisibles1 x y = smarandache y - smarandache x

-- Del ejercicio anterior
-- Menor factorial divisible por n
smarandache :: Integer -> Integer
smarandache = aux [1..]
  where
    aux (m:ms) n | d == n     = m
                 | otherwise  = aux ms (n `div` d)
      where d = gcd n m
