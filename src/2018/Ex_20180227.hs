{-# LANGUAGE UnicodeSyntax #-}

module Ex_20180227 where

{-

La función de Smarandache
=========================

La [función de Smarandache][1], también conocida como la [función de Kempner][2], es la función que asigna a cada número entero positivo n el menor número cuyo factorial es divisible por n y se representa por S(n). Por ejemplo, el número 8 no divide a 1!, 2!, 3!, pero sí divide 4!; por tanto, S(8) = 4.

[1]: http://mathworld.wolfram.com/SmarandacheFunction.html
[2]: https://en.wikipedia.org/wiki/Kempner_function

Definir las funciones

   smarandache        :: Integer -> Integer
   graficaSmarandache :: Integer -> IO ()

tales que

    (smarandache n) es el menor número cuyo factorial es divisible por n. Por ejemplo,

     smarandache 8   ==  4
     smarandache 10  ==  5
     smarandache 16  ==  6

    (graficaSmarandache n) dibuja la gráfica de los n primeros términos de la sucesión de Smarandache. Por ejemplo, (graficaSmarandache 100) dibuja

  ![La_funcion_de_Smarandache_100](Ex_20180227-La_funcion_de_Smarandache_100.png)

    (graficaSmarandache 500) dibuja

  ![La_funcion_de_Smarandache_500](Ex_20180227-La_funcion_de_Smarandache_500.png)


-}

import           Data.List               (genericLength)
import           Graphics.Gnuplot.Simple

smarandache :: Integer -> Integer
smarandache = smarandache2

smarandache1 :: Integer -> Integer
smarandache1 0 = 0
smarandache1 n = (+1) . genericLength
              . takeWhile ((/=0).(`mod` n))
              $ scanl1 (*) [1..]

smarandache2 :: Integer -> Integer
smarandache2 = aux [1..]
  where
    aux (m:ms) n | d == n     = m
                 | otherwise  = aux ms (n `div` d)
      where d = gcd n m

smarandache3 :: Integer -> Integer
smarandache3 = aux 1
  where
    aux m n | d == n     = m
            | otherwise  = aux (m+1) (n `div` d)
      where d = gcd n m

graficaSmarandache :: Integer -> IO ()
graficaSmarandache n =
  plotList [Key Nothing]
           (map smarandache [1..n])
