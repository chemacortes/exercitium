{-# LANGUAGE UnicodeSyntax #-}

module Ex_20180226 where

{-

Generación de progresiones geométricas
======================================

Definir la función

   geometrica :: Int -> Int -> Int -> [Int]

tal que (geometrica a b c) es la lista de los términos de la progresión geométrica cuyo primer término es a, su segundo término es b (que se supone que es múltiplo de a) y los términos son menores o iguales que c. Por ejemplo,

   geometrica 1 3  27   ==  [1,3,9,27]
   geometrica 2 6  100  ==  [2,6,18,54]
   geometrica 3 12 57   ==  [3,12,48]
   geometrica 4 20 253  ==  [4,20,100]
   geometrica 5 25 625  ==  [5,25,125,625]
   geometrica 6 42 42   ==  [6,42]

-}


geometrica :: Int -> Int -> Int -> [Int]
geometrica a b c = takeWhile (<=c) $ iterate (* (b `div` a)) a
