{-# LANGUAGE UnicodeSyntax #-}

module Ex_20180213 where

{-

Menor número divisible por 10^n cuyos dígitos suman n
=====================================================

Definir la función

   menor :: Integer -> Integer

tal que (menor n) es el menor número divisible por 10^n cuyos dígitos suman n. Por ejemplo,

   menor 5   ==  500000
   menor 20  ==  29900000000000000000000

-}


menor :: Integer -> Integer
menor = menor2

menor1 :: Integer -> Integer
menor1 n = 10^n * read (aux n)
  where
    aux :: Integer -> String
    aux d | d < 9 = show d
          | otherwise = aux (d-9) ++ "9"

menor2 :: Integer -> Integer
menor2 n = 10^n * ( (10^m-1) + r*10^m )
  where
    (m,r) = n `divMod` 9
