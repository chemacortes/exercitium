{-# LANGUAGE UnicodeSyntax #-}

module Ex_20180221 where

{-

Representación ampliada de matrices dispersas
=============================================

En el ejercicio anterior se explicó una representación reducida de las matrices dispersas. A partir del número de columnas y la representación reducida se puede construir la matriz.

Definir la función

   ampliada :: Num a => Int -> [[(Int,a)]] -> Matrix a

tal que (ampliada n xss) es la matriz con n columnas cuya representación reducida es xss. Por ejemplo,

   λ> ampliada 3 [[(3,4)],[(2,5)],[]]
   ( 0 0 4 )
   ( 0 5 0 )
   ( 0 0 0 )

   λ> ampliada 3 [[],[]]
   ( 0 0 0 )
   ( 0 0 0 )

   λ> ampliada 2 [[],[],[]]
   ( 0 0 )
   ( 0 0 )
   ( 0 0 )

-}

import           Data.Matrix


ampliada :: Num a => Int -> [[(Int,a)]] -> Matrix a
ampliada n = fromLists . map aux
  where
    zeros = replicate n 0
    aux = foldl setValue zeros

setValue :: [a] -> (Int, a) -> [a]
setValue xs (n,x) = ys ++ x:tail zs
  where
    (ys,zs) = splitAt (n-1) xs
