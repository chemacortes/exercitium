{-# LANGUAGE UnicodeSyntax #-}

module Ex_20180131 where

{-

Recorrido del robot
==================

Los puntos de una retícula se representan mediante pares de enteros

   type Punto = (Int,Int)

y los movimientos de un robot mediante el tipo

   data Movimiento = N Int
                   | S Int
                   | E Int
                   | O Int

donde (N x) significa que se mueve x unidades en la dirección norte y análogamente para las restantes direcciones (S es sur, E es este y O es oeste).

Definir la función

   posicion :: [Movimiento] -> Punto

tal que (posicion ms) es la posición final de un robot que inicialmente está en el el punto (0,0) y realiza los movimientos ms. Por ejemplo,

   posicion [N 3]                           ==  (0,3)
   posicion [N 3, E 5]                      ==  (5,3)
   posicion [N 3, E 5, S 1]                 ==  (5,2)
   posicion [N 3, E 5, S 1, O 4]            ==  (1,2)
   posicion [N 3, E 5, S 1, O 4, N 3]       ==  (1,5)
   posicion [N 3, E 5, S 1, O 4, N 3, S 3]  ==  (1,2)

-}


type Punto = (Int,Int)

data Movimiento = N Int
                | S Int
                | E Int
                | O Int

posicion :: [Movimiento] -> Punto
posicion = foldl aux (0,0)
  where
    aux (x,y) (N j) = (x,y+j)
    aux (x,y) (S j) = (x,y-j)
    aux (x,y) (E i) = (x+i,y)
    aux (x,y) (O i) = (x-i,y)


posicion2 :: [Movimiento] -> Punto
posicion2 xs = let (hs,vs) = unzip (map aux xs)
               in (sum hs, sum vs)
  where
    aux (N j) = (0,j)
    aux (S j) = (0,-j)
    aux (E i) = (i,0)
    aux (O i) = (-i,0)
