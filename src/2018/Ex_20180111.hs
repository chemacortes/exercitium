{-# LANGUAGE UnicodeSyntax #-}

module Ex_20180111 where

{-

Ordenación por frecuencia
=========================

Definir la función

   ordPorFrecuencia :: Ord a => [a] -> [a]

tal que (ordPorFrecuencia xs) es la lista obtenidas ordenando los elementos de xs por su frecuencia, de los que aparecen más a los que aparecen menos, y los que aparecen el mismo número de veces se ordenan de manera creciente según su valor. Por ejemplo,

   ordPorFrecuencia "canalDePanama"      ==  "aaaaannmlecPD"
   ordPorFrecuencia "11012018"           ==  "11110082"
   ordPorFrecuencia [2,3,5,3,7,9,5,3,7]  ==  [3,3,3,7,7,5,5,9,2]

-}

import           Data.Function (on)
import           Data.List     (group, sort, sortBy)
import           Data.Ord      (comparing)


ordPorFrecuencia :: Ord a => [a] -> [a]
ordPorFrecuencia = concat
                 . sortBy (flip (comparing length))
                 . group
                 . sortBy (flip compare)


ordPorFrecuencia3 :: Ord a => [a] -> [a]
ordPorFrecuencia3 =
 concat . reverse . sortBy (compare `on` length). group . sort
