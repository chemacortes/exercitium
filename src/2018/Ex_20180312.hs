{-# LANGUAGE UnicodeSyntax #-}

module Ex_20180312 where

{-

La conjetura de Levy
====================

[Hyman Levy][1] observó que

    7 = 3 + 2 x 2
    9 = 3 + 2 x 3 =  5 + 2 x 2
   11 = 5 + 2 x 3 =  7 + 2 x 2
   13 = 3 + 2 x 5 =  7 + 2 x 3
   15 = 3 + 2 x 5 = 11 + 2 x 2
   17 = 3 + 2 x 7 =  7 + 2 x 5 = 11 + 2 x 3 = 13 + 2 x 2
   19 = 5 + 2 x 7 = 13 + 2 x 3

y conjeturó que todos los número impares mayores o iguales que 7 se pueden escribir como la suma de un primo y el doble de un primo. El objetivo de los siguientes ejercicios es comprobar la [conjetura de Levy][2].

Definir las siguientes funciones

   descomposicionesLevy :: Integer -> [(Integer,Integer)]
   graficaLevy          :: Integer -> IO ()

tales que

    (descomposicionesLevy x) es la lista de pares de primos (p,q) tales que x = p + 2q. Por ejemplo,

     descomposicionesLevy  7  ==  [(3,2)]
     descomposicionesLevy  9  ==  [(3,3),(5,2)]
     descomposicionesLevy 17  ==  [(3,7),(7,5),(11,3),(13,2)]

    (graficaLevy n) dibuja los puntos (x,y) tales que x pertenece a [7,9..7+2x(n-1)] e y es el número de descomposiciones de Levy de x. Por ejemplo, (graficaLevy 200) dibuja


![La_conjetura_de_Levy-200](Ex_20180312-La_conjetura_de_Levy-200.png)

Comprobar con QuickCheck la conjetura de Levy.

[1]: https://en.wikipedia.org/wiki/Hyman_Levy
[2]: http://mathworld.wolfram.com/LevysConjecture.html

-}

import           Data.Numbers.Primes (primes,isPrime)
import           Graphics.Gnuplot.Simple (plotList, Attribute(Key))
import           Test.QuickCheck


descomposicionesLevy :: Integer -> [(Integer, Integer)]
descomposicionesLevy n =
  [ (p, q)
  | p <- takeWhile (n >=) (tail primes)
  , let q = (n - p) `div` 2
  , isPrime q
  ]

graficaLevy :: Integer -> IO ()
graficaLevy n = plotList
  [Key Nothing]
  [ (x, (length . descomposicionesLevy) x) | x <- [7, 9 .. 7 + 2 * (n - 1)] ]

prop_levy :: Positive Integer -> Bool
prop_levy (Positive n) =
  let x = 5 + 2 * n in (not . null) (descomposicionesLevy x)
