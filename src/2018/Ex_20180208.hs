{-# LANGUAGE UnicodeSyntax #-}

module Ex_20180208 where

{-

Recorrido de árboles en espiral
===============================

Los árboles se pueden representar mediante el siguiente tipo de datos

   data Arbol a = N a [Arbol a]
     deriving Show

Por ejemplo, los árboles

         1             1             1
        /  \          / \           / \
       /    \        8   3         8   3
      2      3          /|\       /|\  |
     / \    / \        4 5 6     4 5 6 7
    4   5  6   7

se representan por

   ej1, ej2, ej3 :: Arbol Int
   ej1 = N 1 [N 2 [N 4 [], N 5 []], N 3 [N 6 [], N 7 []]]
   ej2 = N 1 [N 8 [], N 3 [N 4 [], N 5 [], N 6 []]]
   ej3 = N 1 [N 8 [N 4 [], N 5 [], N 6 []], N 3 [N 7 []]]

Derecorridoinir la recorridounción

   espiral :: Arbol a -> [a]

tal que (espiral x) es la lista de los nodos del árbol x recorridos en espiral; es decir, la raíz de x, los nodos del primer nivel de izquierda a derecha, los nodos del segundo nivel de derecha a izquierda y así sucesivamente. Por ejemplo,

   espiral ej1  ==  [1,2,3,7,6,5,4]
   espiral ej2  ==  [1,8,3,6,5,4]
   espiral ej3  ==  [1,8,3,7,6,5,4]

-}


data Arbol a = N a [Arbol a]
 deriving Show

ej1, ej2, ej3 :: Arbol Int
ej1 = N 1 [N 2 [N 4 [], N 5 []], N 3 [N 6 [], N 7 []]]
ej2 = N 1 [N 8 [], N 3 [N 4 [], N 5 [], N 6 []]]
ej3 = N 1 [N 8 [N 4 [], N 5 [], N 6 []], N 3 [N 7 []]]



espiral :: Arbol a -> [a]
espiral = espiral2


espiral1 :: Arbol a -> [a]
espiral1 a = concat $ zipWith ($) (cycle [reverse, id]) (subListas [a])

subListas :: [Arbol a] -> [[a]]
subListas [] = []
subListas xs = as : (subListas.concat) ys
  where
    (as,ys) = unzip [(x,zs) | (N x zs) <- xs]


data Dir = Izq | Der deriving (Show, Eq)

espiral2 :: Arbol a -> [a]
espiral2 a = recorrido Izq [a]

recorrido :: Dir -> [Arbol a] -> [a]
recorrido _ [] = []
recorrido d xs | d == Der  = as ++ (recorrido Izq . reverse . concat) ys
               | otherwise = as ++ (recorrido Der . concat) ys
  where
    (as,ys) = unzip [(x,zs) | (N x zs) <- xs]
