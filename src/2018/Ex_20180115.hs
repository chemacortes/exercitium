{-# LANGUAGE UnicodeSyntax #-}

module Ex_20180115 where

{-

Sumas parciales de Nicómaco
===========================

Nicómaco de Gerasa vivió en Palestina entre los siglos I y II de nuestra era. Escribió *Arithmetike eisagoge* (Introducción a la aritmética) que es el primer trabajo en donde se trata la Aritmética de forma separada a la Geometría. En el tratado se encuentra la siguiente proposición: "si se escriben los números impares

   1, 3, 5, 7, 9, 11, 13, 15, 17, ...

entonces el primero es el cubo de 1; la suma de los dos siguientes, el cubo de 2; la suma de los tres siguientes, el cubo de 3; y así sucesivamente.""

Definir las siguientes funciones

   listasParciales :: [a] -> [[a]]
   sumasParciales  :: [Int] -> [Int]

tales que

    (listasParciales xs) es la lista obtenido agrupando los elementos de la lista infinita xs de forma que la primera tiene 0 elementos; la segunda, el primer elemento de xs; la tercera, los dos siguientes; y así sucesivamente. Por ejemplo,

     λ> take 7 (listasParciales [1..])
     [[],[1],[2,3],[4,5,6],[7,8,9,10],[11,12,13,14,15],[16,17,18,19,20,21]]
     λ> take 7 (listasParciales [1,3..])
     [[],[1],[3,5],[7,9,11],[13,15,17,19],[21,23,25,27,29],[31,33,35,37,39,41]]

    (sumasParciales xs) es la lista de las sumas parciales de la lista infinita xs. Por ejemplo,

     λ> take 15 (sumasParciales [1..])
     [0,1,5,15,34,65,111,175,260,369,505,671,870,1105,1379]
     λ> take 15 (sumasParciales [1,3..])
     [0,1,8,27,64,125,216,343,512,729,1000,1331,1728,2197,2744]

Comprobar con QuickChek la propiedad de Nicómaco; es decir, que para todo número natural n, el término n-ésimo de (sumasParciales [1,3..]) es el cubo de n.

-}

import           Test.QuickCheck

listasParciales :: [a] -> [[a]]
listasParciales = aux [0..]
  where
    aux (n:ns) xs = take n xs : aux ns (drop n xs)

listasParciales2 :: [a] -> [[a]]
listasParciales2 = aux 0
  where aux n xs = ys : aux (n+1) zs
          where (ys,zs) = splitAt n xs

sumasParciales :: [Int] -> [Int]
sumasParciales = map sum . listasParciales


prop_nicomaco :: Positive Int -> Bool
prop_nicomaco (Positive n) = xs!!n == n^3
  where
    xs = sumasParciales [1,3..]
