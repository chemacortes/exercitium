module Ex_20160118 where

{-

Primos que contienen al 2016
============================

Definir la sucesión

   primosCon2016 :: [Integer]

tal que sus elementos son los números primos que contienen al 2016. Por ejemplo,

   take 5 primosCon2016  ==  [20161,120163,120167,201611,201623]
   primosCon2016 !! 111  ==  3020167

-}

import Data.Numbers.Primes
import Data.List

primosCon2016a :: [Integer]
primosCon2016a = [p | p <- primes
                    , "2016" `isInfixOf` show p ]

primosCon2016 :: [Integer]
primosCon2016 = filter (isInfixOf "2016" . show) primes
