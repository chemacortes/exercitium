module Ex_20160105 where

{-

Puntos en una región
====================

Definir la función

   puntos :: Integer -> [(Integer,Integer)]

tal que (puntos n) es la lista de los puntos (x,y) con coordenadas enteras de
la cuadrícula [1..n]x[1..n] (es decir, 1 ≤ x,y ≤ n) tales que |x²-xy-y²| = 1. 
Por ejemplo,

   length (puntos 10)          ==  5
   length (puntos 100)         ==  10
   length (puntos 1000)        ==  15
   length (puntos (10^50000))  ==  239249

-}

import Data.List (unfoldr)

puntos1 :: Integer -> [(Integer,Integer)]
puntos1 n = [(i,j) | i <- [1..n], j <- [1..n]
                  , abs (i^2-i*j-j^2) == 1 ]

puntos :: Integer -> [(Integer,Integer)]
puntos n = unfoldr f (1,1)
    where f (x,y) | x > n     = Nothing
                  | otherwise = Just ((x,y),(x+y,x))