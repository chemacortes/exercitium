module Ex_20160420 where

{-

Término ausente en una progresión aritmética
============================================

Una progresión aritmética es una sucesión de números tales que la diferencia de
dos términos sucesivos cualesquiera de la sucesión es constante.

Definir la función

   ausente :: Integral a => [a] -> a

tal que (ausente xs) es el único término ausente de la progresión aritmética xs. Por ejemplo,

   ausente [3,7,9,11]                ==  5
   ausente [3,5,9,11]                ==  7
   ausente [3,5,7,11]                ==  9
   ausente ([1..9]++[11..])          ==  10
   ausente ([1..10^6] ++ [2+10^6])  ==  1000001

Nota. Se supone que la lista tiene al menos 3 elementos, que puede ser
-}


ausente :: Integral a => [a] -> a
ausente = ausente2

-- Esta solución falla con diferencias negativas: ausente [11,7,5,3]
ausente1 :: Integral a => [a] -> a
ausente1 xs@(x:y:z:_) = snd . head . dropWhile (uncurry (==)) $ zip xs [x,x+r..]
    where r = min (y-x) (z-y)

ausente2 :: Integral a => [a] -> a
ausente2 xs@(x:y:z:_) = head [v | (w, v) <- zip xs [x, x+r..], w /= v]
    where r = if abs (y-x) > abs (z-y) then z-y else y-x

ausente3 :: Integral a => [a] -> a
ausente3 s@(x1:x2:x3:xs)
    | x1+x3 /= 2*x2 = x1+(x3-x2)
    | otherwise = head [a | (a,b) <- zip [x1,x2..] s, a /= b]
