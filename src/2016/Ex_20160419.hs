module Ex_20160419 where

{-

Matriz zigzagueante
===================

La matriz zizagueante de orden n es la matriz cuadrada con n filas y n columnas
y cuyos elementos son los n² primeros números naturales colocados de manera
creciente a lo largo de las diagonales secundarias. Por ejemplo, La matriz
zigzagueante de orden 5 es

    0  1  5  6 14
    2  4  7 13 15
    3  8 12 16 21
    9 11 17 20 22
   10 18 19 23 24

La colocación de los elementos se puede ver gráficamente en
Matriz zigzagueante

![Matriz zigzagueante](file:./Ex_20160419.gif)


Definir la función

   zigZag :: Int -> Array (Int,Int) Int

tal que (zigZag n) es la matriz zigzagueante de orden n. Por ejemplo,

   ghci> elems (zigZag 3)
   [0,1,5, 2,4,6, 3,7,8]
   ghci> elems (zigZag 4)
   [0,1,5,6, 2,4,7,12, 3,8,11,13, 9,10,14,15]
   ghci> elems (zigZag 5)
   [0,1,5,6,14, 2,4,7,13,15, 3,8,12,16,21, 9,11,17,20,22, 10,18,19,23,24]
   ghci> take 15 (elems (zigZag 1000))
   [0,1,5,6,14,15,27,28,44,45,65,66,90,91,119]

-}

import Data.Array
import Data.List (sortBy)

zigZag :: Int -> Array (Int,Int) Int
zigZag n = listArray ((1,1),(n,n)) $ concat (zipWith (++) xss yss)
    where xss = lineas $ inicial n
          yss = espejado n xss

-- Primera línea de la matriz
inicial :: Int -> [Int]
inicial n = take n $ scanl (+) 0 $ concat [[1,i]|i<-[4,8..]]

-- Creación de la parte superior de la matriz, hasta la diagonal
lineas :: [Int] -> [[Int]]
lineas xs = aux xs unos
    where unos = concat (repeat [1,-1])
          aux (x:xs) us =  (x:xs) : aux (zipWith (+) xs us) (tail us)
          aux _ _ = []

-- Construcción de la parte inferior de la matriz a partir de la superior
espejado :: Int -> [[Int]] -> [[Int]]
espejado n xss = [ reverse [n*n-1-x|x <- init xs] | xs <- reverse xss]


-- SOLUCIONES OFICIALES
-- 1ª solución
-- ===========

zigZag1 :: Int -> Array (Int,Int) Int
zigZag1 n = array ((1,1),(n,n)) (zip (ordenZigZag n) [0..])


-- (ordenZigZag n) es la lista de puntos del cuadrado nxn recorridos en
-- zig-zag por las diagonales secundarias. Por ejemplo,
--    ghci> ordenZigZag 4
--    [(1,1), (1,2),(2,1), (3,1),(2,2),(1,3), (1,4),(2,3),(3,2),(4,1),
--     (4,2),(3,3),(2,4), (3,4),(4,3), (4,4)]
ordenZigZag :: Int -> [(Int,Int)]
ordenZigZag n = concat [aux n m | m <- [2..2*n]]
    where aux n m | odd m     = [(x,m-x) | x <- [max 1 (m-n)..min n (m-1)]]
                  | otherwise = [(m-x,x) | x <- [max 1 (m-n)..min n (m-1)]]

-- 2ª solución
-- ===========

zigZag2 :: Int -> Array (Int,Int) Int
zigZag2 n = array ((1,1),(n,n)) (zip (ordenZigZag2 n) [0..])

-- (ordenZigZag2 n) es la lista de puntos del cuadrado nxn recorridos en
-- zig-zag por las diagonales secundarias. Por ejemplo,
--    ghci> ordenZigZag2 4
--    [(1,1), (1,2),(2,1), (3,1),(2,2),(1,3), (1,4),(2,3),(3,2),(4,1),
--     (4,2),(3,3),(2,4), (3,4),(4,3), (4,4)]
ordenZigZag2 :: Int -> [(Int,Int)]
ordenZigZag2 n = sortBy comp [(x,y) | x <- [1..n], y <- [1..n]]
    where comp (x1,y1) (x2,y2) | x1+y1 < x2+y2 = LT
                               | x1+y1 > x2+y2 = GT
                               | even (x1+y1)  = compare y1 y2
                               | otherwise     = compare x1 x2
