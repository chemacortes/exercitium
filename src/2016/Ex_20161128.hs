module Ex_20161128 where

{-

Representación binaria de los números de Carol
==============================================

Un número de Carol es un número entero de la forma 4^n-2^{n+1}-1 o,
equivalentemente, (2^n-1)^2-2. Los primeros números de Carol son -1, 7, 47, 223,
959, 3967, 16127, 65023, 261119, 1046527.

Definir las funciones

   carol        :: Integer -> Integer
   carolBinario :: Integer -> Integer

tales que

    (carol n) es el n-ésimo número de Carol. Por ejemplo,

     carol  3  ==  47
     carol  4  ==  223
     carol 25  ==  1125899839733759

    (carolBinario n) es la representación binaria del n-ésimo número de Carol. Por ejemplo,

     carolBinario 3  ==  101111
     carolBinario 4  ==  11011111
     carolBinario 5  ==  1110111111

Comprobar con QuickCheck que, para n > 2, la representación binaria del n-ésimo
número de Carol es el número formado por n-2 veces el dígito 1, seguido por un
0 y a continuación n+1 veces el dígito 1.

-}

import Test.QuickCheck
import Data.List (genericReplicate)

carol        :: Integer -> Integer
carol n = 4^n - 2^(n+1) - 1


carolBinario :: Integer -> Integer
carolBinario = int2Bin . carol

int2Bin :: Integer -> Integer
int2Bin n | d == 0      = r
          | otherwise   = int2Bin d * 10 + r
  where (d,r) = n `divMod` 2


prop_carol :: Positive Integer -> Property
prop_carol (Positive n) = n > 2 ==>
    carolBinario n == read (nUnos (n-2) ++ "0" ++ nUnos (n+1))
    where nUnos k = genericReplicate k '1'
