module Ex_20161201 where

{-

Sumas de posiciones pares e impares
===================================

Definir la función

   sumasParesImpares :: [Integer] -> (Integer,Integer)

tal que (sumasParesImpares) xs es el par formado por la suma de los elementos de
xs en posiciones pares y por la suma de los elementos de xs en posiciones
impares. Por ejemplo,

   sumasParesImpares []         ==  (0,0)
   sumasParesImpares [3]        ==  (3,0)
   sumasParesImpares [3,2]      ==  (3,2)
   sumasParesImpares [3,2,1]    ==  (4,2)
   sumasParesImpares [3,2,1,5]  ==  (4,7)
   sumasParesImpares [1..10^7]  ==  (25000000000000,25000005000000)

-}


sumasParesImpares1 :: [Integer] -> (Integer,Integer)
sumasParesImpares1 []     = (0,0)
sumasParesImpares1 (x:xs) = (x+b, a)
  where (a,b) = sumasParesImpares1 xs

sumasParesImpares2 :: [Integer] -> (Integer, Integer)
sumasParesImpares2 []       = (0,0)
sumasParesImpares2 [x]      = (x,0)
sumasParesImpares2 (x:y:xs) = (x + si, y + sp)
                           where (si,sp) = sumasParesImpares2 xs

sumasParesImpares3 :: [Integer] -> (Integer,Integer)
sumasParesImpares3 xs = (sum xs - pares, pares )
  where pares = sum [x | (x,i) <- zip xs [1..], even i]
