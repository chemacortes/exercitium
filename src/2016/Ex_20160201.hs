module Ex_20160201 where

{-

El algoritmo binario del mcd
============================

El máximo común divisor (mcd) de dos números enteros no negativos se puede 
calcular mediante un algoritmo binario basado en las siguientes propiedades:

    Si a,b son pares, entonces mcd(a,b) = 2*mcd(a/2,b/2)
    Si a es par y b impar, entonces mcd(a,b) = mcd(a/2,b)
    Si a es impar y b par, entonces mcd(a,b) = mcd(a,b/2)
    Si a y b son impares y a > b, entonces mcd(a,b) = mcd((a-b)/2,b)
    Si a y b son impares y a < b, entonces mcd(a,b) = mcd(a,(b-a)/2)
    mcd(a,0) = a
    mcd(0,b) = b
    mcd(a,a) = a

Por ejemplo, el cálculo del mcd(660,420) es

   mcd(660,420)
   = 2*mcd(330,210)    [por 1]
   = 2*2*mcd(165,105)  [por 1]
   = 2*2*mcd(30,105)   [por 4]
   = 2*2*mcd(15,105)   [por 2]
   = 2*2*mcd(15,45)    [por 4]
   = 2*2*mcd(15,15)    [por 4]
   = 2*2*15            [por 8]
   = 60

Definir la función

   mcd :: Integer -> Integer -> Integer

Definir la función

tal que (mcd a b) es el máximo común divisor de a y b calculado mediante el 
algoritmo binario del mcd. Por ejemplo,

   mcd 660 420  ==  60
   mcd 3 0      ==  3
   mcd 0 3      ==  3

Comprobar con QuickCheck que, para los enteros no negativos, las funciones mcd 
y gcd son equivalentes.


-}

import Test.QuickCheck

mcd :: Integer -> Integer -> Integer
mcd a 0 = a
mcd 0 b = b
mcd a b | even a && even b      = 2 * mcd (a `div` 2) (b `div` 2)
        | even a && odd b       = mcd (a `div` 2) b
        | odd a && even b       = mcd a (b `div` 2)
        | a > b                 = mcd ((a - b) `div` 2) b
        | a < b                 = mcd a ((b - a) `div` 2)
        | otherwise             = a

prop_mcd :: NonNegative Integer -> NonNegative Integer -> Bool
prop_mcd a b = mcd a' b' == gcd a' b' 
    where a' = getNonNegative a
          b' = getNonNegative b

