module Ex_20160111 where

{-

Parte libre de cuadrados y parte cuadrada de un número

    11-enero-2016 06:00 

Califica el nivel del ejercicio (1 Voto)

La parte libre de cuadrados de un número n es el producto de todos sus 
divisores primos con exponente impar en la factorización prima de n. Por 
ejemplo, la parte libre de cuadrados de 360 es 10 ya que 360 = 2³3²5 y 2.5 = 10; 
además, 360 = 10.6²

La parte cuadrada de un número n es el mayor número cuadrado que divide a n. 
Por ejemplo, la parte cuadrada de 360 es 6.

Definir las funciones

   parteLibre    :: Integer -> Integer
   parteCuadrada :: Integer -> Integer

tales que

    (parteLibre x) es la parte libre de x. Por ejemplo,

     parteLibre 360                 ==  10
     parteLibre 1800                ==  2
     [parteLibre n | n <- [1..14]]  ==  [1,2,3,1,5,6,7,2,1,10,11,3,13,14]

    (parteCuadrada x) es la parte cuadrada de x. Por ejemplo,

     parteCuadrada 360                 ==  36
     parteCuadrada 1800                ==  900
     [parteCuadrada n | n <- [1..14]]  ==  [1,1,1,4,1,1,1,4,9,1,1,4,1,1]

-}

import Data.List (group)
import Data.Numbers.Primes (primeFactors)

parteLibre    :: Integer -> Integer
parteLibre n = product [ x | (x:xs) <- (group . primeFactors) n
                           , even (length xs) ]  

parteCuadrada :: Integer -> Integer
parteCuadrada n = n `div` parteLibre n
