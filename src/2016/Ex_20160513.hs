module Ex_20160513 where

{-

Problema de las puertas
=======================

Un hotel dispone de n habitaciones y n camareros. Los camareros tienen la
costumbre de cambiar de estado las puertas (es decir, abrir las cerradas y
cerrar las abiertas). El proceso es el siguiente:

-   Inicialmente todas las puertas están cerradas.
-   El primer camarero cambia de estado las puertas de todas las habitaciones.
-   El segundo cambia de estado de las puertas de las habitaciones pares.
-   El tercero cambia de estado todas las puertas que son múltiplos de 3.
-   El cuarto cambia de estado todas las puertas que son múltiplos de 4
-   Así, hasta que ha pasado el último camarero.

Por ejemplo, para n = 5

   Pase    | Puerta 1 | Puerta 2 | Puerta 3 | Puerta 4 | Puerta 5
   --------+----------+----------+----------+----------+----------
   Inicial | Cerrada  | Cerrada  | Cerrada  | Cerrada  | Cerrada
   Pase 1  | Abierta  | Abierta  | Abierta  | Abierta  | Abierta
   Pase 2  | Abierta  | Cerrada  | Abierta  | Cerrada  | Abierta
   Pase 3  | Abierta  | Cerrada  | Cerrada  | Cerrada  | Abierta
   Pase 4  | Abierta  | Cerrada  | Cerrada  | Abierta  | Abierta
   Pase 5  | Abierta  | Cerrada  | Cerrada  | Abierta  | Cerrada

Los estados de las puertas se representan por el siguiente tipo de datos

   data Estado = Abierta | Cerrada
                 deriving (Eq, Show)

Definir la función

   final :: Int -> [Estado]

tal que (final n) es la lista de los estados de las n puertas después
de que hayan pasado los n camareros. Por
ejemplo,

   ghci> final 5
   [Abierta,Cerrada,Cerrada,Abierta,Cerrada]
   ghci> final 7
   [Abierta,Cerrada,Cerrada,Abierta,Cerrada,Cerrada,Cerrada]

-}

import Data.List

data Estado = Abierta | Cerrada
              deriving (Eq, Show)

final :: Int -> [Estado]
final = map estadoFinal . cambios

-- Calcula cuantos cambios sufre cada una de la n puertas
cambios :: Int -> [Int]
cambios n = map length . group . sort
          $ concatMap pase [1..n]
    where pase p = [p,2*p..n] -- puertas que cambian en un pase

-- Deduce el estado final de la puerta según el número de cambios
estadoFinal :: Int -> Estado
estadoFinal n | odd n     = Abierta
              | otherwise = Cerrada
