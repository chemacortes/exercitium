module Ex_20160211 where

{-

Compactación de listas
======================

Definir la función

   compactada :: [Maybe Int] -> [Int]

tal que (compacta xs) es la lista obtenida al compactar xs con las siguientes
reglas:

    - se eliminan los elementos Nothing;
    - si dos elementos consecutivos tienen el mismo valor, se sustituyen por el
      sucesor de su valor y
    - los restantes elementos no se cambian.

Por ejemplo,

    λ> compactada [Just 1,Nothing,Just 1,Just 4,Just 4,Just 3,Just 6]
    [2,5,3,6]
    λ> compactada [Just 1,Nothing,Just 1,Just 1,Just 4,Just 3,Just 6]
    [2,1,4,3,6]
    λ> compactada [Just 1,Nothing,Just 1,Just 1,Just 4,Just 4,Just 6]
    [2,1,5,6]
    λ> compactada [Just 1,Nothing,Just 1,Just 1,Just 4,Just 3,Just 4]
    [2,1,4,3,4]
    λ> compactada [Just 1,Nothing,Just 1,Just 2,Just 4,Just 3,Just 4]
    [2,2,4,3,4]
-}

import Data.Maybe (catMaybes)

compactada :: [Maybe Int] -> [Int]
compactada = aux . catMaybes
    where aux []  = []
          aux [x] = [x]
          aux (x:y:xs) | x == y    = succ x : aux xs
                       | otherwise = x : aux (y:xs)

compactada2 :: [Maybe Int] -> [Int]
compactada2 []                              = []
compactada2 [Just a]                        = [a]
compactada2 (Nothing:xs)                    = compactada2 xs
compactada2 (Just a:Nothing:xs)             = compactada2 (Just a:xs)
compactada2 (Just a:Just b:xs) | a==b       = succ a: compactada2 xs
                               | otherwise  = a : compactada2 (Just b:xs)
