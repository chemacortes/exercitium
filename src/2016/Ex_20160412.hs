module Ex_20160412 where

{-
Caminos reducidos
=================

Un camino es una sucesión de pasos en una de las cuatros direcciones Norte, Sur,
Este, Oeste. Ir en una dirección y a continuación en la opuesta es un esfuerzo
que se puede reducir, Por ejemplo, el camino [Norte,Sur,Este,Sur] se puede
reducir a [Este,Sur].

Un camino se dice que es reducido si no tiene dos pasos consecutivos en
direcciones opuesta. Por ejemplo, [Este,Sur] es reducido y [Norte,Sur,Este,Sur]
no lo es.

En Haskell, las direcciones y los caminos se pueden definir por

   data Direccion = N | S | E | O deriving (Show, Eq)
   type Camino = [Direccion]

Definir la función

   reducido :: Camino -> Camino

tal que (reducido ds) es el camino reducido equivalente al camino ds. Por ejemplo,

   reducido []                              ==  []
   reducido [N]                             ==  [N]
   reducido [N,O]                           ==  [N,O]
   reducido [N,O,E]                         ==  [N]
   reducido [N,O,E,S]                       ==  []
   reducido [N,O,S,E]                       ==  [N,O,S,E]
   reducido [S,S,S,N,N,N]                   ==  []
   reducido [N,S,S,E,O,N]                   ==  []
   reducido [N,S,S,E,O,N,O]                 ==  [O]
   reducido (take (10^7) (cycle [N,E,O,S])) ==  []

Nótese que en el penúltimo ejemplo las reducciones son

       [N,S,S,E,O,N,O]
   --> [S,E,O,N,O]
   --> [S,N,O]
   --> [O]
-}

import Data.List

data Direccion = N | S | E | O deriving (Show, Eq)
type Camino = [Direccion]

enOposicion :: Direccion -> Direccion -> Bool
enOposicion N S = True
enOposicion S N = True
enOposicion E O = True
enOposicion O E = True
enOposicion _ _ = False

reducido :: Camino -> Camino
reducido = reducido3

reducido1 :: Camino -> Camino
reducido1 zs = aux zs []
    where aux [] ys     = reverse ys
          aux (x:xs) [] = aux xs [x]
          aux (x:xs) (y:ys) | enOposicion x y = aux xs ys
                            | otherwise       = aux xs (x:y:ys)

reducido2 :: Camino -> Camino
reducido2 = reverse . foldl' aux []
    where aux [] x = [x]
          aux (y:ys) x | enOposicion x y = ys
                       | otherwise       = (x:y:ys)

reducido3 :: Camino -> Camino
reducido3 = foldr aux []
    where aux x [] = [x]
          aux x (y:ys) | enOposicion x y = ys
                       | otherwise       = (x:y:ys)
