module Ex_20160525 where

{-

Centro de gravedad de una lista
===============================

Se dice que una lista de números xs es equilibrada si existe una posición k tal
que la suma de los elementos de xs en las posiciones menores que k es igual a
la de los elementos de xs en las posiciones mayores que k. La posición k se
llama el centro de gravedad de xs. Por ejemplo, la lista [1,3,4,5,-2,1] es
equilibrada, y su centro de gravedad es 2, ya que la suma de [1,3] es igual a
la de [5,-2,1]. En cambio, la lista [1,6,4,5,-2,1] no tiene centro de gravedad.

Definir la función

   centro :: (Num a, Eq a) => [a] -> Maybe Int

tal que (centro xs) es justo el centro de gravedad de xs, si la lista xs es
equilibrada y Nothing en caso contrario. Por ejemplo,

   centro [1,3,4,5,-2,1]           ==  Just 2
   centro [1,6,4,5,-2,1]           ==  Nothing
   centro [1,2,3,4,3,2,1]          ==  Just 3
   centro [1,100,50,-51,1,1]       ==  Just 1
   centro [1,2,3,4,5,6]            ==  Nothing
   centro [20,10,30,10,10,15,35]   ==  Just 3
   centro [20,10,-80,10,10,15,35]  ==  Just 0
   centro [10,-80,10,10,15,35,20]  ==  Just 6
   centro [0,0,0,0,0]              ==  Just 0
   centro [-1,-2,-3,-4,-3,-2,-1]   ==  Just 3

-}

import Data.List
import Data.Maybe

centro :: (Num a, Eq a) => [a] -> Maybe Int
centro = centro5


centro1 :: (Num a, Eq a) => [a] -> Maybe Int
centro1 xs = find chequea [0..length xs-1]
    where chequea k = sum (take k xs) == sum (drop (k+1) xs)

centro2 :: (Num a, Eq a) => [a] -> Maybe Int
centro2 xs = find aux [0..length xs-1]
    where aux k = let (ys, _:zs) = splitAt k xs
                  in sum ys == sum zs

centro3 :: (Num a, Eq a) => [a] -> Maybe Int
centro3 xs = aux 0 xs
    where
        s = sum xs
        aux _ [] = Nothing
        aux k (z:zs) | s - z == 2 * sum zs = Just k
                     | otherwise           = aux (k+1) zs

centro4 :: (Num a, Eq a) => [a] -> Maybe Int
centro4 xs =
    listToMaybe [ k | (k,ys,_:zs) <- zip3 [0..] (inits xs) (tails xs)
                    , sum ys == sum zs]

-- Solución oficial
centro5 :: (Num a, Eq a) => [a] -> Maybe Int
centro5 xs = aux 0 0 (sum xs) xs where
    aux _ _ _ [] = Nothing
    aux k i d (z:zs) | i == d - z = Just k
                     | otherwise  = aux (k + 1) (i + z) (d - z) zs
