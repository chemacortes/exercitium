module Ex_20161212 where

{-

Mayores que la mitad
====================

Definir la función

   mayoresMitad :: Ord a => [a] -> [a]

tal que (mayoresMitad xs) es la lista de los elementos de xs que son mayores que
la mitad de los elementos de xs, suponiendo que los elementos de xs son
distintos. Por ejemplo,

   sort (mayoresMitad [1,6,3,4])    ==  [4,6]
   sort (mayoresMitad [1,6,3,4,7])  ==  [4,6,7]
   sort (mayoresMitad [1,6,3,4,2])  ==  [3,4,6]
   length (mayoresMitad3 [1..10^6]) ==  500000

Nota: Se considera que si la lista tiene 2n+1 elementos, su mitad tiene n elementos.

-}

import Data.List (sort)

mayoresMitad :: Ord a => [a] -> [a]
mayoresMitad xs = drop mitad (sort xs)
  where mitad = length xs `div` 2
