module Ex_20160331 where

{-

Diccionario de frecuencias
==========================

Definir la función

   frecuencias :: Ord a => [a] -> Map a Int

tal que (frecuencias xs) es el diccionario formado por los elementos de xs
junto con el número de veces que aparecen en xs. Por ejemplo,

   ghci> frecuencias "sosos"
   fromList [('o',2),('s',3)]
   ghci> frecuencias (show (10^100))
   fromList [('0',100),('1',1)]
   ghci> frecuencias (take (10^6) (cycle "abc"))
   fromList [('a',333334),('b',333333),('c',333333)]
   ghci> size (frecuencias (take (10^6) (cycle [1..10^6])))
   1000000

-}

import qualified Data.List as L
import Data.Map.Strict

frecuencias :: Ord a => [a] -> Map a Int
frecuencias = frecuencias5

frecuencias1 :: Ord a => [a] -> Map a Int
frecuencias1 xs =
    fromList [(head ys, length ys) | ys <- (L.group . L.sort) xs ]

frecuencias2 :: Ord a => [a] -> Map a Int
frecuencias2 = L.foldl' aux empty
    where aux m x = insertWith (+) x 1 m

frecuencias3 :: Ord a => [a] -> Map a Int
frecuencias3 [] = empty
frecuencias3 (x:xs) = insertWith (+) x 1 (frecuencias3 xs)

frecuencias4 :: Ord a => [a] -> Map a Int
frecuencias4 xs = fromListWith (+) $ zip xs (repeat 1)

frecuencias5 :: Ord a => [a] -> Map a Int
frecuencias5 = fromListWith (+) . flip zip (repeat 1)

frecuencias6 :: Ord a => [a] -> Map a Int
frecuencias6 = fromListWith (+) . L.map (\x -> (x,1))

frecuencias7 :: Ord a => [a] -> Map a Int
frecuencias7 xs = fromListWith (+) [(x,1)|x<-xs]
