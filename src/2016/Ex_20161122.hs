module Ex_20161122 where

{-

Números consecutivos compuestos
===============================

Una serie compuesta de longitud n es una lista de n números consecutivos que son
todos compuestos. Por ejemplo, [8,9,10] y [24,25,26] son dos series compuestas
de longitud 3.

Cada serie compuesta se puede representar por el par formado por su primer y
último elemento. Por ejemplo, las dos series anteriores se pueden representar
pos (8,10) y (24,26) respectivamente.

Definir la función

   menorSerieCompuesta :: Integer -> (Integer,Integer)

tal que (menorSerieCompuesta n) es la menor serie compuesta (es decir, la que
tiene menores elementos) de longitud n. Por ejemplo,

   menorSerieCompuesta 3    ==  (8,10)
   menorSerieCompuesta 4    ==  (24,27)
   menorSerieCompuesta 5    ==  (24,28)
   menorSerieCompuesta 150  ==  (4652354,4652503)

Comprobar con QuickCheck que para n > 1, el primer elemento de
(menorSerieCompuesta n) es igual al primero de (menorSerieCompuesta (n-1))
o al primero de (menorSerieCompuesta (n+1)).

-}

import Data.Numbers.Primes
import Test.QuickCheck


menorSerieCompuesta :: Integer -> (Integer,Integer)
menorSerieCompuesta n = (m, m+n-1)
  where m = head [ p+1 | (p,q) <- zip primes (tail primes)
                       , p+n < q ]

prop_SerieCompuesta :: Positive Integer -> Property
prop_SerieCompuesta (Positive n) = n > 1 ==> m `elem` [previo, post]
  where m = fst (menorSerieCompuesta n)
        previo = fst (menorSerieCompuesta (n-1))
        post = fst (menorSerieCompuesta (n+1))
