module Ex_20161111 where

{-

Números perfectos y cojonudos
=============================

Un [número perfecto][1] es un número entero positivo que es igual a la suma de
sus divisores propios. Por ejemplo, el 28 es perfecto porque sus divisores
propios son 1, 2, 4, 7 y 14 y 1+2+4+7+14 = 28.

Un entero positivo x es un número cojonudo si existe un n tal que n > 0,
x = 2^n·(2^(n+1)-1) y 2^(n+1)-1 es primo. Por ejemplo, el 28 es cojonudo ya que
para n = 2 se verifica que 2 > 0, 28 = 2^2·(2^3-1) y 2^3-1 = 7 es primo.

Definir las funciones

   esPerfecto                     :: Integer -> Bool
   esCojonudo                     :: Integer -> Bool
   equivalenciaCojonudosPerfectos :: Integer -> Bool

tales que

    (esPerfecto x) se verifica si x es perfecto. Por ejemplo,

     esPerfecto 28  ==  True
     esPerfecto 30  ==  False

    (esCojonudo x) se verifica si x es cojonudo. Por ejemplo,

     esCojonudo 28                   ==  True
     esCojonudo 30                   ==  False
     esCojonudo 2305843008139952128  ==  True

    (equivalenciaCojonudosPerfectos n) se verifica si para todos los números x
    menores o iguales que n se tiene que x es perfecto si, y sólo si, x es
    cojonudo. Por ejemplo,

     equivalenciaCojonudosPerfectos 3000  ==  True

[1]: https://en.wikipedia.org/wiki/Perfect_number

-}

import Data.List
import Data.Numbers.Primes


esPerfecto :: Integer -> Bool
esPerfecto n = sum (divisoresPropios n) == n

divisoresPropios :: Integer -> [Integer]
divisoresPropios n = delete n . fmap product . nub . subsequences $ primeFactors n

esCojonudo1 :: Integer -> Bool
esCojonudo1 n = n `elem` [2^i * p | i <- [1..nDoses]
                                 , let p = 2^(i+1)-1, isPrime p]
    where nDoses = length . filter (==2) $ primeFactors n

esCojonudo2 :: Integer -> Bool
esCojonudo2 n = isPrime p && doses * p == n
    where doses = product . filter (==2) $ primeFactors n
          p = 2*doses-1

esCojonudo :: Integer -> Bool
esCojonudo n | length ps /= 1  = False
             | otherwise      = head ps == 2 * product ds -1
    where (ds,ps) = partition (==2) (primeFactors n)

equivalenciaCojonudosPerfectos :: Integer -> Bool
equivalenciaCojonudosPerfectos n = all (\x -> esPerfecto x == esCojonudo x) [1..n]

falla :: Integer -> Integer
falla n = head [ x | x <- [1..n], esPerfecto x /= esCojonudo x]

propios :: [Integer]
propios = filter esPerfecto [1..]

cojonudos :: [Integer]
cojonudos = filter esCojonudo [1..]
