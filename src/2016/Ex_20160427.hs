module Ex_20160427 where

{-

Mínimo número de cambios para igualar una lista
===============================================

Definir la función

   nMinimoCambios :: Ord a => [a] -> Int

tal que (nMinimoCambios xs) es el menor número de elementos de xs que hay que
cambiar para que todos sean iguales. Por ejemplo,

   nMinimoCambios [3,5,3,7,9,6]      ==  4
   nMinimoCambios [3,5,3,7,3,3]      ==  2
   nMinimoCambios "Salamanca"        ==  5
   nMinimoCambios (4 : [1..500000])  ==  499999

En el primer ejemplo, los elementos que hay que cambiar son 5, 7, 9 y 6.

-}

import Data.List

nMinimoCambios :: Ord a => [a] -> Int
nMinimoCambios xs = length xs - maximum [length ys | ys <- (group.sort) xs]
