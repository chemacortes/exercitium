module Ex_20160129 where

{-

Agrupamiento por propiedad
==========================

Definir la función

   agrupa :: (a -> Bool) -> [a] -> [[a]]

tal que (agrupa p xs) es la lista obtenida separando los elementos consecutivos
de xs que verifican la propiedad p de los que no la verifican. Por ejemplo,

   agrupa odd  [1,2,0,4,9,6,4,5,7,2]  ==  [[1],[2,0,4],[9],[6,4],[5,7],[2]]
   agrupa even [1,2,0,4,9,6,4,5,7,2]  ==  [[],[1],[2,0,4],[9],[6,4],[5,7],[2]]
   agrupa (>4) [1,2,0,4,9,6,4,5,7,2]  ==  [[],[1,2,0,4],[9,6],[4],[5,7],[2]]
   agrupa (<4) [1,2,0,4,9,6,4,5,7,2]  ==  [[1,2,0],[4,9,6,4,5,7],[2]]

Comprobar con QuickCheck que para cualquier propiedad p y cualquier lista xs,
la concatenación de (agrupa p xs) es xs; es decir,

   prop_agrupa :: Blind (Int -> Bool) -> [Int] -> Bool
   prop_agrupa (Blind p) xs =
       concat (agrupa1 p xs) == xs

Nota. Usar la librería [Test.QuickCheck.Modifiers](https://hackage.haskell.org/package/QuickCheck-2.7.6/docs/Test-QuickCheck-Modifiers.html).

-}

import Test.QuickCheck
import Test.QuickCheck.Modifiers

agrupa :: (a -> Bool) -> [a] -> [[a]]
agrupa _ [] = []
agrupa p xs = ys : agrupa (not . p) zs
    where (ys,zs) = span p xs

prop_agrupa :: Blind (Int -> Bool) -> [Int] -> Bool
prop_agrupa (Blind p) xs =
       concat (agrupa p xs) == xs
