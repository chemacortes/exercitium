module Ex_20160108 where

{-

La función indicatriz de Euler
==============================

La [indicatriz de Euler][1] (también llamada función φ de Euler) es una función 
importante en teoría de números. Si n es un entero positivo, entonces φ(n) se 
define como el número de enteros positivos menores o iguales a n y coprimos con 
n. Por ejemplo, φ(36) = 12 ya que los números menores o iguales a 36 y coprimos 
con 36 son doce: 1, 5, 7, 11, 13, 17, 19, 23, 25, 29, 31, y 35.

Definir la función

   phi :: Integer -> Integer

tal que (phi n) es igual a φ(n). Por ejemplo,

   phi 36                     ==  12
   map phi [10..20]           ==  [4,10,4,12,6,8,8,16,6,18,8]
   phi (3^10^5) `mod` (10^9)  ==  681333334

Comprobar con QuickCheck que, para todo n > 0, φ(10ⁿ) tiene n dígitos.

[1]: https://es.wikipedia.org/wiki/Funci%C3%B3n_%CF%86_de_Euler "Función φ de Euler"

-}

import Data.Numbers.Primes (primeFactors)
import Data.List (genericLength, group)
import Test.QuickCheck


phi1 :: Integer -> Integer
phi1 n = genericLength [x | x <- [1..n], gcd x n == 1]

-- Aplicando el producto de Euler 
phi :: Integer -> Integer
phi n = product [ (x - 1) * product xs | (x:xs) <- group (primeFactors n) ]


prop_phi :: Integer -> Property
prop_phi n = n > 0 ==>
    n == genericLength (show (phi (10^n)))
