module Ex_20160204 where

{-

Números primos de Hilbert
=========================

Un número de Hilbert es un entero positivo de la forma 4n+1. Los primeros 
números de Hilbert son 1, 5, 9, 13, 17, 21, 25, 29, 33, 37, 41, 45, 49, 53, 
57, 61, 65, 69, …

Un primo de Hilbert es un número de Hilbert n que no es divisible por ningún 
número de Hilbert menor que n (salvo el 1). Los primeros primos de Hilbert son 
5, 9, 13, 17, 21, 29, 33, 37, 41, 49, 53, 57, 61, 69, 73, 77, 89, 93, 97, 101, 
109, 113, 121, 129, 133, 137, …

Definir la sucesión

   primosH :: [Integer]

tal que sus elementos son los primos de Hilbert. Por ejemplo,

   take 15 primosH  == [5,9,13,17,21,29,33,37,41,49,53,57,61,69,73]
   primosH !! 20000 == 203221

-}

import Data.Numbers.Primes (isPrime, primeFactors)

-- Números de Hilbert
numerosH :: [Integer]
numerosH = [1,5..]


-- Primos de Hilbert

-- Solución 1: directa del enunciado
primosH1 :: [Integer]
primosH1 = filter esPrimoH numerosH

-- Solución 2: por propiedad de la operación módulo 4, los primos de Hilbert
-- o son primos o son subprimos con la forma (4*a+3)*(4*b+3)
primosH2 :: [Integer]
primosH2 = [ n | n <- tail numerosH, isPrime n || semiPrimoX n ]


primosH :: [Integer]
primosH = primosH2

-- Es primo de Hilbert
esPrimoH :: Integer -> Bool
esPrimoH n = all noDivideA [5,9..m]
    where noDivideA x = n `mod` x /= 0
          m = ceiling (sqrt (fromIntegral n))

-- Primo Pitagórico: primo con forma (4*n+1)
esPrimoPitagorico :: Integer -> Bool
esPrimoPitagorico n = isPrime n && (n-1) `mod` 4 == 0

-- Semiprimo: producto de dos primos
semiPrimo :: Integer -> Bool
semiPrimo n = length (primeFactors n) == 2

-- Semiprimo con forma (4*n+3)
semiPrimoX :: Integer -> Bool
semiPrimoX n = length xs == 2 && all (\x -> (x-3) `mod` 4 == 0) xs
    where xs = primeFactors n
