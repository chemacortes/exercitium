module Ex_20160119 where


{-

Relleno de matrices
===================

Dada una matriz cuyos elementos son 0 ó 1, su relleno es la matriz obtenida
haciendo iguales a 1 los elementos de las filas y de las columna que contienen
algún uno. Por ejemplo, el relleno de la matriz de la izquierda es la de la
derecha:

   0 0 0 0 0    1 0 0 1 0
   0 0 0 0 0    1 0 0 1 0
   0 0 0 1 0    1 1 1 1 1
   1 0 0 0 0    1 1 1 1 1
   0 0 0 0 0    1 0 0 1 0

Las matrices se pueden representar mediante tablas cuyos índices son pares de
enteros

   type Matriz = Array (Int,Int) Int

por ejemplo, la matriz de la izquierda de la figura anterior se define por

   ej :: Matriz
   ej = listArray ((1,1),(5,5)) [0, 0, 0, 0, 0,
                                 0, 0, 0, 0, 0,
                                 0, 0, 0, 1, 0,
                                 1, 0, 0, 0, 0,
                                 0, 0, 0, 0, 0]

Definir la función

   relleno :: Matriz -> Matriz

tal que (relleno p) es el relleno de la matriz p. Por ejemplo,

   λ> elems (relleno ej)
   [1,0,0,1,0,
    1,0,0,1,0,
    1,1,1,1,1,
    1,1,1,1,1,
    1,0,0,1,0]

-}

import Data.Array

type Matriz = Array (Int,Int) Int

ej :: Matriz
ej = listArray ((1,1),(5,5)) [0, 0, 0, 0, 0,
                              0, 0, 0, 0, 0,
                              0, 0, 0, 1, 0,
                              1, 0, 0, 0, 0,
                              0, 0, 0, 0, 0]


relleno :: Matriz -> Matriz
relleno m = m // [ ((i,j),1) | i <- [1..dx], j <- [1..dy]
                             , 1 `elem` (fila i ++ columna j) ]
    where
        ((_,_),(dx,dy)) = bounds m
        fila n    = [ m!(n,i) | i <- [1..dy]]
        columna n = [ m!(i,n) | i <- [1..dx]]

