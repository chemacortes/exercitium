module Ex_20160329 where

{-

Evaluación de expresiones aritméticas
=====================================

Las expresiones aritméticas se pueden definir mediante el siguiente tipo de dato

   data Expr  = N Int | V Char | Sum Expr Expr | Mul Expr Expr
                deriving Show

Por ejemplo, (x+3)+(7*y) se representa por

   ejExpr :: Expr
   ejExpr = Sum (Sum (V 'x') (N 3))(Mul (N 7) (V 'y'))

Definir la función

   valor :: Expr -> Maybe Int

tal que (valor e) es ‘Just v’ si la expresión e es numérica y v es su valor, o
bien ‘Nothing’ si e no es numérica. Por ejemplo:

   valor (Sum (N 7) (N 9))                            == Just 16
   valor (Sum (Sum (V 'x') (N 3))(Mul (N 7) (V 'y'))) == Nothing
   valor (Sum (Sum (N 1) (N 3))(Mul (N 7) (N 2)))     == Just 18

-}

import Control.Monad (liftM2)

data Expr  = N Int | V Char | Sum Expr Expr | Mul Expr Expr
             deriving Show

valor :: Expr -> Maybe Int
valor (N v)     = Just v
valor (V _)     = Nothing
valor (Sum x y) = (liftM2 (+)) (valor x) (valor y)
valor (Mul x y) = (liftM2 (*)) (valor x) (valor y)
