module Ex_20160205 where

{-

Factorizaciones de números de Hilbert
=====================================

Un número de Hilbert es un entero positivo de la forma 4n+1. Los primeros 
números de Hilbert son 1, 5, 9, 13, 17, 21, 25, 29, 33, 37, 41, 45, 49, 53, 57, 
61, 65, 69, …

Un primo de Hilbert es un número de Hilbert n que no es divisible por ningún 
número de Hilbert menor que n (salvo el 1). Los primeros primos de Hilbert son 
5, 9, 13, 17, 21, 29, 33, 37, 41, 49, 53, 57, 61, 69, 73, 77, 89, 93, 97, 101, 
109, 113, 121, 129, 133, 137, …

Definir la función

   factorizacionesH :: Integer -> [[Integer]]

tal que (factorizacionesH n) es la listas de primos de Hilbert cuyo producto es 
el número de Hilbert n. Por ejemplo,

  factorizacionesH  25  ==  [[5,5]]
  factorizacionesH  45  ==  [[5,9]]
  factorizacionesH 441  ==  [[9,49],[21,21]]

Comprobar con QuickCheck que todos los números de Hilbert son factorizables 
como producto de primos de Hilbert (aunque laa factorización, como para el 441, 
puede no ser única).

-}

import Test.QuickCheck
 
-- numH y primosH se obtienen del ejercicio anterior
import Ex_20160204 (numerosH, primosH)
 
factorizacionesH :: Integer -> [[Integer]]
factorizacionesH m = aux m primosH
    where
      aux 1 _   = [[]]
      aux _ []  = []
      aux n (x:xs) | x > n      = []
                   | r == 0     = [x:ys | ys <- aux d (x:xs)] ++ aux n xs
                   | otherwise  = aux n xs
        where (d,r) = n `divMod` x
 
-- Generador de 10000 números de Hilbert
genNumH :: Gen Integer
genNumH = elements (take 10000 numerosH)
 
-- Propiedad que comprueba que todo número de Hilbert es
-- factorizable en producto de primos de Hilbert
prop_numH :: Property
prop_numH = forAll genNumH ((not . null) . factorizacionesH)
