module Ex_20161206 where

{-

Sin ceros finales
=================

Definir la función

   sinCerosFinales :: Int -> Int

tal que (sinCerosFinales n) es el número obtenido eliminando los ceros finales
de n. Por ejemplo,

   sinCerosFinales 104050     == 10405
   sinCerosFinales 960000     == 96
   sinCerosFinales 100050     == 10005
   sinCerosFinales (-10050)   == -1005
   sinCerosFinales 12         == 12
   sinCerosFinales 0          == 0

Comprobar con QuickCheck que, para cualquier número entero n,

   sinCerosFinales (sinCerosFinales n) == sinCerosFinales n

-}

import Test.QuickCheck

sinCerosFinales1 :: Int -> Int
sinCerosFinales1 0 = 0
sinCerosFinales1 n | n `mod` 10 == 0 = sinCerosFinales1 (n `div` 10)
                   | otherwise       = n

sinCerosFinales :: Int -> Int
sinCerosFinales 0 = 0
sinCerosFinales n = until (\x -> mod x 10 /= 0) (`div` 10) n

prop_sinCerosFinales :: Int -> Bool
prop_sinCerosFinales n =
    sinCerosFinales (sinCerosFinales n) == sinCerosFinales n
