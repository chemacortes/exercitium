module Ex_20161215 where

{-

Posiciones de equilibrio
========================

Se dice que k es una **posición de equilibrio** de una lista xs si la suma de
los elementos de xs en las posiciones menores que k es igual a la suma de los
elementos de xs en las posiciones mayores que k. Por ejemplo, en la lista
[-7,1,5,2,-4,3,0] el 3 es una posición de equilibrio ya que -7+1+5 = -4+3+0;
también lo es el 6 ya que -7+1+5+2+(-4)+3 = 0.

Definir la función,

   equilibrios :: (Num a, Eq a) => [a] -> [Int]

tal que (equilibrios xs) es la lista de las posiciones de equilibrio de xs. Por
ejemplo,

   equilibrios [-7,1,5,2,-4,3,0]  ==  [3,6]
   equilibrios [1..10^6]          ==  []

-}

import Data.List

equilibrios1 :: (Num a, Eq a) => [a] -> [Int]
equilibrios1 xs = [length ys | (ys,zs) <- zip (inits xs) (tails xs)
                             , (not.null) zs
                             , sum ys == sum (drop 1 zs) ]


equilibrios :: (Num a, Eq a) => [a] -> [Int]
equilibrios []     = []
equilibrios (x:xs) = [ i | (izq,der,i) <- zip3 izqs ders [0..]
                         , izq == der ]
    where izqs = scanl (+) 0 (x:xs)
          ders = scanl (-) (sum xs) xs


equilibrios4 :: (Num a, Eq a) => [a] -> [Int]
equilibrios4 xs =
  [n | (n,x,y) <- zip3 [0..] (scanl1 (+) xs) (scanr1 (+) xs)
     , x == y]
