module Ex_20160202 where

{-

Ordenación por frecuencia
=========================

Definir la función

   ordPorFecuencia :: Ord a => [a] -> [a]

tal que (ordPorFecuencia xs) es la lista obtenidas ordenando los elementos de 
xs por su frecuencia, de los que aparecen menos a los que aparecen más. Por 
ejemplo,

   ordPorFecuencia "canalDePanama"  ==  "DPcelmnnaaaaa"
   ordPorFecuencia "20012016"       ==  "61122000"

-}

import Data.List
import Data.Function (on)

ordPorFecuencia1 :: Ord a => [a] -> [a]
ordPorFecuencia1 xs =
    (concat . snd . unzip . sort) [ (length ys, ys) | ys <- (group . sort) xs ]
    
ordPorFecuencia :: Ord a => [a] -> [a]
ordPorFecuencia = concat . sortBy (compare `on` length). group . sort
