module Ex_20161121 where

{-

Conmutaciones ondulantes
========================

Una lista binaria es ondulante si sus elementos son alternativamente 0 y 1. Por
ejemplo, las listas [0,1,0,1,0] y [1,0,1,0] son ondulantes.

Definir la función

   minConmutacionesOndulante :: [Int] -> Int

tal que (minConmutacionesOndulante xs) es el mínimo número de conmutaciones (es
decir, cambios de 0 a 1 o de 1 a 0) necesarias para transformar xs en una lista
ondulante. Por ejemplo,

   minConmutacionesOndulante [1,1,1]                ==  1
   minConmutacionesOndulante [0,0,0,1,0,1,0,1,1,1]  ==  2

En el primer ejemplo basta conmutar el elemento en la posición 1 para obtener
[1,0,1] y el segundo ejemplo los elementos en las posiciones 1 y 8 para obtener
[0,1,0,1,0,1,0,1,0,1].

-}

minConmutacionesOndulante1 :: [Int] -> Int
minConmutacionesOndulante1 xs = min (diferencias xs ondulante)
                                   (diferencias xs (tail ondulante))
  where diferencias xs ys = length [1 | (x,y) <- zip xs ys, x/=y]
        ondulante = cycle [0,1]


minConmutacionesOndulante :: [Int] -> Int
minConmutacionesOndulante xs = min n (length xs - n)
    where n = length [1 | (x, y) <- zip xs ondulante, x/=y]
          ondulante = cycle [0,1]
