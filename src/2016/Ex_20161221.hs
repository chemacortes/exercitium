module Ex_20161221 where

{-

Día de la semana
================

Definir la función

   dia :: Int -> Int -> Int -> String

tal que (dia d m a) es el día de la semana correspondiente al día d del mes m
del año a. Por ejemplo,

   dia 21 12 2016  ==  "Miercoles"
   dia 14  4 1936  ==  "Martes"

Nota: Este ejercicio ha sido propuesto por Miguel Ibáñez.

-}


-- Por Congruencia de Zeller (https://es.wikipedia.org/wiki/Congruencia_de_Zeller)
dia :: Int -> Int -> Int -> String
dia d m a | m <= 2    = dia d (m+12) (a-1)
          | otherwise = dias!!(h `mod` 7)
  where (j,k) = a `divMod` 100
        h = d + (26*(m+1)) `div` 10 + k + k `div` 4 + j `div` 4 + 5*j
        dias = ["Sabado","Domingo","Lunes","Martes","Miercoles","Jueves","Viernes"]
