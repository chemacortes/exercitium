module Ex_20160317 where

{-

Máxima suma de elementos consecutivos
=====================================

Definir la función

   sumaMaxima :: [Integer] -> Integer

tal que (sumaMaxima xs) es el valor máximo de la suma de elementos consecutivos
de la lista xs. Por ejemplo,

   sumaMaxima []             ==  0
   sumaMaxima [2,-2,3,-3,4]  ==  4
   sumaMaxima [-1,-2,-3]     ==  0
   sumaMaxima [2,-1,3,-2,3]  ==  5
   sumaMaxima [1,-1,3,-2,4]  ==  5
   sumaMaxima [2,-1,3,-2,4]  ==  6
   sumaMaxima [1..10^6]      ==  500000500000

Comprobar con QuickCheck que

   sumaMaxima xs == sumaMaxima (reverse xs)

-}

{-

Un ejemplo que falla en muchos casos:

    sumaMaxima [100, -1, -2, -3, 100]   == 194

-}


import Data.List
import Test.QuickCheck

sumaMaxima :: [Integer] -> Integer
sumaMaxima = sumaMaxima3

sumaMaxima1 :: [Integer] -> Integer
sumaMaxima1 = maximum . map sum . concat . map inits . tails

sumaMaxima2 :: [Integer] -> Integer
sumaMaxima2 [] = 0
sumaMaxima2 xs@(_:zs) = maximum (map sum (inits xs)) `max` sumaMaxima2 zs

sumaMaxima3 :: [Integer] -> Integer
sumaMaxima3 = aux 0
    where aux m [] = m
          aux m (x:xs) | m+x < 0   = m `max` aux 0 xs
                       | otherwise = m `max` aux (m+x) xs


prop_sumaMaxima :: [Integer] -> Bool
prop_sumaMaxima xs =
    sumaMaxima xs == sumaMaxima (reverse xs)
