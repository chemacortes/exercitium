module Ex_20161104 where

{-

Primos de Kamenetsky
====================

Un número primo se dice que es un primo de Kamenetsky si al anteponerlo
cualquier dígito se obtiene un número compuesto. Por ejemplo, el 5 es un primo
de Kamenetsky ya que 15, 25, 35, 45, 55, 65, 75, 85 y 95 son compuestos. También
lo es 149 ya que 1149, 2149, 3149, 4149, 5149, 6149, 7149, 8149 y 9149 son
compuestos.

Definir la sucesión

   primosKamenetsky :: [Integer]

tal que sus elementos son los números primos de Kamenetsky. Por ejemplo,

   take 5 primosKamenetsky  ==  [2,5,149,401,509]

-}


import Data.Numbers.Primes

primosKamenetsky :: [Integer]
primosKamenetsky = filter esKamenetsky primes
  where
    esKamenetsky n = all (not.isPrime) [read (c:show n)| c <- ['1'..'9']]
