module Ex_20160114 where

{-

Lista tautológica de literales
==============================

En lógica matemática, un literal es una fórmula atómica o su negación. Se puede 
definir por el tipo de dato

   data Literal = Atom String
                | Neg Literal
                deriving (Eq, Show)

Por ejemplo, los literales p y ¬q se representan por las expresiones (Atom “p”) 
y (Neg (Atom “q”)), respectivamente.

Una lista de literales (que se interpreta como su disyunción) es un tautología 
si, y sólo si, contiene a una fórmula atómica y su negación.

Definir la función

   tautologia :: [Literal] -> Bool

tal que (tautologia xs) se verifica si la lista de literales xs es una 
tautología. Por ejemplo,

   λ> tautologia [Atom "p", Neg (Atom "q"), Neg (Atom "p")]
   True
   λ> tautologia [Atom "p", Neg (Atom "q"), Neg (Atom "r")]
   False
   λ> tautologia [Atom "p", Neg (Atom "q"), Neg (Atom "q")]
   False

-}

data Literal = Atom String
             | Neg Literal
             deriving (Eq, Show)

tautologia1 :: [Literal] -> Bool
tautologia1 []                  = True
tautologia1 (a@(Atom _):xs)     = Neg a `elem` xs   || tautologia1 xs
tautologia1 (Neg (Atom a):xs)   = Atom a `elem` xs  || tautologia1 xs
tautologia1 (_:xs)              =                      tautologia1 xs

tautologia2 :: [Literal] -> Bool
tautologia2 xs = any aux xs
    where aux f@(Atom _) = Neg f `elem` xs
          aux _          = False
          

tautologia :: [Literal] -> Bool
tautologia xs = or [Neg p `elem` xs | p@(Atom _) <- xs]


