module Ex_20160524 where

{-

El problema de las N torres
===========================

El problema de las N torres consiste en colocar N torres en un tablero con N
filas y N columnas de forma que no haya dos torres en la misma fila ni en la
misma columna.

Cada solución del problema de puede representar mediante una matriz con ceros y
unos donde los unos representa las posiciones ocupadas por las torres y los
ceros las posiciones libres. Por ejemplo,

   ( 0 1 0 )
   ( 1 0 0 )
   ( 0 0 1 )

representa una solución del problema de las 3 torres.

Definir las funciones

   torres  :: Int -> [Matrix Int]
   nTorres :: Int -> Integer

tales que
+ (torres n) es la lista de las soluciones del problema de las n torres. Por
ejemplo,

      λ> torres 3
      [( 1 0 0 )
       ( 0 1 0 )
       ( 0 0 1 )
      ,( 1 0 0 )
       ( 0 0 1 )
       ( 0 1 0 )
      ,( 0 1 0 )
       ( 1 0 0 )
       ( 0 0 1 )
      ,( 0 1 0 )
       ( 0 0 1 )
       ( 1 0 0 )
      ,( 0 0 1 )
       ( 1 0 0 )
       ( 0 1 0 )
      ,( 0 0 1 )
       ( 0 1 0 )
       ( 1 0 0 )
      ]

    (nTorres n) es el número de soluciones del problema de las n torres. Por
    ejemplo,

      λ> nTorres 3
      6
      λ> length (show (nTorres (10^4)))
      35660

-}


import Data.Matrix
import Data.List (permutations)

torres  :: Int -> [Matrix Int]
torres = torres2

torres1  :: Int -> [Matrix Int]
torres1 n = [ fromLists xs | xs <- (permutations . toLists) i ]
    where i = identity n

torres2  :: Int -> [Matrix Int]
torres2  = map fromLists . permutations . toLists . identity

nTorres :: Int -> Integer
nTorres n = product [1..fromIntegral n]
