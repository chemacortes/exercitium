module Ex_20161230 where

{-

Segmentos comunes maximales
===========================

Los segmentos de “abcd” son

   ["","a","ab","abc","abcd","b","bc","bcd","c","cd","d"]

Los segmentos comunes de “abcd” y “axbce” son

   ["","a","b","bc","c"]

Los segmentos comunes maximales (es decir, no contenidos en otros segmentos) de
“abcd” y “axbce” son

   ["a","bc"]

Definir la función

   segmentosComunesMaximales :: Eq a => [a] -> [a] -> [[a]]

tal que (segmentosComunesMaximales xs ys) es la lista de los segmentos comunes
maximales de xs e ys. Por ejemplo,

   segmentosComunesMaximales "abcd" "axbce"  ==  ["a","bc"]


-}

import Data.List

segmentosComunesMaximales :: Eq a => [a] -> [a] -> [[a]]
segmentosComunesMaximales = (maximales . ) . segmentosComunes

segmentos :: Eq a => [a] -> [[a]]
segmentos = nub . concatMap inits . tails

segmentosComunes :: Eq a => [a] -> [a] -> [[a]]
segmentosComunes xs = filter (`isInfixOf` xs) . segmentos

maximales :: Eq a => [[a]] -> [[a]]
maximales xss = [ xs | xs <- xss, maximal xs xss]

maximal :: Eq a => [a] -> [[a]] -> Bool
maximal xs =  all (not . (xs `isInfixOf`)) . delete xs
