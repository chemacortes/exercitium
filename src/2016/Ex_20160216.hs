module Ex_20160216 where

{-

Diagonales de matrices como listas
==================================

Las matrices se pueden representar como listas de listas de la misma longitud, 
donde cada uno de sus elementos representa una fila de la matriz.

Definir la función

   diagonal :: [[a]] -> [a]

tal que (diagonal xss) es la diagonal de la matriz xss. Por ejemplo,

   diagonal [[3,5,2],[4,7,1],[6,9,0]]           ==  [3,7,0]
   diagonal [[3,5],[4,7],[6,9]]                 ==  [3,7]
   diagonal [[3,5,2],[4,7,1]]                   ==  [3,7]
   sum (diagonal (replicate 20000 [1..20000]))  ==  200010000

-}


diagonal :: [[a]] -> [a]
diagonal []  = []
diagonal xss = zipWith (!!) xss [0..n-1]
    where n = (length . head) xss
