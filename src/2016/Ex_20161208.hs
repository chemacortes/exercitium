module Ex_20161208 where

{-

Suma de los máximos de los subconjuntos
=======================================

Los subconjuntos distinto del vacío del conjunto {3, 2, 5}, junto con sus
máximos elementos, son

   {3}       su máximo es 3
   {2}       su máximo es 2
   {5}       su máximo es 5
   {3, 2}    su máximo es 3
   {3, 5}    su máximo es 5
   {2, 5}    su máximo es 5
   {3, 2, 5} su máximo es 5

Por tanto, la suma de los máximos elementos de los subconjuntos de {3, 2, 5} es
3 + 2 + 5 + 3 + 5 + 5 + 5 = 28.

Definir la función

   sumaMaximos :: [Integer] -> Integer

tal que (sumaMaximos xs) es la suma de los máximos elementos de los subconjuntos
de xs. Por ejemplo,

   sumaMaximos [3,2,5]    ==  28
   sumaMaximos [4,1,6,3]  ==  71
   sumaMaximos [1..100]   ==  125497409422594710748173617332225
   length (show (sumaMaximos [1..10^5]))  ==  30108
   sumaMaximos [1..10^5] `mod` (10^7)     ==  4490625

-}

import Data.List

sumaMaximos1 :: [Integer] -> Integer
sumaMaximos1 xs = sum [maximum ys | ys <- subsequences xs, (not.null) ys]

sumaMaximos2 :: [Integer] -> Integer
sumaMaximos2 []   = 0
sumaMaximos2 xs = x * 2^(length xs - 1) + sumaMaximos2 (xs\\[x])
  where x = maximum xs

sumaMaximos3 :: [Integer] -> Integer
sumaMaximos3 xs = aux $ sortBy (flip compare) xs -- (reverse.sort) xs
  where aux [] = 0
        aux (z:zs) = z * 2^(length zs) + aux zs

sumaMaximos4 :: [Integer] -> Integer
sumaMaximos4 xs = sum [ y*2^i | (y,i) <- zip (sort xs) [0..]]

sumaMaximos :: [Integer] -> Integer
sumaMaximos = sum . zipWith (\p x ->x * 2^p) [0..] . sort
