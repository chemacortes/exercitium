module Ex_20161116 where

{-

Máximo producto en la partición de un número
============================================

El artículo de esta semana de Antonio Roldán en su blog [Números y hoja de cálculo][1]
es [Máximo producto en la partición de un número (1)][2]

Una partición de un entero positivo n es una forma de descomponer n como suma de enteros positivos. Dos sumas se considerarán iguales si solo difieren en el orden de los sumandos. Por ejemplo, las 11 particiones de 6 (con sus correspondientes productos) son

   6 = 6                      |  6                     = 6
   6 = 5 + 1                  |  5 x 1                 = 5
   6 = 4 + 2                  |  4 x 2                 = 8
   6 = 4 + 1 + 1              |  4 x 1 x 1             = 4
   6 = 3 + 3                  |  3 x 3                 = 9
   6 = 3 + 2 + 1              |  3 x 2 x 1             = 6
   6 = 3 + 1 + 1 + 1          |  3 x 1 x 1 x 1         = 3
   6 = 2 + 2 + 2              |  2 x 2 x 2             = 8
   6 = 2 + 2 + 1 + 1          |  2 x 2 x 1 x 1         = 4
   6 = 2 + 1 + 1 + 1 + 1      |  2 x 1 x 1 x 1 x 1     = 2
   6 = 1 + 1 + 1 + 1 + 1 + 1  |  1 x 1 x 1 x 1 x 1 x 1 = 1

Se observa que el máximo producto de las particiones de 6 es 9.

Definir la función

   maximoProductoParticiones :: Int -> Int

tal que (maximoProductoParticiones n) es el máximo de los productos de las particiones de n. Por ejemplo,

   maximoProductoParticiones 4     ==  4
   maximoProductoParticiones 5     ==  6
   maximoProductoParticiones 6     ==  9
   maximoProductoParticiones 7     ==  12
   maximoProductoParticiones 8     ==  18
   maximoProductoParticiones 9     ==  27
   maximoProductoParticiones 50    ==  86093442
   maximoProductoParticiones 100   ==  7412080755407364
   maximoProductoParticiones 200   ==  61806308765265224723841283607058
   length (show (maximoProductoParticiones (10^7)))  ==  1590405

Comprobar con QuickChek que los únicos posibles factores de (maximoProductoParticiones n) son 2 y 3.

[1]: http://hojaynumeros.blogspot.com.es/ "Números y hoja de cálculo"
[2]: http://hojaynumeros.blogspot.com.es/2016/11/maximo-producto-en-la-particion-de-un.html

-}

import Test.QuickCheck
import Data.Numbers.Primes

maximoProductoParticiones :: Integer -> Integer
maximoProductoParticiones = maximoProductoParticiones3


maximoProductoParticiones1 :: Integer -> Integer
maximoProductoParticiones1 = maximum . map product . particiones

particiones :: Integer -> [[Integer]]
particiones 0 = [[]]
particiones n = [ x:xs | x <- [n,n-1..1]
                       , xs <- particiones (n-x)
                       , all (<=x) xs ]


maximoProductoParticiones2 :: Integer -> Integer
maximoProductoParticiones2 0 = 1
maximoProductoParticiones2 1 = 1
maximoProductoParticiones2 n = maximum [i * maximoProductoParticiones2 (n-i)| i <- [2..n]]

maximoProductoParticiones3 :: Integer -> Integer
maximoProductoParticiones3 0 = 1
maximoProductoParticiones3 n = maximum $ zipWith (*) [n,n-1..1] maximos

maximos :: [Integer]
maximos = map maximoProductoParticiones3 [0..]

-- aplicando la propiedad
maximoProductoParticiones4 :: Integer -> Integer
maximoProductoParticiones4 n | d == 0     = 3^k
                             | d == 1     = 3^(k-1) * 4
                             | otherwise  = 3^k * 2
    where (k, d) = divMod n 3


prop_MPP :: Positive Integer -> Bool
prop_MPP (Positive n) = all (`elem` [2,3]) m
    where m = primeFactors (maximoProductoParticiones n)
