module Ex_20160115 where

{-

Huecos maximales entre primos
=============================

El hueco de un número primo p es la distancia entre p y primo siguiente de p. 
Por ejemplo, el hueco de 7 es 4 porque el primo siguiente de 7 es 11 y 4 = 11-7. 
Los huecos de los primeros números son

   Primo Hueco
    2    1
    3    2
    7    4
   11    2

El hueco de un número primo p es maximal si es mayor que los huecos de todos 
los números menores que p. Por ejemplo, 4 es un hueco maximal de 7 ya que los 
huecos de los primos menores que 7 son 1 y 2 y ambos son menores que 4. La 
tabla de los primeros huecos maximales es

   Primo Hueco
     2    1
     3    2
     7    4
    23    6
    89    8
   113   14
   523   18
   887   20

Definir la sucesión

   primosYhuecosMaximales :: [(Integer,Integer)]

cuyos elementos son los números primos con huecos maximales junto son sus 
huecos. Por ejemplo,

   λ> take 8 primosYhuecosMaximales
   [(2,1),(3,2),(7,4),(23,6),(89,8),(113,14),(523,18),(887,20)]
   λ> primosYhuecosMaximales !! 20
   (2010733,148)
   
-}

import Data.Numbers.Primes

primosYhuecosMaximales :: [(Integer,Integer)]
primosYhuecosMaximales = maximales primosYhuecos 0

primosYhuecos :: [(Integer,Integer)]
primosYhuecos = zipWith (\p q -> (p, q-p)) primes (tail primes)

maximales :: [(Integer,Integer)]-> Integer -> [(Integer,Integer)]
maximales ((p,r):xs) d | r > d      = (p,r):maximales xs r
                       | otherwise  =       maximales xs d


primosYhuecosMaximales2 :: [(Integer,Integer)]
primosYhuecosMaximales2 = aux primosYhuecos
    where aux ((x,y):ps) = (x,y) : aux (dropWhile (\(a,b) -> b <= y) ps)
 
-- 3ª solución
-- ===========
 
primosYhuecosMaximales3 :: [(Integer,Integer)]
primosYhuecosMaximales3 = aux 0 primes
    where aux n (x:y:zs) | y-x > n   = (x,y-x) : aux (y-x) (y:zs)
                         | otherwise = aux n (y:zs)
