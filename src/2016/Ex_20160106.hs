module Ex_20160106 where

{-

Sucesión de suma de cuadrados de los dígitos
============================================

Definir la función

   sucSumaCuadradosDigitos :: Integer -> [Integer]

tal que (sucSumaCuadradosDigitos n) es la sucesión cuyo primer término es n y 
los restantes se obtienen sumando los cuadrados de los dígitos de su término 
anterior. Por ejemplo,

   λ> take 20 (sucSumaCuadradosDigitos 2016)
   [2016,41,17,50,25,29,85,89,145,42,20,4,16,37,58,89,145,42,20,4]
   λ> take 20 (sucSumaCuadradosDigitos 1976)
   [1976,167,86,100,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
   λ> sucSumaCuadradosDigitos 2016 !! (10^8)
   145

-}

-- REF: https://en.wikipedia.org/wiki/Happy_number

import qualified Data.Map as Map

sucSumaCuadradosDigitos1 :: Integer -> [Integer]
sucSumaCuadradosDigitos1 = iterate sumaCuadradosDigitos

sumaCuadradosDigitos :: Integer -> Integer
sumaCuadradosDigitos n = sum [read [d] ^ 2 | d <- show n]


sucSumaCuadradosDigitos2 :: Integer -> [Integer]
sucSumaCuadradosDigitos2 n =
    n : [sumaCuadradosDigitos x | x <- sucSumaCuadradosDigitos2 n]

-- Una intentona de hacer sumaCuadradosDigitos usando Maps
recode :: Map.Map Char Integer
recode = Map.fromList [(d, read [d] ^ 2)| d <- ['0'..'9']]

sumaCuadradosDigitos2 :: Integer -> Integer
sumaCuadradosDigitos2 n = sum $ map ((Map.!) recode) (show n)

-- Con truco (conocemos los ciclos que se producen)
sucSumaCuadradosDigitos3 :: Integer -> [Integer]
sucSumaCuadradosDigitos3 1  = repeat 1
sucSumaCuadradosDigitos3 89 = cycle [89,145,42,20,4,16,37,58]
sucSumaCuadradosDigitos3 n  = 
    n : sucSumaCuadradosDigitos3 (sumaCuadradosDigitos n)


-- SOLUCIÓN OFICIAL

sucSumaCuadradosDigitos :: Integer -> [Integer]
sucSumaCuadradosDigitos n = xs ++ cycle ys
    where (xs,ys) = sucCompactaSumaCuadradosDigitos n
 
-- (sucCompactaSumaCuadradosDigitos n) es el par formado por la parte
-- pura y la periódica de (sucSumaCuadradosDigitos n). Por ejemplo, 
--    λ> sucCompactaSumaCuadradosDigitos 2016
--    ([2016,41,17,50,25,29,85],[89,145,42,20,4,16,37,58])
--    λ> sucCompactaSumaCuadradosDigitos 1976
--    ([1976,167,86,100],[1])
sucCompactaSumaCuadradosDigitos :: Integer -> ([Integer],[Integer])
sucCompactaSumaCuadradosDigitos = 
    partePuraPeriodica . sucSumaCuadradosDigitos1
 
-- (partePuraPeriodica xs) es el par formado por la parte pura y la
-- periódica de xs. Por ejemplo,
--    λ> partePuraPeriodica (sucSumaCuadradosDigitos 2016)
--    ([2016,41,17,50,25,29,85],[89,145,42,20,4,16,37,58])
--    λ> partePuraPeriodica (sucSumaCuadradosDigitos 1976)
--    ([1976,167,86,100],[1])
partePuraPeriodica :: [Integer] -> ([Integer],[Integer])
partePuraPeriodica = aux []
    where aux as (b:bs) | b `elem` as = span (/=b) (reverse as)
                        | otherwise = aux (b:as) bs


