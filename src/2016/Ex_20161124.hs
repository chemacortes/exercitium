module Ex_20162224 where

{-

Menor potencia de 2 comenzando un número dado
=============================================

Definir las siguientes funciones

   potenciasDe2  :: Integer -> [Integer]
   menorPotenciaDe2 :: Integer -> Integer

tales que

    (potenciasDe2 a) es la lista de las potencias de 2 que comienzan por a. Por ejemplo,

     λ> take 5 (potenciasDe2 3)
     [32,32768,33554432,34359738368,35184372088832]
     λ> take 2 (potenciasDe2 102)
     [1024,102844034832575377634685573909834406561420991602098741459288064]

    (menorPotenciaDe2 a) es la menor potencia de 2 que comienza con el número a. Por ejemplo,

     λ> menorPotenciaDe2 10
     1024
     λ> menorPotenciaDe2 25
     256
     λ> menorPotenciaDe2 62
     6277101735386680763835789423207666416102355444464034512896
     λ> menorPotenciaDe2 425
     42535295865117307932921825928971026432
     λ> menorPotenciaDe2 967140655691
     9671406556917033397649408
     λ> [menorPotenciaDe2 a | a <- [1..10]]
     [1,2,32,4,512,64,70368744177664,8,9007199254740992,1024]

Comprobar con QuickCheck que, para todo entero positivo a, existe una potencia de 2 que empieza por a.

-}

import Data.List (isPrefixOf)
import Test.QuickCheck
import Data.Bits

potenciasDe2_1  :: Integer -> [Integer]
potenciasDe2_1 n = [ p | x <- [0..]
                       , let p = 2^x
                       , show n `isPrefixOf` show p ]

potenciasDe2_2  :: Integer -> [Integer]
potenciasDe2_2 n = filter aux (iterate (*2) 1)
  where aux = (show n `isPrefixOf`) . show


potenciasDe2  :: Integer -> [Integer]
potenciasDe2 n = filter aux (iterate (`shift`1) 1)
  where aux = (show n `isPrefixOf`) . show

menorPotenciaDe2 :: Integer -> Integer
menorPotenciaDe2 = head . potenciasDe2

prop_potenciasDe2 :: Positive Integer -> Bool
prop_potenciasDe2 (Positive n) = (not.null) (potenciasDe2 n)
