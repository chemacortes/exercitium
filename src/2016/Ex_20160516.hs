module Ex_20160516 where

{-

Sucesión duplicadora
====================

Para cada entero positivo n, existe una única sucesión que empieza en 1,
termina en n y en la que cada uno de sus elementos es el doble de su anterior
o el doble más uno. Dicha sucesión se llama la sucesión duplicadora de n. Por
ejemplo, la sucesión duplicadora de 13 es [1, 3, 6, 13], ya que

    3 = 2*1 +1
    6 = 2*3
   13 = 2*6 +1

Definir la función

   duplicadora :: Integer -> [Integer]

tal que (duplicadora n) es la sucesión duplicadora de n. Por ejemplo,

   duplicadora 13                   ==  [1,3,6,13]
   duplicadora 17                   ==  [1,2,4,8,17]
   length (duplicadora (10^40000))  ==  132878

-}


duplicadora :: Integer -> [Integer]
duplicadora = duplicadora4

duplicadora1 :: Integer -> [Integer]
duplicadora1 1 = [1]
duplicadora1 n | odd n     = duplicadora1 ((n-1) `div` 2) ++ [n]
               | otherwise = duplicadora1 (n `div` 2) ++ [n]

duplicadora2 :: Integer -> [Integer]
duplicadora2 n = aux n []
    where aux 1 xs = 1:xs
          aux n xs | odd n     = aux ((n-1) `div` 2) (n:xs)
                   | otherwise =  aux (n `div` 2) (n:xs)

duplicadora3 :: Integer -> [Integer]
duplicadora3 n = aux n []
    where aux 1 xs = 1:xs
          aux n xs = aux (n `div` 2) (n:xs)

duplicadora4 :: Integer -> [Integer]
duplicadora4 = reverse . takeWhile (/=0) . iterate (`div` 2)
