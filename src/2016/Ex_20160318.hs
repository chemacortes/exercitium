module Ex_20160318 where

{-

Comportamiento del último dígito en primos consecutivos
=======================================================

El pasado 11 de marzo se ha publicado el artículo *Unexpected biases in the
distribution of consecutive primes* en el que muestra que los números primos
repelen a otros primos que terminan en el mismo dígito.

La lista de los últimos dígitos de los 30 primeros números es

   [2,3,5,7,1,3,7,9,3,9,1,7,1,3,7,3,9,1,7,1,3,9,3,9,7,1,3,7,9,3]

Se observa que hay 6 números que su último dígito es un 1 y, de sus
consecutivos, 4 terminan en 3 y 2 terminan en 7.

Definir la función

   distribucionUltimos :: Int -> M.Matrix Integer

tal que (distribucionUltimos n) es la matriz cuyo elemento (i,j) indica cuántos
de los n primeros números primos terminan en i y su siguiente número primo
termina en j. Por ejemplo,

   λ> distribucionUltimos 30
   ( 0 0 4 0 0 0 2 0 0 )
   ( 0 0 1 0 0 0 0 0 0 )
   ( 0 0 0 0 1 0 3 0 4 )
   ( 0 0 0 0 0 0 0 0 0 )
   ( 0 0 0 0 0 0 1 0 0 )
   ( 0 0 0 0 0 0 0 0 0 )
   ( 4 0 1 0 0 0 0 0 2 )
   ( 0 0 0 0 0 0 0 0 0 )
   ( 2 0 3 0 0 0 1 0 0 )

   λ> distribucionUltimos (10^5)
   ( 4104    0 7961    0    0    0 8297    0 4605 )
   (    0    0    1    0    0    0    0    0    0 )
   ( 5596    0 3604    0    1    0 7419    0 8387 )
   (    0    0    0    0    0    0    0    0    0 )
   (    0    0    0    0    0    0    1    0    0 )
   (    0    0    0    0    0    0    0    0    0 )
   ( 6438    0 6928    0    0    0 3627    0 8022 )
   (    0    0    0    0    0    0    0    0    0 )
   ( 8830    0 6513    0    0    0 5671    0 3995 )

Nota: Se observa cómo se “repelen” ya que en las filas del 1, 3, 7 y 9 el menor
elemento es el de la diagonal.

-}

import Data.Numbers.Primes
import Data.Matrix as M
import Data.List

distribucionUltimos :: Int -> M.Matrix Integer
distribucionUltimos = distribucionUltimos1

distribucionUltimos1 :: Int -> M.Matrix Integer
distribucionUltimos1 = foldl f z . ultimosConsecutivos
    where f p (idx, x) = setElem x idx p
          z = M.zero 9 9

-- serie con el último dígito de cada número primo
ultimos :: Int -> [Int]
ultimos n = take n $ map (`mod`10) primes

-- combinaciones de pares con número de apariciones
ultimosConsecutivos :: Int -> [((Int,Int), Integer)]
ultimosConsecutivos n = [(head xs, genericLength xs)| xs <- yss]
    where ys = ultimos n
          yss = group . sort $ zip ys (tail ys)


distribucionUltimos2 :: Int -> M.Matrix Integer
distribucionUltimos2 n = M.matrix 9 9 (\idx -> genericLength $ filter (==idx) yss)
    where ys = ultimos n
          yss = zip ys (tail ys)




