module Ex_20160113 where

{-

Puntos visibles en la cuadrícula de un plano
============================================

La cuadrícula entera de lado n, Cₙ, es el conjunto de los puntos (x,y) donde x 
e y son números enteros tales que 1 ≤ x, y ≤ n.

Un punto (x,y) de Cₙ es visible desde el origen si el máximo común divisor de x 
e y es 1. Por ejemplo, el punto (4,6) no es visible porque está ocultado por el 
(2,3); en cambio, el (2,3) sí es visible.

El conjunto de los puntos visibles en la cuadrícula entera de lado 6 son (1,1), 
(1,2), (1,3), (1,4), (1,5), (1,6), (2,1), (2,3), (2,5), (3,1), (3,2), (3,4), 
(3,5), (4,1), (4,3), (4,5), (5,1), (5,2), (5,3), (5,4), (5,6), (6,1) y (6,5).

Definir la función

   nVisibles :: Integer -> Integer

tal que (nVisibles n) es el número de los puntos visibles en la cuadrícula de 
lado n.Por ejemplo,

   nVisibles 6       ==  23
   nVisibles 10      ==  63
   nVisibles 100     ==  6087
   nVisibles 1000    ==  608383
   nVisibles 10000   ==  60794971
   nVisibles 100000  ==  6079301507

-}
 
import Data.List

nVisibles1 :: Integer -> Integer
nVisibles1 n = genericLength [(i,j) | i <- [1..n]
                                    , j <- [1..n]
                                    , gcd i j == 1]

nVisibles2 :: Integer -> Integer
nVisibles2 n = 1 + 2 * genericLength [(i,j) | i <- [1..n]
                                            , j <- [1..(i-1)]
                                            , gcd i j == 1]
