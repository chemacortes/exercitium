module Ex_20161110 where

{-

Subconjuntos acotados
=====================

Definir la función

   subconjuntosAcotados :: [a] -> Int -> [[a]]

tal que (subconjuntosAcotados xs k) es la lista de los subconjuntos de xs con k
elementos como máximo. Por ejemplo,

   λ> subconjuntosAcotados "abcd" 1
   ["a","b","c","d",""]
   λ> subconjuntosAcotados "abcd" 2
   ["ab","ac","ad","a","bc","bd","b","cd","c","d",""]
   λ> subconjuntosAcotados "abcd" 3
   ["abc","abd","ab","acd","ac","ad","a",
    "bcd","bc","bd","b","cd","c","d",""]
   λ> length (subconjuntosAcotados [1..1000] 2)
   500501
   λ> length (subconjuntosAcotados2 [1..2000] 2)
   2001001

-}


subconjuntosAcotados :: [a] -> Int -> [[a]]
subconjuntosAcotados _  0     = [[]]
subconjuntosAcotados xs 1     = [[x] | x <- xs] ++ [[]]
subconjuntosAcotados [] _     = [[]]
subconjuntosAcotados (x:xs) k =  [x:ys | ys <- subconjuntosAcotados xs (k-1)]
                              ++ subconjuntosAcotados xs k
