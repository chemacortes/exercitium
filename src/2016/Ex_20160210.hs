module Ex_20160210 where

{-

Eliminación de espacios extremos
================================

Definir la función

   sinEspaciosExtremos :: String -> String

tal que (sinEspaciosExtremos cs) es la cadena obtenida eliminando los espacios 
blancos de los extremos de la cadena cs. Por ejemplo,

   λ> sinEspaciosExtremos "   El lunes a las 12:30.     "
   "El lunes a las 12:30."

-}


sinEspaciosExtremos :: String -> String
sinEspaciosExtremos  = foldr aux [] . dropWhile (==' ')
    where aux ' ' [] = []
          aux x xs   = x:xs
