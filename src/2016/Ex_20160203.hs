module Ex_20160203 where

{-

La sucesión de Thue-Morse
=========================

La serie de Thue-Morse comienza con el término [0] y sus siguientes términos se 
construyen añadiéndole al anterior su complementario. Los primeros términos de 
la serie son

   [0]
   [0,1]
   [0,1,1,0]
   [0,1,1,0,1,0,0,1]
   [0,1,1,0,1,0,0,1,1,0,0,1,0,1,1,0]

De esta forma se va formando una sucesión

   0,1,1,0,1,0,0,1,1,0,0,1,0,1,1,0,...

que se conoce como la sucesión de Thue-Morse.

Definir la sucesión

   sucThueMorse :: [Int]

cuyos elementos son los de la sucesión de Thue-Morse. Por ejemplo,

   λ> take 30 sucThueMorse
   [0,1,1,0,1,0,0,1,1,0,0,1,0,1,1,0,1,0,0,1,0,1,1,0,0,1,1,0,1,0]
   λ> map (sucThueMorse4 !!) [1234567..1234596] 
   [1,1,0,0,1,0,1,1,0,1,0,0,1,0,1,1,0,0,1,1,0,1,0,0,1,1,0,0,1,0]
   λ> map (sucThueMorse4 !!) [4000000..4000030] 
   [1,0,0,1,0,1,1,0,0,1,1,0,1,0,0,1,0,1,1,0,1,0,0,1,1,0,0,1,0,1,1]

Comprobar con QuickCheck que si s(n) representa el término n-ésimo de la 
sucesión de Thue-Morse, entonces

   s(2n)   = s(n)
   s(2n+1) = 1 - s(n)
   
-}

import Test.QuickCheck


sucThueMorse :: [Int]
sucThueMorse = 0 : concat yss
    where xss = [0] : zipWith (++) xss yss
          yss = [1] : zipWith (++) yss xss

prop_ThueMorse :: Positive Int -> Bool
prop_ThueMorse n =  sucThueMorse!!(2*n') == sn
                 && sucThueMorse!!(2*n'+1) == 1 - sn
    where n' = getPositive n
          sn = sucThueMorse!!n'
