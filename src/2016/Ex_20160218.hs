module Ex_20160218 where

{-

Cantidad de números Pentanacci impares
======================================

Los números de Pentanacci se definen mediante las ecuaciones

   P(0) = 0
   P(1) = 1
   P(2) = 1
   P(3) = 2
   P(4) = 4
   P(n) = P(n-1) + P(n-2) + P(n-3) + P(n-4) + P(n-5), si n > 4

Los primeros números de Pentanacci son

  0, 1, 1, 2, 4, 8, 16, 31, 61, 120, 236, 464, 912, 1793, 3525, ...

Se obseeva que

    hasta P(5) hay 1 impar: el 1 (aunque aparece dos veces);
    hasta P(7) hay 2 impares distintos: 1 y 31;
    hasta P(10) hay 3 impares distintos: 1, 31 y 61;
    hasta P(15) hay 5 impares distintos: 1, 31 y 61, 1793 y 3525.

Definir la función

   nPentanacciImpares :: Integer -> Integer

tal que (nPentanacciImpares n) es la cantidad de números impares distintos 
desde P(0) hasta P(n). Por ejemplo,

   nPentanacciImpares 5                             ==  1
   nPentanacciImpares 7                             ==  2
   nPentanacciImpares 10                            ==  3
   nPentanacciImpares 15                            ==  5
   nPentanacciImpares 100                           ==  33
   nPentanacciImpares 1000                          ==  333
   nPentanacciImpares 10000                         ==  3333
   nPentanacciImpares (10^(10^6)) `mod` (10^9)      ==  333333333
   length (show (nPentanacciImpares (10^(10^6))))   ==  1000000

-}

import Data.List

import Ex_20160217 (pentanacci)

nPentanacciImpares :: Integer -> Integer
nPentanacciImpares = nPentanacciImpares3
----------------------------------------

nPentanacciImpares1 :: Integer -> Integer
nPentanacciImpares1 n = genericLength . nub . filter odd
                      $ genericTake (n+1) pentanacci

nPentanacciImpares2 :: Integer -> Integer
nPentanacciImpares2 n = (genericLength . filter (==False) 
                      $ genericTake (n+1) pentanacciParidad ) - 1

pentanacciParidad :: [Bool]
pentanacciParidad = True:False:False:True:True: map (f . take 5) (tails pentanacciParidad)
    where f = even . length . filter (==False)

nPentanacciImpares3 :: Integer -> Integer
nPentanacciImpares3 0 = 0
nPentanacciImpares3 1 = 1
nPentanacciImpares3 2 = 1
nPentanacciImpares3 n = 2 * d - 1 + r `min` 2
    where (d, r) = n `divMod` 6

    
nPentanacciImpares4 :: Integer -> Integer
nPentanacciImpares4 0 = 0
nPentanacciImpares4 1 = 1
nPentanacciImpares4 2 = 1
nPentanacciImpares4 n = 2 * d - 1 + case r of { 0 -> 0; 1 -> 1; _ -> 2 }
    where (d, r) = n `divMod` 6
