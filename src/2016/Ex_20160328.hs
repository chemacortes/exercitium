module Ex_20160328 where


{-

Posiciones de máximos locales
=============================

Los vectores se definen usando tablas como sigue:

   type Vector a = Array Int a

Un elemento de un vector es un máximo local si no tiene ningún elemento
adyacente mayor o igual que él.

Definir la función

   posMaxVec :: Ord a => Vector a -> [Int]

tal que (posMaxVec p) devuelve las posiciones del vector p en las que p tiene
un máximo local. Por ejemplo,

   posMaxVec (listArray (1,6) [3,2,6,7,5,3]) == [1,4]
   posMaxVec (listArray (1,2) [5,5])         == []
   posMaxVec (listArray (1,1) [5])           == [1]

-}

import Data.Array

type Vector a = Array Int a

posMaxVec :: Ord a => Vector a -> [Int]
posMaxVec p = [i | (i,x) <- assocs p
                 , i == a || p!(i-1) < x
                 , i == b || p!(i+1) < x ]
    where (a,b) = bounds p
