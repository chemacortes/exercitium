module Ex_20160120 where

{-

Antiimágenes en una función creciente
=====================================

Definir la función

   antiimagen :: (Int -> Int) -> Int -> Maybe Int

tal que `(antiimagen f y)` es justo el `x` tal que `f(x) = y`, si `y` pertenece
a la imagen de la función creciente `f`, o nada, en caso contrario. Por ejemplo,

   antiimagen (^2) 25  ==  Just 5
   antiimagen (^3) 25  ==  Nothing

Nota. Se supone que f está definida sobre los números naturales.

-}

import Data.List

antiimagen1 :: (Int -> Int) -> Int -> Maybe Int
antiimagen1 f y = find ((==y).f)
                $ takeWhile ((<=y).f)[1..]

antiimagen :: (Int -> Int) -> Int -> Maybe Int
antiimagen f y = lookup y . takeWhile ((y>=).fst) 
               $ [(f x, x) | x <- [1..]]

-- Aplicando búsqueda dicotómica
antiimagen2 :: (Integer -> Integer) -> Integer -> Maybe Integer
antiimagen2 f y = bisect f y (lim `div` 2) lim
    where lim = head [ x | x <- iterate (2*) 1, f x > y ]

bisect :: (Integer -> Integer) -> Integer -> Integer -> Integer -> Maybe Integer
bisect f y a b | f a == y   = Just a
               | a >= b     = Nothing
               | f m <= y   = bisect f y m b
               | otherwise  = bisect f y (a+1) m
    where m = (a + b) `div` 2
