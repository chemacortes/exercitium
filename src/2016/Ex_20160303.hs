module Ex_20160303 where

{-

Números N cuyos cuadrados tienen dos copias de cada dígito de N
===============================================================

La [sucesión A114258 de la OEIS][1] está formada por los números n tales que el
número de ocurrencia de cada dígito d de n en n² es el doble del número de
ocurrencia de d en n. Por ejemplo, 72576 es un elemento de A114258 porque tiene
un 2, un 5, un 6 y dos 7 y su cuadrado es 5267275776 que tiene exactamente
dos 2, dos 5, dos 6 y cuatro 7.

Un número es especial si pertenece a la sucesión A114258.

Definir la sucesión

   especiales :: [Integer]

cuyos elementos son los números especiales. Por ejemplo,

   take 5 especiales  ==  [72576,406512,415278,494462,603297]

[1]: https://oeis.org/A114258

-}

import Data.List

especiales :: [Integer]
especiales = filter esEspecial [1..]
    where esEspecial n = null $ show n \\ (show (n*n) \\ show n)
