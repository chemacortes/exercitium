module Ex_20160418 where

{-

Mínima diferencia entre elementos de una lista
==============================================

Definir la función

   minimaDiferencia :: (Num a, Ord a) => [a] -> a

tal que (minimaDiferencia xs) es el menor valor absoluto de las diferencias
entre todos los pares de elementos de xs (que se supone que tiene al menos 2
elementos). Por ejemplo,

   minimaDiferencia [1,5,3,19,18,25]  ==  1
   minimaDiferencia [30,5,20,9]       ==  4
   minimaDiferencia [30,5,20,9,5]     ==  0
   minimaDiferencia [1..10^6]         ==  1

En el primer ejemplo la menor diferencia es 1 y se da entre los elementos 19 y
18; en el 2ª es 4 entre los elementos 5 y 9 y en la 3ª es 0 porque el elemento
5 está repetido.

-}

import Data.List

minimaDiferencia :: (Num a, Ord a) => [a] -> a
minimaDiferencia = minimaDiferencia3

minimaDiferencia1 :: (Num a, Ord a) => [a] -> a
minimaDiferencia1 xs = minimum [ abs (x-y) | (x:ys) <- tails xs, y <- ys ]

minimaDiferencia2 :: (Num a, Ord a) => [a] -> a
minimaDiferencia2 xs = minimum [ abs (x-y) | (x:y:_) <- (map (take 2) . tails . sort) xs ]

minimaDiferencia3 :: (Num a, Ord a) => [a] -> a
minimaDiferencia3 xs = minimum $ zipWith diferencia xs' (tail xs')
    where xs' = sort xs

diferencia :: (Num a, Ord a) => a -> a -> a
diferencia x y = abs (x-y)
