module Ex_20160226 where

{-

Particiones en sumas de cuadrados
=================================

Definir las funciones

   particionesCuadradas        :: Integer -> [[Integer]]
   nParticionesCuadradas       :: Integer -> Integer
   graficaParticionesCuadradas :: Integer -> IO ()

tales que

-   (particionesCuadradas n) es la listas de conjuntos de cuadrados cuya suma
    es n. Por ejemplo,

      particionesCuadradas 3  ==  [[1,1,1]]
      particionesCuadradas 4  ==  [[4],[1,1,1,1]]
      particionesCuadradas 9  ==  [[9],[4,4,1],[4,1,1,1,1,1],[1,1,1,1,1,1,1,1,1]]

-   (nParticionesCuadradas n) es el número de conjuntos de cuadrados cuya suma
    es n. Por ejemplo,

      nParticionesCuadradas 3    ==  1
      nParticionesCuadradas 4    ==  2
      nParticionesCuadradas 9    ==  4
      nParticionesCuadradas 50   ==  104
      nParticionesCuadradas 100  ==  1116
      nParticionesCuadradas 200  ==  27482

-   (graficaParticionesCuadradas n) dibuja la gráfica de la sucesión

      [nParticionesCuadradas k | k <- [0..n]]

Por ejemplo, con (graficaParticionesCuadradas 100) se obtiene

![Particiones en sumas de cuadrados](./Ex_20160226_Particiones_en_sumas_de_cuadrados.png)

-}

import Data.List
import Graphics.Gnuplot.Simple

particionesCuadradas :: Integer -> [[Integer]]
particionesCuadradas = particionesCuadradas2

particionesCuadradas1 :: Integer -> [[Integer]]
particionesCuadradas1 0 = [[]]
particionesCuadradas1 n = nub . map (reverse . sort)
                       $ [x:ys | x <- xs, ys <- particionesCuadradas1 (n-x)]
    where
        xs = reverse $ takeWhile (<=n) cuadrados
        cuadrados = map (^2) [2..]

particionesCuadradas2 :: Integer -> [[Integer]]
particionesCuadradas2 n = aux cuadrados n
    where
        cuadrados = reverse $ takeWhile (<=n) [x^2 | x <- [1..]]
        aux []  _    = [[]]
        aux _   0    = [[]]
        aux [1] r    = [genericReplicate r 1]
        aux (x:xs) r | x <= r =  map (x:) (aux (x:xs) (r-x)) ++ aux xs r
                     | otherwise = aux xs r

nParticionesCuadradas :: Integer -> Integer
nParticionesCuadradas = genericLength . particionesCuadradas

graficaParticionesCuadradas :: Integer  -> IO ()
graficaParticionesCuadradas n =
    plotList [Key Nothing] [nParticionesCuadradas k | k <- [0..n]]
