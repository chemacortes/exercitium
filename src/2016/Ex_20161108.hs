module Ex_20161108 where

{-

Conjetura de Rassias
====================

El artículo de esta semana del blog [Números y hoja de cálculo][1] está dedicado
a la [Conjetura de Rassias][2]. Dicha conjetura afirma que

    Para cada número primo p > 2 existen dos primos a y b, con a < b, tales que
    (p-1)a = b+1

Dado un primo p > 2, los pares de Rassia de p son los pares de primos (a,b), con
a < b, tales que (p-1)a = b+1. Por ejemplo, (2,7) y (3,11) son pares de Rassia
de 5 ya que

    2 y 7 son primos, 2 < 7 y (5-1)·2 = 7+1
    3 y 11 son primos, 3 < 11 y (5-1)·3 = 11+1

Definir las siguientes funciones

   paresRassias     :: Integer -> [(Integer,Integer)]
   conjeturaRassias :: Integer -> Bool

tales que

    (paresRassias p) es la lista de los pares de Rassias del primo p (que se
    supone que es mayor que 2). Por ejemplo,

     take 3 (paresRassias 5)    == [(2,7),(3,11),(5,19)]
     take 3 (paresRassias 1229) == [(71,87187),(113,138763),(191,234547)]

    (conjeturaRassia x) se verifica si para todos los primos menores que x (y
    mayores que 2) se cumple la conjetura de Rassia. Por ejemplo,

     conjeturaRassias (10^5)  ==  True

[1]: http://hojaynumeros.blogspot.com.es/ "Números y hoja de cálculo"
[2]: http://hojaynumeros.blogspot.com.es/2016/11/conjetura-de-rassias.html

-}

import Data.Numbers.Primes

paresRassias :: Integer -> [(Integer,Integer)]
paresRassias p = [(a,b) | a <- primes, let b = (p-1)*a - 1, isPrime b]

conjeturaRassias :: Integer -> Bool
conjeturaRassias n = and [(not.null.paresRassias) x | x <- takeWhile (<n) (tail primes)]
