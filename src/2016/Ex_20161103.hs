module Ex_20161103 where

{-

Números de Harshad hereditarios
===============================

Un número de Harshad es un entero divisible entre la suma de sus dígitos. Por
ejemplo, 201 es un número de Harshad porque es divisible por 3 (la suma de sus
dígitos). Cuando se elimina el último dígito de 201 se obtiene 20 que también es
un número de Harshad. Cuando se elimina el último dígito de 20 se obtiene 2 que
también es un número de Harshad. Los números como el 201 que son de Harshad y que
los números obtenidos eliminando sus últimos dígitos siguen siendo de Harshad se
llaman números de Harshad hereditarios por la derecha. Definir la función

   numeroHHD :: Int -> Bool

tal que (numeroHHD n) se verifica si n es un número de Harshad hereditario por
la derecha. Por ejemplo,

   numeroHHD 201  ==  True
   numeroHHD 140  ==  False
   numeroHHD 1104 ==  False

Calcular el mayor número de Harshad hereditario por la derecha con tres dígitos.

-}

import Data.List (inits)

numeroHHD :: Int -> Bool
numeroHHD n = and [aux xs | xs <- inits (show n), length xs > 1]
  where suma xs = sum [read [c]| c <- xs]
        aux xs = read xs `mod` suma xs == 0

esNumeroHD :: Int -> Bool
esNumeroHD n = n `mod` m == 0
  where m = sum [read [c]| c <- show n]

maxNumeroHHD :: Int
maxNumeroHHD = head $ filter numeroHHD [999,998..100]
