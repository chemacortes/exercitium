module Ex_20160311 where

{-

Máxima suma en una matriz
=========================

Las matrices puede representarse mediante tablas cuyos índices son pares de
números naturales:

   type Matriz = Array (Int,Int) Int

Definir la función

   maximaSuma :: Matriz -> Int

tal que (maximaSuma p) es el máximo de las sumas de las listas de elementos de
la matriz p tales que cada elemento pertenece sólo a una fila y a una columna.
Por ejemplo,

   ghci> maximaSuma (listArray ((1,1),(3,3)) [1,2,3,8,4,9,5,6,7])
   17

ya que las selecciones, y sus sumas, de la matriz

   |1 2 3|
   |8 4 9|
   |5 6 7|

son

   [1,4,7] --> 12
   [1,9,6] --> 16
   [2,8,7] --> 17
   [2,9,5] --> 16
   [3,8,6] --> 17
   [3,4,5] --> 12

Hay dos selecciones con máxima suma: [2,8,7] y [3,8,6].

-}

import Data.Array
import Data.List (permutations)

type Matriz = Array (Int,Int) Int

maximaSuma :: Matriz -> Int
maximaSuma p = maximum [ sum (map (p!) xs) | xs <- xss ]
    where (_,(a,b)) = bounds p
          xss = [zip [1..a] xs | xs <- permutations [1..b]]
