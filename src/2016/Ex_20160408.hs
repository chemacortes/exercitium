module Ex_20160408 where

{-

Raíz entera
===========

Definir la función

   raizEnt :: Integer -> Integer -> Integer

tal que (raizEnt x n) es la raíz entera n-ésima de x; es decir, el mayor número
entero y tal que y^n <= x. Por ejemplo,

   raizEnt  8 3      ==  2
   raizEnt  9 3      ==  2
   raizEnt 26 3      ==  2
   raizEnt 27 3      ==  3
   raizEnt (10^50) 2 ==  10000000000000000000000000

Comprobar con QuickCheck que para todo número natural n,

    raizEnt (10^(2*n)) 2 == 10^n

-}

import Test.QuickCheck

raizEnt :: Integer -> Integer -> Integer
raizEnt = raizEnt2

raizEnt1 :: Integer -> Integer -> Integer
raizEnt1 x n = last $ takeWhile (\y -> y^n <= x) [1..x]

-- por búsqueda dicotómica
raizEnt2 :: Integer -> Integer -> Integer
raizEnt2 x n = aux 1 x
    where aux a b | d == x    = c
                  | a >= c    = a
                  | d > x     = aux a c
                  | otherwise = aux c b
              where c = (a+b) `div` 2
                    d = c^n


prop_raizEnt :: Positive Integer -> Bool
prop_raizEnt (Positive n) = raizEnt (10^(2*n)) 2 == 10^n
