module Ex_20160428 where

{-

Pandigitales primos
===================

Un número con n dígitos es pandigital si contiene todos los dígitos del 1 a n
exactamente una vez. Por ejemplo, 2143 es un pandigital con 4 dígitos y, además,
es primo.

Definir la constante

   pandigitalesPrimos :: [Int]

tal que sus elementos son los números pandigitales, ordenados de mayor a menor.
Por ejemplo,

   take 3 pandigitalesPrimos       ==  [7652413,7642513,7641253]
   2143 `elem` pandigitalesPrimos  ==  True
   length pandigitalesPrimos       ==  538

-}

import Data.List
import Data.Numbers.Primes

reverseSort :: Ord a => [a] -> [a]
reverseSort = sortBy (flip compare)

pandigitalesPrimos :: [Int]
pandigitalesPrimos = pandigitalesPrimos2

pandigitalesPrimos1 :: [Int]
pandigitalesPrimos1 =
    concatMap (reverseSort . filter isPrime . map read . permutations)
              (init $ tails "987654321")

pandigitalesPrimos2 :: [Int]
pandigitalesPrimos2 =
    concatMap (reverseSort . filter isPrime . map read . filter f . permutations)
              (init $ tails "987654321")
        where f xs = last xs `elem` "9731"

pandigitalesPrimos3 :: [Int]
pandigitalesPrimos3 = filter isPrime (pandigitales 9)

pandigitalesPrimos4 :: [Int]
pandigitalesPrimos4 = filter isPrime (reverse pandigitales2)


pandigitales :: Int -> [Int]
pandigitales 1 = [1]
pandigitales n =
    reverseSort [read xs | xs <- permutations ys] ++ pandigitales (n-1)
    where ys = take n "123456789"

pandigitales2 :: [Int]
pandigitales2 =
    concat [(sort . map read) xs | xs <- map permutations ys]
    where ys = tail $ inits "123456789"
