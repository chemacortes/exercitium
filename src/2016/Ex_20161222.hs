module Ex_20161222 where

{-

Ordenación por una fila
=======================

Las matrices se pueden representar por listas de lista. Por ejemplo, la matriz

   |1 2 5|
   |3 0 7|
   |9 1 6|
   |6 4 2|

se puede representar por

   ej :: [[Int]]
   ej = [[1,2,5],
         [3,0,7],
         [9,1,6],
         [6,4,2]]

Definir la función

   ordenaPorFila :: Ord a => [[a]] -> Int -> [[a]]

tal que (ordenaPorFila xss k) es la matriz obtenida ordenando xs por los
elementos de la fila k. Por ejemplo,

   ordenaPorFila ej 1  ==  [[2,1,5],[0,3,7],[1,9,6],[4,6,2]]
   ordenaPorFila ej 2  ==  [[2,5,1],[0,7,3],[1,6,9],[4,2,6]]
   ordenaPorFila ej 3  ==  [[5,2,1],[7,0,3],[6,1,9],[2,4,6]]

-}

import Data.List (sort, sortBy, transpose)
import Data.Function (on)

ej :: [[Int]]
ej = [[1,2,5],
      [3,0,7],
      [9,1,6],
      [6,4,2]]

ordenaPorFila :: Ord a => [[a]] -> Int -> [[a]]
ordenaPorFila xss k = transpose . ordenaPor k $ transpose xss
  where
    ordenaPor n = sortBy (compare `on` (!!n))

ordenaPorFila2 :: Ord a => [[a]] -> Int -> [[a]]
ordenaPorFila2 xss k = map ordenaFila xss
  where
    idxs = [ i | (_,i) <- sort $ zip (xss!!k) [0..] ]
    ordenaFila xs = map (xs!!) idxs

ordenaPorFila3 :: Ord a => [[a]] -> Int -> [[a]]
ordenaPorFila3 xss k = [ [ x | (_,x) <- sort $ zip (xss!!k) xs ] | xs <- xss ]
