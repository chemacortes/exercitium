module Ex_20160223 where

{-

Múltiplos con ceros y unos
==========================

Se observa que todos los primeros números naturales tienen al menos un múltiplo
no nulo que está formado solamente por ceros y unos. Por ejemplo, 1×10=10,
2×5=10, 3×37=111, 4×25=100, 5×2=10, 6×185=1110; 7×143=1001; 8X125=1000;
9×12345679=111111111.

Definir la función

   multiplosCon1y0 :: Integer -> [Integer]

tal que (multiplosCon1y0 n) es la lista de los múltiplos de n cuyos dígitos son
1 ó 0. Por ejemplo,

   take 4 (multiplosCon1y0 3)      ==  [111,1011,1101,1110]
   take 3 (multiplosCon1y0 23)     ==  [110101,1011011,1101010]
   head (multiplosCon1y0 1234658)  ==  110101101101000000110

Comprobar con QuickCheck que todo entero positivo tiene algún múltiplo cuyos
dígitos son 1 ó 0.

-}

import Test.QuickCheck

-----
multiplosCon1y0 :: Integer -> [Integer]
multiplosCon1y0 = multiplosCon1y0b
-----

multiplosCon1y0a :: Integer -> [Integer]
multiplosCon1y0a n = filter todos1y0 [n,n*2..]

todos1y0 :: Integer -> Bool
todos1y0 n = all (`elem` "01") (show n)

multiplosCon1y0b :: Integer -> [Integer]
multiplosCon1y0b n = [x | x <- cerosYunos, x `mod` n == 0]

cerosYunos :: [Integer]
cerosYunos = 1 : concat [ [10*x,10*x+1] | x <- cerosYunos]


prop_multiplosCon1y0 :: Positive Integer -> Bool
prop_multiplosCon1y0 = not . null . multiplosCon1y0 . getPositive
