module Ex_20161219 where

{-

Selección por posición
======================

Definir la función

   seleccion :: Ord a => [a] -> [Int] -> [a]

tal que (seleccion xs ps) es la lista ordenada de los elementos que ocupan las
posiciones indicadas en la lista ps. Por ejemplo,

   seleccion [6,2,4,7] [2,0]      ==  [4,6]
   seleccion ['a'..'z'] [0,2..10] ==  "acegik"

-}

seleccion :: Ord a => [a] -> [Int] -> [a]
seleccion xs = map (xs!!)

seleccion2 :: Ord a => [a] -> [Int] -> [a]
seleccion2 = map.(!!)
