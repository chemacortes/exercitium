module Ex_20160314 where

{-

Primo suma de dos cuadrados
===========================

Definir la sucesión

   primosSumaDe2Cuadrados :: [Integer]

cuyos elementos son los números primos que se pueden escribir como sumas de dos
cuadrados. Por ejemplo,

   λ> take 20 primosSumaDe2Cuadrados
   [2,5,13,17,29,37,41,53,61,73,89,97,101,109,113,137,149,157,173,181]
   λ> primosSumaDe2Cuadrados !! (2*10^5)
   5803241

En el ejemplo anterior,

    13 está en la sucesión porque es primo y 13 = 2²+3².
    11 no está en la sucesión porque no se puede escribir como suma de dos
    cuadrados (en efecto, 11-1=10, 11-2²=7 y 11-3²=2 no son cuadrados).
    20 no está en la sucesión porque, aunque es suma de dos cuadrados
    (20=4²+2²), no es primo.

-}

import Data.Numbers.Primes

primosSumaDe2Cuadrados :: [Integer]
primosSumaDe2Cuadrados = primosSumaDe2Cuadrados2

primosSumaDe2Cuadrados1 :: [Integer]
primosSumaDe2Cuadrados1 = filter (not . null . representaciones) primes

representaciones :: Integer -> [(Integer,Integer)]
representaciones n = [(x, y) | x <- [1..raizEntera n]
                              , let y = raizEntera (n-x^2)
                              , y >= x
                              , n == x^2 + y^2]

raizEntera :: Integer -> Integer
raizEntera n = ceiling (sqrt (fromIntegral n))


{-
    Es claro que todo primo distinto de 2 es de la forma 4n+1 o 4n+3 (pues 4n
    sería múltiplo de 4 y 4n+2 sería múltiplo de 2, por lo que no sería primo).
    Además, sabemos que un cuadrado perfecto siempre es congruente con 0 o con
    1 módulo 4 (si el número es par, (2n)^2 = 4n^2, y si el número es impar
    (2n+1)^2 = 4n^2+4n+1). Luego la suma de dos cuadrados perfectos será
    congruente con 0, 1 o 2, módulo 4, pero nunca congruente con 3. Por tanto,
    deducimos que todo primo expresable como suma de cuadrados ha de ser
    forzosamente de la forma 4n+1. Sin embargo, esto no basta, puesto que en
    esta definición también se utiliza que todo primo de la forma 4n+1 es suma
    de dos cuadrados perfectos. Este hecho efectivamente se da, y su prueba se
    la debemos a Fermat (Teorema de Fermat sobre la suma de dos cuadrados).

    -- abrdelrod
-}

primosSumaDe2Cuadrados2 :: [Integer]
primosSumaDe2Cuadrados2 = 2 : [ x |  x <- primes, x `mod` 4 == 1 ]

