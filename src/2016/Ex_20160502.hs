module Ex_20160502 where

{-

Expresiones aritmética normalizadas
===================================

El siguiente tipo de dato representa expresiones construidas con variables,
sumas y productos

   data Expr = V String
             | S Expr Expr
             | P Expr Expr

Por ejemplo, x.(y+z) se representa por (P (V “x”) (S (V “y”) (V “z”)))

Una expresión es un término si es un producto de variables. Por ejemplo, x.(y.z)
es un término pero x+(y.z) ni x.(y+z) lo son.

Una expresión está en forma normal si es una suma de términos. Por ejemplo,
x.(y,z) y x+(y.z) está en forma normal; pero x.(y+z) y (x+y).(x+z) no lo están.

Definir las funciones

   esTermino, esNormal :: Expr -> Bool

tales que

    (esTermino a) se verifica si a es un término. Por ejemplo,

     esTermino (V "x")                                    == True
     esTermino (P (V "x") (P (V "y") (V "z")))            == True
     esTermino (P (V "x") (S (V "y") (V "z")))            == False
     esTermino (S (V "x") (P (V "y") (V "z")))            == False

    (esNormal a) se verifica si a está en forma normal. Por ejemplo,

     esNormal (V "x")                                     == True
     esNormal (P (V "x") (P (V "y") (V "z")))             == True
     esNormal (S (V "x") (P (V "y") (V "z")))             == True
     esNormal (P (V "x") (S (V "y") (V "z")))             == False
     esNormal (P (S (V "x") (V "y")) (S (V "y") (V "z"))) == False

-}

data Expr = V String
          | S Expr Expr
          | P Expr Expr

esTermino, esNormal :: Expr -> Bool

esTermino (V _)   = True
esTermino (S _ _) = False
esTermino (P a b) = esTermino a && esTermino b

esNormal (S a b) = esTermino a && esTermino b
esNormal x = esTermino x

