module Ex_20160505 where

{-

Número de divisiones en el algoritmo de Euclides
================================================

Dados dos números naturales, a y b, es posible calcular su máximo común divisor
mediante el Algoritmo de Euclides. Este algoritmo se puede resumir en la
siguiente fórmula:

   mcd(a,b) = a,                   si b = 0
            = b,                   si a = 0
            = mcd (a módulo b, b), si a > b
            = mcd (a, b módulo a), si b >= a

Definir la función

   mcdYdivisiones :: Int -> Int -> (Int,Int)

tal que (mcdYdivisiones a b) es el número de divisiones usadas en el cálculo del
máximo común divisor de a y b mediante el algoritmo de Euclides. Por ejemplo,

   mcdYdivisiones 252 198 == (18,4)

ya que los 4 divisiones del cálculo son

     mcd 252 198
   = mcd  54 198
   = mcd  54  36
   = mcd  18  36
   = mcd  18   0

Comprobar con QuickCheck que el número de divisiones requeridas por el algoritmo
de Euclides para calcular el MCD de a y b es igual o menor que cinco veces el
número de dígitos de menor de los números a y b.

-}

import Test.QuickCheck

mcdYdivisiones :: Int -> Int -> (Int,Int)
mcdYdivisiones a b
     | b == 0    = (a, 0)
     | a == 0    = (b, 0)
     | a > b     = let (m,n) = mcdYdivisiones (a `mod` b) b in (m, n+1)
     | otherwise = let (m,n) = mcdYdivisiones a (b `mod` a) in (m, n+1)

prop_numDivisiones :: Positive Int -> Positive Int -> Bool
prop_numDivisiones (Positive a) (Positive b) = n <= 5*m
    where (_,n) = mcdYdivisiones a b
          m = (length.show) (min a b)
