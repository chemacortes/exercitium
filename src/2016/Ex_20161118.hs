module Ex_2016000 where

{-

Números de Dudeney
==================

La semana pasada, Pepe Muñoz Santonja publicó en su blog [Algo más que números][1]
el artículo [Números de Dudeney en la base OEIS][2]

[1]: http://algomasquenumeros.blogspot.com.es/ "Algo más que números"
[2]: http://algomasquenumeros.blogspot.com.es/2016/11/numeros-de-dudeney-en-la-base-oeis.html

Un número de Dudeney es un número entero n tal que el cubo de la suma de sus
dígitos es igual a n. Por ejemplo, 512 es un número de Dudeney ya que
(5+1+2)^3 = 8^3 = 512.

Se puede generalizar variando el exponente: Un número de Dudeney de orden k es
un número entero n tal que la potencia k-ésima de la suma de sus dígitos es
igual a n. Por ejemplo, 2401 es un número de Dudeney de orden 4 ya que
(2+4+0+1)^4 = 7^4 = 2401.

Definir la función

   numerosDudeney :: Integer -> [Integer]

tal que (numerosDudeney k) es la lista de los números de Dudeney oe orden k. Por
ejemplo,

   take 6 (numerosDudeney 3)  == [1,512,4913,5832,17576,19683]
   take 6 (numerosDudeney 4)  == [1,2401,234256,390625,614656,1679616]
   take 2 (numerosDudeney 20) == [1,1215766545905692880100000000000000000000]

Comprobar con QuickCheck que 19683 es el mayor número de Dudeney de orden 3.

-}

import Test.QuickCheck

numerosDudeney :: Integer -> [Integer]
numerosDudeney k = filter (esDudeney k) xs
  where xs = [x^k | x <- [1..]]

esDudeney :: Integer -> Integer -> Bool
esDudeney k x = x == (sum (digitos x))^k

digitos :: Integer -> [Integer]
digitos n = [read [c] | c <- show n]

prop_dudeney :: (Positive Integer) -> Bool
prop_dudeney (Positive n) = (not.esDudeney 3) (n+19683)
