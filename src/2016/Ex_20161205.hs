module Ex_20161205 where

{-

Listas engarzadas
=================

Una lista de listas es engarzada si el último elemento de cada lista coincide
con el primero de la siguiente.

Definir la función

   engarzada :: Eq a => [[a]] -> Bool

tal que (engarzada xss) se verifica si xss es una lista engarzada. Por ejemplo,

   engarzada [[1,2,3], [3,5], [5,9,0]] == True
   engarzada [[1,4,5], [5,0], [1,0]]   == False
   engarzada [[1,4,5], [], [1,0]]      == False
   engarzada [[2]]                     == True
-}

engarzada1 :: Eq a => [[a]] -> Bool
engarzada1 [_]       = True
engarzada1 (xs@(_:_):ys@(y:_):xss) | last xs == y  = engarzada1 (ys:xss)
                                   | otherwise     = False
engarzada1 _         = False

engarzada :: Eq a => [[a]] -> Bool
engarzada xss = and $ zipWith cadena xss (tail xss)
  where cadena [] _  = False
        cadena _ []  = False
        cadena xs ys = last xs == head ys
