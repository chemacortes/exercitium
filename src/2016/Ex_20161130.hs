module Ex_20161130 where

{-

Problema de las particiones óptimas
===================================

El **problema de la particiones óptimas** consiste en dada una lista xs
dividirla en dos sublistas ys y zs tales que el valor absoluto de la diferencia
de la suma de los elementos de xs y la suma de los elemento de zs sea lo menor
posible. Cada una de estas divisiones (ys,zs) es una partición óptima de xs. Por
ejemplo, la **partición óptima** de [2,3,5] es ([2,3],[5]) ya que |(2+3) – 5| = 0.
Una lista puede tener distintas particiones óptimas. Por ejemplo, [1,1,2,3]
tiene dos particiones óptimas ([1,2],[1,3]) y ([1,1,2],[3]) ambas con diferencia
1 (es decir, 1 = |(1+2)-(1+3)| = |(1+1+2)-3|).

Definir la función

   particionesOptimas :: [Int] -> [([Int],[Int])]

tal que (particionesOptimas xs) es la lista de las particiones óptimas de xs.
Por ejemplo,

   particionesOptimas [2,3,5]    ==  [([2,3],[5])]
   particionesOptimas [1,1,2,3]  ==  [([1,2],[1,3]),([1,1,2],[3])]

-}

import Data.List

particionesOptimas :: [Int] -> [([Int],[Int])]
particionesOptimas xs = nubBy (\x y -> (fst x,fst y) == (snd y,snd x))
                              [(ys, xs\\ys) | ys <- yss]
  where medida ys = abs (sum xs - 2 * sum ys)
        xss = [(ys, medida ys) | ys <- nub (subsequences xs)]
        minimo = minimum (map snd xss)
        yss = [ys | (ys,m) <- xss, minimo == m]
