module Ex_20161114 where

{-

Persistencia multiplicativa de un número
========================================

La persistencia multiplicativa de un número es la cantidad de pasos requeridos
para reducirlo a una cifra multiplicando sus dígitos. Por ejemplo, la
persistencia de 39 es 3 porque 3×9 = 27, 2×7 = 14 y 1×4 = 4.

Definir las funciones

   persistencia     :: Integer -> Integer
   menorPersistente :: Integer -> Integer

tales que

    (persistencia x) es la persistencia de x. Por ejemplo,

     persistencia 39                             ==   3
     persistencia 2677889                        ==   8
     persistencia 26888999                       ==   9
     persistencia 3778888999                     ==  10
     persistencia 277777788888899                ==  11
     persistencia 77777733332222222222222222222  ==  11

    (menorPersistente n) es el menor número con persistencia n. Por ejemplo,

     menorPersistente 0  ==  1
     menorPersistente 1  ==  10
     menorPersistente 2  ==  25
     menorPersistente 3  ==  39
     menorPersistente 4  ==  77
     menorPersistente 5  ==  679
     menorPersistente 6  ==  6788
     menorPersistente 7  ==  68889

Comprobar con QuickCheck si todos los números menores que 10^233 tienen una
persistencia multiplicativa menor o igual que 11.

Nota: Este ejercicio ha sido propuesto por Marcos Giráldez.

-}

import Data.List (genericLength)
import Test.QuickCheck

persistencia :: Integer -> Integer
persistencia n = genericLength .takeWhile (>=10)
               $ iterate (product . digitos) n

menorPersistente :: Integer -> Integer
menorPersistente n = head [x | x <- [1..], persistencia x == n]

prop_persistencia :: Positive Integer -> Property
prop_persistencia (Positive n) = n < 10^233 ==> persistencia n <= 11


digitos :: Integer -> [Integer]
digitos n = [read [c] | c <- show n]
