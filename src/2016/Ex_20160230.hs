module Ex_20160230 where

{-

Números cuyos dígitos coinciden con los de sus factores primos
==============================================================

Un número n es especial si al unir los dígitos de sus factores primos, se
obtienen exactamente los dígitos de n, aunque puede ser en otro orden. Por
ejemplo, 1255 es especial, pues los factores primos de 1255 son 5 y 251.

Definir la función

   esEspecial :: Integer -> Bool

tal que (esEspecial n) se verifica si un número n es especial. Por ejemplo,

   esEspecial 1255 == True
   esEspecial 125  == False
   esEspecial 132  == False

Comprobar con QuickCheck que todo número primo es especial.

Calcular los 5 primeros números especiales que no son primos.

-}

import Data.Numbers.Primes
import Data.List
import Test.QuickCheck

esEspecial :: Integer -> Bool
esEspecial n = sort xs == (sort . show) n
    where xs = concatMap show .nub $ primeFactors n

prop_primos:: Integer -> Property
prop_primos n =
    isPrime (abs n) ==> esEspecial (abs n)

prop_especial :: Property
prop_especial = forAll (elements smallPrimes) esEspecial
    where smallPrimes = take 100000 primes

-- primeros especiales no primos
--   735,1255,3792,7236,11913,...
primerosEspecialNoPrimos :: [Integer]
primerosEspecialNoPrimos = [x | x <- [1..]
                              , esEspecial x
                              , not (isPrime x) ]

