module Ex_20160406 where

{-

Inverso multiplicativo modular
==============================

El [inverso multiplicativo modular][1] de un entero n módulo p es el número m,
entre 1 y p-1, tal que

   m * n ≡ 1 (mod p)

Por ejemplo, el inverso multiplicativo de 2 módulo 5 es 3, ya que 1 <= 3 <= 4 y
2×3 ≡ 1 (mod 5).

El inverso multipicativo de n módulo p existe si y sólo si n y p son coprimos;
es decir, si mcd(n,p) = 1.


Definir la función

   invMod :: Integer -> Integer -> Maybe Integer

tal que (invMod n p) es justo el inverso multiplicativo de n módulo p, si
existe y Nothing en caso contrario. Por ejemplo,

   λ> invMod 2 5
   Just 3
   λ> invMod 2 6
   Nothing
   λ> [(x,invMod x 5) | x <- [0..4]]
   [(0,Nothing),(1,Just 1),(2,Just 3),(3,Just 2),(4,Just 4)]
   λ> [(x,invMod x 6) | x <- [0..5]]
   [(0,Nothing),(1,Just 1),(2,Nothing),(3,Nothing),(4,Nothing),(5,Just 5)]
   λ> let n = 10^7 in invMod (10^n) (1+10^n) == Just (10^n)
   True

[1]: https://es.wikipedia.org/wiki/Inverso_multiplicativo_(aritmética_modular)

-}

import Data.Maybe
import Data.List

invMod :: Integer -> Integer -> Maybe Integer
invMod = invMod4

invMod1 :: Integer -> Integer -> Maybe Integer
invMod1 n p = listToMaybe [ m | m <- [1..p-1], m*n `mod` p == 1]

invMod2 :: Integer -> Integer -> Maybe Integer
invMod2 n p = find (\m -> m*n `mod` p == 1) [1..p-1]

invMod3 :: Integer -> Integer -> Maybe Integer
invMod3 n p | gcd n p /= 1  = Nothing
            | otherwise     = Just (aux n p)
    where aux 1 _ = 1
          aux x r = (x'*r+1) `div` x
              where  x' = x - aux (r `mod` x) x

-- Usando el Algoritmo extendido de Euclides
invMod4 :: Integer -> Integer -> Maybe Integer
invMod4 n p | m /= 1    = Nothing
            | x < 0     = Just (x + p)
            | otherwise = Just x
    where (x,_,m) = mcdExt n p

-- [Algoritmo extendido de Euclides]
-- (mcd a b) es la terna (x,y,g) tal que g es el máximo común divisor
-- de a y b y se cumple que ax + by = g. Por ejemplo,
--    mcdExt  2  5  ==  (-2,1,1)
--    mcdExt  2  6  ==  ( 1,0,2)
--    mcdExt 12 15  ==  (-1,1,3)
mcdExt :: Integer -> Integer -> (Integer,Integer,Integer)
mcdExt a 0 = (1, 0, a)
mcdExt a b = (t, s - q * t, g)
  where (q, r)    = a `quotRem` b
        (s, t, g) = mcdExt b r
