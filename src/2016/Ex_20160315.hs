module Ex_20160315 where

{-

Mayor sección inicial sin repetidos
===================================

Definir la función

   seccion :: Eq a => [a] -> [a]

tal que (seccion xs) es el mayor sección inicial de xs que no contiene ningún
elemento repetido. Por ejemplo:

   seccion [1,2,3,2,4,5]                      == [1,2,3]
   seccion "caceres"                          == "ca"
   length (seccion ([1..7531] ++ [1..10^9]))  ==  7531

-}

import Data.List

seccion :: Eq a => [a] -> [a]
seccion = seccion3

seccion1 :: Eq a => [a] -> [a]
seccion1 xs = take n xs
    where n = length $ takeWhile (uncurry (==)) (zip xs (nub xs))

seccion2 :: Eq a => [a] -> [a]
seccion2 = last . takeWhile (\xs -> xs == nub xs) . inits


seccion3 :: Eq a => [a] -> [a]
seccion3 = aux []
    where aux ys [] = reverse ys
          aux ys (x:xs) | x `notElem` ys = aux (x:ys) xs
                        | otherwise      = reverse ys
