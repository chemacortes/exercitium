module Ex_20160401 where

{-

Representación decimal de números racionales
============================================

Los números decimales se representan por ternas, donde el primer elemento es la
parte entera, el segundo es el anteperíodo y el tercero es el período. Por ejemplo,

    6/2  = 3                  se representa por (3,[],[])
    1/2  = 0.5                se representa por (0,[5],[])
    1/3  = 0.333333...        se representa por (0,[],[3])
   23/14 = 1.6428571428571... se representa por (1,[6],[4,2,8,5,7,1])

Su tipo es

   type Decimal = (Integer,[Integer],[Integer])

Los números racionales se representan por un par de enteros, donde el primer
elemento es el numerador y el segundo el denominador. Por ejemplo, el número
2/3 se representa por (2,3). Su tipo es

   type Racional = (Integer,Integer)

Definir las funciones

   decimal  :: Racional -> Decimal
   racional :: Decimal -> Racional

tales que

    (decimal r) es la representación decimal del número racional r. Por ejemplo,

        decimal (1,4)    ==  (0,[2,5],[])
        decimal (1,3)    ==  (0,[],[3])
        decimal (23,14)  ==  (1,[6],[4,2,8,5,7,1])

    (racional d) es el número racional cuya representación decimal es d. Por ejemplo,

        racional (0,[2,5],[])           ==  (1,4)
        racional (0,[],[3])             ==  (1,3)
        racional (1,[6],[4,2,8,5,7,1])  ==  (23,14)

Con la función decimal se puede calcular los períodos de los números racionales.
Por ejemplo,

   ghci> let (_,_,p) = decimal (23,14) in concatMap show p
   "428571"
   ghci> let (_,_,p) = decimal (1,47) in concatMap show p
   "0212765957446808510638297872340425531914893617"
   ghci> let (_,_,p) = decimal (1,541) in length (concatMap show p)
   540

Comprobar con QuickCheck si las funciones decimal y racional son inversas.

-}

import Test.QuickCheck

type Decimal = (Integer,[Integer],[Integer])
type Racional = (Integer,Integer)

decimal  :: Racional -> Decimal
decimal r | null ds     = (0, [], [])
          | otherwise   = (fst (head ds), as', ps')
    where
        ds = digitos r
        (as, ps) = partePuraPeriodica (tail ds)
        as' = map fst as
        ps' = map fst ps

digitos :: Racional -> [Racional]
digitos (n,d) | n == 0    = []
              | r == 0    = [(e,0)]
              | otherwise = (e,r) : digitos (10*r, d)
    where (e,r) = n `divMod` d

partePuraPeriodica :: Eq a => [a] -> ([a],[a])
partePuraPeriodica = aux []
    where
        aux as []  = (reverse as, [])
        aux as (b:bs) | b `elem` as = span (/=b) (reverse as)
                      | otherwise = aux (b:as) bs

racional :: Decimal -> Racional
racional (e, as, ps) | null ps   = reduce (x, 10^a)
                     | otherwise = reduce (y-x, 10^b-10^a)
    where
        a = length as
        x = read (concatMap show (e:as))
        b = length as + length ps
        y = read (concatMap show (e:as ++ ps))

reduce :: Racional -> Racional
reduce (n,d) = (n `div` g, d `div` g)
    where g = gcd n d


prop_inversa :: (Positive Integer, Positive Integer) -> Bool
prop_inversa (n,d) = (racional . decimal) r == reduce r
    where r = reduce (getPositive n, getPositive d)
