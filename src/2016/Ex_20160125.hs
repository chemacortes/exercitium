module Ex_20160125 where

{-

Serie de Thue-Morse
===================

La serie de Thue-Morse comienza con el término [0] y sus siguientes términos se
construyen añadiéndole al anterior su complementario. Los primeros términos de
la serie son

   [0]
   [0,1]
   [0,1,1,0]
   [0,1,1,0,1,0,0,1]
   [0,1,1,0,1,0,0,1,1,0,0,1,0,1,1,0]

Definir la lista

   serieThueMorse :: [[Integer]]

tal que sus elementos son los términos de la serie de Thue-Morse. Por ejemplo,

   λ> take 4 serieThueMorse
   [[0],[0,1],[0,1,1,0],[0,1,1,0,1,0,0,1]]

Comprobar con QuickCheck que cada término de la serie de Thue-Morse se obtiene
del anterior sustituyendo los 1 por 1, 0 y los 0 por 0, 1.

-}

import Test.QuickCheck

serieThueMorse1 :: [[Integer]]
serieThueMorse1 = [0] : zipWith (++) serieThueMorse1 (compl serieThueMorse1)
    where compl = map (map complementario)

complementario :: Integer -> Integer
complementario 0 = 1
complementario _ = 0

serieThueMorse2 :: [[Integer]]
serieThueMorse2 = iterate f [0]
    where f xs = xs ++ map complementario xs

serieThueMorse3 :: [[Integer]]
serieThueMorse3 = map fstx $ iterate f ([0],[1],[])
    where f (xs,ys,zs) = let ws=ys++zs in (xs++ws,ws,xs)
          fstx (x,_,_) = x

serieThueMorse :: [[Integer]]
serieThueMorse = [0] : zipWith (++) serieThueMorse zs
      where zs = [1] : zipWith (++) zs serieThueMorse
-- Nota: zs es la serie complemento de la de Thue-Morse


prop_ThueMorse :: Positive Int -> Property
prop_ThueMorse n = n < 20 ==>
        serieThueMorse!!(npos+1) == concatMap f (serieThueMorse!!npos)
    where npos = getPositive n
          f 0 = [0,1]
          f 1 = [1,0]
