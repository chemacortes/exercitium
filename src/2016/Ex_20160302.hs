module Ex_20160302 where

{-

Índices de números de Fibonacci
===============================

Los primeros términos de la sucesión de Fibonacci son

   0, 1, 1, 2, 3, 5, 8, 13, 21, 34

Se observa que el 6º término de la sucesión (comenzando a contar en 0) es el
número 8.

Definir la función

   indiceFib :: Integer -> Maybe Integer

tal que (indiceFib x) es justo el número n si x es el n-ésimo términos de la
sucesión de Fibonacci o Nothing en el caso de que x no pertenezca a la sucesión.
Por ejemplo,

    indiceFib 8                                           == Just 6
    indiceFib 9                                           == Nothing
    indiceFib 21                                          == Just 8
    indiceFib 22                                          == Nothing
    indiceFib 280571172992510140037611932413038677189525  == Just 200
    indiceFib 123456789012345678901234567890123456789012  == Nothing

-}

import Data.List

indiceFib :: Integer -> Maybe Integer
indiceFib x | last xs == x  = Just (genericLength xs - 1)
            | otherwise     = Nothing
    where xs = takeWhile (<=x) fibs


fibs :: [Integer]
fibs = 0 : scanl (+) 1 fibs

fibs2 :: [Integer]
fibs2 = 0 : 1 : zipWith (+) fibs2 (tail fibs2)
