module Ex_20161102 where

{-

Triángulos geométricos
======================

Un triángulo geométrico es un triángulo de lados enteros, representados por la
terna (a,b,c) tal que a ≤ b ≤ c y están en progresión geométrica, es decir,
b^2 = a*c. Por ejemplo, un triángulo de lados a = 144, b = 156 y c = 169.

Definir la función

   numeroTG :: Integer -> Int

tal que (numeroTG n) es el número de triángulos geométricos de perímetro menor
o igual que n. Por ejemplo

    numeroTG 10  == 3
    numeroTG 100 == 42
    numeroTG 200 == 91

Nota: Los triángulos geométricos de perímetro menor o igual que 20 son

   [(1,1,1),(2,2,2),(3,3,3),(4,4,4),(5,5,5),(6,6,6),(4,6,9)]

Se observa que (1,2,4) aunque cumple que 1+2+4 <= 20 y 2^2 = 1*4 no pertenece a
la lista ya que 1+2 > 4 y, por tanto, no hay ningún triángulo cuyos lados midan
1, 2 y 4.

-}

numeroTG1 :: Integer -> Int
numeroTG1 n = length [ (a,b,c) | c <- [1..n-2]
                               , b <- [1..c]
                               , a <- [1..min b (n-c-b)]
                               , b^2 == a*c]


{-

Corrigiendo la anterior solución para así aplicar correctamente la observación
de la nota:

Se aplican las siguientes relaciones:

  a <= b <= c
  a + b >= c
  a + b + c <= n

De las que se deducen los siguiente límites:

  c - b <= a <= n - c - b
  c <= n/2

-}

numeroTG :: Integer -> Int
numeroTG = length . triangulos

triangulos :: Integer -> [(Integer,Integer,Integer)]
triangulos n = [ (a,b,c) | c <- [1..n `div` 2]
                         , b <- [1..c]
                         , a <- [c-b..n-c-b]
                         , b^2 == a*c]

-- solución "oficial" más optimizada
numeroTG2 :: Integer -> Int
numeroTG2 n =
   length [(a,b,c) | c <- [1..n]
                   , b <- [1..c]
                   , b^2 `rem` c == 0
                   , let a = b^2 `div` c
                   , a+b > c
                   , a+b+c <= n
                   ]
