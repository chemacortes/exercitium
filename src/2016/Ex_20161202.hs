module Ex_20161202 where

{-

Listas alternadas
=================

Una lista de números enteros se llama alternada si sus elementos son
alternativamente par/impar o impar/par.

Definir la función

   alternada :: [Int] -> Bool

tal que (alternada xs) se verifica si xs es una lista alternada. Por ejemplo,

   alternada [1,2,3]     == True
   alternada [1,4,6,5]   == False
   alternada [1,4,3,5]   == False
   alternada [8,1,2,3,4] == True
   alternada [8,1,2,3]   == True
   alternada [8]         == True
   alternada [7]         == True

-}

alternada1 :: [Int] -> Bool
alternada1 []  = True
alternada1 (x:xs) | even x    = and ys
                  | otherwise = (not.or) ys
    where ys = zipWith id (cycle [odd,even]) xs


alternada2 :: [Int] -> Bool
alternada2 [] = True
alternada2 (x:xs) = f $ zipWith id (cycle [odd,even]) xs
  where f = if even x then and else not.or

alternada :: [Int] -> Bool
alternada xs  = and [ even x == z | (x,z) <- zip xs zs]
  where zs = iterate not (even (head xs))
