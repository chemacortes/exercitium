module Ex_20160526 where

{-

Caminos en una retícula
=======================

El **problema de los caminos** en una retícula consiste en, dada una retícula
rectangular con m filas y n columnas, determinar todos los caminos para ir
desde el vértice inferior izquierdo hasta el vértice superior derecho donde
los movimientos permitidos son mover hacia el siguiente vértice a la derecha o
arriba.

Por ejemplo, en la siguiente retícula un posible camino es el indicado en rojo.

    ![Camino](Ex_20160526.png)


Para representar los caminos se definen los siguientes tipos de datos:

    data Direccion = D | A deriving (Show, Eq)
    type Camino = [Direccion]

Por tanto, el camino de la figura anterior se representa por la lista [D,D,A,D,A].

Definir las funciones

   caminos  :: Int -> Int -> [Camino]
   nCaminos :: Int -> Int -> Integer

tales que

    (caminos m n) es la lista de los caminos en una retícula rectangular con m
    filas y n columnas. Por ejemplo,

     λ> caminos 2 2
     [[A,A,D,D],[A,D,A,D],[A,D,D,A],[D,A,A,D],[D,A,D,A],[D,D,A,A]]
     λ> caminos 1 3
     [[A,A,A,D],[A,A,D,A],[A,D,A,A],[D,A,A,A]]
     λ> caminos 2 3
     [[A,A,A,D,D],[A,A,D,A,D],[A,A,D,D,A],[A,D,A,A,D],[A,D,A,D,A],[A,D,D,A,A],
      [D,A,A,A,D],[D,A,A,D,A],[D,A,D,A,A],[D,D,A,A,A]]

    (nCaminos m n) es el número de los caminos en una retícula rectangular con
    m filas y n columnas. Por ejemplo,

     nCaminos 2 2  ==  6
     nCaminos 1 3  ==  4
     nCaminos 2 3  ==  10
     length (show (nCaminos 20000 30000))  ==  14612
     length (show (nCaminos 30000 20000))  ==  14612

-}

import Data.List

data Direccion = D | A deriving (Show, Eq)
type Camino = [Direccion]

caminos  :: Int -> Int -> [Camino]
caminos m 0 = [replicate m A]
caminos 0 n = [replicate n D]
caminos m n =  map (A:) (caminos (m-1) n)
            ++ map (D:) (caminos m (n-1))

nCaminos :: Int -> Int -> Integer
nCaminos = nCaminos3

nCaminos1 :: Int -> Int -> Integer
nCaminos1 m n = genericLength $ caminos m n

nCaminos2 :: Int -> Int -> Integer
nCaminos2 _ 0 = 1
nCaminos2 0 _ = 1
nCaminos2 m n = nCaminos2 (m-1) n + nCaminos2 m (n-1)

nCaminos3 :: Int -> Int -> Integer
nCaminos3 = nCombinatorio

-- Número combinatorio
-- nCombinatorio n p == (n+p)! / n! p!
nCombinatorio :: Int -> Int -> Integer
nCombinatorio n p = product [n'+1..(n'+p')] `div` product [1..p']
    where n' = fromIntegral (max n p)
          p' = fromIntegral (min n p)
