module Ex_20160523 where

{-

La sucesión del reloj astronómico de Praga
==========================================

La cadena infinita “1234321234321234321…”, formada por la repetición de los
dígitos 123432, tiene una propiedad (en la que se basa el funcionamiento del
reloj astronómico de Praga: la cadena se puede partir en una sucesión de
números, de forma que la suma de los dígitos de dichos números es la serie de
los números naturales, como se observa a continuación:

    1, 2, 3, 4, 32, 123, 43, 2123, 432, 1234, 32123, ...
    1, 2, 3, 4,  5,   6,  7,    8,   9,   10,    11, ...

Definir la lista

   reloj :: [Integer]

cuyos elementos son los términos de la sucesión anterior. Por ejemplo,

   ghci> take 11 reloj
   [1,2,3,4,32,123,43,2123,432,1234,32123]

Nota: La relación entre la sucesión y el funcionamiento del reloj se puede ver
en [The Mathematics Behind Prague’s Horloge Introduction][1].

[1]: www.global-sci.org/mc/galley/prague_sc_pic/prague_eng.pdf

-}

import Data.List

reloj :: [Integer]
reloj = trocea cadena 1
    where cadena = cycle "123432"

trocea :: String -> Integer -> [Integer]
trocea xs n = read ys : trocea zs (n+1)
    where (ys, zs) = head . dropWhile ((/=n).sumaDigitos.fst)
                   $ zip (inits xs) (tails xs)

sumaDigitos :: String -> Integer
sumaDigitos xs = sum [read [c] | c <- xs ]
