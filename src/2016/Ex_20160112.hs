module Ex_20160112 where

{-

Cambios de signo
================

En una lista xs se produce un cambio de signo por cada elemento x de la lista 
junto el primero de los elementos de xs con signo opuesto al de x. Por ejemplo,
en la lista [6,5,-4,0,-2,-7,0,-8,-1,4] hay 2 cambios de signo (entre (5,-4) y 
(-1,4)) y en la lista [6,5,-4,0, 2,-7,0,-8,-1,4] hay 4 cambios de signo (entre 
(5,-4), (-4,2), (2,-7) y(-1,4)).

Definir la función

   nCambios :: (Num a, Ord a) => [a] -> Int

tal que (nCambios xs) es el número de cambios de signos de la lista xs. Por 
ejemplo,

   nCambios [6,5,-4,0,-2,-7,0,-8,-1,4]  ==  2
   nCambios [6,5,-4,0, 2,-7,0,-8,-1,4]  ==  4

-}

import Data.List

nCambios1 :: (Num a, Ord a) => [a] -> Int
nCambios1 []      = 0
nCambios1 [_]      = 0
nCambios1 (x:0:xs) = nCambios1 (x:xs)
nCambios1 (x:y:xs) | signum x == (-signum y)  = 1 + nCambios1 (y:xs) 
                   | otherwise                =     nCambios1 (y:xs)

                  
nCambios2 :: (Num a, Ord a) => [a] -> Int
nCambios2 xs = if ngrupos > 1 then ngrupos - 1 else 0 
    where ngrupos = (length . group) [signum x | x <- xs, x /= 0 ]

nCambios :: (Num a, Ord a) => [a] -> Int
nCambios = pred . length . groupBy (\x y -> signum x * signum y >= 0)
