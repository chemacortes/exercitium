module Ex_20160219 where

{-

Dígitos en la factorización
===========================

El enunciado del [problema 652][1] de [Números y algo más][2] es el siguiente

    Si factorizamos los factoriales de un número en función de sus divisores
    primos y sus potencias, ¿Cuál es el menor número n tal que entre los
    factores primos y los exponentes de estos, n! contiene los dígitos del cero
    al nueve? Por ejemplo

        6! = 2⁴x3²x5¹, le faltan los dígitos 0,6,7,8 y 9
        12! = 2¹⁰x3⁵x5²x7¹x11¹, le faltan los dígitos 4,6,8 y 9

Definir la función

   digitosDeFactorizacion :: Integer -> [Integer]

tal que (digitosDeFactorizacion n) es el conjunto de los dígitos que aparecen
en la factorización de n. Por ejemplo,

   digitosDeFactorizacion (factorial 6)   ==  [1,2,3,4,5]
   digitosDeFactorizacion (factorial 12)  ==  [0,1,2,3,5,7]

Usando la función anterior, calcular la solución del problema.

Comprobar con QuickCheck que si n es mayor que 100, entonces

   digitosDeFactorizacion (factorial n) == [0..9]

[1]: http://simplementenumeros.blogspot.com.es/2011/04/652-desafios-del-contest-center-i.html
[2]: http://simplementenumeros.blogspot.com.es/

-}

import Data.List (nub, sort, group)
import Data.Numbers.Primes (primeFactors)
import Test.QuickCheck

digitosDeFactorizacion :: Integer -> [Integer]
digitosDeFactorizacion n = (nub . sort . concatMap f) factores
    where factores = [(head xs, length xs)| xs <- (group.primeFactors) n]
          digitos x = [read [c] | c <- show x]
          f (a,b) = digitos a ++ digitos b

-- SOLUCIÓN: 49
solucion :: Integer
solucion = (head . filter sol) [1..]
    where sol x = digitosDeFactorizacion (factorial x) == [0..9]


factorial :: Integer -> Integer
factorial n = product [2..n]

prop_digitosDeFactorizacion :: Positive Integer -> Property
prop_digitosDeFactorizacion np = n > 100 ==>
        digitosDeFactorizacion (factorial n) == [0..9]
    where n = getPositive np
