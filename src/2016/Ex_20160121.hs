module Ex_20160121 where

{-

Números belgas
==============

Un número n es k-belga si la sucesión cuyo primer elemento es k y cuyos 
elementos se obtienen sumando reiteradamente las cifras de n contiene a n. Por 
ejemplo,

- El 18 es 0-belga, porque a partir del 0 vamos a ir sumando sucesivamente 1, 8, 
  1, 8, … hasta llegar o sobrepasar el 18: 0, 1, 9, 10, 18, … Como se alcanza el 
  18, resulta que el 18 es 0-belga.

- El 19 no es 1-belga, porque a partir del 1 vamos a ir sumando sucesivamente 1, 
  9, 1, 9, … hasta llegar o sobrepasar el 19: 0, 1, 10, 11, 20, 21, … Como no 
  se alcanza el 19, resulta que el 19 no es 1-belga.

Definir la función

   esBelga :: Int -> Int -> Bool

tal que (esBelga k n) se verifica si n es k-belga. Por ejemplo,

   esBelga 0 18                              ==  True
   esBelga 1 19                              ==  False
   esBelga 0 2016                            ==  True
   [x | x <- [0..30], esBelga 7 x]           ==  [7,10,11,21,27,29]
   [x | x <- [0..30], esBelga 10 x]          ==  [10,11,20,21,22,24,26]
   length [n | n <- [1..9000], esBelga 0 n]  ==  2857

Comprobar con QuickCheck que para todo número entero positivo n, si k es el 
resto de n entre la suma de los dígitos de n, entonces n es k-belga.

-}

import Test.QuickCheck

esBelga :: Int -> Int -> Bool
esBelga k n = n `elem` takeWhile (<=n) serie
    where serie = scanl (+) k $ cycle (digitos n)

digitos :: Int -> [Int]
digitos n = [ read [c] | c <- show n ]

prop_belgas :: Int -> Bool
prop_belgas 0 = esBelga 0 0
prop_belgas n = esBelga k n'
    where n'= abs n
          k = n' `rem` sum (digitos n')


esBelga2 :: Int -> Int -> Bool
esBelga2 k n =
    k <= n && n == head (dropWhile (<n) (scanl (+) (k + q * s) ds))
    where ds = digitos n
          s  = sum ds
          q  = (n - k) `div` s