module Ex_20160128 where

{-

Sumas de dos primos
===================

Definir la sucesión

   sumasDeDosPrimos :: [Integer]

cuyos elementos son los números que se pueden escribir como suma de dos números
primos. Por ejemplo,

    λ> take 23 sumasDeDosPrimos
    [4,5,6,7,8,9,10,12,13,14,15,16,18,19,20,21,22,24,25,26,28,30,31]

-}

import Data.Numbers.Primes

sumasDeDosPrimos :: [Integer]
sumasDeDosPrimos = filter esSumaDeDosPrimos [4..]

esSumaDeDosPrimos1 :: Integer -> Bool
esSumaDeDosPrimos1 n = any (isPrime . (n-))
                     $ takeWhile (<= n `div` 2) primes

esSumaDeDosPrimos2 :: Integer -> Bool
esSumaDeDosPrimos2 n | odd n     = isPrime (n-2)
                     | otherwise = esSumaDeDosPrimos1 n

-- Aplicando la conjetura de Goldbach (sin demostrar)
esSumaDeDosPrimos3 :: Integer -> Bool
esSumaDeDosPrimos3 n = even n || isPrime (n-2)

esSumaDeDosPrimos :: Integer -> Bool
esSumaDeDosPrimos = esSumaDeDosPrimos2
