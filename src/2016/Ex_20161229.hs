module Ex_20161229 where

{-

Números de Perrin
=================

Los [números de Perrin][1] se definen por la relación de recurrencia

   P(n) = P(n - 2) + P(n - 3) si n > 2,

con los valores iniciales

   P(0) = 3, P(1) = 0 y P(2) = 2.

Definir la sucesión

   sucPerrin :: [Integer]

cuyos elementos son los números de Perrin. Por ejemplo,

   λ> take 15 sucPerrin
   [3,0,2,3,2,5,5,7,10,12,17,22,29,39,51]
   λ> length (show (sucPerrin !! (2*10^5)))
   24425

Comprobar con QuickCheck si se verifica la siguiente propiedad: para todo entero
n > 1, el n-ésimo término de la sucesión de Perrin es divisible por n si y sólo
si n es primo.

Nota: Aunque QuickCheck no haya encontrado contraejemplos, la propiedad no es
cierta. Sólo lo es una de las implicaciones: si n es primo, entonces el n-ésimo
término de la sucesión de Perrin es divisible por n. La otra es falsa y los
primeros contraejemplos son

   271441, 904631, 16532714, 24658561, 27422714, 27664033, 46672291


[1]: https://en.wikipedia.org/wiki/Perrin_number "números de Perrin"
-}

import Test.QuickCheck
import Data.Numbers.Primes (isPrime)
import Data.List (unfoldr,genericIndex)

sucPerrin :: [Integer]
sucPerrin = sucPerrin2

sucPerrin1 :: [Integer]
sucPerrin1 = 3 : 0 : 2 : zipWith (+) sucPerrin1 (tail sucPerrin1)

sucPerrin2 :: [Integer]
sucPerrin2 = [ x | (x,_,_) <- iterate (\(a,b,c) -> (b,c,a+b)) (3,0,2) ]

sucPerrin3 :: [Integer]
sucPerrin3 = unfoldr (\(a, (b,c)) -> Just (a, (b,(c,a+b)))) (3,(0,2))


prop_perrin :: Positive Integer -> Property
prop_perrin (Positive n) = n > 1 ==> esDivisible == esPrimo
  where esDivisible = (genericIndex sucPerrin n) `mod` n == 0
        esPrimo = isPrime n
