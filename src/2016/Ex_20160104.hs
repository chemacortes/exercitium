module Ex_20160104 where

{-

2016 es un número práctico
==========================

Un entero positivo n es un número práctico si todos los enteros positivos 
menores que él se pueden expresar como suma de distintos divisores de n. Por 
ejemplo, el 12 es un número práctico, ya que todos los enteros positivos 
menores que 12 se pueden expresar como suma de divisores de 12 (1, 2, 3, 4 y 6) 
sin usar ningún divisor más de una vez en cada suma:

    1 = 1
    2 = 2
    3 = 3
    4 = 4
    5 = 2 + 3
    6 = 6
    7 = 1 + 6
    8 = 2 + 6
    9 = 3 + 6
   10 = 4 + 6
   11 = 1 + 4 + 6

En cambio, 14 no es un número práctico ya que 6 no se puede escribir como suma, 
con sumandos distintos, de divisores de 14.

Definir la función

   esPractico :: Integer -> Bool

tal que (esPractico n) se verifica si n es un número práctico. Por ejemplo,

   esPractico 12                                      ==  True
   esPractico 14                                      ==  False
   esPractico 2016                                    ==  True
   esPractico 42535295865117307932921825928971026432  ==  True

-}

import Data.Numbers.Primes (primeFactors)
import Data.List

-- Aplicando la propuesta de Stewart y Sierpiński
-- https://en.wikipedia.org/wiki/Practical_number#Characterization_of_practical_numbers
esPractico :: Integer -> Bool
esPractico 1 = True
esPractico n = even n && and (zipWith (<=) (tail factores) sumaDivisores)
    where pss = group (primeFactors n)
          factores = [head ys | ys <- pss]
          -- suma progresión geométrica Sn = r^0 + r^1 + ... + r^n
          sumag xs = let r = head xs in (r * product xs - 1) `div` (r - 1)
          sumaDivisores = map (+1) $ scanl1 (*) (map sumag pss)


-- No se usa en la solución
sumaDivisores2 :: Integer -> Integer
sumaDivisores2 n = 1 + sum (nub [product ys | ys <- subsequences (primeFactors n)])
