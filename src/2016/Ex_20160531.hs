module Ex_20160531 where

{-

Sucesión infinita de todas las palabras
=======================================

El conjunto de todas las palabras se puede ordenar como en los diccionarios:

     a,   b, ...,   z,   A,   B, ...,   Z,
    aa,  ab, ...,  az,  aA,  aB, ...,  aZ,
    ba,  bb, ...,  bz,  bA,  bB, ...,  bZ,
   ...
    za,  zb, ...,  zz,  zA,  zB, ...,  zZ,
   aaa, aab, ..., aaz, aaA, aaB, ..., aaZ,
   baa, bab, ..., baz, baA, baB, ..., baZ,
   ...

Definir las funciones

   palabras :: [String]
   posicion :: String -> Integer

tales que

    palabras es la lista ordenada de todas las palabras. Por ejemplo,

     λ> take 10 (drop 100 palabras)
     ["aW","aX","aY","aZ","ba","bb","bc","bd","be","bf"]
     λ> take 10 (drop 2750 palabras)
     ["ZU","ZV","ZW","ZX","ZY","ZZ","aaa","aab","aac","aad"]
     λ> palabras !! (10^6)
     "gePO"

    (posicion n) es la palabra que ocupa la posición n en la lista ordenada de
    todas las palabras. Por ejemplo,

   posicion "c"                    ==  2
   posicion "ab"                   ==  53
   posicion "ba"                   ==  104
   posicion "eva"                  ==  14664
   posicion "adan"                 ==  151489
   posicion "HoyEsMartes"          ==  4957940944437977046
   posicion "EnUnLugarDeLaMancha"  ==  241779893912461058861484239910864

Comprobar con QuickCheck que para todo entero positivo n se verifica que

   posicion (palabras `genericIndex` n) == n

-}

import Data.List
import Test.QuickCheck

abc :: [(Char,Integer)]
abc = zip (['a'..'z'] ++ ['A'..'Z']) [1..]

palabras :: [String]
palabras = tail . concat $ iterate aux [""]
    where aux xs = [ c:s | (c,_) <- abc, s <- xs]

posicion :: String -> Integer
posicion xs = sum (zipWith (*) ys potencias) - 1
    where b = genericLength abc
          potencias = [ b^n | n <- [0..]]
          (Just ys) = mapM (`lookup` abc) (reverse xs)

prop_palabras :: Positive Integer -> Bool
prop_palabras (Positive n) =
    posicion (palabras `genericIndex` n) == n
