module Ex_20161223 where

{-

Listas duplicadas
=================

Se observa que en la cadena “aabbccddeffgg” todos los caracteres están
duplicados excepto el ‘e’. Al añadirlo obtenemos la lista “aabbccddeeffgg” y se
dice que esta última está duplicada.

También se observa que “aaaabbbccccdd” no está duplicada (porque hay un número
impar de ‘b’ consecutivas). Añadiendo una ‘b’ se obtiene “aaaabbbbccccdd” que
está duplicada.

Definir las funciones

   esDuplicada :: Eq a => [a] -> Bool
   duplica     :: Eq a => [a] -> [a]

tales que

    (esDuplicada xs) se verifica si xs es una lista duplicada. Por ejemplo,

     esDuplicada "aabbccddeffgg"   ==  False
     esDuplicada "aabbccddeeffgg"  ==  True
     esDuplicada "aaaabbbccccdd"   ==  False
     esDuplicada "aaaabbbbccccdd"  ==  True

    (duplica xs) es la lista obtenida duplicando los elementos de xs que no lo
    están. Por ejemplo,

     duplica "b"        ==  "bb"
     duplica "abba"     ==  "aabbaa"
     duplica "Betis"    ==  "BBeettiiss"
     duplica [1,1,1]    ==  [1,1,1,1]
     duplica [1,1,1,1]  ==  [1,1,1,1]

Comprobar con QuickCheck que, para cualquier lista de enteros xs, se verifica la siguiente propiedad:

  esDuplicada (duplica xs)

-}

import Data.List
import Test.QuickCheck

esDuplicada :: Eq a => [a] -> Bool
esDuplicada = all (even.length) . group


duplica :: Eq a => [a] -> [a]
duplica xs = concatMap dupl (group xs)
  where dupl ys@(y:_) | (even.length) ys = ys
                      | otherwise        = y : ys


prop_duplica :: Eq a => [a] -> Bool
prop_duplica = esDuplicada . duplica
