module Ex_20160407 where

{-

Números de Lucas
================

Los números de Lucas son los elementos de la sucesión L(n) definida por

   L(0) = 2
   L(1) = 1
   L(n) = L(n-1) + L(n-2), si n > 1.

Los primeros números de Lucas son

   2, 1, 3, 4, 7, 11, 18, 29, 47, 76, 123, ...

Definir las funciones

   nLucas :: Integer -> Integer
   lucas  :: [Integer]

tales que

    (nLucas n) es el n-ésimo número de Lucas. Por ejemplo,

   nLucas 5                       ==  11
   nLucas 32                      ==  4870847
   length (show (nLucas (10^5)))  ==  20899

    lucas es la lista de los números de Lucas. Por ejemplo,

   take 11 lucas ==  [2,1,3,4,7,11,18,29,47,76,123]
-}

import Data.List

lucas  :: [Integer]
lucas = lucas2

nLucas :: Integer -> Integer
nLucas = genericIndex lucas

lucas1  :: [Integer]
lucas1 = 2 : 1 : zipWith (+) lucas1 (tail lucas1)

lucas2  :: [Integer]
lucas2 = 2 : scanl (+) 1 lucas2

