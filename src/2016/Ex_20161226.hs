module Ex_20161226 where

{-

Ternas coprimas
===============

Definir la lista

   ternasCoprimas :: [(Integer,Integer,Integer)]

cuyos elementos son ternas de [primos relativos][1] (a,b,c) tales que
a < b y a + b = c. Por ejemplo,

   λ> take 7 ternasCoprimas
   [(1,2,3),(1,3,4),(2,3,5),(1,4,5),(3,4,7),(1,5,6),(2,5,7)]
   λ> ternasCoprimas !! 300000
   (830,993,1823)

[1]: https://es.wikipedia.org/wiki/Números_primos_entre_sí

-}


ternasCoprimas :: [(Integer,Integer,Integer)]
ternasCoprimas = [(a,b,a+b) | b <- [2..]
                            , a <- [1..b-1]
                            , gcd a b == 1 ]
