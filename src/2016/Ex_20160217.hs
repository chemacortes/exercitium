module Ex_20160217 where

{-

Números de Pentanacci
=====================

Los números de Fibonacci se definen mediante las ecuaciones

   F(0) = 0
   F(1) = 1
   F(n) = F(n-1) + F(n-2), si n > 1

Los primeros números de Fibonacci son

   0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, ...

Una generalización de los anteriores son los números de Pentanacci definidos
por las siguientes ecuaciones

   P(0) = 0
   P(1) = 1
   P(2) = 1
   P(3) = 2
   P(4) = 4
   P(n) = P(n-1) + P(n-2) + P(n-3) + P(n-4) + P(n-5), si n > 4

Los primeros números de Pentanacci son

  0, 1, 1, 2, 4, 8, 16, 31, 61, 120, 236, 464, 912, 1793, 3525, ...

Definir la sucesión

   pentanacci :: [Integer]

cuyos elementos son los números de Pentanacci. Por ejemplo,

   λ> take 15 pentanacci
   [0,1,1,2,4,8,16,31,61,120,236,464,912,1793,3525]
   λ> (pentanacci !! 70000) `mod` (10^30)
   231437922897686901289110700696
   λ> length (show (pentanacci !! 70000))
   20550

-}

import Data.List (tails)

pentanacci :: [Integer]
pentanacci = 0:1:1:2:4: map (sum . take 5) (tails pentanacci)

pentanacci2 :: [Integer]
pentanacci2 = 0 : map head (iterate aux [1, 0, 0, 0, 0])
    where aux [a,b,c,d,e] = [a+b+c+d+e,a,b,c,d]

pentanacci3 :: [Integer]
pentanacci3 = p (0, 1, 1, 2, 4)
    where p (a, b, c, d, e) = a : p (b, c, d, e, a + b + c + d + e)
