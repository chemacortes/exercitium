module Ex_20160208 where

{-

Representaciones de un número como suma de dos cuadrados
========================================================

Definir la función

   representaciones :: Integer -> [(Integer,Integer)]

tal que (representaciones n) es la lista de pares de números naturales (x,y)
tales que n = x² + y² con x <= y. Por ejemplo.

   representaciones  20           ==  [(2,4)]
   representaciones  25           ==  [(0,5),(3,4)]
   representaciones 325           ==  [(1,18),(6,17),(10,15)]
   representaciones 100000147984  ==  [(0,316228)]

Comprobar con QuickCheck que un número natural n se puede representar como suma
de dos cuadrados si, y sólo si, en la factorización prima de n todos los
exponentes de sus factores primos congruentes con 3 módulo 4 son pares.

-}

import Data.Numbers.Primes (primeFactors)
import Data.List (group)
import Test.QuickCheck

representaciones1 :: Integer -> [(Integer,Integer)]
representaciones1 n = [(x, y) | x <- [0..r], y <- [x..r], n == x^2 + y^2]
    where r = raizEntera n

representaciones2 :: Integer -> [(Integer,Integer)]
representaciones2 n = [(x, y) | x <- [0..raizEntera n]
                              , let y = raizEntera (n-x^2)
                              , y >= x
                              , n == x^2 + y^2]

representaciones :: Integer -> [(Integer,Integer)]
representaciones = representaciones2

raizEntera :: Integer -> Integer
raizEntera n = ceiling (sqrt (fromIntegral n))


prop_rep :: Positive Integer -> Bool
prop_rep n = (not . null . representaciones) np == all even exponentes
    where
        np = getPositive n
        exponentes = [ length xs | xs <- group (primeFactors np)
                                 , head xs `mod` 4 == 3 ]
