module Ex_20160422 where

{-

Conflictos de horarios
======================

Los horarios de los cursos se pueden representar mediante matrices donde las
filas indican los curso, las columnas las horas de clase y el valor
correspondiente al curso i y la hora j es verdadero indica que i tiene clase a
la hora j.

En Haskell, podemos usar la matrices de la librería Data.Matrix y definir el
tipo de los horarios por

   type Horario = Matrix Bool

Un ejemplo de horario es

   ejHorarios1 :: Horario
   ejHorarios1 = fromLists [[True,  True,  False, False],
                            [False, True,  True,  False],
                            [False, False, True,  True]]

en el que el 1º curso tiene clase a la 1ª y 2ª hora, el 2º a la 2ª y a la 3ª y
el 3º a la 3ª y a la 4ª.

Definir la función

   cursosConflictivos :: Horario -> [Int] -> Bool

tal que (cursosConflictivos h is) se verifica para si los cursos de la lista is
hay alguna hora en la que más de uno tiene clase a dicha hora. Por ejemplo,

   cursosConflictivos ejHorarios1 [1,2]  ==  True
   cursosConflictivos ejHorarios1 [1,3]  ==  False
-}

import Data.Matrix

type Horario = Matrix Bool

ejHorarios1 :: Horario
ejHorarios1 = fromLists [[True,  True,  False, False],
                         [False, True,  True,  False],
                         [False, False, True,  True]]

cursosConflictivos :: Horario -> [Int] -> Bool
cursosConflictivos h is = any conflicto columnas
    where columnas = [[h!(i,j) | i <- is] | j <- [1..ncols h]]
          conflicto xs = length (filter id xs) > 1
