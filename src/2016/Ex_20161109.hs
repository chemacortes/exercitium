module Ex_20161109
    (
    ) where

{-

Variación de la conjetura de Goldbach
=====================================

La conjetura de Goldbach afirma que

>    Todo número entero mayor que 5 se puede escribir como suma de tres números primos.

En este ejercicio consideraremos la variación consistente en exigir que los tres
sumandos sean distintos.

Definir las funciones

   sumas3PrimosDistintos      :: Int -> [(Int,Int,Int)]
   conKsumas3PrimosDistintos  :: Int -> Int -> [Int]
   noSonSumas3PrimosDistintos :: Int -> [Int]

tales que

    (sumas3PrimosDistintos n) es la lista de las descomposiciones decrecientes
    de n como tres primos distintos. Por ejemplo,

   sumas3PrimosDistintos 26  ==  [(13,11,2),(17,7,2),(19,5,2)]
   sumas3PrimosDistintos 18  ==  [(11,5,2),(13,3,2)]
   sumas3PrimosDistintos 10  ==  [(5,3,2)]
   sumas3PrimosDistintos 11  ==  []

    (conKsumas3PrimosDistintos k n) es la lista de los números menores o iguales
    que n que se pueden escribir en k forma distintas como suma de tres primos
    distintos. Por ejemplo,

   conKsumas3PrimosDistintos 3 99  ==  [26,27,29,32,36,42,46,48,54,58,60]
   conKsumas3PrimosDistintos 2 99  ==  [18,20,21,22,23,24,25,28,30,34,64,70]
   conKsumas3PrimosDistintos 1 99  ==  [10,12,14,15,16,19,40]
   conKsumas3PrimosDistintos 0 99  ==  [11,13,17]

    (noSonSumas3PrimosDistintos n) es la lista de los números menores o iguales
    que n que no se pueden escribir como suma de tres primos distintos. Por ejemplo,

   noSonSumas3PrimosDistintos 99   ==  [11,13,17]
   noSonSumas3PrimosDistintos 500  ==  [11,13,17]

-}

import Data.Numbers.Primes


segment :: Ord a => a -> a -> [a] -> [a]
segment a b = takeWhile (<b) . dropWhile (<=a)

sumas3PrimosDistintos :: Int -> [(Int,Int,Int)]
sumas3PrimosDistintos = sumas3PrimosDistintos2

-- versión directa
sumas3PrimosDistintos1 :: Int -> [(Int,Int,Int)]
sumas3PrimosDistintos1 n = [(a,b,c) | a <- takeWhile (<n) primes
                                    , b <- takeWhile (<a) primes
                                    , let c = n - a - b, c < b
                                    , isPrime c ]

-- versión más optimizada
sumas3PrimosDistintos2 :: Int -> [(Int,Int,Int)]
sumas3PrimosDistintos2 n = [(a,b,c) | a <- primesRange (n `div` 3) n
                                    , b <- primesRange ((n - a) `div` 2) a
                                    , let c = n - a - b
                                    , isPrime c ]
      where primesRange a b = (takeWhile (<b) . dropWhile (<=a)) primes




conKsumas3PrimosDistintos  :: Int -> Int -> [Int]
conKsumas3PrimosDistintos k n = [i | i <- [1..n]
                                   , (length . sumas3PrimosDistintos) i == k]

noSonSumas3PrimosDistintos :: Int -> [Int]
noSonSumas3PrimosDistintos = undefined
