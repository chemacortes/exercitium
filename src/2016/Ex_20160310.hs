module Ex_20160310 where

{-

Paridad del número de divisores
===============================

Definir la función

   nDivisoresPar :: Integer -> Bool

tal que (nDivisoresPar n) se verifica si n tiene un número par de divisores.
Por ejemplo,

   nDivisoresPar 12                     ==  True
   nDivisoresPar 16                     ==  False
   nDivisoresPar (product [1..2*10^4])  ==  True

-}

import Data.Numbers.Primes
import Data.List

nDivisoresPar :: Integer -> Bool
nDivisoresPar = any odd . map length . group . primeFactors

