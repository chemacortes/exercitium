module Ex_20160414 where

{-

Suma de los elementos de las diagonales matrices espirales
==========================================================

Empezando con el número 1 y moviéndose en el sentido de las agujas del reloj se
obtienen las matrices espirales

   |1 2|   |7 8 9|   | 7  8  9 10|   |21 22 23 24 25|
   |4 3|   |6 1 2|   | 6  1  2 11|   |20  7  8  9 10|
           |5 4 3|   | 5  4  3 12|   |19  6  1  2 11|
                     |16 15 14 13|   |18  5  4  3 12|
                                     |17 16 15 14 13|

La suma los elementos de sus diagonales es

    en la 2×2: 1+3+2+4 = 10
    en la 3×3: 1+3+5+7+9 = 25
    en la 4×4: 1+2+3+4+7+10+13+16 = 56
    en la 5×5: 1+3+5+7+9+13+17+21+25 = 101

Definir la función

   sumaDiagonales :: Integer -> Integer

tal que (sumaDiagonales n) es la suma de los elementos en las diagonales de la
matriz espiral de orden nxn. Por ejemplo.

   sumaDiagonales 1         ==  1
   sumaDiagonales 2         ==  10
   sumaDiagonales 3         ==  25
   sumaDiagonales 4         ==  56
   sumaDiagonales 5         ==  101
   sumaDiagonales (1+10^6)  ==  666669166671000001
   sumaDiagonales (10^2)    ==         671800
   sumaDiagonales (10^3)    ==        667168000
   sumaDiagonales (10^4)    ==       666716680000
   sumaDiagonales (10^5)    ==      666671666800000
   sumaDiagonales (10^6)    ==     666667166668000000
   sumaDiagonales (10^7)    ==    666666716666680000000
   sumaDiagonales (10^8)    ==   666666671666666800000000
   sumaDiagonales (10^9)    ==  666666667166666668000000000

Comprobar con QuickCheck que el último dígito de (sumaDiagonales n) es 0, 4 ó 6
si n es par y es 1, 5 ó 7 en caso contrario.

-}

import Test.QuickCheck

sumaDiagonales :: Integer -> Integer
sumaDiagonales = sumaDiagonales4


sumaDiagonales1 :: Integer -> Integer
sumaDiagonales1 1 = 1
sumaDiagonales1 2 = 10
sumaDiagonales1 n = sumaDiagonales1 (n-2) + 4*(n*n) - 6*n + 6


sumaDiagonales2 :: Integer -> Integer
sumaDiagonales2 = sum . elementosEnDiagonales

-- (elementosEnDiagonales n) es la lista de los elementos en las
-- diagonales de la matriz espiral de orden nxn. Por ejemplo,
--    elementosEnDiagonales 1  ==  [1]
--    elementosEnDiagonales 2  ==  [1,2,3,4]
--    elementosEnDiagonales 3  ==  [1,3,5,7,9]
--    elementosEnDiagonales 4  ==  [1,2,3,4,7,10,13,16]
--    elementosEnDiagonales 5  ==  [1,3,5,7,9,13,17,21,25]
elementosEnDiagonales :: Integer -> [Integer]
elementosEnDiagonales n
    | even n    = tail (scanl (+) 0 (concatMap (replicate 4) [1,3..n-1]))
    | otherwise = scanl (+) 1 (concatMap (replicate 4) [2,4..n-1])

sumaDiagonales3 :: Integer -> Integer
sumaDiagonales3 n | odd  n    =  1 + sum [4*i^2-6*i+6 | i <- [3,5..n]]
                  | otherwise =      sum [4*i^2-6*i+6 | i <- [2,4..n]]


-- Aplicando las fórmulas para las sumas de series de números naturales:
--
--   sum [x^2 | x <- [0..n]] == n (n+1) (2n+1) / 6
--   sum [ x  | x <- [0..n]] == n (n+1) / 2
--
-- ... y aplicando un cambio de variables
--
--   odd n  --> 2n+1
--   even n --> 2n
--
sumaDiagonales4 :: Integer -> Integer
sumaDiagonales4 n | odd n     = (p-9) `div` 6
                  | otherwise =  p    `div` 6
    where p = 4*n^(3::Integer) + 3*n^(2::Integer) + 8*n

prop_sumaDiagonales :: Positive Integer -> Bool
prop_sumaDiagonales (Positive n) | even n    = x `elem` [0,4,6]
                                 | otherwise = x `elem` [1,5,7]
    where x = sumaDiagonales n `mod` 10
