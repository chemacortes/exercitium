module Ex_20161220 where

{-

Ordenación por una columna
==========================

Las matrices se pueden representar por listas de lista. Por ejemplo, la matriz

   |1 2 5|
   |3 0 7|
   |9 1 6|
   |6 4 2|

se puede representar por

   ej :: [[Int]]
   ej = [[1,2,5],
         [3,0,7],
         [9,1,6],
         [6,4,2]]

Definir la función

   ordenaPor :: Ord a => [[a]] -> Int -> [[a]]

tal que (ordenaPor xss k) es la matriz obtenida ordenando xs por los elementos
de la columna k. Por ejemplo,

   ordenaPor ej 0  ==  [[1,2,5],[3,0,7],[6,4,2],[9,1,6]]
   ordenaPor ej 1  ==  [[3,0,7],[9,1,6],[1,2,5],[6,4,2]]
   ordenaPor ej 2  ==  [[6,4,2],[1,2,5],[9,1,6],[3,0,7]]

-}

import Data.List (sortBy)
import Data.Function (on)

ej :: [[Int]]
ej = [[1,2,5],
      [3,0,7],
      [9,1,6],
      [6,4,2]]

ordenaPor :: Ord a => [[a]] -> Int -> [[a]]
ordenaPor xss k = sortBy (compare `on` (!!k)) xss
