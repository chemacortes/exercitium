module Ex_20160413 where

{-

Descomposiciones con sumandos 1 ó 2
===================================

Definir la funciones

   sumas  :: Int -> [[Int]]
   nSumas :: Int -> Integer

tales que

    (sumas n) es la lista de las descomposiciones de n como sumas cuyos
    sumandos son 1 ó 2. Por ejemplo,

      sumas 1            ==  [[1]]
      sumas 2            ==  [[1,1],[2]]
      sumas 3            ==  [[1,1,1],[1,2],[2,1]]
      sumas 4            ==  [[1,1,1,1],[1,1,2],[1,2,1],[2,1,1],[2,2]]
      length (sumas 26)  ==  196418
      length (sumas 33)  ==  5702887

    (nSumas n) es el número de descomposiciones de n como sumas cuyos sumandos
    son 1 ó 2. Por ejemplo,

      nSumas 4                      ==  5
      nSumas 123                    ==  36726740705505779255899443
      length (show (nSumas 123456)) ==  25801
-}

import Data.List


-- ----------------------------------------------------
--
-- Nos quedamos con las versiones más eficientes
--
sumas  :: Int -> [[Int]]
sumas = sumas4

nSumas :: Int -> Integer
nSumas = nSumas4

-- ----------------------------------------------------

sumas1  :: Int -> [[Int]]
sumas1 n = concat [ composicion n1 n2 | n2 <- [0..n `div` 2]
                                      , let n1 = n - n2*2 ]

-- combinaciones posibles de `n1` unos y `n2` doses
composicion :: Int -> Int -> [[Int]]
composicion n1 n2 = nub . permutations $ replicate n1 1 ++ replicate n2 2


sumas2 :: Int -> [[Int]]
sumas2 1 = [[1]]
sumas2 2 = [[1,1],[2]]
sumas2 n = [1:xs | xs <- sumas2 (n-1)] ++ [2:xs | xs <- sumas2 (n-2)]

-- 1ª definición de sumas
sumas3 :: Int -> [[Int]]
sumas3 0 = [[]]
sumas3 1 = [[1]]
sumas3 n = [1:xs | xs <- sumas3 (n-1)] ++ [2:xs | xs <- sumas3 (n-2)]

-- 2ª definición de sumas
sumas4 :: Int -> [[Int]]
sumas4 n = aux !! n
    where aux     = [[]] : [[1]] : zipWith f (tail aux) aux
          f xs ys = map (1:) xs ++ map (2:) ys


nSumas1 :: Int -> Integer
nSumas1 = genericLength . sumas

nSumas2 :: Int -> Integer
nSumas2 n = sum [ comb m1 m2 | m1 <- [n,n-2..0], let m2 = (n - m1) `div` 2 ]

-- comb n m == factorial (n+m) `div` ((factorial n) * (factorial m))
comb :: Int -> Int -> Integer
comb n m = product [s+1..fromIntegral (n+m)] `div` product [1..p]
    where s = fromIntegral $ max n m
          p = toInteger $ min n m

factorial :: Int -> Integer
factorial x = product [1..fromIntegral x]


nSumas3 :: Int -> Integer
nSumas3 n = aux `genericIndex` n
    where aux = 1 : 1 : zipWith (+) aux (tail aux)

nSumas4 :: Int -> Integer
nSumas4 n = fibonacci !! n
      where fibonacci = 1:scanl (+) 1 fibonacci


