module Ex_20160411 where

{-

Primos permutables
==================

Un primo permutable es un número primo tal que todos los números obtenidos
permutando sus cifras son primos. Por ejemplo, 337 es un primo permutable ya
que 337, 373 y 733 son primos.

Definir las funciones

   esPrimoPermutable :: Integer -> Bool
   primosPermutables :: [Integer]

tales que

    (esPrimoPermutable x) se verifica si x es un primo permutable. Por ejemplo,

     esPrimoPermutable 97  ==  True
     esPrimoPermutable 337 ==  True
     esPrimoPermutable 23  ==  False

    primosPermutables es la lista de los primos permutables. Por ejemplo,

     λ> take 20 primosPermutables
     [2,3,5,7,11,13,17,31,37,71,73,79,97,113,131,199,311,337,373,733]

-}

import Data.Numbers.Primes
import Data.List

esPrimoPermutable :: Integer -> Bool
esPrimoPermutable x = all isPrime xs
    where xs :: [Integer]
          xs = [read ys | ys <- (nub . permutations . show) x]

primosPermutables :: [Integer]
primosPermutables = filter esPrimoPermutable primes
