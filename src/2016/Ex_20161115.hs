module Ex_20161115 where

{-

Cuadrados ondulantes
====================

Un número se dice ondulante si sus cifras alternan entre dos valores. Por
ejemplo, 272 es ondulante, así como 2727. El primer cuadrado ondulante no
trivial (todos los cuadrados de dos cifras son ondulantes) es 121 = 11^2.

Definir la función

   cuadradosOndulantes :: Integer -> [Integer]

tal que (cuadradosOndulantes n) es la lista de los cuadrados ondulantes menores
que n^2. Por ejemplo,

   λ> cuadradosOndulantes 50000
   [1,4,9,16,25,36,49,64,81,121,484,676,69696]

Nota: Este ejercicio ha sido propuesto por Marcos Giráldez.

-}

import Data.List (isPrefixOf)


cuadradosOndulantes :: Integer -> [Integer]
cuadradosOndulantes n = filter esOndulante [x^2| x <- [1..n]]


esOndulante1 :: Integer -> Bool
esOndulante1 n = and (zipWith (==) xs (drop 2 xs))
  where xs = show n

esOndulante :: Integer -> Bool
esOndulante n = (drop 2 xs) `isPrefixOf` xs
  where xs = show n
