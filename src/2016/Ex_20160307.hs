module Ex_20160307 where

{-

Menor no expresable como suma
=============================

Definir la función

   menorNoSuma :: [Integer] -> Integer

tal que (menorNoSuma xs) es el menor número que no se puede escribir como suma
de un subconjunto de xs, donde se supone que xs es un conjunto de números
enteros positivos. Por ejemplo,

   menorNoSuma [6,1,2]    ==  4
   menorNoSuma [1,2,3,9]  ==  7
   menorNoSuma [5]        ==  1
   menorNoSuma [1..20]    ==  211
   menorNoSuma [1..10^6]  ==  500000500001

Comprobar con QuickCheck que para todo n,

   menorNoSuma [1..n] == 1 + sum [1..n]
-}

import Data.List
import Test.QuickCheck

menorNoSuma :: [Integer] -> Integer
menorNoSuma = menorNoSuma3

menorNoSuma1 :: [Integer] -> Integer
menorNoSuma1 xs = head $ dropWhile (esSuma xs') [1..]
    where xs' = sortBy (flip compare) xs

esSuma :: [Integer] -> Integer -> Bool
esSuma [] _     = False
esSuma (x:xs) y | x == y    = True
                | x > y     = esSuma xs y
                | otherwise = esSuma xs (y-x) || esSuma xs y

menorNoSuma2 :: [Integer] -> Integer
menorNoSuma2 = aux . sortBy (flip compare)
    where aux :: [Integer] -> Integer
          aux [] = 1
          aux (x:xs) | x > m     = m
                     | otherwise = m + x
                where m = aux xs

menorNoSuma3 :: [Integer] -> Integer
menorNoSuma3 xs = aux (sort xs) 1
    where aux [] m = m
          aux (y:ys) m | y > m     = m
                       | otherwise = aux ys (m + y)

prop_menorNoSuma :: Positive Integer -> Bool
prop_menorNoSuma pn = menorNoSuma [1..n] == 1 + sum [1..n]
    where n = getPositive pn
