module Ex_20160404 where

{-

Factorial generalizado
======================

El factorial generalizado de `x` respecto de `y` y `z` es el producto
`x(x-z)(x-2z) … (x-(y-1)z)`. Por ejemplo, el factorial generalizado de 7 respecto
de 3 y 2 es 7x5x3 = 105 y el de 7 respecto de 2 y 3 es 7×4 = 28

Definir la función

   factGen :: Integer -> Integer -> Integer -> Integer

tal que (factGen x y z) es el factorial generalizado de x respecto de y y z.
Por ejemplo,

   factGen 7 3 2  ==  105
   factGen 7 2 3  ==  28

Nota: Se supone que x, y y z son positivos y z < x.

Comprobar con QuickCheck que (factGen x x 1) es el factorial de x.

-}

import Test.QuickCheck

factGen :: Integer -> Integer -> Integer -> Integer
factGen x y z = product [x, x-z..(x-(y-1)*z)]

prop_factGen :: Positive Integer -> Bool
prop_factGen (Positive x) = factGen x x 1 == product [1..x]
