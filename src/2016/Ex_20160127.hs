module Ex_20160127 where

{-

De árboles a listas
===================

Los árboles binarios con datos en nodos y hojas se definen por

   data Arbol a = H a | N a (Arbol a) (Arbol a) deriving Show

Por ejemplo, el árbol

          3
         / \
        /   \
       4     7
      / \   / \
     5   0 0   3
    / \
   2   0

se representa por

   ejArbol :: Arbol Integer
   ejArbol = N 3 (N 4 (N 5 (H 2)(H 0)) (H 0)) (N 7 (H 0) (H 3))

Definir la función

   sucesores :: Arbol a -> [(a,[a])]

tal que (sucesores t) es la lista de los pares formados por los elementos del 
árbol t junto con sus sucesores. Por ejemplo,

   λ> sucesores ejArbol
   [(3,[4,7]),(4,[5,0]),(5,[2,0]),(2,[]),(0,[]),(0,[]),
    (7,[0,3]),(0,[]),(3,[])]

-}


data Arbol a = H a
             | N a (Arbol a) (Arbol a)
             deriving Show

ejArbol :: Arbol Integer
ejArbol = N 3 (N 4 (N 5 (H 2)(H 0)) (H 0)) (N 7 (H 0) (H 3))

sucesores :: Arbol a -> [(a,[a])]
sucesores (H x)     = [(x,[])]
sucesores (N x i d) = (x,[raiz i, raiz d]) : sucesores i ++ sucesores d
    where raiz (H y)     = y
          raiz (N y _ _) = y

