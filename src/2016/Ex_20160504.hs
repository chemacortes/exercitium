module Ex_20160504 where

{-

Período de una lista
====================

El período de una lista xs es la lista más corta ys tal que xs se puede obtener
concatenando varias veces la lista ys. Por ejemplo, el período “abababab” es “ab”
ya que “abababab” se obtiene repitiendo tres veces la lista “ab”.

Definir la función

   periodo :: Eq a => [a] -> [a]

tal que (periodo xs) es el período de xs. Por ejemplo,

   periodo "ababab"      ==  "ab"
   periodo "buenobueno"  ==  "bueno"
   periodo "oooooo"      ==  "o"
   periodo "sevilla"     ==  "sevilla"

-}

import Data.List

periodo :: Eq a => [a] -> [a]
periodo [] = []
periodo xs = head [ ys | ys <- (tail . inits) xs
                       , length xs `mod` length ys == 0
                       , xs `isPrefixOf` concat (repeat ys) ]
