module Ex_20161227 where

{-

Números super pandigitales
==========================

Un entero positivo n es [pandigital][1] en base b si su expresión en base b
contiene todos los dígitos de 0 a b-1 al menos una vez. Por ejemplo,

    el 2 es pandigital en base 2 porque 2 en base 2 es 10,
    el 11 es pandigital en base 3 porque 11 en base 3 es 102 y
    el 75 es pandigital en base 4 porque 75 en base 4 es 1023.

Un número n es super pandigital de orden m si es pandigital en todas las bases
desde 2 hasta m. Por ejemplo, 978 es super pandigital de orden 5 pues

    en base 2 es: 1111010010
    en base 3 es: 1100020
    en base 4 es: 33102
    en base 5 es: 12403

Definir la función

   superPandigitales :: Integer -> [Integer]

tal que (superPandigitales m) es la lista de los números super pandigitales de
orden m. Por ejemplo,

   take 3 (superPandigitales 3) == [11,19,21]
   take 3 (superPandigitales 4) == [75,99,114]
   take 3 (superPandigitales 5) == [978,1070,1138]


[1]: https://en.wikipedia.org/wiki/Pandigital_number "Número Pandigital"

-}

import Data.List (delete,(\\))

superPandigitales :: Integer -> [Integer]
superPandigitales m = [ x | x <- [1..]
                          , all (flip esPandigital x) [2..m] ]

esPandigital :: Integer -> Integer -> Bool
esPandigital b = aux [0..b-1]
  where aux [] _ = True
        aux _  0 = False
        aux xs x = aux (delete r xs) d
          where (d,r) = x `divMod` b

superPandigitales2 :: Integer -> [Integer]
superPandigitales2 b =
  [x | x <- [1..]
     , and [esPandigital2 x a | a <- [2..b]]]

esPandigital2 :: Integer -> Integer -> Bool
esPandigital2 x b = null ([0..b-1] \\ codif x)
  where codif 0 = []
        codif n = mod n b : codif (div n b)
