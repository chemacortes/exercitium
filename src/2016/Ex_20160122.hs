module Ex_20160122 where

{-

Sumas digitales de primos consecutivos
======================================

Definir la función

   primosConsecutivosConSumasDigitalesPrimas :: Int -> [[Integer]]

tal que (primosConsecutivosConSumasDigitalesPrimas k) es la sucesión de listas 
de k primos consecutivos tales que las sumas ordenadas de sus dígitos también 
son primos consecutivos. Por ejemplo,

   λ> take 5 (primosConsecutivosConSumasDigitalesPrimas 2)
   [[2,3],[3,5],[5,7],[41,43],[43,47]]
   λ> take 5 (primosConsecutivosConSumasDigitalesPrimas 3)
   [[2,3,5],[3,5,7],[41,43,47],[191,193,197],[193,197,199]]
   λ> take 4 (primosConsecutivosConSumasDigitalesPrimas 4)
   [[2,3,5,7],[3,5,7,11],[191,193,197,199],[821,823,827,829]]
   λ> primosConsecutivosConSumasDigitalesPrimas 4 !! 50
   [129197,129209,129221,129223]

-}

import Data.Numbers.Primes  (primes)
import Data.List            (tails, sort)

primosConsecutivosConSumasDigitalesPrimas :: Int -> [[Integer]]
primosConsecutivosConSumasDigitalesPrimas n =
        [ xs | xs <- xss
             , sumasDigitales xs `enListaOrdenada` xss ]
    where xss = consecutivos n primes

digitos :: Integer -> [Integer]
digitos n = [ read [c] | c <- show n ]

sumasDigitales :: [Integer] -> [Integer]
sumasDigitales = sort . map (sum . digitos)

consecutivos :: Int -> [a] -> [[a]]
consecutivos n xs = map (take n) (tails xs)

enListaOrdenada :: Ord a => a -> [a] -> Bool
enListaOrdenada x xs = x `elem` takeWhile (<=x) xs
