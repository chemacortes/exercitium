module Ex_20160316 where

{-

Rotación de una matriz
======================

En la siguiente figura, al rotar girando 90 grados en el sentido del reloj la
matriz de la izquierda, obtenemos la de la derecha

   1 2 3        7 4 1
   4 5 6        8 5 2
   7 8 9        9 6 3

Definir la función

   rota :: Matrix Int -> Matrix Int

tal que (rota p) es la matriz obtenida girando en el sentido del reloj la
matriz cuadrada p. Por ejemplo,

   λ> rota (fromList 3 3 [1..9])
   ( 7 4 1 )
   ( 8 5 2 )
   ( 9 6 3 )

   λ> rota (fromList 3 3 [7,4,1,8,5,2,9,6,3])
   ( 9 8 7 )
   ( 6 5 4 )
   ( 3 2 1 )

-}

import Data.Matrix
import Data.Vector as V (toList)

rota :: Matrix Int -> Matrix Int
rota = rota2

rota1 :: Matrix Int -> Matrix Int
rota1 p = fromLists [[ p!(i,j) | i <- reverse [1..nrows p]] | j <- [1..ncols p]]

rota2 :: Matrix Int -> Matrix Int
rota2 p = fromLists . map reverse $ toCols p
    where toCols m = [V.toList (getCol n m) | n <- [1..ncols p]]

rota3 :: Matrix Int -> Matrix Int
rota3 = fromLists . map reverse . toLists . transpose

rota4 :: Matrix Int -> Matrix Int
rota4 p = matrix n m (\(i,j) -> p ! (n+1-j,i))
    where m = nrows p
          n = ncols p