module Ex_20161207 where

{-

Elementos que respetan la ordenación
====================================

Se dice que un elemento x de una lista xs respeta la ordenación si x es mayor o
igual que todos lo que tiene delante en xs y es menor o igual que todos lo que
tiene detrás en xs. Por ejemplo, en la lista lista [3,2,1,4,6,5,7,9,8] el número
4 respeta la ordenación pero el número 5 no la respeta (porque es mayor que el 6
que está delante).

Definir la función

   respetuosos :: Ord a => [a] -> [a]

tal que (respetuosos xs) es la lista de los elementos de xs que respetan la
ordenación. Por ejemplo,

   respetuosos [3,2,1,4,6,4,7,9,8]  ==  [4,7]
   respetuosos [2,1,3,4,6,4,7,8,9]  ==  [3,4,7,8,9]
   respetuosos "abaco"              ==  "aco"
   respetuosos "amor"               ==  "amor"
   respetuosos "romanos"            ==  "s"
   respetuosos [1..9]               ==  [1,2,3,4,5,6,7,8,9]
   respetuosos [9,8..1]             ==  []

Comprobar con QuickCheck que para cualquier lista de enteros xs se verifican las
siguientes propiedades:

    todos los elementos de (sort xs) respetan la ordenación y
    en la lista (nub (reverse (sort xs))) hay como máximo un elemento que respeta
    la ordenación.

-}

import Data.List
import Test.QuickCheck

respetuosos :: Ord a => [a] -> [a]
respetuosos xs = [ x | (x,ys,zs) <- zip3 xs (inits xs) (tails xs)
                     , all (<=x) ys
                     , all (>=x) zs ]

respetuosos2 :: Ord a => [a] -> [a]
respetuosos2 xs = [a | (a, b) <-zip (scanl1 max xs) (scanr1 min xs), a == b]

prop_respetuosos1 :: [Int] -> Bool
prop_respetuosos1 xs = respetuosos ys == ys
  where ys = sort xs

prop_respetuosos2 :: [Int] -> Bool
prop_respetuosos2 xs = length (respetuosos ys) <= 1
  where ys = nub (reverse (sort xs))
