module Ex_20161213 where

{-

Agrupación por orden de aparición
=================================

Definir la función

   agrupacion :: Eq a => [a] -> [a]

tal que (agrupacion xs) es la lista obtenida agrupando los elementos de xs según su primera aparición. Por ejemplo,

   agrupacion [3,1,5,1,7,1,5,3]  ==  [3,3,1,1,1,5,5,7]
   agrupacion "babcacb"          ==  "bbbaacc"

-}

import Data.List (partition)

agrupacion :: Eq a => [a] -> [a]
agrupacion []     = []
agrupacion (x:xs) = x : ys ++ agrupacion zs
  where (ys,zs) = partition (==x) xs
