module Ex_20160107 where

{-

Fórmula dual
============

Las fórmulas proposicionales construidas con las constantes verdadero (⊤), 
falso (⊥), las variables proposicionales y las conectivas de negación (¬), 
conjunción (∧) y disyunción (∨) se pueden definir usando el siguiente tipo de 
datos

   data Prop = Const Bool
             | Var Char
             | Neg Prop
             | Conj Prop Prop
             | Disj Prop Prop
             deriving (Eq, Show)

Por ejemplo, la fórmula (A ∧ ⊥) ∨ (⊤ ∧ B) se representa por

   Disj (Conj (Var 'A') (Const False)) (Conj (Const True) (Var 'B'))

La fórmula dual de una fórmula p es la fórmula obtenida intercambiando en p 
las ∧ por ∨ y también las ⊤ por ⊥. Por ejemplo, la dual de (A ∧ ⊥) ∨ (⊤ ∧ B) 
es (A ∨ ⊤) ∧ (⊥ ∨ B)

Definir la función

   dual :: Prop -> Prop

tal que (dual p) es la dual de p. Por ejemplo,

   λ> dual (Disj (Conj (Var 'A') (Const False)) (Conj (Const True) (Var 'B')))
   Conj (Disj (Var 'A') (Const True)) (Disj (Const False) (Var 'B'))
   
-}

data Prop = Const Bool
          | Var Char
          | Neg Prop
          | Conj Prop Prop
          | Disj Prop Prop
          deriving (Eq, Show)

dual :: Prop -> Prop
dual (Const a)  = Const (not a)
dual (Conj a b) = Disj (dual a) (dual b)
dual (Disj a b) = Conj (dual a) (dual b)
dual p          = p

