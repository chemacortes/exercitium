module Ex_20160309 where

{-

Máxima longitud de las sublistas comunes
========================================

Las sublistas comunes de "1325" y "36572" son "","3","32","35","2" y "5". El
máximo de sus longitudes es 2.

Definir la función

   maximo :: Eq a => [a] -> [a] -> Int

tal que (maximo xs ys) es el máximo de las longitudes de las sublistas comunes
de xs e ys. Por ejemplo,

   maximo "1325" "36572"       == 2
   maximo [1,4..33] [2,4..33]  == 5
   maximo [1..10^6] [1..10^6]  == 100000

-}

import Data.List

maximo :: Eq a => [a] -> [a] -> Int
maximo = maximo2

maximo1 :: Eq a => [a] -> [a] -> Int
maximo1 [] _          = 0
maximo1 _ []          = 0
maximo1 (x:xs) (y:ys) | x == y    = 1 + maximo1 xs ys
                      | otherwise = max (maximo1 (x:xs) ys)
                                        (maximo1 xs (y:ys))

maximo2 :: Eq a => [a] -> [a] -> Int
maximo2 [] _          = 0
maximo2 _ []          = 0
maximo2 (x:xs) (y:ys) | x == y    = 1 + maximo2 xs ys
                      | otherwise = maximum [
                            maximo2 (x:xs) ys',
                            maximo2 xs' (y:ys),
                            maximo2 xs ys
                         ]
    where xs' = dropWhile (/=y) xs
          ys' = dropWhile (/=x) ys

-- nueva versión de maximo2
maximo3 :: Eq a => [a] -> [a] -> Int
maximo3 l1@(x:xs) l2@(y:ys)
    | x == y    = 1 + maximo3 xs ys
    | otherwise = max (maximo3 xs l2) (maximo3 l1 ys)
maximo3 _ _ = 0

-- versión poco eficiente con subsecuencias
maximo4 :: Eq a => [a] -> [a] -> Int
maximo4 xs ys =
    maximum (map length (sublistasComunes xs ys))

sublistasComunes :: Eq a => [a] -> [a] -> [[a]]
sublistasComunes xs ys =
    subsequences xs `intersect` subsequences ys
