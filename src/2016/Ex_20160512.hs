module Ex_20160512 where

{-

Juego de bloques con letras
===========================

Para el juego de los bloques se dispone de un conjunto de bloques con una letra
en cada una de sus dos caras. El objetivo del juego consiste en formar palabras
sin que se pueda usar un bloque más de una vez y sin diferenciar mayúsculas de
minúsculas. Por ejemplo, si se tiene tres bloques de forma que el 1º tiene las
letras A y B, el 2ª la N y la O y el 3º la O y la A entonces se puede obtener
la palabra ANA de dos formas: una con los bloques 1, 2 y 3 y otra con los 3, 2 y 1.

Definir la función

   soluciones :: [String] -> String -> [[String]]

tal que (soluciones bs cs) es la lista de las soluciones del juego de los bloque
usando los bloques bs (cada bloque es una cadena de dos letras mayúsculas) para
formar la palabra cs. Por ejemplo,

   ghci> soluciones ["AB","NO","OA"] "ANA"
   [["AB","NO","OA"],["OA","NO","AB"]]
   ghci> soluciones ["AB","NE","OA"] "Bea"
   [["AB","NE","OA"]]
   ghci> soluciones ["AB","NE","OA"] "EvA"
   []
   ghci> soluciones ["AB","NO","OA","NC"] "ANA"
   [["AB","NO","OA"],["AB","NC","OA"],["OA","NO","AB"],["OA","NC","AB"]]
   ghci> soluciones ["AB","NO","OA","NC"] "Anca"
   [["AB","NO","NC","OA"],["OA","NO","NC","AB"]]
   ghci> soluciones (["AB","NO","OA"] ++ replicate (10^6) "PQ") "ANA"
   [["AB","NO","OA"],["OA","NO","AB"]]

-}

import Data.List (delete)
import Data.Char (toUpper)

soluciones :: [String] -> String -> [[String]]
soluciones _ []      = [[]]
soluciones [] _      = [[]]
soluciones bs (c:cs) = [ xs:xss | xs <- filter (toUpper c `elem`) bs
                                , xss <- soluciones (delete xs bs) cs ]
