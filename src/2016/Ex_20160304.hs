module Ex_20160304 where

{-

Número de islas rectangulares de una matriz
===========================================

En este problema se consideran matrices cuyos elementos son 0 y 1. Los valores
1 aparecen en forma de islas rectangulares separadas por 0 de forma que como
máximo las islas son diagonalmente adyacentes. Por ejemplo,

   ej1, ej2 :: Array (Int,Int) Int
   ej1 = listArray ((1,1),(6,3))
                   [0,0,0,
                    1,1,0,
                    1,1,0,
                    0,0,1,
                    0,0,1,
                    1,1,0]
   ej2 = listArray ((1,1),(6,6))
                   [1,0,0,0,0,0,
                    1,0,1,1,1,1,
                    0,0,0,0,0,0,
                    1,1,1,0,1,1,
                    1,1,1,0,1,1,
                    0,0,0,0,1,1]

Definir la función

   numeroDeIslas :: Array (Int,Int) Int -> Int

tal que (numeroDeIslas p) es el número de islas de la matriz p. Por ejemplo,

   numeroDeIslas ej1  ==  3
   numeroDeIslas ej2  ==  4

-}

import Data.Array

ej1, ej2 :: Array (Int,Int) Int
ej1 = listArray ((1,1),(6,3))
               [0,0,0,
                1,1,0,
                1,1,0,
                0,0,1,
                0,0,1,
                1,1,0]
ej2 = listArray ((1,1),(6,6))
               [1,0,0,0,0,0,
                1,0,1,1,1,1,
                0,0,0,0,0,0,
                1,1,1,0,1,1,
                1,1,1,0,1,1,
                0,0,0,0,1,1]

numeroDeIslas :: Array (Int,Int) Int -> Int
numeroDeIslas p = length [ (i,j) | (i,j) <- indices p
                                 , p!(i,j) == 1
                                 , j == 1 || p!(i,j-1) == 0
                                 , i == 1 || p!(i-1,j) == 0 ]
