module Ex_20161107 where
{-

Primo anterior
==============

Definir la función

   primoAnterior :: Integer -> Integer

tal que (primoAnterior n) es el mayor primo menor que n (donde n > 2). Por ejemplo,

   primoAnterior 10     ==  7
   primoAnterior 17     ==  13
   primoAnterior 30     ==  29
   primoAnterior 2016   ==  2011
   primoAnterior 15726  ==  15683

Calcular el menor número cuya distancia a su primo anterior es mayor que 40.


-}

import Data.Numbers.Primes

paresPrimos :: [(Integer, Integer)]
paresPrimos = zip primes (tail primes)

primoAnterior :: Integer -> Integer
primoAnterior = primoAnterior4

primoAnterior1 :: Integer -> Integer
primoAnterior1 n = last $ takeWhile (<n) primes

primoAnterior2 :: Integer -> Integer
primoAnterior2 n = head $ filter isPrime [n,n-1..]

primoAnterior3 :: Integer -> Integer
primoAnterior3 n = head [x | (x,y) <- paresPrimos, y >= n]

-- Solución oficial
primoAnterior4 :: Integer -> Integer
primoAnterior4 3 = 2
primoAnterior4 n =
  head [x | x <- [a,a-2..]
          , isPrime x]
  where a | even n    = n-1
          | otherwise = n-2


menor :: Integer -> Integer
menor = menor3

menor1 :: Integer -> Integer
menor1 n = head $ filter (\x -> x == primoAnterior x + n) candidatos
  where candidatos = map (+n) primes

-- Sin usar `primoAnterior`
menor2 :: Integer -> Integer
menor2 n = aux primes
  where aux (x:y:xs) | x+n < y   = x
                     | otherwise = aux (y:xs)

menor3 :: Integer -> Integer
menor3 n = n + head [x | (x,y) <- paresPrimos, x+n < y]
