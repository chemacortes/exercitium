module Ex_20160308 where

{-

Elemento ausente
================

Sea xs una lista y n su longitud. Se dice que xs es casi completa si sus
elementos son los números enteros entre 0 y n excepto uno. Por ejemplo, la
lista [3,0,1] es casi completa.

Definir la función

   ausente :: [Integer] -> Integer

tal que (ausente xs) es el único entero (entre 0 y la longitud de xs) que no
pertenece a la lista casi completa xs. Por ejemplo,

   ausente [3,0,1]               ==  2
   ausente [1,2,0]               ==  3
   ausente (1+10^7:[0..10^7-1])  ==  10000000

-}

import Data.List

ausente :: [Integer] -> Integer
ausente = ausente1

ausente1 :: [Integer] -> Integer
ausente1 xs = head $ [0..genericLength xs] \\ xs

ausente2 :: [Integer] -> Integer
ausente2 xs | null ys   = n
            | otherwise = head ys
    where n = genericLength xs
          ys = [i | (x,i) <- zip (sort xs) [0..], x /= i]

ausente3 :: [Integer] -> Integer
ausente3 xs =
    ((n * (n+1)) `div` 2) - sum xs
    where n = genericLength xs  