module Ex_20161209 where

{-

Caracteres en la misma posición que en el alfabeto
==================================================

Un carácter c de una cadena cs está bien colocado si la posición de c en cs es
la misma que en el abecedario (sin distinguir entre mayúsculas y minúsculas).
Por ejemplo, los elementos bien colocados de la cadena “aBaCEria” son ‘a’, ‘B’
y ‘E’.

Definir la función

   nBienColocados :: String -> Int

tal que (nBienColocados cs) es el número de elementos bien colocados de la
cadena cs. Por ejemplo,

   nBienColocados "aBaCEria"                    ==  3
   nBienColocados "xBxxExxxIxxxxNxxxxxTxxxXYZ"  ==  8


-}

import Data.Char (toLower)

nBienColocados1 :: String -> Int
nBienColocados1 cs = length [c | (c,x) <- zip cs ['a'..'z']
                              , toLower c == x]


nBienColocados :: String -> Int
nBienColocados = length . filter (uncurry (==)) . zip ['a'..'z'] . fmap toLower
