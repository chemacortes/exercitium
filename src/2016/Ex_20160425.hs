module Ex_20160425 where

{-

Productos simultáneos de dos y tres números consecutivos
========================================================

Definir la función

   productos :: Integer -> Integer -> [[Integer]]

tal que (productos n x) es las listas de n elementos consecutivos cuyo producto
es x. Por ejemplo,

   productos 2 6     ==  [[2,3]]
   productos 3 6     ==  [[1,2,3]]
   productos 4 1680  ==  [[5,6,7,8]]
   productos 2 5     ==  []

Comprobar con QuickCheck que si n > 0 y x > 0, entonces

   productos n (product [x..x+n-1]) == [[x..x+n-1]]

Usando productos, definir la función

   productosDe2y3consecutivos :: [Integer]

cuyos elementos son los números naturales (no nulos) que pueden expresarse
simultáneamente como producto de dos y tres números consecutivos. Por ejemplo,

   head productosDe2y3consecutivos  ==  6

Nota. Según demostró Mordell en 1962, productosDe2y3consecutivos sólo tiene dos elementos.

-}

import Data.List
import Test.QuickCheck

productos :: Integer -> Integer -> [[Integer]]
productos = productos3

productos1 :: Integer -> Integer -> [[Integer]]
productos1 n x = [xs | xs <- xss, genericLength xs == n, product xs == x]
    where xss = map (genericTake n) (tails [1..x])

productos2 :: Integer -> Integer -> [[Integer]]
productos2 n x = [[y..y+n-1] | y <- [1..x],
                               product [y..y+n-1] == x]

-- Se puede reducir el intervalo de búsqueda teniendo en cuenta las
-- siguientes desigualdades
--    y*(y+1)* ... (y+n-1) = x
--    y^n <= x <= (y+n-1)^n
--    y <= x^(1/n), x^(1/n)-n+1 <= y
--    x^(1/n)-n+1 <= y <= x^(1/n)

productos3 :: Integer -> Integer -> [[Integer]]
productos3 n x = [[z..z+n-1] | z <- [y-n+1..y],
                               product [z..z+n-1] == x]
    where y = floor ((fromIntegral x)**(1/(fromIntegral n)))



prop_productos :: Positive Integer -> Positive Integer -> Bool
prop_productos (Positive n) (Positive x) =
    productos n (product [x..x+n-1]) == [[x..x+n-1]]


productosDe2y3consecutivos :: [Integer]
productosDe2y3consecutivos = [x | x <- [1..]
                                , (not.null.productos 2) x
                                , (not.null.productos 3) x ]
