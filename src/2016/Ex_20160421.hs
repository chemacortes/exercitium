module Ex_20160421 where

{-

Polinomios cuadráticos generadores de primos
============================================

En 1772, Euler publicó que el polinomio n² + n + 41 genera 40 números primos
para todos los valores de n entre 0 y 39. Sin embargo, cuando n=40,
40²+40+41 = 40(40+1)+41 es divisible por 41.

Usando ordenadores, se descubrió el polinomio n² – 79n + 1601 que genera 80
números primos para todos los valores de n entre 0 y 79.

Definir la función

   generadoresMaximales :: Integer -> (Int,[(Integer,Integer)])

tal que (generadoresMaximales n) es el par (m,xs) donde

    - xs es la lista de pares (x,y) tales que n²+xn+y es uno de los polinomios
    que genera un número máximo de números primos consecutivos a partir de cero
    entre todos los polinomios de la forma n²+an+b, con |a| ≤ n y |b| ≤ n y

    - m es dicho número máximo.

Por ejemplo,

   generadoresMaximales    4  ==  ( 3,[(-2,3),(-1,3),(3,3)])
   generadoresMaximales    6  ==  ( 5,[(-1,5),(5,5)])
   generadoresMaximales   50  ==  (43,[(-5,47)])
   generadoresMaximales  100  ==  (48,[(-15,97)])
   generadoresMaximales  200  ==  (53,[(-25,197)])
   generadoresMaximales 1650  ==  (80,[(-79,1601)])
-}

import Data.Numbers.Primes

generadoresMaximales :: Integer -> (Int,[(Integer,Integer)])
generadoresMaximales n = (m, [ab | (x,ab) <- ys, x==m])
    where xs = [(a,b) | b <- takeWhile (<=n) primes
                      , a <- takeWhile ((<=n).abs) [p-b-1| p <- primes] ]
          ys = [ (length (generador ab), ab) | ab <- xs ]
          m = (maximum . map fst) ys

-- Generador de números primos según la fórmula n²+xn+y
generador :: (Integer, Integer) -> [Integer]
generador (x,y) = takeWhile isPrime [ n*n + x*n + y | n <- [0..]]

