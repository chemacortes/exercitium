module Ex_20160209 where

{-

Número de representaciones de n como suma de dos cuadrados
==========================================================

Sea n un número natural cuya factorización prima es

   n = 2^a * p(1)^b(1) *...* p(n)^b(n) * q(1)^c(1) *...* q(m)^c(m)

donde los p(i) son los divisores primos de n congruentes con 3 módulo 4 y los
q(j) son los divisores primos de n congruentes con 1 módulo 4.

Entonces, el número de formas de descomponer n como suma de dos cuadrados es 0
si algún b(i) es impar, y es el techo (es decir, el número entero más próximo
por exceso) de

   ((1+c(1)) *...* (1+c(m))) / 2

en caso contrario. Por ejemplo, el número 2^3*(3^9*7^8)*(5^3*13^6) no se puede
descomponer como sumas de dos cuadrados (porque el exponente de 3 es impar) y
el número 2^3*(3^2*7^8)*(5^3*13^6) tiene 14 descomposiciones como suma de dos
cuadrados (porque los exponentes de 3 y 7 son pares y el techo de
((1+3)*(1+6))/2 es 14).

Definir la función

   nRepresentaciones :: Integer -> Integer

tal que (nRepresentaciones n) es el número de formas de representar n como suma
de dos cuadrados. Por ejemplo,

   nRepresentaciones (2^3*3^9*5^3*7^8*13^6)        ==  0
   nRepresentaciones (2^3*3^2*5^3*7^8*13^6)        ==  14
   head [n | n <- [1..], nRepresentaciones n > 8]  ==  71825

Usando la función representaciones del ejercicio anterior, comprobar con
QuickCheck la siguiente propiedad

   prop_nRepresentaciones :: Integer -> Property
   prop_nRepresentaciones n =
       n > 0 ==>
         nRepresentaciones2 n == genericLength (representaciones n)

-}

import Data.Numbers.Primes
import Data.List
import Test.QuickCheck

import Ex_20160208 (representaciones)

nRepresentaciones :: Integer -> Integer
nRepresentaciones n | any odd bs    = 0
                    | otherwise     = ceiling $ fromIntegral producto / 2
    where
        ns = [(head xs,genericLength xs) | xs <- group $ primeFactors n]
        bs = [b | (p,b) <- ns, p `mod` 4 == 3]
        cs = [c | (q,c) <- ns, q `mod` 4 == 1]
        producto = (product . map (1+)) cs


prop_nRepresentaciones :: Positive Integer -> Bool
prop_nRepresentaciones n =
        nRepresentaciones np == (genericLength . representaciones) np
    where np = getPositive n
