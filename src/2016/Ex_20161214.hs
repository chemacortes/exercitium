module Ex_20161214 where

{-

Distancia a Erdős
=================

Una de las razones por la que el matemático húngaro [Paul Erdős][1] es conocido
es por la multitud de colaboraciones que realizó durante toda su carrera, un
total de 511. Tal es así que se establece la distancia a Erdős como la distancia
que has estado de coautoría con Erdős. Por ejemplo, si eres Paul Erdős tu
distancia a Erdős es 0, si has escrito un artículo con Erdős tu distancia es 1,
si has escrito un artículo con alguien que ha escrito un artículo con Erdős tu
distancia es 2, etc. El objetivo de este problema es definir una función que a
partir de una lista de pares de coautores y un número natural n calcular la
lista de los matemáticos a una distancia n de Erdős.

[1]: https://es.wikipedia.org/wiki/Paul_Erdős

Para el problema se considerará la siguiente lista de coautores

   coautores :: [(String,String)]
   coautores =
     [("Paul Erdos","Ernst Straus"),("Paul Erdos","Pascual Jordan"),
      ("Paul Erdos","D. Kleitman"),("Albert Einstein","Ernst Straus"),
      ("John von Newmann","David Hilbert"),("S. Glashow","D. Kleitman"),
      ("John von Newmann","Pascual Jordan"), ("David Pines","D. Bohm"),
      ("Albert Einstein","Otto Stern"),("S. Glashow", "M. Gell-Mann"),
      ("Richar Feynman","M. Gell-Mann"),("M. Gell-Mann","David Pines"),
      ("David Pines","A. Bohr"),("Wolfgang Pauli","Albert Einstein"),
      ("D. Bohm","L. De Broglie"), ("Paul Erdos","J. Conway"),
      ("J. Conway", "P. Doyle"),("Paul Erdos","A. J. Granville"),
      ("A. J. Granville","B. Mazur"),("B. Mazur","Andrew Wiles")]

La lista anterior es real y se ha obtenido del artículo Famous trails to Paul Erdős.

Definir la función

   numeroDeErdos :: [(String, String)] -> Int -> [String]

tal que (numeroDeErdos xs n) es la lista de lista de los matemáticos de la lista
cd coautores xs que se encuentran a una distancia n de Erdős. Por ejemplo,

   λ> numeroDeErdos coautores 0
   ["Paul Erdos"]
   λ> numeroDeErdos coautores 1
   ["Ernst Straus","Pascual Jordan","D. Kleitman","J. Conway","A. J. Granville"]
   λ> numeroDeErdos coautores 2
   ["Albert Einstein","John von Newmann","S. Glashow","P. Doyle","B. Mazur"]

Nota: Este ejercicio ha sido propuesto por Enrique Naranjo.

-}

import Data.Maybe (mapMaybe)

coautores :: [(String,String)]
coautores =
  [("Paul Erdos","Ernst Straus"),("Paul Erdos","Pascual Jordan"),
   ("Paul Erdos","D. Kleitman"),("Albert Einstein","Ernst Straus"),
   ("John von Newmann","David Hilbert"),("S. Glashow","D. Kleitman"),
   ("John von Newmann","Pascual Jordan"), ("David Pines","D. Bohm"),
   ("Albert Einstein","Otto Stern"),("S. Glashow", "M. Gell-Mann"),
   ("Richar Feynman","M. Gell-Mann"),("M. Gell-Mann","David Pines"),
   ("David Pines","A. Bohr"),("Wolfgang Pauli","Albert Einstein"),
   ("D. Bohm","L. De Broglie"), ("Paul Erdos","J. Conway"),
   ("J. Conway", "P. Doyle"),("Paul Erdos","A. J. Granville"),
   ("A. J. Granville","B. Mazur"),("B. Mazur","Andrew Wiles")]

numeroDeErdos :: [(String, String)] -> Int -> [String]
numeroDeErdos _ 0   = ["Paul Erdos"]
numeroDeErdos xs n  = mapMaybe (correspondeCon cercanos) xs
  where cercanos = numeroDeErdos xs =<< [0..n-1]


correspondeCon ::  [String] -> (String, String) -> Maybe String
correspondeCon xs (a,b) | a `elem` xs && b `elem` xs  = Nothing
                        | a `elem` xs                 = Just b
                        | b `elem` xs                 = Just a
                        | otherwise                   = Nothing
