module Ex_20161117 where

{-

Números poderosos
=================

Un número es poderoso si es igual a la suma de sus dígitos elevados a sus
respectivas posiciones. Por ejemplo, los números 89, 135 y 1306 son poderosos
ya que

     89 = 8^1 + 9^2
    135 = 1^1 + 3^2 + 5^3.
   1306 = 1^1 + 3^2 + 0^3 + 6^4

Definir la función

   esPoderoso :: Integer -> Bool

tal que (esPoderoso n) se verifica si n es poderoso. Por ejemplo,

   λ> esPoderoso 135
   True
   λ> esPoderoso 80
   False
   λ> esPoderoso 89
   True
   λ> esPoderoso 12157692622039623539
   True
   λ> [n | n <- [10..30000], esPoderoso n]
   [89,135,175,518,598,1306,1676,2427]

Comprobar con QuickCheck que 12157692622039623539 es el mayor número poderoso.

-}

import Test.QuickCheck

esPoderoso :: Integer -> Bool
esPoderoso n = n == sum (zipWith (^) (digitos n) [1..])

digitos :: Integer -> [Integer]
digitos n = [read [c] | c <- show n]

prop_poderosos :: Positive Integer -> Bool
prop_poderosos (Positive n) =
  (not.esPoderoso) (n + 12157692622039623539)
