module Ex_20151120 where

{-

Ternas con suma acotada
=======================

Definir la función

   ternasAcotadas :: [Int] -> Int -> [(Int,Int,Int)]

tal que (ternasAcotadas xs n) es el conjunto de ternas de números naturales de 
xs cuya suma es menor que n. Por ejemplo,

   ternasAcotadas [5,1,3,4,7] 12      ==  [(1,3,4),(1,3,5),(1,3,7),(1,4,5)]
   ternasAcotadas [5,1,3,4,7] 11      ==  [(1,3,4),(1,3,5),(1,4,5)]
   ternasAcotadas [5,1,3,4,7] 10      ==  [(1,3,4),(1,3,5)]
   ternasAcotadas [5,1,3,4,7]  9      ==  [(1,3,4)]
   ternasAcotadas [5,1,3,4,7]  8      ==  []
   ternasAcotadas [1..10^6] 8         ==  [(1,2,3),(1,2,4)]
   ternasAcotadas [10^6,10^6-1..1] 8  ==  [(1,2,3),(1,2,4)]

-}

import Data.List

-- Primera solución, poco optimizada O(n^3) 
ternasAcotadas1 :: [Int] -> Int -> [(Int,Int,Int)]
ternasAcotadas1 xs n = sort [toTuple ys | ys <- (subsequences . sort) xs
                                        , length ys == 3
                                        , sum ys < n ]
    where toTuple [a,b,c] = (a,b,c)
    

ternasAcotadas :: [Int] -> Int -> [(Int,Int,Int)]
ternasAcotadas xs n = [(a,b,c) | a <- ys, b <- ys, c <- ys
                               , a < b && b < c
                               , a+b+c < n ]
    where ys = filter (`elem` xs) [1..n] 

ternasAcotadas2 :: [Int] -> Int -> [(Int,Int,Int)]
ternasAcotadas2 xs n = nub (aux (sort xs))
    where aux xs = [(x,y,z) | (x:ys) <- tails (takeWhile (< n) xs)
                            , (y:zs) <- tails (takeWhile (< (n-x)) ys)
                            , z <- takeWhile (< (n-x-y)) zs]