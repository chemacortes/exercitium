module Ex_20151029 where

{-

Próximos a múltiplos de 6
=========================

Se dice que un par de números (x,y) está próximo a un múltiplo de 6 si es de la 
forma (6*n-1,6*n+1). Por ejemplo, (17,19) está cerca de un múltiplo de 6 porque 
(17,19) = (6*3-1,6*3+1).

Definir la función

   proximosAmultiplosDe6 :: (Integer,Integer) -> Bool

tal que (proximosAmultiplosDe6 (x,y)) se verifica si el par (x,y) está próximo 
a un múltiplo de 6. Por ejemplo,

   proximosAmultiplosDe6 (17,19)                          ==  True
   proximosAmultiplosDe6 (18,20)                          ==  False
   proximosAmultiplosDe6 (5,19)                           ==  False
   proximosAmultiplosDe6 (1,3)                            ==  False
   proximosAmultiplosDe6 (74074073407403,74074073407405)  ==  True
   proximosAmultiplosDe6 (86419752308637,86419752308639)  ==  False
   
-}

proximosAmultiplosDe6 :: (Integer,Integer) -> Bool
proximosAmultiplosDe6 (x,y) = (y == x+2) && (x+1) `mod` 6 == 0
