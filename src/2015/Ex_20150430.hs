module Ex_20150430 where

{-
Rotaciones de un número
=======================

Definir la función

   rotacionesNumero :: Integer -> [Integer]

(rotacionesNumero n) es la lista de las rotaciones obtenidas desplazando el primer
dígito de n al final. Por ejemplo,

   rotacionesNumero 325  ==  [325,253,532]
-}

-- composición de funciones
rotacionesNumero :: Integer -> [Integer]
rotacionesNumero n = (take (length ns) . map read . iterate rotl) ns
    where ns = show n
          rotl (x:xs) = xs ++ [x]

-- compresión de listas
rotacionesNumero2 :: Integer -> [Integer]
rotacionesNumero2 = map read . rota . show

rota :: [a] -> [[a]]
rota xs = [zs++ys | i <- [0..(length xs -1)], let (ys,zs) = splitAt i xs]
