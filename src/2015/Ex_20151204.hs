module Ex_20151204 where

{-

Los números de Smith
====================

Un número de Smith es un número natural compuesto que cumple que la suma de sus 
dígitos es igual a la suma de los dígitos de todos sus factores primos (si 
tenemos algún factor primo repetido lo sumamos tantas veces como aparezca). Por 
ejemplo, el 22 es un número de Smith ya que

    22 = 2*11 y
   2+2 = 2+(1+1)

y el 4937775 también lo es ya que

   4937775       = 3*5*5*65837 y 
   4+9+3+7+7+7+5 = 3+5+5+(6+5+8+3+7)

Definir las funciones

   esSmith :: Integer -> Bool
   smith :: [Integer]

tales que

    (esSmith x) se verifica si x es un número de Smith. Por ejemplo,

     esSmith 22          ==  True
     esSmith 29          ==  False
     esSmith 2015        ==  False
     esSmith 4937775     ==  True
     esSmith 4567597056  ==  True

    smith es la lista cuyos elementos son los números de Smith. Por ejemplo,

     λ> take 17 smith
     [4,22,27,58,85,94,121,166,202,265,274,319,346,355,378,382,391]
     
-}

import Data.Numbers.Primes

esSmith :: Integer -> Bool
esSmith n | length ps < 2   = False  -- No es compuesto
          | otherwise       = sum (digitos n) == sum (concatMap digitos ps)
    where ps = primeFactors n

digitos :: Integer -> [Integer]
digitos x = [read [c] | c <- show x]

smith :: [Integer]
smith = filter esSmith [1..]
