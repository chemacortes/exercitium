module Ex_20150529 where

{-

Matrices latinas
================

Una matriz latina de orden n es una matriz cuadrada de orden n tal que todos
sus elementos son cero salvo los de su fila y columna central, si n es impar; o
los de sus dos filas y columnas centrales, si n es par.

Definir la función

   latina :: Int -> Array (Int,Int) Int

tal que (latina n) es la siguiente matriz latina de orden n:

    Para n impar:

     | 0  0... 0 1   0 ... 0   0|
     | 0  0... 0 2   0 ... 0   0|
     | 0  0... 0 3   0 ... 0   0|
     | .........................|
     | 1  2..............n-1   n|
     | .........................|
     | 0  0... 0 n-2 0 ... 0   0|
     | 0  0... 0 n-1 0 ... 0   0|
     | 0  0... 0 n   0 ... 0   0|

    Para n par:

     | 0  0... 0 1   n    0 ...   0   0|
     | 0  0... 0 2   n-1  0 ...   0   0|
     | 0  0... 0 3   n-2  0 ...   0   0|
     | ................................|
     | 1  2.....................n-1   n|
     | n n-1 .................... 2   1|
     | ................................|
     | 0  0... 0 n-2  3   0 ...   0   0|
     | 0  0... 0 n-1  2   0 ...   0   0|
     | 0  0... 0 n    1   0 ...   0   0|

Por ejemplo,

   ghci> elems (latina 5)
   [0,0,1,0,0,
    0,0,2,0,0,
    1,2,3,4,5,
    0,0,4,0,0,
    0,0,5,0,0]
   ghci> elems (latina 6)
   [0,0,1,6,0,0,
    0,0,2,5,0,0,
    1,2,3,4,5,6,
    6,5,4,3,2,1,
    0,0,5,2,0,0,
    0,0,6,1,0,0]

-}

import Data.Array

-- definida por función
latina :: Int -> Array (Int,Int) Int
latina n = listArray ((1,1),(n,n)) [ elemento (i,j) | i <- [1..n], j <- [1..n]]
    where
        m = n `div` 2
        elemento | even n    = ePar
                 | otherwise = eImpar
        eImpar (i,j) | i == m+1  = j
                     | j == m+1  = i
                     | otherwise = 0
        ePar (i,j)   | i == m    = j
                     | j == m    = i
                     | i == m+1  = n-j+1
                     | j == m+1  = n-i+1
                     | otherwise = 0

-- por actualización incremental
latina2 :: Int -> Array (Int,Int) Int
latina2 n | even n    = ceros
                     // [ ((i, m), i)       | i <- [1..n]]
                     // [ ((m, j), j)       | j <- [1..n]]
                     // [ ((i, m+1), n-i+1) | i <- [1..n]]
                     // [ ((m+1, j), n-j+1) | j <- [1..n]]
          | otherwise = ceros
                     // [ ((i, m+1), i) | i <- [1..n]]
                     // [ ((m+1, j), j) | j <- [1..n]]
    where
        m = n `div` 2
        ceros = listArray ((1,1),(n,n)) (repeat 0)
