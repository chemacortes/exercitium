module Ex_20150605 where

{-
Menor número con una cantidad de divisores dada
===============================================

Definir la función

   menor :: Integer -> Integer -> Integer

tal que (menor n m) es el menor número, módulo m, con 2^n divisores. Por ejemplo,

   menor 4 1000                     ==  120
   menor 4 100                      ==  20
   [menor n (10^9) | n <- [1..98]]  ==  [2,6,24,120,840,7560,83160,1081080]
   menor 500500 500500506           ==  8728302
-}


-- FIXME: no entiendo el enunciado.
--  Si 20 es la solución de menor 4 100, entonces también tendría que serlo de menor 4 1000
--  En el caso de la compresión de lista, ¿cómo se filtran los valores?

import Data.Numbers.Primes
import Data.List


menor :: Integer -> Integer -> Integer
menor n m = head [x `mod` m | x <- [1..], numDivisores x == 2^n]

numDivisores :: Integer -> Integer
numDivisores =  product . map ((+1) . genericLength) . group . primeFactors


-- 2ª solución
-- ===========

menor2 :: Integer -> Integer -> Integer
menor2 n m = producto m . genericTake n $ potencias

-- potencias es la sucesión de las potencias de la forma p^(2^k),
-- donde p es un número primo y k es un número natural, ordenadas de
-- menor a mayor. Por ejemplo,
--    take 14 potencias    ==  [2,3,4,5,7,9,11,13,16,17,19,23,25,29]
potencias :: [Integer]
potencias = 2 : mezcla (tail primes) (map (^2) potencias)

-- (mezcla xs ys) es la lista obtenida mezclando las dos listas xs e ys,
-- que se suponen ordenadas y disjuntas. Por ejemplo,
--    ghci> take 15 (mezcla [2^n | n <- [1..]] [3^n | n <- [1..]])
--    [2,3,4,8,9,16,27,32,64,81,128,243,256,512,729]
mezcla :: Ord a => [a] -> [a] -> [a]
mezcla (x:xs) (y:ys) | x < y = x : mezcla xs (y:ys)
                     | x > y = y : mezcla (x:xs) ys
mezcla _ _ = []

-- (producto m xs) es el productos de los elementos de xs módulo m. Por
-- ejemplo,
--    producto 1000 [12,23,45,67,89]  ==  460
--    product       [12,23,45,67,89]  ==  74060460
producto :: Integer -> [Integer] -> Integer
producto m = foldl1' (\a b -> a * b `mod` m)


