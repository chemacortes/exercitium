module Ex_20151117 where

{-

Productos de N números consecutivos
===================================

La semana pasada se planteó en Twitter el siguiente problema

    Se observa que 

         1x2x3x4 = 2x3x4 
         2x3x4x5 = 4x5x6

    ¿Existen ejemplos de otros productos de cuatro enteros consecutivos iguales 
    a un producto de tres enteros consecutivos? 

Definir la función

   esProductoDeNconsecutivos :: Integer -> Integer -> Maybe Integer

tal que (esProductoDeNconsecutivos n x) es (Just m) si x es el producto de n 
enteros consecutivos a partir de m y es Nothing si x no es el producto de n 
enteros consecutivos. Por ejemplo,

   esProductoDeNconsecutivos 3   6  == Just 1
   esProductoDeNconsecutivos 4   6  == Nothing
   esProductoDeNconsecutivos 4  24  == Just 1
   esProductoDeNconsecutivos 3  24  == Just 2
   esProductoDeNconsecutivos 3 120  == Just 4
   esProductoDeNconsecutivos 4 120  == Just 2

Para ejemplos mayores,

   λ> esProductoDeNconsecutivos 3 (product [10^20..2+10^20])
   Just 100000000000000000000
   λ> esProductoDeNconsecutivos 4 (product [10^20..2+10^20])
   Nothing
   λ> esProductoDeNconsecutivos 4 (product [10^20..3+10^20])
   Just 100000000000000000000

Usando la función esProductoDeNconsecutivos resolver el problema.

-}

import Data.Maybe

-- Función producto
pk :: Integer -> Integer -> Integer
pk n x = product [x..x+n-1]


-- Definición directa
esProductoDeNconsecutivos1 :: Integer -> Integer -> Maybe Integer
esProductoDeNconsecutivos1 n x =
    lookup x [ (pk n i, i) | i <- [1..x]]

-- Aplicando búsqueda dicotómica
esProductoDeNconsecutivos :: Integer -> Integer -> Maybe Integer
esProductoDeNconsecutivos n x = busqDicotomica comp 1 x
    where comp z = compare x (pk n z)

-- Búsqueda dicotómica O(log n)
-- Aplicable en conjuntos ordenados 
busqDicotomica :: (Integer -> Ordering) -> Integer -> Integer -> Maybe Integer
busqDicotomica comp a b | comp a == EQ      = Just a
                        | medio <= a        = Nothing
                        | comp medio == LT  = busqDicotomica comp a medio
                        | otherwise         = busqDicotomica comp medio b
    where medio = a + (b-a) `div` 2

-- Probando con los primeros números naturales se obtiene una nueva solución:
-- 19x20x21x22 == 55x56x57
soluciones :: [([Integer], [Integer])]
soluciones = [ ([i..i+3],[j..j+2]) | (Just i,j) <- zip xs [1..limite] ]
    where xs = map (esProductoDeNconsecutivos 4 . pk 3) [1..]
          limite = 10000



--- SOLUCIÓN:
-- 1ª solución
-- ===========
 
esProductoDeNconsecutivos1b :: Integer -> Integer -> Maybe Integer
esProductoDeNconsecutivos1b n x 
    | x == y    = Just y
    | otherwise = Nothing    
    where y = head (dropWhile (<x) (productosDeNconsecutivos n))
 
-- (productosDeNconsecutivos n) es la lista de los números que son
-- productos de n enteros consecutivos. Por ejemplo,
--    take 5 (productosDeNconsecutivos 3)  == [6,24,60,120,210]
--    take 5 (productosDeNconsecutivos 4)  == [24,120,360,840,1680]
--    take 5 (productosDeNconsecutivos 5)  == [120,720,2520,6720,15120]
productosDeNconsecutivos :: Integer -> [Integer]
productosDeNconsecutivos n =
    [product [x..x+n-1] | x <- [1..]]
 
-- 2ª solución
-- ===========
 
esProductoDeNconsecutivos2 :: Integer -> Integer -> Maybe Integer
esProductoDeNconsecutivos2 n x = aux k
    where k = floor (fromIntegral x ** (1/(fromIntegral n))) - (n `div` 2)
          aux m | y == x    = Just m
                | y <  x    = aux (m+1)
                | otherwise = Nothing
                where y = product [m..m+n-1]
 
-- Solución del problema
-- =====================
 
soluciones2 :: [Integer]
soluciones2 = [x | x <- [121..]
                , isJust (esProductoDeNconsecutivos2 4 x)
                , isJust (esProductoDeNconsecutivos2 3 x)]
 
-- El cálculo es
--    λ> head soluciones
--    175560
--    λ> esProductoDeNconsecutivos2 4 175560
--    Just 19
--    λ> esProductoDeNconsecutivos2 3 175560
--    Just 55
--    λ> product [19,20,21,22] 
--    175560
--    λ> product [55,56,57]
--    175560
--    λ> product [19,20,21,22] == product [55,56,57]
--    True
 
-- Se puede definir una función para automatizar el proceso anterior:
soluciones3 :: [(Integer,[Integer],[Integer])]
soluciones3 = [(x,[a..a+3],[b..b+2]) 
               | x <- [121..]
               , let y = esProductoDeNconsecutivos2 4 x
               , isJust y
               , let z = esProductoDeNconsecutivos2 3 x
               , isJust z
               , let a = fromJust y
               , let b = fromJust z
               ]
 
-- El cálculo es 
--    λ> head soluciones2
--    (175560,[19,20,21,22],[55,56,57])