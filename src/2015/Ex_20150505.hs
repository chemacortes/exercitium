module Ex_20150505 where

{-
Representación matricial de relaciones binarias
===============================================

Dada una relación r sobre un conjunto de números enteros, la matriz asociada a r
es una matriz booleana p (cuyos elementos son True o False), tal que p(i,j) = True
si y sólo si i está relacionado con j mediante la relación r.

Las relaciones binarias homogéneas y las matrices booleanas se pueden representar por

   type Relacion = ([Int],[(Int,Int)])
   type Matriz = Array (Int,Int) Bool

Definir la función

   matrizRB:: Relacion -> Matriz

tal que (matrizRB r) es la matriz booleana asociada a r. Por ejemplo,

   ghci> matrizRB ([1..3],[(1,1), (1,3), (3,1), (3,3)])
   array ((1,1),(3,3)) [((1,1),True) ,((1,2),False),((1,3),True),
                        ((2,1),False),((2,2),False),((2,3),False),
                        ((3,1),True) ,((3,2),False),((3,3),True)]
   ghci> matrizRB ([1..3],[(1,3), (3,1)])
   array ((1,1),(3,3)) [((1,1),False),((1,2),False),((1,3),True),
                        ((2,1),False),((2,2),False),((2,3),False),
                        ((3,1),True) ,((3,2),False),((3,3),False)]
   ghci> let n = 10^4 in matrizRB ([1..n],[(1,n),(n,1)]) ! (n,n)
   False
-}

import Data.Array

type Relacion = ([Int],[(Int,Int)])
type Matriz = Array (Int,Int) Bool

matrizRB:: Relacion -> Matriz
matrizRB (xs,rs) = listArray ((1,1),(n,n)) [(i,j) `elem` rs | i <- xs, j <- xs]
    where n = last xs

-- una implementación más robusta
matrizRB2:: Relacion -> Matriz
matrizRB2 (xs,rs) = array dim $ map relacion (range dim)
    where relacion idx = (idx, idx `elem` rs)
          (n,m) = (minimum xs, maximum xs)
          dim = ((n,n),(m,m))

-- mejora para matrices poco densas
matrizRB3 :: Relacion -> Matriz
matrizRB3 (xs,rs) = listArray dim (replicate (rangeSize dim) False) // [(i,True)|i<-rs]
    where (n,m) = (minimum xs, maximum xs)
          dim = ((n,n),(m,m))

-- con actualización
matrizRB4 :: Relacion -> Matriz
matrizRB4 r =
    accumArray (||) False ((1,1),(n,n)) (zip (snd r) (repeat True))
    where n = maximum (fst r)

