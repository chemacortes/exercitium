module Ex_20150511 where

{-
Representación decimal de números racionales
============================================

Continuando con los ejercicios propuestos por los alumnos, Antonio García
Blázquez ha propuesto un ejercicio que usa el período de los números decimales.
El ejercicio de hoy, basado en su propuesta, trata de la representación decimal
de los números racionales.

Los números decimales se representan por ternas, donde el primer elemento es la
parte entera, el segundo es el anteperíodo y el tercero es el período. Por ejemplo,

    6/2  = 3                  se representa por (3,[],[])
    1/2  = 0.5                se representa por (0,[5],[])
    1/3  = 0.333333...        se representa por (0,[],[3])
   23/14 = 1.6428571428571... se representa por (1,[6],[4,2,8,5,7,1])

Su tipo es

   type Decimal = (Integer,[Integer],[Integer])

Los números racionales se representan por un par de enteros, donde el primer
elemento es el numerador y el segundo el denominador. Por ejemplo, el número 2/3
se representa por (2,3). Su tipo es

   type Racional = (Integer,Integer)

Definir las funciones

   decimal  :: Racional -> Decimal
   racional :: Decimal -> Racional

tales que

    (decimal r) es la representación decimal del número racional r. Por ejemplo,

        decimal (1,4)    ==  (0,[2,5],[])
        decimal (1,3)    ==  (0,[],[3])
        decimal (23,14)  ==  (1,[6],[4,2,8,5,7,1])

    (racional d) es el número racional cuya representación decimal es d. Por ejemplo,

        racional (0,[2,5],[])           ==  (1,4)
        racional (0,[],[3])             ==  (1,3)
        racional (1,[6],[4,2,8,5,7,1])  ==  (23,14)

Con la función decimal se puede calcular los períodos de los números racionales.
Por ejemplo,

   ghci> let (_,_,p) = decimal (23,14) in concatMap show p
   "428571"
   ghci> let (_,_,p) = decimal (1,47) in concatMap show p
   "0212765957446808510638297872340425531914893617"
   ghci> let (_,_,p) = decimal (1,541) in length (concatMap show p)
   540

Comprobar con QuickCheck si las funciones decimal y racional son inversas.

-}

import           Data.List
import           Test.QuickCheck

type Decimal = (Integer,[Integer],[Integer])
type Racional = (Integer,Integer)

-- simplifica fracciones
reduce :: (Integer, Integer) -> (Integer, Integer)
reduce (a,b) = (a `div` m, b `div` m) where m = gcd a b

-----------------------------------------------------------------------

decimal :: Racional -> Decimal
decimal (0, _)  = (0, [], [])
decimal (x, y)  = (e, anteperiodo, periodoRange)
    where
        (x', y') = reduce (x, y)
        (e,xe) = divMod x' y'
        (n,np) = npos (xe, y')
        (anteperiodo, periodo) = splitAt (fromIntegral n) (digitos xe y')
        periodoRange = take (fromIntegral np) periodo

-- calcula número de posiciones del anteperiodo y periodo en función
-- del número de 2 y/ó 5 que tengan los factores primos del denominador
npos :: (Integer,Integer) -> (Integer, Integer)
npos (x,y)
    | n == 0    = (0, nper ps)    -- periódico puro
    | ps == 1   = (n, 0)          -- no periódico
    | otherwise = (n, nper ps)    -- periódico mixto
    where
        (_, y') = reduce (x, y)
        (n2, n5) = (nfact y' 2 0, nfact y' 5 0)
        n = max n2 n5
        ps = y' `div` (2^n2 * 5^n5) -- resto factores
        
-- número de elementos del periodo
nper :: Integer -> Integer
nper y = head [ n | n <- [1..], mod ((10^n)-1) y == 0 ]

-- número de veces que se puede dividir un número por otro
nfact :: Integer -> Integer -> Integer -> Integer
nfact x y n
    | x `mod` y == 0    = nfact (x `div` y) y (n+1)
    | otherwise         = n 

-- secuencia de dígitos de un número fraccionario
digitos :: Integer -> Integer -> [Integer]
digitos 0 _ = []
digitos x y = d : digitos x' y  where (d, x') = divMod (10*x) y

--------------------------------------------------------------------------------

-- numerador: la diferencia entre la parte anterior al período seguida del 
--            período menos la parte anterior al período.
-- denominador: tantos 9 como cifras tiene el período, seguidos de tantos 0
--              como cifras tiene la parte no periódica.
racional :: Decimal -> Racional
racional (e, xs, []) = reduce (junta (e:xs), 10^n) where n = length xs    -- fracional exacto
racional (e, xs, ps) = reduce (num, den)
    where
        num = junta (e:xs ++ ps) - junta (e:xs)
        den = junta (replicate (length ps) 9 ++ replicate (length xs) 0)

-- concatena los dígitos
junta :: [Integer] -> Integer
junta = read . concatMap show

--------------------------------------------------------------------------------

prop_racionalDecimal :: Racional -> Property
prop_racionalDecimal (a,b) =
    a/=0 && b/=0 && gcd a b == 1 ==> (racional . decimal) r == r
    where r = (abs a,abs b)

prop_decimalRacional :: Decimal -> Property
prop_decimalRacional (e, xs, ps) =
        and [ null ps' || last ps' /= 0,
              (not.null) ps' || null xs' || last xs' /= 0,
              (not.null) ps' && (not.null) xs' && last ps' /= last xs',
              not (xs' `isInfixOf` ps'),
              not (ps' `isInfixOf` xs'),
              ps' /= reverse ps' ]
        ==> (decimal . racional) d == d
    where
        d = (abs e,map ((`mod`10).abs) xs,map ((`mod`10).abs) ps)
        (_,xs',ps') = d

runTest :: IO ()
runTest = do quickCheck prop_racionalDecimal
             quickCheck prop_decimalRacional
             
