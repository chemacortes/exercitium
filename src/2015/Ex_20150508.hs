module Ex_20150508 where

{-
Potencias de primos con exponentes potencias de dos
===================================================

Se llaman **potencias de Fermi-Dirac** a los n�meros de la forma p^(2^k), donde
p es un número primo y k es un número natural.

Definir la sucesión

   potencias :: [Integer]

cuyos términos sean las potencias de Fermi-Dirac ordenadas de menor a mayor.
Por ejemplo,

   take 14 potencias    ==  [2,3,4,5,7,9,11,13,16,17,19,23,25,29]
   potencias !! 60      ==  241
   potencias !! (10^6)  ==  15476303
-}

import           Data.Numbers.Primes

potencias :: [Integer]
potencias = ordena [ [ p^(2^k) | p <- primes ] | k <- [0..] ]

potencias2 :: [Integer]
potencias2 = ordena (iterate f primes)
    where f xs = zipWith (*) xs xs

ordena :: Ord a => [[a]] -> [a]
ordena ((x:xs):xss) = x : mezcla xs (ordena xss)
    where
        mezcla (x:xs) (y:ys)
            | x <= y    = x : mezcla xs (y:ys)
            | otherwise = y : mezcla (x:xs) ys

potencias3 :: [Integer]
potencias3 = ordena [ [ p^(2^k) | p <- primes ] | k <- [0..] ]
    where
        ordena ((x:xs):xss) = x : mezcla xs (ordena xss)
        mezcla (x:xs) (y:ys)
            | x <= y    = x : mezcla xs (y:ys)
            | otherwise = y : mezcla (x:xs) ys
               