module Ex_20150528 where

{-
Refinamiento de montículos
==========================

Definir la función

   refina :: Ord a => Monticulo a -> [a -> Bool] -> Monticulo a

tal que (refina m ps) es el montículo formado por los elementos del montículo m
que cumplen todos los predicados de la lista ps. Por ejemplo,

   ghci> refina (foldr inserta vacio [1..22]) [(<7), even]
   M 2 1 (M 4 1 (M 6 1 Vacio Vacio) Vacio) Vacio
   ghci> refina (foldr inserta vacio [1..22]) [(<1), even]
   Vacio
-}

import I1M.Monticulo

refina :: Ord a => Monticulo a -> [a -> Bool] -> Monticulo a
refina m ps
    | esVacio m             = m
    | and [ p q | p <- ps ] = inserta q $ refina (resto m) ps
    | otherwise             =             refina (resto m) ps
    where q = menor m
