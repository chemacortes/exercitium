module Ex_20151125 where

{-

Mayor semiprimo menor que n
===========================

Un número semiprimo es un número natural que es producto de dos números primos 
no necesariamente distintos. Por ejemplo, 26 es semiprimo (porque 26 = 2*13 ) 
y 49 también lo es (porque 49 = 7*7).

Definir la función

   mayorSemiprimoMenor :: Integer -> Integer

tal que (mayorSemiprimoMenor n) es el mayor semiprimo menor que n (suponiendo 
que n > 4). Por ejemplo,

   mayorSemiprimoMenor 27      ==  26
   mayorSemiprimoMenor 50      ==  49
   mayorSemiprimoMenor 49      ==  46
   mayorSemiprimoMenor (10^6)  ==  999997

-}

import Data.Numbers.Primes

mayorSemiprimoMenor :: Integer -> Integer
mayorSemiprimoMenor n = head [ p | p <- [n-1,n-2..2]
                                 , (length . primeFactors) p == 2]

