module Ex_20150622 where

{-

Máximos de una lista
====================

Definir la función

   maximos :: Ord a => [a] -> [a]

tal que (maximos xs) es la lista de los elementos de xs que son mayores que 
todos sus anteriores. Por ejemplo,

   maximos [1,-3,5,2,3,4,7,6,7]                         ==  [1,5,7]
   maximos "bafcdegag"                                  ==  "bfg"
   maximos (concat (replicate (10^6) "adxbcde")++"yz")  ==  "adxyz"
   length (maximos [1..10^6])                           ==  1000000


-}

import Data.List (nub)

maximos :: Ord a => [a] -> [a]
maximos []      = []
maximos (x:xs)  = x : maximos (dropWhile (x>=) xs)


maximos2 :: Ord a => [a] -> [a]
maximos2 = nub . scanl1 max 
