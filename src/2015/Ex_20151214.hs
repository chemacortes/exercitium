module Ex_20151214 where

{-

Año cúbico
==========

El año 2016 será un año cúbico porque se puede escribir como la suma de los 
cubos de 7 números consecutivos; en efecto,

   2016 = 3³+ 4³ +...+ 9³

Definir la función

   esCubico :: Integer -> Bool

tal que (esCubico x) se verifica si x se puede escribir como la suma de los 
cubos de 7 números consecutivos. Por ejemplo,

   esCubico 2016                ==  True
   esCubico 2017                ==  False
   esCubico 189005670081900441  ==  True
   esCubico 189005670081900442  ==  False

-}

esCubico :: Integer -> Bool
esCubico n = n `elem` takeWhile (<=n) cubico
    where s = floor $ (fromIntegral n/7)**(1/3)
          cubico = [sum [j^3|j<-[i-3..i+3]] | i <- [s..]]

lesCubico :: Integer -> Int
lesCubico n = length $ takeWhile (<=n) cubico
    where s = floor $ (fromIntegral n/7)**(1/3)
          cubico = [sum [j^3|j<-[i-3..i+3]] | i <- [s..]]
