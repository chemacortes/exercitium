module Ex_20150619 where

{-

La sucesión del reloj astronómico de Praga
==========================================

La cadena infinita “1234321234321234321…”, formada por la repetición de los 
dígitos 123432, tiene una propiedad (en la que se basa el funcionamiento del 
[reloj astronómico de Praga][1]): la cadena se puede partir en una sucesión de 
números, de forma que la suma de los dígitos de dichos números es la serie de 
los números naturales, como se observa a continuación:

    1, 2, 3, 4, 32, 123, 43, 2123, 432, 1234, 32123, ...
    1, 2, 3, 4,  5,   6,  7,    8,   9,   10,    11, ...

Definir la lista

   reloj :: [Integer]

cuyos elementos son los términos de la sucesión anterior. Por ejemplo,

   ghci> take 11 reloj
   [1,2,3,4,32,123,43,2123,432,1234,32123]

Nota: La relación entre la sucesión y el funcionamiento del reloj se puede ver 
en [The mathematics behind Prague’s horloge][2].


[1]: https://es.wikipedia.org/wiki/Reloj_Astron%C3%B3mico_de_Praga
[2]: http://www.global-sci.org/mc/galley/prague_sc_pic/prague_eng.pdf

-}


reloj :: [Integer]
reloj = aux sucesion 1
    where aux xs n = let (xs1, xs2) = splitAt (corte xs n) xs
                     in listaANumero xs1 : aux xs2 (n+1)

reloj2 :: [Integer]
reloj2 = aux 1 [] sucesion
 where aux n ys (x:xs) | sum ys == n = listaANumero ys:aux (n+1) [] (x:xs)
                       | otherwise = aux n (ys++[x]) xs


-- cadena infinita de la sucesión del reloj de Praga
sucesion :: [Integer]
sucesion = concat $ repeat [1,2,3,4,3,2]

-- número de dígitos que sumados dan un número dado
corte :: [Integer] -> Integer -> Int
corte (x:xs) n
    | x == n    = 1
    | otherwise = 1 + corte xs (n-x)

-- concatenar los dígitos de una lista
listaANumero :: [Integer] -> Integer
listaANumero = read.concatMap show








