module Ex_20151112 where

{-

Unión general de conjuntos
==========================

Definir las funciones

   unionGeneral        :: Eq a => [[a]] -> [a]
   interseccionGeneral :: Eq a => [[a]] -> [a]

tales que

    (unionGeneral xs) es la unión de los conjuntos de la lista de conjuntos xs 
    (es decir, el conjunto de los elementos que pertenecen a alguno de los 
    elementos de xs). Por ejemplo,

     unionGeneral []                    ==  []
     unionGeneral [[1]]                 ==  [1]
     unionGeneral [[1],[1,2],[2,3]]     ==  [1,2,3]
     unionGeneral ([[x] | x <- [1..9]]) == [1,2,3,4,5,6,7,8,9]

    (interseccionGeneral xs) es la intersección de los conjuntos de la lista de 
    conjuntos xs (es decir, el conjunto de los elementos que pertenecen a todos 
    los elementos de xs). Por ejemplo,

     interseccionGeneral [[1]]                      ==  [1]
     interseccionGeneral [[2],[1,2],[2,3]]          ==  [2]
     interseccionGeneral [[2,7,5],[1,5,2],[5,2,3]]  ==  [2,5]
     interseccionGeneral ([[x] | x <- [1..9]])      ==  []
     interseccionGeneral (replicate (10^6) [1..5])  ==  [1,2,3,4,5]

-}

import Data.List

-- El enunciado no especifica qué ocurre cuando un conjunto tiene elementos
-- repetidos. Asumiremos que en el resultado no deben aparecer elementos duplicados

--
--  unionGeneral
--

-- Solución directa
unionGeneral :: Eq a => [[a]] -> [a]
unionGeneral = nub . concat

-- Una solución elegante usando folding
unionGeneral1 :: Eq a => [[a]] -> [a]
unionGeneral1 = nub . foldr union []

-- Una solución recursiva: 
unionGeneral2 :: Eq a => [[a]] -> [a]
unionGeneral2 [] = []
unionGeneral2 (xs:xss) = xs' ++ filter (`notElem` xs') (unionGeneral2 xss)
    where xs' = nub xs

-- Se puede optimizar más si se realiza el filtrado antes de hacer la unión.
unionGeneral3 :: Eq a => [[a]] -> [a]
unionGeneral3 [] = []
unionGeneral3 (xs:xss) = xs' ++ unionGeneral3 yss
    where xs' = nub xs
          yss = map (filter (`notElem` xs')) xss


--
-- interseccionGeneral
--

-- Usando folding          
interseccionGeneral :: Eq a => [[a]] -> [a]
interseccionGeneral = nub . foldr1 intersect
          
-- Recursiva
interseccionGeneral2 :: Eq a => [[a]] -> [a]
interseccionGeneral2 [] = []
interseccionGeneral2 (xs:xss) = [ x | x <- nub xs, all (x `elem`) xss]
