module Ex_20151222 where

{-

Producto infinito
=================

Definir la función

   productoInfinito :: [Integer] -> [Integer]

tal que (productoInfinito xs) es la lista infinita que en la posición N tiene 
el producto de los N primeros elementos de la lista infinita xs. Por ejemplo,

   take 5 (productoInfinito [1..])    ==  [1,2,6,24,120]
   take 5 (productoInfinito [2,4..])  ==  [2,8,48,384,3840]
   take 5 (productoInfinito [1,3..])  ==  [1,3,15,105,945]

Nota: Este ejercicio es parte del examen del grupo 3 del 2 de diciembre.

-}

import Data.List

productoInfinito1 :: [Integer] -> [Integer]
productoInfinito1 (x:xs) = x: map (x*) (productoInfinito1 xs)

productoInfinito2 :: [Integer] -> [Integer]
productoInfinito2 = scanl1 (*)

productoInfinito3 :: [Integer] -> [Integer]
productoInfinito3 xs = [product (take n xs) | n <- [1..]]

productoInfinito :: [Integer] -> [Integer]
productoInfinito = productoInfinito2

