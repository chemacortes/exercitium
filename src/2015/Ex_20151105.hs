module Ex_20151105 where

{-

Capicúas productos de dos números de dos dígitos
================================================

El número 9009 es capicúa y es producto de dos números de dos dígitos, pues 
9009 = 91*99.

Definir la lista

   capicuasP2N2D :: [Int]

cuyos elementos son los números capicúas que son producto de 2 números de dos 
dígitos. Por ejemplo,

   take 5  capicuasP2N2D  ==  [121,242,252,272,323]
   length  capicuasP2N2D  ==  74
   drop 70 capicuasP2N2D  ==  [8008,8118,8448,9009]
-}

import Data.List

-- Búsqueda de capicúas entre los productos de números de dos cifras 
capicuasP2N2D :: [Int]
capicuasP2N2D = nub . sort $ filter capicua xs
    where capicua n = (reverse . show) n == show n
          xs = [ a*b | a <- [10..99]
                     , b <- [a..99] ]

-- Proceso contrario, buscar entre los capicúas los que sean productos de
-- números de dos cifras. No necesita hacer una ordenación y un deduplicado
-- posterior
capicuasP2N2D' :: [Int]
capicuasP2N2D' = filter esP2N2D capicuas

-- Chequea si un número es factorizable en dos numeros de dos cifras cada uno
esP2N2D :: Int -> Bool
esP2N2D x = (not.null) [ j | i <- [10..99]
                           , x `mod` i == 0
                           , let j = x `div` i
                           , 9 < j && j < 99  ]

-- Obtención de todos los capicúas "candidatos", ordenados y sin duplicados.
capicuas :: [Int]
capicuas =  [ read [a,b,a]   | a <- ['1'..'9'], b <- ['0'..'9'] ]
         ++ [ read [a,b,b,a] | a <- ['1'..'9'], b <- ['0'..'9'] ]
