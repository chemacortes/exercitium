module Ex_20151111 where

{-

Ceros finales del factorial
===========================

Definir la función

   cerosDelFactorial :: Integer -> Integer

tal que (cerosDelFactorial n) es el número de ceros en que termina el factorial 
de n. Por ejemplo,

   cerosDelFactorial 24                           ==  4
   cerosDelFactorial 25                           ==  6
   length (show (cerosDelFactorial (1234^5678)))  ==  17552

-}

import Data.List (genericLength)

cerosDelFactorial :: Integer -> Integer
cerosDelFactorial n = sum (takeWhile (>0) (iterate (`div` 5) (n `div` 5)))


factorial :: Integer -> Integer
factorial n = product [1..n]

cerosDelFactorial2 :: Integer -> Integer
cerosDelFactorial2 n = ceros2 (factorial n)
 
ceros2 :: Integer -> Integer
ceros2 n = genericLength (takeWhile (=='0') (reverse (show n)))
