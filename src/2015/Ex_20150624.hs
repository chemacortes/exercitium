module Ex_20150624 where

{-

Codificación de Fibonacci
=========================

La codificación de Fibonacci de un número n es una cadena
d = d(0)d(1)…d(k-1)d(k) de ceros y unos tal que

   n = d(0)*F(2) + d(1)*F(3) +...+ d(k-1)*F(k+1)
   d(k-1) = d(k) = 1

donde F(i) es el i-ésimo término de la sucesión de Fibonacci.

   0,1,1,2,3,5,8,13,21,34,...

Por ejemplo. La codificación de Fibonacci de 4 es "1011" ya que los dos últimos
elementos son iguales a 1 y

   1*F(2) + 0*F(3) + 1*F(4) = 1*1 + 0*2 + 1*3 = 4

La codificación de Fibonacci de los primeros números se muestra en la siguiente
tabla

    1  = 1     = F(2)           ≡ 11
    2  = 2     = F(3)           ≡ 011
    3  = 3     = F(4)           ≡ 0011
    4  = 1+3   = F(2)+F(4)      ≡ 1011
    5  = 5     = F(5)           ≡ 00011
    6  = 1+5   = F(2)+F(5)      ≡ 10011
    7  = 2+5   = F(3)+F(5)      ≡ 01011
    8  = 8     = F(6)           ≡ 000011
    9  = 1+8   = F(2)+F(6)      ≡ 100011
   10  = 2+8   = F(3)+F(6)      ≡ 010011
   11  = 3+8   = F(4)+F(6)      ≡ 001011
   12  = 1+3+8 = F(2)+F(4)+F(6) ≡ 101011
   13  = 13    = F(7)           ≡ 0000011
   14  = 1+13  = F(2)+F(7)      ≡ 1000011

Definir la función

   codigoFib :: Integer -> String

tal que (codigoFib n) es la codificación de Fibonacci del número n. Por ejemplo,

   ghci> codigoFib 65
   "0100100011"
   ghci> [codigoFib n | n <- [1..7]]
   ["11","011","0011","1011","00011","10011","01011"]

-}

import Data.List

codigoFib :: Integer -> String
codigoFib n = snd $ foldr codifica (n, "1") fibs'
    where fibs' = (drop 2 . takeWhile (n>=)) fibs
          codifica z (m, acc) | m >= z    = (m-z, '1':acc)
                              | otherwise = (m,   '0':acc)

-- secuencia de números de Fibonacci
-- 0,1,1,2,3,5,8,13,21,34,...
fibs :: [Integer]
fibs = 0 : 1 : zipWith(+) fibs (tail fibs)

-- por inducción
codigoFib2 :: Integer -> String
codigoFib2 n = codigos `genericIndex` (n-1)

codigos :: [String]
codigos = "11" : concatMap aux codigos
    where aux xs | head xs == '1' = [ '0':xs ]
                 | otherwise      = [ '0':xs, '1':xs ]


