module Ex_20150616 where

{-
Suma de posteriores
===================

Definir la función

   sumaPosteriores :: [Int] -> [Int]

tal que (sumaPosteriores xs) es la lista obtenida sustituyendo cada elemento de
xs por la suma de los elementos posteriores. Por ejemplo,

   sumaPosteriores [1..8]        == [35,33,30,26,21,15,8,0]
   sumaPosteriores [1,-3,2,5,-8] == [-4,-1,-3,-8,0]

Comprobar con QuickCheck que el último elemento de la lista (sumaPosteriores xs)
siempre es 0.

-}

import Test.QuickCheck
import Data.List

-- definición directa
sumaPosteriores :: [Int] -> [Int]
sumaPosteriores []      = []
sumaPosteriores (_:xs)  = sum xs : sumaPosteriores xs


-- generando la lista de posteriores
sumaPosteriores2 :: [Int] -> [Int]
sumaPosteriores2 = map sum . tails . tail


-- por scanr (más eficiente)
sumaPosteriores3 :: [Int] -> [Int]
sumaPosteriores3 = scanr (+) 0 . tail


prop_sumaPosteriores :: [Int] -> Property
prop_sumaPosteriores xs = (not.null) xs ==>
    (last . sumaPosteriores) xs == 0