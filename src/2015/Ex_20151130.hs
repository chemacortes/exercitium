module Ex_20151130 where

{-

Separación y mezcla de listas
=============================

Definir las funciones

   separacion :: [a] -> ([a],[a])
   mezcla     :: ([a],[a]) -> [a]

tales que (separacion xs) es el par formado eligiendo alternativamente 
elementos de xs mientras que mezcla intercala los elementos de las dos 
listas. Por ejemplo,

   separacion [1..5]                   ==  ([1,3,5],[2,4])
   mezcla ([1,3,5],[2,4])              ==  [1,2,3,4,5]
   separacion "Telescopio"             ==  ("Tlsoi","eecpo")
   mezcla ("Tlsoi","eecpo")            ==  "Telescopio"
   take 5 (fst (separacion [2,4..]))   ==  [2,6,10,14,18]
   take 6 (mezcla ([2,4..],[7,14..]))  ==  [2,7,4,14,6,21]

Comprobar con QuickCheck que

   mezcla (separacion xs) == xs
   
-}

import Test.QuickCheck

separacion :: [a] -> ([a],[a])
separacion []       = ([],[])
separacion (x:xs)   = (x:zs, ys)
    where (ys, zs) = separacion xs 

mezcla     :: ([a],[a]) -> [a]
mezcla ([], ys)     = ys
mezcla (xs, [])     = xs
mezcla ( x:xs, y:ys) = x : y : mezcla (xs, ys)


prop1 :: Eq a => [a] -> Bool
prop1 xs = (mezcla . separacion) xs == xs

runTest :: IO ()
runTest = quickCheck $ forAll (listOf arbitrary :: Gen [Int]) prop1

