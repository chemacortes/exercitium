module Ex_20150525 where

{-
Inversiones de un número
========================

Un número tiene una inversión cuando existe un dígito x a la derecha de otro
dígito de forma que x es menor que y. Por ejemplo, en el número 1745 hay dos
inversiones ya que 4 es menor que 7 y 5 es menor que 7 y están a la derecha
de 7.

Definir la función

   nInversiones :: Integer -> Int

tal que (nInversiones n) es el número de inversiones de n. Por ejemplo,

   nInversiones 1745  ==  2
-}

import Data.List

-- ejemplos para pruebas
ej,ej2 :: Integer
ej = read $ concat (replicate 100 "12341200934912312")      -- 589600
ej2 = read $ concat (replicate 1000 "12341200934912312")    -- 58996000



-- definición recursiva
nInversiones :: Integer -> Int
nInversiones = aux . show
    where aux []        = 0
          aux (x:xs)    = length (filter (x>) xs) + aux xs

-- usando zipWith
nInversiones2 :: Integer -> Int
nInversiones2 n = sum $ zipWith f digitos (tail $ tails digitos)
    where digitos = show n
          f x xs = length (filter (<x) xs)

-- en una sóla línea
nInversiones3 :: Integer -> Int
nInversiones3 =
    length . concatMap (\(x:xs) -> filter (x>) xs) . (init . tails) . show

-- versión compacta
nInversiones4 :: Integer -> Int
nInversiones4 = sum . map inversiones . tails . show
    where inversiones (x:xs) = length $ filter (x>) xs
          inversiones _      = 0
