module Ex_20151028 where

{-

Diferencia simétrica
====================

La diferencia simétrica de dos conjuntos es el conjunto cuyos elementos son 
aquellos que pertenecen a alguno de los conjuntos iniciales, sin pertenecer a 
ambos a la vez. Por ejemplo, la diferencia simétrica de {2,5,3} y {4,2,3,7} es 
{5,4,7}.

Definir la función

   diferenciaSimetrica :: Eq a => [a] -> [a] -> [a]

tal que (diferenciaSimetrica xs ys) es la diferencia simétrica de xs e ys. 

Por ejemplo,

   diferenciaSimetrica [2,5,3] [4,2,3,7]  ==  [5,4,7]
   diferenciaSimetrica [2,5,3] [5,2,3]    ==  []

-}

import Data.List


-- Solución exclusivamente con operaciones de conjuntos
diferenciaSimetrica :: Eq a => [a] -> [a] -> [a]
diferenciaSimetrica xs ys = (xs\\ys) ++ (ys\\xs)

-- Del enunciado no se deduce qué pasa en el caso de que las listas tengan
-- elementos repetidos. Las "operaciones de conjuntos" sólo eliminan el
-- primer elemento encontrado:
-- eg:  diferenciaSimetrica [2,5,3] [5,2,3,2] == [2]

-- Una solución que cumple estrictamente el enunciado:
diferenciaSimetrica2 :: Eq a => [a] -> [a] -> [a]
diferenciaSimetrica2 xs ys =  filter (`notElem` ys) xs ++ filter (`notElem` xs) ys

