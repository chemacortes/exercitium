module Ex_20150521 where

{-
Matrices marco y transiciones
=============================

Las **posiciones frontera** de una matriz de orden `m x n` son aquellas que
están en la fila 1 o la fila m o la columna 1 o la columna n. El resto se dirán
**posiciones interiores**. Observa que cada elemento en una posición interior
tiene exactamente 8 vecinos en la matriz.

Dada una matriz, un **paso de transición** genera una nueva matriz de la misma
dimensión pero en la que se ha sustituido cada elemento interior por la suma de
sus 8 vecinos. Los elementos frontera no varían.

Definir las funciones

   marco      :: Int -> Int -> Integer -> Matrix Integer
   paso       :: Matrix Integer -> Matrix Integer
   itPasos    :: Int -> Matrix Integer -> Matrix Integer
   pasosHasta :: Integer -> Int

tales que

    (marco m n z) genera la matriz de dimensión `m x n` que contiene el entero z
    en las posiciones frontera y 0 en las posiciones interiores. Por ejemplo,

     ghci> marco 5 5 1
     ( 1 1 1 1 1 )
     ( 1 0 0 0 1 )
     ( 1 0 0 0 1 )
     ( 1 0 0 0 1 )
     ( 1 1 1 1 1 )

    (paso t) calcula la matriz generada tras aplicar un paso de transición a la
    matriz t. Por ejemplo,

     ghci> paso (marco 5 5 1)
     ( 1 1 1 1 1 )
     ( 1 5 3 5 1 )
     ( 1 3 0 3 1 )
     ( 1 5 3 5 1 )
     ( 1 1 1 1 1 )

    (itPasos k t) es la matriz obtenida tras aplicar k pasos de transición a
    partir de la matriz t. Por ejemplo,

     ghci> itPasos 10 (marco 5 5 1)
     (       1       1       1       1       1 )
     (       1 4156075 5878783 4156075       1 )
     (       1 5878783 8315560 5878783       1 )
     (       1 4156075 5878783 4156075       1 )
     (       1       1       1       1       1 )

    (pasosHasta k) es el número de pasos de transición a partir de la matriz
    (marco 5 5 1) necesarios para que en la matriz resultante aparezca un
    elemento mayor que k. Por ejemplo,

     pasosHasta 4         ==  1
     pasosHasta 6         ==  2
     pasosHasta (2^2015)  ==  887
-}

import           Data.Matrix

marco      :: Int -> Int -> Integer -> Matrix Integer
marco m n z = matrix m n f
    where f (i,j) | esInterior (m,n) (i,j)  = 0
                  | otherwise               = z

paso :: Matrix Integer -> Matrix Integer
paso t = matrix m n suma
    where
        (m,n) = (nrows t, ncols t)
        vecinos (i,j) = [ t!(i+x,j+y) | x <- [-1..1], y <- [-1..1], (x,y) /= (0,0)]
        suma (i,j) | esInterior (m,n) (i,j) = sum $ vecinos (i,j)
                   | otherwise              = t!(i,j)

itPasos :: Int -> Matrix Integer -> Matrix Integer
itPasos k = foldr1 (.) (replicate k paso)

pasosHasta :: Integer -> Int
pasosHasta k = aux (marco 5 5 1)
    where aux t | any (>k) (toList t) = 0
                | otherwise           = 1 + aux (paso t)

esInterior :: (Int, Int) -> (Int, Int) -> Bool
esInterior (m,n) (i,j) = 1<i && i<m && 1<j && j<n


