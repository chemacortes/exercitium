module Ex_20151115 where

{-

Sumas alternas de factoriales
=============================

Las primeras sumas alternas de los factoriales son números primos; en efecto,

   3! - 2! + 1! = 5
   4! - 3! + 2! - 1! = 19
   5! - 4! + 3! - 2! + 1! = 101
   6! - 5! + 4! - 3! + 2! - 1! = 619
   7! - 6! + 5! - 4! + 3! - 2! + 1! = 4421
   8! - 7! + 6! - 5! + 4! - 3! + 2! - 1! = 35899

son primos, pero

   9! - 8! + 7! - 6! + 5! - 4! + 3! - 2! + 1! = 326981

no es primo.

Definir las funciones

   sumaAlterna         :: Integer -> Integer
   sumasAlternas       :: [Integer]
   conSumaAlternaPrima :: [Integer]

tales que

    (sumaAlterna n) es la suma alterna de los factoriales desde n hasta 1. Por 
    ejemplo,

     sumaAlterna 3  ==  5
     sumaAlterna 4  ==  19
     sumaAlterna 5  ==  101
     sumaAlterna 6  ==  619
     sumaAlterna 7  ==  4421
     sumaAlterna 8  ==  35899
     sumaAlterna 9  ==  326981

    sumasAlternas es la sucesión de las sumas alternas de factoriales. Por 
    ejemplo,

     λ> take 10 sumasAlternas
     [0,1,1,5,19,101,619,4421,35899,326981]

    conSumaAlternaPrima es la sucesión de los números cuya suma alterna de 
    factoriales es prima. Por ejemplo,

     λ> take 8 conSumaAlternaPrima
     [3,4,5,6,7,8,10,15]

-}

import Data.Numbers.Primes

sumaAlterna         :: Integer -> Integer
sumasAlternas       :: [Integer]
conSumaAlternaPrima :: [Integer]

sumaAlterna 0 = 0
sumaAlterna n = product [1..n] - sumaAlterna (n-1)

sumasAlternas = map sumaAlterna [0..]

conSumaAlternaPrima = filter (isPrime . sumaAlterna) [0..]


sumaAlterna2         :: Integer -> Integer
sumasAlternas2       :: [Integer]
conSumaAlternaPrima2 :: [Integer]

sumaAlterna2 n = foldl (flip (-)) 0 $ scanl1 (*) [1..n]
sumasAlternas2 = scanl (flip (-)) 0 $ scanl1 (*) [1..]
conSumaAlternaPrima2 = filter (isPrime . sumaAlterna2) [0..]

