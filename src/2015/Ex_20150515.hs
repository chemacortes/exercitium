module Ex_20150515 where

{-
Números alternados
==================

Decimos que un número es alternado si no tiene dos cifras consecutivas iguales
ni tres cifras consecutivas en orden creciente no estricto o decreciente no
estricto. Por ejemplo, los números 132425 y 92745 son alternados, pero los
números 12325 y 29778 no. Las tres primeras cifras de 12325 están en orden
creciente y 29778 tiene dos cifras iguales consecutivas.

Definir la constante

   alternados :: [Integer]

cuyo valor es la lista infinita de los números alternados. Por ejemplo,

   take 10 alternados                      ==  [0,1,2,3,4,5,6,7,8,9]
   length (takeWhile (< 1000) alternados)  ==  616
   alternados !! 1234567                   ==  19390804
-}

type BinOp a = a -> a -> Bool

-- aplica indefinidamente la operación binaria, intercambiando los argumentos (flip)
-- por ejemplo, (alterna (>)) equivale a [(>),(<),(>),....]
alterna :: BinOp a -> [a] -> [a] -> Bool
alterna binOp xs ys = and $ zipWith3 id (iterate flip binOp) xs ys

alternados :: [Integer]
alternados = filter (alternado.show) [0..]
    where alternado (x:y:xs) | x > y     = alterna (<) (y:xs) xs
                             | x < y     = alterna (>) (y:xs) xs
                             | otherwise = False
          alternado _ = True
            

-- mediante zips
alternado2 :: Integer -> Bool
alternado2 = aux.show
    where aux xs = and (zipWith3 f xs (drop 1 xs) (drop 2 xs)) &&
                   and (zipWith (/=) xs (drop 1 xs))
          f a b c = not $ (a <= b && b <= c) || (a >= b && b >= c)
          
-- recursivo
alternados3 :: [Integer]
alternados3 = filter (alternado3 . show) [0..]
    where alternado3 (x:y:z:zs) = and [x /= y,
                                      not (x < y && y < z),
                                      not (x > y && y > z),
                                      alternado3 (y:z:zs)]
          alternado3 [x,y]      = x /= y
          alternado3 _          = True