module Ex_20150519 where

{-
Aplicación de funciones a nodos y hojas
=======================================

Representamos los árboles binarios con elementos en las hojas y en los nodos mediante el tipo de dato

   data Arbol a = H a | N a (Arbol a) (Arbol a) deriving Show

Definir la función

   aplica :: (a -> a) -> (a -> a) -> Arbol a -> Arbol a

tal que (aplica f g a) devuelve el árbol obtenido al aplicar la función f a las
hojas del árbol a y la función g a los nodos interiores. Por ejemplo,

   ghci> aplica (+1)(*2) (N 5 (N 2 (H 1) (H 2)) (N 3 (H 4) (H 2)))
   N 10 (N 4 (H 2) (H 3)) (N 6 (H 5) (H 3))
-}

data Arbol a = H a | N a (Arbol a) (Arbol a) deriving Show

aplica :: (a -> a) -> (a -> a) -> Arbol a -> Arbol a
aplica f g (H x) = H (f x)
aplica f g (N x a b) = N (g x) (aplica f g a) (aplica f g b)

