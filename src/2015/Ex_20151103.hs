module Ex_20151103 where

{-

Números libres de cuadrados
===========================

Un número es libre de cuadrados si no es divisible el cuadrado de ningún entero 
mayor que 1. Por ejemplo, 70 es libre de cuadrado porque sólo es divisible por 
1, 2, 5, 7 y 70; en cambio, 40 no es libre de cuadrados porque es divisible por 
2².

Definir la función

   libreDeCuadrados :: Integer -> Bool

tal que (libreDeCuadrados x) se verifica si x es libre de cuadrados. Por ejemplo,

   libreDeCuadrados 70                 ==  True
   libreDeCuadrados 40                 ==  False
   libreDeCuadrados 510510             ==  True
   libreDeCuadrados (((10^10)^10)^10)  ==  False

-}

libreDeCuadrados :: Integer -> Bool
libreDeCuadrados n = and [ n `mod` k /= 0 | k <- takeWhile (<n) cuadrados ]
    where cuadrados = [ i*i | i <- [2..]]
