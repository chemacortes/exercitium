module Ex_20150421 where

{-|

Caminos reducidos
=================

Un camino es una sucesión de pasos en una de las cuatros direcciones Norte, Sur,
Este, Oeste. Ir en una dirección y a continuación en la opuesta es un esfuerzo
que se puede reducir, Por ejemplo, el camino [Norte,Sur,Este,Sur] se puede
reducir a [Este,Sur].

Un camino se dice que es reducido si no tiene dos pasos consecutivos en
direcciones opuesta. Por ejemplo, [Este,Sur] es reducido y [Norte,Sur,Este,Sur]
no lo es.

En Haskell, las direcciones y los caminos se pueden definir por

   data Direccion = N | S | E | O deriving (Show, Eq)
   type Camino = [Direccion]

Definir la función

   reducido :: Camino -> Camino

tal que (reducido ds) es el camino reducido equivalente al camino ds. Por ejemplo,

   reducido []                              ==  []
   reducido [N]                             ==  [N]
   reducido [N,O]                           ==  [N,O]
   reducido [N,O,E]                         ==  [N]
   reducido [N,O,E,S]                       ==  []
   reducido [N,O,S,E]                       ==  [N,O,S,E]
   reducido [N,S,S,E,O,N]                   ==  []
   reducido [N,S,S,E,O,N,O]                 ==  [O]
   reducido (take (10^7) (cycle [N,E,O,S])) ==  []

Nótese que en el penúltimo ejemplo las reducciones son

       [N,S,S,E,O,N,O]
   --> [S,E,O,N,O]
   --> [S,N,O]
   --> [O]

-}

data Direccion = N | S | E | O deriving (Show, Eq)
type Camino = [Direccion]

opuesto :: Direccion -> Direccion
opuesto N = S
opuesto S = N
opuesto E = O
opuesto O = E

reducido :: Camino -> Camino
reducido = reverse . reducidoAux []

reducidoAux :: Camino -> Camino -> Camino
reducidoAux acc [] = acc
reducidoAux [] (x:xs) = reducidoAux [x] xs
reducidoAux (c:cs) (x:xs)
    | x == opuesto c = reducidoAux cs xs
    | otherwise = reducidoAux (x:c:cs) xs

