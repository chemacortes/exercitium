module Ex_20151123 where

{-

Listas de igual longitud
========================

Definir la función

   mismaLongitud :: [[a]] -> Bool

tal que (mismaLongitud xss) se verifica si todas las listas de la lista de 
listas xss tienen la misma longitud. Por ejemplo,

   mismaLongitud [[1,2],[6,4],[0,0],[7,4]] == True
   mismaLongitud [[1,2],[6,4,5],[0,0]]     == False

-}


import Data.Function (on)
import Data.List

mismaLongitud1 :: [[a]] -> Bool
mismaLongitud1 xss = and $ zipWith ((==) `on` length) xss (tail xss)


mismaLongitud2 :: [[a]] -> Bool
mismaLongitud2 = (==1) . length . nub . map length


mismaLongitud :: [[a]] -> Bool
mismaLongitud = (==1) . length . nubBy ((==) `on` length)


mismaLongitud6 :: [[a]] -> Bool
mismaLongitud6 = null . drop 1 . nub . map length
