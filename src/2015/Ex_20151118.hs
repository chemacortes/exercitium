module Ex_20151118 where

{-

Intersecciones parciales
========================

Definir la función

   interseccionParcial :: Eq a => Int -> [[a]] -> [a]

tal que (interseccionParcial n xss) es la lista de los elementos que pertenecen 
al menos a n conjuntos de xss. Por ejemplo,

   interseccionParcial 1 [[3,4],[4,5,9],[5,4,7]]  == [3,4,5,9,7]
   interseccionParcial 2 [[3,4],[4,5,9],[5,4,7]]  == [4,5]
   interseccionParcial 3 [[3,4],[4,5,9],[5,4,7]]  == [4]
   interseccionParcial 4 [[3,4],[4,5,9],[5,4,7]]  == []

-}

import Data.List

interseccionParcial :: Eq a => Int -> [[a]] -> [a]
interseccionParcial n xss = [ i | i <- (nub . concat) xss
                                , count (i `elem`) xss >= n ]
    where count pred = length . filter pred
    
