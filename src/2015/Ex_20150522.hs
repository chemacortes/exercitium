module Ex_20150522 where

{-
Números comenzando con un dígito dado
=====================================

Definir la función
------------------

   comienzanCon :: [Int] -> Int -> [Int]

tal que (comienzanCon xs d) es la lista de los elementos de xs que empiezan por
el dígito d. Por ejemplo,

   comienzanCon [123,51,11,711,52] 1 == [123,11]
   comienzanCon [123,51,11,711,52] 5 == [51,52]
   comienzanCon [123,51,11,711,52] 6 == []
-}

import Data.Char (intToDigit)
import Data.List (isPrefixOf)

comienzanCon :: [Int] -> Int -> [Int]
comienzanCon xs d = filter ((==intToDigit d) . head . show) xs

comienzanCon2 :: [Int] -> Int -> [Int]
comienzanCon2 xs d = filter ((show d `isPrefixOf`) . show) xs

