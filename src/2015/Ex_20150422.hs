module Ex_20150422 where

{-|

Fracciones cancelativas
=======================

Una fracción x/y es cancelativa si se cumplen las siguientes condiciones:

- x/y es propia (es decir, x < y),
- ninguno de los números x e y son múltiplos de 10
- existe un dígito d tal que al borrar una ocurrencia de d en x y otra en y se
  obtiene una fracción cuyo valor coincide con x/y.

Por ejemplo, 16/64 es cancelativa ya que borrando el 6 en el numerador y el
denominador se obtiene 1/4 que es igual a la original: 16/64 = 1/4.

Definir la función

   cancelativas :: Int -> Int -> [((Int, Int), (Int, Int))]

tal que (cancelativas m n) es la lista de las fracciones cancelativas con su
denominador entre m y n. Por ejemplo,

   ghci> cancelativas 1 100
   [((16,64),(1,4)),((26,65),(2,5)),((19,95),(1,5)),((49,98),(4,8))]
   ghci> cancelativas 101 150
   [((22,121),(2,11)),((33,132),(3,12)),((34,136),(4,16)),((44,143),(4,13))]
   ghci> length (cancelativas 1 200)
   18

-}

import           Control.Monad (guard)
import           Data.List

-- Combinaciones resultantes de quitar un dígito de un número entero
comb :: Int -> [(Char, Int)]
comb n = nub [(c, read xs)| (c, xs) <- zip (show n) (subsecuencia (show n))]

-- Lista de secuencias de una lista con un elemento menos
subsecuencia :: Eq a => [a] -> [[a]]
subsecuencia [] = []
subsecuencia [_] = []
subsecuencia (x:xs) = xs : map (x:) (subsecuencia xs)


-- Para interpretar el resultado:  (a,b),(c,d) ==> a/b, c/d
cancelativas :: Int -> Int -> [((Int, Int), (Int, Int))]
cancelativas n m =
    do
        -- primer denominador
        b <- [n..m]
        guard (b `mod` 10 /= 0)

        -- candidatos a segundo denominador
        (digit, d) <- comb b

        -- candidatos a segundo numerador
        c <- [1..d-1]

        -- obtención del primer numerador
        let (a, r) = (b * c) `divMod` d
        guard (r == 0)
        guard (a `mod` 10 /= 0)
        guard ( (digit:show c) `elem` permutations (show a))

        return ((a,b),(c,d))
