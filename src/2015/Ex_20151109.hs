module Ex_20151109 where

{-

Números autodescriptivos
========================

Un número n es autodescriptivo cuando para cada posición k de n (empezando a 
contar las posiciones a partir de 0), el dígito en la posición k es igual al 
número de veces que ocurre k en n. Por ejemplo, 1210 es autodescriptivo porque 
tiene 1 dígito igual a '0', 2 dígitos iguales a '1', 1 dígito igual a '2' y 
ningún dígito igual a '3'.

Definir la función

   autodescriptivo :: Integer -> Bool

tal que (autodescriptivo n) se verifica si n es autodescriptivo. Por ejemplo,

   λ> autodescriptivo 1210
   True
   λ> [x | x <- [1..100000], autodescriptivo x]
   [1210,2020,21200]
   λ> autodescriptivo 9210000001000
   True

-}

import Data.Char
import Data.List

autodescriptivo :: Integer -> Bool
autodescriptivo n = and . zipWith (==) xs $ map cuenta ['0'..'9']
    where xs = show n
          cuenta x = (intToDigit . length . filter (==x)) xs

autodescriptivo2 :: Integer -> Bool
autodescriptivo2 n = xs `isPrefixOf` 
                     [ digitos !! cuenta c | c <- digitos]
    where xs = show n
          digitos = ['0'..'9']
          cuenta x = count (== x) xs

autodescriptivo3 :: Integer -> Bool
autodescriptivo3 n = and $ zipWith (==)
                     (map digitToInt xs)
                     [count (==c) xs |  c <- ['0'..'9']]
    where xs = show n

count :: Eq a => (a -> Bool) -> [a] -> Int
count p = length . filter p