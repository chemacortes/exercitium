module Ex_20151203 where

{-

Los números de Armstrong
========================

Un número de n dígitos es un número de Armstrong si es igual a la suma de las 
n-ésimas potencias de sus dígitos. Por ejemplo, 371, 8208 y 4210818 son 
números de Armstrong ya que

       371 = 3^3 + 7^3 + 1^3
      8208 = 8^4 + 2^4 + 0^4 + 8^4 
   4210818 = 4^7 + 2^7 + 1^7 + 0^7 + 8^7 + 1^7 + 8^7

Definir las funciones

   esArmstrong :: Integer -> Bool
   armstrong   :: [Integer]

tales que

    (esArmstrong x) se verifica si x es un número de Armstrong. Por ejemplo,

     esArmstrong 371                                      ==  True
     esArmstrong 8208                                     ==  True
     esArmstrong 4210818                                  ==  True
     esArmstrong 2015                                     ==  False
     esArmstrong 115132219018763992565095597973971522401  ==  True
     esArmstrong 115132219018763992565095597973971522402  ==  False

    `armstrong` es la lista cuyos elementos son los números de Armstrong. Por 
    ejemplo,

     λ> take 18 armstrong
     [1,2,3,4,5,6,7,8,9,153,370,371,407,1634,8208,9474,54748,92727]

Comprobar con QuickCheck que los números mayores que
115132219018763992565095597973971522401 no son números de Armstrong.

-}

import Test.QuickCheck

esArmstrong :: Integer -> Bool
esArmstrong n = n == sum [read [c] ^ k | c <- cs]
    where cs = show n
          k = length cs

armstrong :: [Integer]
armstrong = filter esArmstrong [1..]

prop_mayores :: Integer -> Property
prop_mayores n = n > 0 ==> 
    (not.esArmstrong) (n + 115132219018763992565095597973971522401)
