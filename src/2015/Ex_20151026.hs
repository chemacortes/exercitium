module Ex_20151026 where

{-

Mayor resto
===========

El resultado de dividir un número n por un divisor d es un cociente q y un resto r.

Definir la función

   mayorResto :: Int -> Int -> (Int,[Int])

tal que (mayorResto n d) es el par (m,xs) tal que m es el mayor resto de dividir 
n entre x (con 1 ≤ x < d) y xs es la lista de números x menores que d tales que 
el resto de n entre x es m. Por ejemplo,

   mayorResto 20 10  ==  (6,[7])
   mayorResto 50 8   ==  (2,[3,4,6])
   
-}


-- Solución oficial
mayorResto :: Int -> Int -> (Int,[Int])
mayorResto n d = (m, [x | x <- [1..d-1], n `rem` x == m])
    where m = maximum [n `rem` x | x <- [1..d-1]]


-- Solución optimizada
mayorResto2 :: Int -> Int -> (Int,[Int])
mayorResto2 n d = aux 0 [] [1..d-1]
    where aux m xs [] = (m, xs)
          aux m xs (y:ys) | r == m      = aux m (xs ++ [y]) ys
                          | r > m       = aux r [y] ys
                          | otherwise   = aux m xs ys
            where r = n `rem` y

-- Mediante fold
mayorResto3 :: Int -> Int -> (Int,[Int])
mayorResto3 n d = foldr aux (0, [1]) [2..d-1]
    where aux x (m,xs) | r == m     = (m, x:xs)
                       | r > m      = (r, [x])
                       | otherwise  = (m, xs)
            where r = n `rem` x

