module Ex_20151023 where

{-

Entero positivo con ciertas propiedades
=======================================

El 6 de octubre, se propuso en el blog Gaussianos el siguiente problema

    Demostrar que para todo entero positivo n, existe otro entero positivo que 
    tiene las siguientes propiedades:

        - Tiene exactamente n dígitos.
        - Ninguno de sus dígitos es 0.
        - Es divisible por la suma de sus dígitos.

Definir la función

   especiales :: Integer -> [Integer]

tal que (especiales n) es la lista de los números enteros que cumplen las 3 
propiedades anteriores para n. Por ejemplo,

   take 3 (especiales 2)  ==  [12,18,21]
   take 3 (especiales 3)  ==  [111,112,114]
   head (especiales 30)   ==  111111111111111111111111111125
   length (especiales 3)  ==  108
   null (especiales 1000) ==  False

En el primer ejemplo, 12 es un número especial para 2 ya que tiene exactamente 
2 dígitos, ninguno de sus dígitos es 0 y 12 es divisible por la suma de sus 
dígitos.

-}

import Data.Char (intToDigit, digitToInt)

-- Siguiendo la definición
especiales :: Integer -> [Integer]
especiales n = [x | x <- [(10^n-1) `div` 9..10^n-1]
                  , esEspecial x]
 
esEspecial :: Integer -> Bool
esEspecial x = 
    notElem 0 digitos &&
    x `mod` sum digitos == 0        
    where digitos = [read [d] | d <- show x]

-- Usando un generador de candidatos
especiales2 :: Integer -> [Integer]
especiales2 n = [ m | (m, s) <- aux
                   , m `mod` s == 0]
    where aux = [(read xs, sumaDigitos xs) | xs <- gen n]

sumaDigitos :: String -> Integer
sumaDigitos = toInteger . sum . map digitToInt

-- Generador de cadenas de dígitos ordenadas de una longitud dada.
-- Las cadenas no tienen ningún '0'
--
-- Ej: gen 2 = ["11","12","13","14","15",...,"98","99"]
gen :: Integer -> [String]
gen 0 = [""]
gen n = [intToDigit c : xs | c <- [1..9]
                           , xs <- gen (n-1)]
  
-- De manera más compacta                                
especiales3 :: Integer -> [Integer]
especiales3 n = [ m | (m, s) <- gen2 n
                    , m `mod` s == 0]
                                  
-- Generador de candidatos a números especiales y la suma de sus dígitos
gen2 :: Integer -> [(Integer, Integer)]
gen2 0 = [(0,0)]
gen2 n = [(m*10+c, s+c) | (m,s) <- gen2 (n-1)
                        , c <- [1..9] ]
