module Ex_20150603 where

{-
Distancia esperada entre dos puntos de un cuadrado unitario
===========================================================

Definir, por simulación, la función

   distanciaEsperada :: Int -> Double

tal que (distanciaEsperada n) es la distancia esperada entre n puntos del
cuadrado unitario de vértices opuestos (0,0) y (1,1), elegidos aleatoriamente.
Por ejemplo,

   distanciaEsperada 10    ==  0.4815946544198219
   distanciaEsperada 10    ==  0.5558438642543654
   distanciaEsperada 100   ==  0.5699663553203216
   distanciaEsperada 100   ==  0.5085629461572269
   distanciaEsperada 1000  ==  0.5376963424746385
   distanciaEsperada 1000  ==  0.523432374720393

Nota. El valor exacto de la distancia esperada es

   (sqrt(2) + 2 + 5*log(1+sqrt(2)))/15 = 0.5214054331647207

-}

import System.Random
import System.IO.Unsafe (unsafePerformIO)
import Control.Monad (liftM, replicateM)

distanciaEsperada :: Int -> Double
distanciaEsperada n = distanciaMedia xs
    where xs = map unsafePerformIO $ replicate n puntoAleatorio

-- definición monádica (más correcta)
distanciaEsperada2 :: Int -> IO Double
distanciaEsperada2 n = liftM distanciaMedia $ replicateM n puntoAleatorio

-- distancia media entre puntos de un trazo
distanciaMedia :: [(Double,Double)] -> Double
distanciaMedia xs = sum (zipWith distancia xs (drop 1 xs)) / fromIntegral (length xs -1)
    where distancia (x1,y1) (x2,y2) = sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2))

-- obtención aleatoria de puntos en el cuadrado unitario
puntoAleatorio :: IO (Double, Double)
puntoAleatorio = do x <- g
                    y <- g
                    return (x, y)
    where g = getStdRandom $ randomR (0.0, 1.0)





