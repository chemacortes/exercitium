module Ex_20151104 where

{-
Números muy divisibles por 3
============================

Se dice que un número n es muy divisible por 3 si es divisible por 3 y sigue 
siendo divisible por 3 si vamos quitando dígitos por la derecha. Por ejemplo, 
96060 es muy divisible por 3 porque 96060, 9606, 960, 96 y 9 son todos 
divisibles por 3.

Definir las funciones

   muyDivPor3             :: Integer -> Bool
   numeroMuyDivPor3Cifras :: Integer -> Integer

tales que

    (muyDivPor3 n) se verifica si n es muy divisible por 3. Por ejemplo,

     muyDivPor3 96060 == True
     muyDivPor3 90616 == False

    (numeroMuyDivPor3CifrasC k) es la cantidad de números de k cifras muy 
    divisibles por 3. Por ejemplo,

     numeroMuyDivPor3Cifras 5                    == 768
     numeroMuyDivPor3Cifras 7                    == 12288
     numeroMuyDivPor3Cifras (10^6) `rem` (10^6)  == 332032

-}

-- Un número es divisible por 3 si la suma de sus cifras es divisible por 3. 
-- Aplicando esta propiedad de la divisibilidad, por inducción se puede 
-- comprobar que para un "número muy divisible por 3" todas y cada una de sus 
-- cifras tienen que ser divisibles por 3, o ser '0'.
muyDivPor3 :: Integer -> Bool
muyDivPor3 n = and [ c `elem` "0369" | c <- show n]

-- Los "números muy divisibles por 3" se construyen con cuatro cifras, "0369". 
-- La cantidad de estos números sería 4^n, a los que habría que restar los que 
-- empiezan por '0'
numeroMuyDivPor3Cifras :: Integer -> Integer
numeroMuyDivPor3Cifras n = 3 * 4^(n - 1)
