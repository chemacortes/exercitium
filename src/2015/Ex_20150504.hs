module Ex_20150504 where

{-
Números cuyas cifras coinciden con las de sus factores primos
=============================================================

Un número n es especial si al unir las cifras de sus factores primos,
se obtienen exactamente las cifras de n, aunque puede ser en otro orden.
Por ejemplo, 1255 es especial, pues los factores primos de 1255 son 5 y 251.

Definir la función

   esEspecial :: Integer -> Bool

tal que (esEspecial n) se verifica si un número n es especial. Por ejemplo,

   esEspecial 1255 == True
   esEspecial 125  == False

Comprobar con QuickCheck que todo número primo es especial.

Calcular los 5 primeros números especiales que no son primos.
-}

import           Data.List
import           Data.Numbers.Primes
import qualified Data.Set            as S
import           Test.QuickCheck

esEspecial :: Integer -> Bool
esEspecial n = (sort . show) n == (sort . concatMap show) (nub (primeFactors n))

-- usando Sets
esEspecial2 :: Integer -> Bool
esEspecial2 n = S.fromList (show n) == S.fromList (concatMap show (nub (primeFactors n)))

prop_primos :: Integer -> Property
prop_primos n = isPrime (abs n) ==> esEspecial (abs n)


cincoEspecialesNoPrimos :: [Integer]
cincoEspecialesNoPrimos = take 5 [ x | x <- [1..], not (isPrime x), esEspecial x]
