module Ex_20151201 where

{-

Paridad de un árbol
===================

Los árboles binarios con valores en las hojas y en los nodos se definen por

   data Arbol a = H a 
                 | N a (Arbol a) (Arbol a) 
                 deriving Show

Por ejemplo, el árbol

        5         
       / \        
      /   \       
     9     7      
    / \   / \     
   1   4 6   8

se puede representar por

   N 5 (N 9 (H 1) (H 4)) (N 7 (H 6) (H 8))

Decimos que un árbol binario es par si la mayoría de sus nodos son pares e 
impar en caso contrario.

Para representar la paridad se define el tipo Paridad

   data Paridad = Par | Impar deriving (Eq, Show)

Definir la función

   paridad :: Arbol Int -> Paridad

tal que (paridad a) es la paridad del árbol a. Por ejemplo,

   paridad (N 8 (N 6 (H 3) (H 4)) (H 5))  ==  Par
   paridad (N 8 (N 9 (H 3) (H 4)) (H 5))  ==  Impar
   
-}

data Arbol a = H a 
             | N a (Arbol a) (Arbol a) 
             deriving Show
             
data Paridad = Par | Impar deriving (Eq, Show)

paridad :: Arbol Int -> Paridad
paridad a | balancePares a >= 0 = Par
          | otherwise           = Impar

balancePares :: Arbol Int -> Integer
balancePares x =
    case x of 
      (H n)       -> aux n
      (N n a b)   -> aux n + balancePares a + balancePares b
    where aux n = if even n then 1 else -1


paridad2 :: Arbol Int -> Paridad
paridad2 a | elemPar a >= 0 = Par
           | otherwise      = Impar

elemPar :: Arbol Int -> Integer 
elemPar (H n)     | even n    =  1
                  | otherwise = -1
elemPar (N n a b) | even n    =  1 + elemPar a + elemPar b
                  | otherwise = -1 + elemPar a + elemPar b
