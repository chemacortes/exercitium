module Ex_20150420 where

{-|

Con mínimo común denominador
============================

Los números racionales se pueden representar como pares de enteros:

   type Racional a = (a,a)

Definir la función

   reducida :: Integral a => [Racional a] -> [Racional a]

tal que (reducida xs) es la lista de los números racionales donde
cada uno es igual al correspondiente elemento de xs y el denominador
de todos los elementos de (reducida xs) es el menor número que cumple
dicha condición; es decir, si xs es la lista

   [(x_1, y_1), ..., (x_n, y_n)]

entonces (reducida xs) es

   [(z_1, d), ..., (z_n, d)]

tales que

   z_1/d = x_1/y_1, ..., z_n/d = x_n/y_n

y d es el menor posible. Por ejemplo,

   reducida [(1,2),(1,3),(1,4)]  ==  [(6,12),(4,12),(3,12)]
   reducida [(1,2),(1,3),(6,4)]  ==  [(3,6),(2,6),(9,6)]
   reducida [(-7,6),(-10,-8)]    ==  [(-14,12),(15,12)]
   reducida [(8,12)]             ==  [(2,3)]

-}


type Racional a = (a,a)

reducida :: Integral a => [Racional a] -> [Racional a]
reducida xs = [(x*d `div` y, d)| (x,y) <- xs ]
    where d = foldr (lcm . denom) 1 xs
          denom (a,b) =  b `div` gcd a b
