module Ex_20150501 where

{-
Codificación de Gödel
=====================

Dada una lista de números naturales xs, la codificación de Gödel de xs se obtiene
multiplicando las potencias de los primos sucesivos, siendo los exponentes los
elementos de xs. Por ejemplo, si xs = [6,0,4], la codificación de xs es

   2^6 * 3^0 * 5^4 = 64 * 1 * 625 = 40000

Definir las funciones

   codificaG   :: [Integer] -> Integer
   decodificaG :: Integer -> [Integer]

tales que

(codificaG xs) es la codificación de Gödel de xs. Por ejemplo,

     codificaG [6,0,4]           == 40000
     codificaG [3,1,1]           == 120
     codificaG [3,1,0,0,0,0,0,1] == 456
     codificaG [1..6]            == 4199506113235182750

(decodificaG n) es la lista xs cuya codificación es n. Por ejemplo,

     decodificaG 40000               == [6,0,4]
     decodificaG 120                 == [3,1,1]
     decodificaG 456                 == [3,1,0,0,0,0,0,1]
     decodificaG 4199506113235182750 == [1,2,3,4,5,6]

Comprobar con QuickCheck que ambas funciones son inversas.

-}

import           Data.Numbers.Primes
import           Test.QuickCheck

codificaG   :: [Integer] -> Integer
codificaG xs = product $ zipWith (^) primes xs

decodificaG :: Integer -> [Integer]
decodificaG n = decodifica n 0 primes

decodifica :: Integer -> Integer -> [Integer] -> [Integer]
decodifica 1 acc _      = [acc]
decodifica n acc (p:ps)
    | n `mod` p == 0    = decodifica (n `div` p) (acc+1) (p:ps)
    | otherwise         = acc : decodifica n 0 ps


prop_codificaG :: [NonNegative Integer] -> Property
prop_codificaG xs =
    (not.null) xs && last xs' /= 0 ==> (decodificaG . codificaG) xs' == xs'
        where xs' = map getNonNegative xs

prop_decodificaG :: Positive Integer -> Bool
prop_decodificaG (Positive n) = (codificaG . decodificaG) n == n


runTests :: IO ()
runTests = do quickCheck prop_codificaG
              quickCheck prop_decodificaG
