module Ex_20151022 where

{-

Centro de masas
===============

El centro de masas de un sistema discreto es el punto geométrico que 
dinámicamente se comporta como si en él estuviera aplicada la resultante de las 
fuerzas externas al sistema.

Representamos un conjunto de n masas en el plano mediante una lista de n pares 
de la forma ((a(i),b(i)),m(i)) donde (a(i),b(i)) es la posición y m(i) la masa 
puntual. Las coordenadas del centro de masas (a,b) se calculan por

   a = (a(1)*m(1) + a(2)*m(2) + ... + a(n)*m(n)) / (m(1) + m(2) +...+ m(n))
   b = (b(1)*m(1) + b(2)*m(2) + ... + b(n)*m(n)) / (m(1) + m(2) +...+ m(n))

Definir la función

   centrodeMasas :: [((Float,Float),Float)] -> (Float,Float)

tal que (centrodeMasas xs) es las coordenadas del centro
de masas del sistema discreto xs. Por ejemplo:

   centrodeMasas [((-1,3),2),((0,0),5),((1,3),3)] == (0.1,1.5)
   
-}


centrodeMasas :: [((Float,Float),Float)] -> (Float,Float)
centrodeMasas xs = (sum as/sum ms, sum bs/sum ms)
    where (as,bs,ms) = unzip3 [(a*m, b*m, m) | ((a,b), m) <- xs]
    
   
-- Versión recursiva
centrodeMasas2 :: [((Float,Float),Float)] -> (Float,Float)
centrodeMasas2 xs = aux xs (0,0,0)
    where aux [] (asuma,bsuma,msuma) = (asuma/msuma, bsuma/msuma)
          aux (((a,b),m):ys) (asuma, bsuma, msuma) = aux ys (asuma+a*m, bsuma+b*m, msuma+m)

-- Versión fold
centrodeMasas3 :: [((Float,Float),Float)] -> (Float,Float)
centrodeMasas3 xs = (as/ms, bs/ms)
    where ((as,bs),ms) = foldr1 (\((a,b),m) ((as,bs),ms) -> ((as+a*m,bs+b*m),ms+m)) xs
