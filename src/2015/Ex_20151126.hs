module Ex_20151126 where

{-

Repeticiones según la posición
==============================

Definir la función

   transformada :: [a] -> [a]

tal que (transformada xs) es la lista obtenida repitiendo cada elemento tantas 
veces como indica su posición en la lista. Por ejemplo,

   transformada [7,2,5] == [7,2,2,5,5,5]
   transformada "eco"   == "eccooo"

Comprobar con QuickCheck si la transformada de una lista de n números enteros, 
con n ≥ 2, tiene menos de n³ elementos.


-}

import Test.QuickCheck

transformada1 :: [a] -> [a]
transformada1 xs = concat [ replicate n x | (x,n) <- zip xs [1..]]

transformada :: [a] -> [a]
transformada = concat . zipWith replicate [1..]

prop_transformada :: [Int] -> Property
prop_transformada xs = n >= 2 ==> length (transformada xs) < n^3
    where n = length xs

runTest :: IO ()
runTest = quickCheck prop_transformada
