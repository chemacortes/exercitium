module Ex_20150424 where

{-|

Primos circulares
=================

Un primo circular es un número tal que todas las rotaciones de sus dígitos
producen números primos. Por ejemplo, 197 es un primo circular ya que las
rotaciones de sus dígitos son 197, 971 y 719 y los tres números son primos.

Definir la constante

   circulares :: [Integer]

cuyo valor es la lista de los números primos circulares. Por ejemplo,

   take 16 circulares == [2,3,5,7,11,13,17,31,37,71,73,79,97,113,131,197]
   circulares !! 50   == 933199
-}

import           Data.Numbers.Primes

-- Desplazamiento hacia la izquierda
rotl :: [a] -> [a]
rotl [] = []
rotl (x:xs) = xs ++ [x]

-- Sequencias rotadas de una lista
rotaciones :: [a] -> [[a]]
rotaciones xs = take (length xs) $ iterate rotl xs

-- Primos circulares
circulares :: [Integer]
circulares = filter (all isPrime . rotados) primes
    where
        -- resto de circulares para comprobar si son primos
        rotados :: Integer -> [Integer]
        rotados p = map read $ tail (rotaciones (show p))

-- Optimización considerando que todos los dígitos del primo circular
-- tienen que ser números impares
circulares2 :: [Integer]
circulares2 = filter esCircular primes
    where
        esCircular :: Integer -> Bool
        esCircular 2 = True
        esCircular p = all (`elem` "13579") xs  &&
                       all isPrime (map read $ tail (rotaciones xs))
            where xs = show p
