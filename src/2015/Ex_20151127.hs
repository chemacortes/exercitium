module Ex_20151127 where

{-

Particiones en k subconjuntos
=============================

Definir la función

   particiones :: [a] -> Int -> [[[a]]]

tal que (particiones xs k) es la lista de las particiones de xs en k 
subconjuntos disjuntos. Por ejemplo,

   λ> particiones [2,3,6] 2
   [[[2],[3,6]],[[2,3],[6]],[[3],[2,6]]]
   λ> particiones [2,3,6] 3
   [[[2],[3],[6]]]
   λ> particiones [4,2,3,6] 3
   [[[4],[2],[3,6]],[[4],[2,3],[6]],[[4],[3],[2,6]],
    [[4,2],[3],[6]],[[2],[4,3],[6]],[[2],[3],[4,6]]]
   λ> particiones [4,2,3,6] 1
   [[[4,2,3,6]]]
   λ> particiones [4,2,3,6] 4
   [[[4],[2],[3],[6]]]

-}


particiones :: [a] -> Int -> [[[a]]]
particiones [] _    = []
particiones xs 1    = [[xs]]
particiones (x:xs) n =  map ([x]:) (particiones xs (n-1))
                     ++ concatMap (apply (x:)) (particiones xs n)

apply :: (a -> a) -> [a] -> [[a]]
apply _ []  = []
apply f (x:xs) = (f x:xs) : map (x:) (apply f xs) 
