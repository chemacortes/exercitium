module Ex_20150609 where


{-

Cálculo de la suma Σ n*n!
=========================

Definir la función

   suma :: Integer -> Integer

tal que (suma n) es la suma 1 * 1! + 2 * 2! + 3 * 3! + … + n * n!. Por ejemplo,

   suma 1  ==  1
   suma 2  ==  5
   suma 3  ==  23
   suma 4  ==  119
   suma 5  ==  719
   take 9 (show (suma (70000)))  ==  "823780458"

-}

-- fuerza bruta (comprensión de listas)
suma :: Integer -> Integer
suma n = sum [ i * product [2..i] | i <- [1..n] ]

-- recursiva
suma2 :: Integer -> Integer
suma2 n = aux 0 1 1
    where
        aux acc i fact
            | n == i    = acc + i * fact
            | otherwise = aux (acc + i * fact) (i + 1) ((i + 1) * fact)

-- funcional pura (usando scanl1)
suma3 :: Integer -> Integer
suma3 n = sum . zipWith (*) [1..n] $ scanl1 (*) [1..]
