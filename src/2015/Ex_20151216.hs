module Ex_20151216 where

{-

Factorizable respecto de una lista
==================================

Definir la función

   factorizable :: Integer -> [Integer] -> Bool

tal que (factorizable x ys) se verifica si x se puede escribir como producto de 
potencias de elementos de ys. Por ejemplo,

   factorizable 1  [2,5,6]                           ==  True
   factorizable 12 [2,5,3]                           ==  True
   factorizable 12 [2,5,6]                           ==  True
   factorizable 12 [7,5,12]                          ==  True
   factorizable 12 [2,3,1]                           ==  True
   factorizable 12 [2,3,0]                           ==  True
   factorizable 24 [12,4,6]                          ==  True
   factorizable 0  [2,3,0]                           ==  True
   factorizable 12 [5,6]                             ==  False
   factorizable 12 [2,5,1]                           ==  False
   factorizable 0  [2,3,5]                           ==  False
   factorizable (product [1..3000])     [1..100000]  ==  True
   factorizable (1 + product [1..3000]) [1..100000]  ==  False

-}

factorizable :: Integer -> [Integer] -> Bool
factorizable 1 _  = True
factorizable 0 ys = 0 `elem` ys
factorizable x ys = or [ factorizable (x `div` y) ys | y <- ys
                                                     , y /= 0 && y /= 1
                                                     , x `mod` y == 0 ]


factorizable4 :: Integer -> [Integer] -> Bool
factorizable4 1 _  = True
factorizable4 _ [] = False
factorizable4 0 ys = 0 `elem` ys
factorizable4 x (y:ys) =
    (y/=0 && y/=1 && x `mod` y ==0 && factorizable4 (x `div` y) (y:ys))
    || factorizable4 x ys


factorizable1 :: Integer -> [Integer] -> Bool
factorizable1 _ []     = False
factorizable1 1 _      = True
factorizable1 0 ys     = 0 `elem` ys
factorizable1 x (y:ys) = ((y > 1) && (x `mod` y == 0) && factorizable1 (x `div` y) (y:ys))
                       || factorizable1 x ys

factorizable2 :: Integer -> [Integer] -> Bool
factorizable2 _ []     = False
factorizable2 1 _      = True
factorizable2 0 ys     = 0 `elem` ys
factorizable2 x (0:ys) = factorizable2 x ys
factorizable2 x (1:ys) = factorizable2 x ys
factorizable2 x (y:ys) | x `mod` y /= 0 = factorizable2 x ys
                       | otherwise =  factorizable2 (x `div` y) (y:ys)
                                   || factorizable2 x ys

factorizable3 :: Integer -> [Integer] -> Bool
factorizable3 _ []     = False
factorizable3 1 _      = True
factorizable3 0 ys     = 0 `elem` ys
factorizable3 x (y:ys) | y == 0 || y == 1 || x `mod` y /= 0 = factorizable3 x ys
                       | otherwise =  factorizable3 (x `div` y) (y:ys)
                                   || factorizable3 x ys


-- Chequea si x = y^n
esPotenciaDe :: Integer -> Integer -> Bool
esPotenciaDe 0 y                    = y == 0
esPotenciaDe 1 _                    = True
esPotenciaDe x y | x `mod` y == 0   = esPotenciaDe (x `div` y) y
                 | otherwise        = False

