module Ex_20151124 where

{-

Ganadores de las elecciones
===========================

Los resultados de las votaciones a delegado en un grupo de clase se recogen 
mediante listas de asociación. Por ejemplo,

   votos :: [(String,Int)]
   votos = [("Ana Perez",10),("Juan Lopez",7), ("Julia Rus", 27),
            ("Pedro Val",1), ("Pedro Ruiz",27),("Berta Gomez",11)]

Definir la función

   ganadores :: [(String,Int)] -> [String]

tal que (ganadores xs) es la lista de los estudiantes con mayor número de 
votos en xs. Por ejemplo,

    ganadores votos == ["Julia Rus","Pedro Ruiz"]
    
-}


votos :: [(String,Int)]
votos = [("Ana Perez",10),("Juan Lopez",7), ("Julia Rus", 27),
        ("Pedro Val",1), ("Pedro Ruiz",27),("Berta Gomez",11)]

ganadores :: [(String,Int)] -> [String]
ganadores xs = [ nombre | (nombre, n) <- xs, n == m]
    where m = maximum (map snd xs)
