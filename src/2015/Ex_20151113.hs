module Ex_20151113 where

{-

Números muy pares
=================

Un entero positivo x es muy par si tanto x como x² sólo contienen cifras pares. 
Por ejemplo, 200 es muy par porque todas las cifras de 200 y 200² = 40000 son 
pares; pero 26 no lo es porque 26² = 676 tiene cifras impares.

Definir la función

   siguienteMuyPar :: Integer -> Integer

tal que (siguienteMuyPar x) es menor número mayor que x que es muy par. Por 
ejemplo,

   siguienteMuyPar 300           ==  668
   siguienteMuyPar 668           ==  680
   siguienteMuyPar 828268400000  ==  828268460602
-}

siguienteMuyPar :: Integer -> Integer
siguienteMuyPar n = head [ x | x <- siguientesPares
                             , todoCifrasPares x
                             , todoCifrasPares (x*x) ]
    where siguientesPares | even n    = [n+2,n+4..]
                          | otherwise = [n+1,n+3..]
          todoCifrasPares x = all (`elem` "02468") (show x)
