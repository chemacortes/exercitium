module Ex_20150602 where

{-

Caminos en un grafo
===================

Definir las funciones

   grafo   :: [(Int,Int)] -> Grafo Int Int
   caminos :: Grafo Int Int -> Int -> Int -> [[Int]]

tales que

    (grafo as) es el grafo no dirigido definido cuyas aristas son as. Por ejemplo,

     ghci> grafo [(2,4),(4,5)]
     G ND (array (2,5) [(2,[(4,0)]),(3,[]),(4,[(2,0),(5,0)]),(5,[(4,0)])])

    (caminos g a b) es la lista los caminos en el grafo g desde a hasta b sin
    pasar dos veces por el mismo nodo. Por ejemplo,

     ghci> sort (caminos (grafo [(1,3),(2,5),(3,5),(3,7),(5,7)]) 1 7)
     [[1,3,5,7],[1,3,7]]
     ghci> sort (caminos (grafo [(1,3),(2,5),(3,5),(3,7),(5,7)]) 2 7)
     [[2,5,3,7],[2,5,7]]
     ghci> sort (caminos (grafo [(1,3),(2,5),(3,5),(3,7),(5,7)]) 1 2)
     [[1,3,5,2],[1,3,7,5,2]]
     ghci> caminos (grafo [(1,3),(2,5),(3,5),(3,7),(5,7)]) 1 4
     []
     ghci> length (caminos (grafo [(i,j) | i <- [1..10], j <- [i..10]]) 1 10)
     109601

Nota: Este ejercicio debe realizarse usando únicamente las funciones de la
librería de grafos (I1M.Grafo) que se describe [aquí][1] y se encuentra [aquí][2].

[1]: http://www.cs.us.es/~jalonso/cursos/i1m/doc/Tipos_abstractos_de_datos.html#el-tad-de-los-grafos-i1m.grafo
[2]: http://www.cs.us.es/~jalonso/cursos/i1m/codigos/I1M2014.zip

-}

import Data.List
import I1M.Grafo

-- grafo no dirigido definido por sus aristas
grafo   :: [(Int,Int)] -> Grafo Int Int
grafo xs = creaGrafo ND (a,b) [ (i,j,0) | (i,j) <- xs]
    where ys = map fst xs ++ map snd xs
          (a,b) = (minimum ys, maximum ys)

-- lista los caminos en el grafo g desde a hasta b sin
-- pasar dos veces por el mismo nodo
caminos :: Grafo Int Int -> Int -> Int -> [[Int]]
caminos g a b = aux a [a]
    where
        -- 'vs' es la lista de nodos visitados
        aux v vs
            | v == b    = [[b]]
            | otherwise = [ v:xs | c <- adyacentes g v,
                                   c `notElem` vs,
                                   xs <- aux c (c:vs)]


runTests :: IO ()
runTests = do print $ grafo [(2,4),(4,5)]
              print $ sort (caminos (grafo [(1,3),(2,5),(3,5),(3,7),(5,7)]) 1 7)
              print $ sort (caminos (grafo [(1,3),(2,5),(3,5),(3,7),(5,7)]) 2 7)
              print $ sort (caminos (grafo [(1,3),(2,5),(3,5),(3,7),(5,7)]) 1 2)
              print $ caminos (grafo [(1,3),(2,5),(3,5),(3,7),(5,7)]) 1 4
              print $ length (caminos (grafo [(i,j) | i <- [1..10], j <- [i..10]]) 1 10)


