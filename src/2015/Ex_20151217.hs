module Ex_20151217 where

{-

Suma de elementos en posiciones dadas
=====================================

Definir la función

   sumaEnPosicion :: [Int] -> [Int] -> Int

tal que (sumaEnPosicion xs ys) es la suma de todos los elementos de xs cuyas 
posiciones se indican en ys. Por ejemplo,

   sumaEnPosicion [1,2,3] [0,2]  ==  4
   sumaEnPosicion [4,6,2] [1,3]  ==  6
   sumaEnPosicion [3,5,1] [0,1]  ==  8

-}

sumaEnPosicion :: [Int] -> [Int] -> Int
sumaEnPosicion xs ys = sum [ x | (x,n) <- zip xs [0..], n `elem` ys ]
