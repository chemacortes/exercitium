module Ex_20151102 where

{-

Aproximación del número pi
==========================

Una forma de aproximar el número π es usando la siguiente igualdad:

    π         1     1*2     1*2*3     1*2*3*4 
   --- = 1 + --- + ----- + ------- + --------- + ....
    2         3     3*5     3*5*7     3*5*7*9

Es decir, la serie cuyo término general n-ésimo es el cociente entre el 
producto de los primeros n números y los primeros n números impares:

               Π i   
   s(n) =  -----------
            Π (2*i+1)

Definir la función

   aproximaPi :: Double -> Double

tal que (aproximaPi n) es la aproximación del número π calculada con la serie 
anterior hasta el término n-ésimo. Por ejemplo,

   aproximaPi 10   ==  3.141106021601377
   aproximaPi 30   ==  3.1415926533011587
   aproximaPi 50   ==  3.1415926535897922

-}

-- 1ª solución (por comprensión):
aproximaPi :: Double -> Double
aproximaPi n = 
    2 * sum [product [1..i] / product [1,3..2*i+1] | i <- [0..n]]
 
-- 2ª solución (por recursión):
aproximaPi2 :: Double -> Double
aproximaPi2 0 = 2
aproximaPi2 n = 
    aproximaPi2 (n-1) + 2 * product [1..n] / product [3,5..2*n+1]
