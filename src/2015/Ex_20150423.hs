module Ex_20150423 where

{-|
Diccionario de frecuencias
==========================

Definir la función

   frecuencias :: Ord a => [a] -> Map a Int

tal que (frecuencias xs) es el diccionario formado por los elementos de xs junto
con el número de veces que aparecen en xs. Por ejemplo,

   ghci> frecuencias "sosos"
   fromList [('o',2),('s',3)]
   ghci> frecuencias (show (10^100))
   fromList [('0',100),('1',1)]
   ghci> frecuencias (take (10^6) (cycle "abc"))
   fromList [('a',333334),('b',333333),('c',333333)]
   ghci> size (frecuencias (take (10^6) (cycle [1..10^6])))
   1000000

-}

import Data.List -- (group, sort)
import Control.Arrow ((&&&))
import Data.Map (Map, fromList, insertWith, empty, size)

-- versión recursiva
frecuencias :: Ord a => [a] -> Map a Int
frecuencias [] = empty
frecuencias (x:xs) = insertWith (+) x 1 (frecuencias xs)

-- versión fold
frecuencias2 :: Ord a => [a] -> Map a Int
frecuencias2 = foldr (\k -> insertWith (+) k 1) empty

-- versión usando Control.Arrow.&&&
frecuencias3 :: Ord a => [a] -> Map a Int
frecuencias3 xs = fromList $ map (head &&& length) $ group $ sort xs

-- versión "pointless" de la anterior
frecuencias4 :: Ord a => [a] -> Map a Int
frecuencias4 = fromList . map (head &&& length) . group . sort
