module Ex_20150526 where

{-

Reparto de escaños por la ley d’Hont
====================================

El sistema D'Hondt es una fórmula electoral, creada por Victor d'Hondt, que
permite obtener el número de cargos electos asignados a las candidaturas, en
proporción a los votos conseguidos.

Tras el recuento de los votos, se calcula una serie de divisores para cada
partido. La fórmula de los divisores es V/N, donde V representa el número total
de votos recibidos por el partido, y N representa cada uno de los números
enteros desde 1 hasta el número de cargos electos de la circunscripción objeto
de escrutinio. Una vez realizadas las divisiones de los votos de cada partido
por cada uno de los divisores desde 1 hasta N, la asignación de cargos electos
se hace ordenando los cocientes de las divisiones de mayor a menor y asignando
a cada uno un escaño hasta que éstos se agoten

Definir la función

   reparto :: Int -> [Int] -> [(Int,Int)]

tal que (reparto n vs) es la lista de los pares formados por los números de los
partidos y el número de escaño que les corresponden al repartir n escaños en
función de la lista de sus votos. Por ejemplo,

   ghci> reparto 7 [340000,280000,160000,60000,15000]
   [(1,3),(2,3),(3,1)]

es decir,

    al 1º partido (que obtuvo 340000 votos) le corresponden 3 escaños,
    al 2º partido (que obtuvo 280000 votos) le corresponden 3 escaños,
    al 3º partido (que obtuvo 160000 votos) le corresponden 1 escaño.

-}

import Data.List
import Control.Arrow

-- ejemplos
-- ghci> reparto 21 ejVotos2
-- [(1,9),(2,7),(3,4),(4,1)]
ejVotos,ejVotos2,ejVotos3 :: [Int]
ejVotos  = [340000,280000,160000,60000,15000]
ejVotos2 = [391000,311000,184000,73000,27000,12000,2000]
ejVotos3 = [221,195,40,6,4]



reparto :: Int -> [Int] -> [(Int,Int)]
reparto n vs = total . take n . map snd
             $ sortReverse
               [ (x `div` j, i) | j <- [1..n] , (i,x) <- zipWithIndex vs]
    where
        zipWithIndex = zip [1..]
        sortReverse = sortBy (flip compare)
        total xs = map (head &&& length) $ (group.sort) xs

