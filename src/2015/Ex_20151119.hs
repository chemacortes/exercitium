module Ex_20151119 where

{-

Subárboles monovalorados
========================

Los árboles binarios con valores enteros se pueden representar mediante el tipo 
Arbol definido por

   data Arbol = H Int 
              | N Int Arbol Arbol
              deriving Show

Por ejemplo, el árbol

         7
        / \ 
       /   \
      /     \
     4       9
    / \     / \
   1   3   5   6

se puede representar por

   N 7 (N 4 (H 1) (H 3)) (N 9 (H 5) (H 6))

Un árbol es monovalorado si todos sus elementos son iguales. Por ejemplo, de 
los siguientes árboles sólo son monovalorados los dos primeros

    5          9           5          9    
   / \        / \         / \        / \   
  5   5      9   9       5   6      9   7  
                / \                    / \ 
               9   9                  9   9

Definir la función

   monovalorados :: Arbol -> [Arbol]

tal que (monovalorados a) es la lista de los subárboles monovalorados de a. Por 
ejemplo,

   λ> monovalorados (N 5 (H 5) (H 5))
   [N 5 (H 5) (H 5),H 5,H 5]
   λ> monovalorados (N 5 (H 5) (H 6))
   [H 5,H 6]
   λ> monovalorados (N 9 (H 9) (N 9 (H 9) (H 9)))
   [N 9 (H 9) (N 9 (H 9) (H 9)),H 9,N 9 (H 9) (H 9),H 9,H 9]
   λ> monovalorados (N 9 (H 9) (N 7 (H 9) (H 9)))
   [H 9,H 9,H 9]
   λ> monovalorados (N 9 (H 9) (N 9 (H 7) (H 9)))
   [H 9,H 7,H 9]

-}

data Arbol = H Int 
           | N Int Arbol Arbol
           deriving Show

monovalorados :: Arbol -> [Arbol]
monovalorados z@(H _)     = [z]
monovalorados z@(N n a b) | todosIgualA n z     =
                                z : subarboles a ++ subarboles b
                          | otherwise           =
                                monovalorados a ++ monovalorados b

todosIgualA :: Int -> Arbol -> Bool
todosIgualA k (H n)     = k == n
todosIgualA k (N n a b) = (k == n) && todosIgualA k a && todosIgualA k b

subarboles :: Arbol -> [Arbol]
subarboles z@(H _)      = [z]
subarboles z@(N _ a b)  = z : (subarboles a ++ subarboles b)
