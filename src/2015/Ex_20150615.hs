module Ex_20150615 where

{-

Algoritmo de bajada para resolver un sistema triangular inferior
================================================================

Un sistema de ecuaciones lineales Ax = b es triangular inferior si todos los
elementos de la matriz A que están por encima de la diagonal principal son
nulos; es decir, es de la forma

    a(1,1)*x(1)                                               = b(1)
    a(2,1)*x(1) + a(2,2)*x(2)                                 = b(2)
    a(3,1)*x(1) + a(3,2)*x(2) + a(3,3)*x(3)                   = b(3)
    ...
    a(n,1)*x(1) + a(n,2)*x(2) + a(n,3)*x(3) +...+ a(x,x)*x(n) = b(n)

El sistema es compatible si, y sólo si, el producto de los elementos de la
diagonal principal es distinto de cero. En este caso, la solución se puede
calcular mediante el algoritmo de bajada:

    x(1) = b(1) / a(1,1)
    x(2) = (b(2) - a(2,1)*x(1)) / a(2,2)
    x(3) = (b(3) - a(3,1)*x(1) - a(3,2)*x(2)) / a(3,3)
    ...
    x(n) = (b(n) - a(n,1)*x(1) - a(n,2)*x(2) -...- a(n,n-1)*x(n-1)) / a(n,n)

Definir la función

    bajada :: Matrix Double -> Matrix Double -> Matrix Double

tal que (bajada a b) es la solución, mediante el algoritmo de bajada, del
sistema compatible triangular superior ax = b. Por ejemplo,

    ghci> let a = fromLists [[2,0,0],[3,1,0],[4,2,5.0]]
    ghci> let b = fromLists [[3],[6.5],[10]]
    ghci> bajada a b
    ( 1.5 )
    ( 2.0 )
    ( 0.0 )

Es decir, la solución del sistema

    2x            = 3
    3x + y        = 6.5
    4x + 2y + 5 z = 10

es x=1.5, y=2 y z=0.

-}

import Data.Matrix

-- aplicando la fórmula recursiva
bajada :: Matrix Double -> Matrix Double -> Matrix Double
bajada a b = matrix m 1 f
    where m = nrows a
          f (i,_) = (b!(i,1) - sum [ a!(i,k) * f(k,1) | k <- [1..i-1]]) / a!(i,i)

-- por reducción de matrices
bajada2 :: Matrix Double -> Matrix Double -> Matrix Double
bajada2 a b = fromList (nrows a) 1 $ reduccion a (toList b)


reduccion :: Matrix Double -> [Double] -> [Double]
reduccion _ []     = []
reduccion p (x:xs) = r : reduccion (minorMatrix 1 1 p) xs'
    where
        r = x / p!(1,1)
        xs' = [ t - r*p!(i,1) | (i,t) <- zip [2..] xs ]



test :: IO ()
test = let a = fromLists [[2,0,0],[3,1,0],[4,2,5.0]]
           b = fromLists [[3],[6.5],[10]]
       in print $ bajada2 a b
