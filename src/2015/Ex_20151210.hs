module Ex_20151210 where

{-

Primos cubanos
==============

Un primo cubano es un número primo que se puede escribir como diferencia de dos 
cubos consecutivos. Por ejemplo, el 61 es un primo cubano porque es primo y 
61 = 5³-4³.

Definir la sucesión

   cubanos :: [Integer]

tal que sus elementos son los números cubanos. Por ejemplo,

   λ> take 15 cubanos
   [7,19,37,61,127,271,331,397,547,631,919,1657,1801,1951,2269]

-}

import Data.Numbers.Primes (primes, isPrime)

cubanos :: [Integer]
cubanos = filter isPrime $ zipWith (-) (tail xs) xs
    where xs = map (^3) [1..]


{-
-- Falta instalar el módulo
import Data.List.Ordered

cubanos2 :: [Integer]
cubanos2 = isect primes [(i + 1) * 3 * i + 1 | i <- [1..]]
-}