module Ex_20151027 where

{-

Lista con repeticiones
======================

Definir la función

   tieneRepeticiones :: Eq a => [a] -> Bool

tal que (tieneRepeticiones xs) se verifica si xs tiene algún elemento repetido. Por ejemplo,

   tieneRepeticiones [3,2,5,2,7]          ==  True
   tieneRepeticiones [3,2,5,4,7]          ==  False
   tieneRepeticiones (5:[1..2000000000])  ==  True
   tieneRepeticiones [1..20000]           ==  False
   
-}


tieneRepeticiones :: Eq a => [a] -> Bool
tieneRepeticiones [] = False
tieneRepeticiones (x:xs) = x `elem` xs || tieneRepeticiones xs

