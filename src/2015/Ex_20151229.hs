module Ex_20151229 where

{-

Reconocimiento de anterior
==========================

Definir la función

   esAnterior :: Eq a => [a] -> a -> a -> Bool

tal que (esAnterior xs y z) se verifica si `y` ocurre en xs antes que `z` (que 
puede no pertenecer a xs). Por ejemplo,

   esAnterior [1,3,7,2] 3 2  ==  True
   esAnterior [1,3,7,2] 3 1  ==  False
   esAnterior [1,3,7,2] 3 5  ==  True
   esAnterior [1,3,7,2] 5 3  ==  False

-}

esAnterior :: Eq a => [a] -> a -> a -> Bool
esAnterior xs y z = y `elem` takeWhile (/=z) xs
