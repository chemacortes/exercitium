module Ex_20151116 where

{-

Dígitos visibles y ocultos
==========================

Una cadena clave es una cadena que contiene dígitos visibles y ocultos. Los 
dígitos se ocultan mediante las primeras letras minúsculas: la 'a' oculta el 
'0', la 'b' el '1' y así sucesivamente hasta la 'j' que oculta el '9'. Los 
restantes símbolos de la cadena no tienen significado y se pueden ignorar.

Definir la función

   numeroOculto :: String -> Maybe Integer

tal que (numeroOculto cs) es justo el número formado por los dígitos visibles 
u ocultos de la cadena clave cs, si cs tiene dígitos y Nothing en caso 
contrario. Por ejemplo,

   numeroOculto "jihgfedcba"            ==  Just 9876543210
   numeroOculto "JIHGFEDCBA"            ==  Nothing
   numeroOculto "el 23 de Enero"        ==  Just 423344
   numeroOculto "El 23 de Enero"        ==  Just 23344
   numeroOculto "El 23 de enero"        ==  Just 233444
   numeroOculto "Todo para nada"        ==  Just 300030
   numeroOculto (replicate (10^6) 'A')  ==  Nothing

-}

import Data.Maybe (mapMaybe)
import Text.Read  (readMaybe)
import Data.Char (isDigit, ord, chr)

numeroOculto :: String -> Maybe Integer
numeroOculto cs = readMaybe $ mapMaybe (`lookup` zs) cs
    where zs = zip "0123456789abcdefghij"
                   "01234567890123456789"


-- Solución "oficial"
numeroOculto2 :: String -> Maybe Integer
numeroOculto2 cs | null aux  = Nothing
                | otherwise = Just (read aux)
    where aux = filter isDigit (map visible cs)
 
visible :: Char -> Char
visible c | c `elem` ['a'..'j'] = chr (ord c - n)
          | otherwise           = c
          where n = ord 'a' - ord '0'