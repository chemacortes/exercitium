module Ex_20151223 where

{-

Siembra de listas
=================

Definir la función

   siembra :: [Int] -> [Int]

tal que (siembra xs) es la lista ys obtenida al repartir cada elemento x de la 
lista xs poniendo un 1 en las x siguientes posiciones de la lista ys. Por 
ejemplo,

   siembra [4]      ==  [0,1,1,1,1] 
   siembra [0,2]    ==  [0,0,1,1]
   siembra [4,2]    ==  [0,1,2,2,1]

El tercer ejemplo se obtiene sumando la siembra de 4 en la posición 0 (como el 
ejemplo 1) y el 2 en la posición 1 (como el ejemplo 2). Otros ejemplos son

   siembra [0,4,2]          ==  [0,0,1,2,2,1]
   siembra [3]              ==  [0,1,1,1]
   siembra [3,4,2]          ==  [0,1,2,3,2,1]
   siembra [3,2,1]          ==  [0,1,2,3]
   sum $ siembra [1..2500]  ==  3126250

Comprobar con QuickCheck que la suma de los elementos de (siembra xs) es igual 
que la suma de los de xs.

Nota 1: Se supone que el argumento es una lista de números no negativos y que 
se puede ampliar tanto como sea necesario para repartir los elementos.

Nota 2: Este ejercicio es parte del examen del grupo 3 del 2 de diciembre.

-}

import Test.QuickCheck

siembra1 :: [Int] -> [Int]
siembra1 []      = []
--siembra (0:xs)  = 0 : siembra xs
siembra1 (x:xs)  = 0 : zipWith (+) (ys++repeat 0) zs
    where ys = siembra1 xs
          m = max x (length ys)
          zs = replicate x 1 ++ replicate (m - x) 0


siembra2 :: [Int] -> [Int]
siembra2 []      = []
siembra2 (x:xs)  = 0 : map (+1) ys ++ ws ++ replicate (x-m) 1
    where zs = siembra2 xs
          m = min x (length zs)
          (ys,ws) = splitAt m zs

siembra :: [Int] -> [Int]
siembra []  = []
siembra (x:xs) = 0 : suma (replicate x 1) (siembra xs)

suma :: [Int] -> [Int] -> [Int]
suma xs ys  = zipWith (+) xs ys ++ drop m xs ++ drop m ys
    where m = min (length xs) (length ys)

prop_siembra :: [Int] -> Bool
prop_siembra xs = sum xs' == (sum . siembra) xs'
    where xs' = map abs xs


-- Solución "oficial"
-- ==================

siembra4 :: [Int] -> [Int]
siembra4 = suma4 . brotes
 
-- (brotes xs) es la lista de los brotes obtenido sembrando los
-- elementos de xs. Por ejemplo,
--    brotes [3,4,2]  ==  [[0,1,1,1],[0,0,1,1,1,1],[0,0,0,1,1]]
brotes :: [Int] -> [[Int]]
brotes xs = aux xs 1
    where aux (x:xs) n = (replicate n 0 ++ replicate x 1) : aux xs (n+1)
          aux _ _      = []
 
-- (suma xss) es la suma de los elementos de xss (suponiendo que al
-- final de cada elemento se continua con ceros). Por ejemplo,
--    suma [[0,1,1,1],[0,0,1,1,1,1],[0,0,0,1,1]]  ==  [0,1,2,3,2,1]
suma4 :: [[Int]] -> [Int]
suma4 = foldr1 aux
    where aux [] ys = ys
          aux xs [] = xs
          aux (x:xs) (y:ys) = (x+y) : aux xs ys
