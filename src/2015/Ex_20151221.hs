module Ex_20151221 where

{-

Listas hermanadas
=================

Una lista hermanada es una lista de números estrictamente positivos en la que 
cada elemento tiene algún factor primo en común con el siguiente, en caso de 
que exista, o alguno de los dos es un 1. Por ejemplo,

    [2,6,3,9,1,5] es una lista hermanada pues 2 y 6 tienen un factor en común 
    (2); 6 y 3 tienen un factor en común (3); 3 y 9 tienen un factor en común 
    (3); de 9 y 1 uno es el número 1; y de 1 y 5 uno es el número 1.
    [2,3,5] no es una lista hermanada pues 2 y 3 no tienen ningún factor primo 
    en común.

Definir la función

   hermanada :: [Int] -> Bool

tal que (hermanada xs) se verifica si la lista xs es hermanada según la 
definición anterior. Por ejemplo,

   hermanada [2,6,3,9,1,5]   ==  True
   hermanada [2,3,5]         ==  False
   hermanada [2,4..1000000]  ==  True

Nota: Este ejercicio es parte del examen del grupo 3 del 2 de diciembre.

-}

import Data.Numbers.Primes
import Data.List

hermanada1 :: [Int] -> Bool
hermanada1 xs = and $ zipWith factorComun xs (tail xs)

factorComun :: Int -> Int -> Bool
factorComun 1 _ = True
factorComun _ 1 = True
factorComun x y = or [ x `mod` n == 0 && y `mod` n == 0 | n <- takeWhile (<= min x y) primes]

hermanada2 :: [Int] -> Bool
hermanada2 xs = and [ (not.null) (primeFactors x `intersect` primeFactors y)
                     | (x,y) <- zip xs (tail xs)
                     , x > 1
                     , y > 1 ]

hermanada3 :: [Int] -> Bool
hermanada3 xs = and [ (not.null) (ys `intersect` zs)
                     | (ys,zs) <- zip xs' (tail xs')
                     , (not.null) ys
                     , (not.null) zs ]
    where xs' = map primeFactors xs
