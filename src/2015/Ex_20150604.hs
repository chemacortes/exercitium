module Ex_20150604 where

{-
Descomposiciones en sumas de primos
===================================

Definir la función

   sumaDePrimos :: Int -> [[Int]]

tal que (sumaDePrimos x) es la lista de las listas no crecientes de números
primos que suman x. Por ejemplo,

   sumaDePrimos 10  ==  [[7,3],[5,5],[5,3,2],[3,3,2,2],[2,2,2,2,2]]
   sumaDePrimos 0   ==  []
   sumaDePrimos 1   ==  []
-}

import Data.Numbers.Primes

sumaDePrimos :: Int -> [[Int]]
sumaDePrimos n = aux n selPrimos
    where
      selPrimos = reverse $ takeWhile (n>=) primes
      aux 0 _       = []
      aux _ []      = []
      aux x (p:ps) | x < p     = aux x ps
                   | x == p    = [x] : aux x ps
                   | otherwise = [ p:xs | xs <- aux (x-p) (p:ps)] ++ aux x ps
