module Ex_20151021 where

{-

Refinamiento de listas
======================

Definir la función

   refinada :: [Float] -> [Float]

tal que (refinada xs) es la lista obtenida intercalando entre cada dos elementos 
consecutivos de xs su media aritmética. Por ejemplo,

   refinada [2,7,1,8]  ==  [2.0,4.5,7.0,4.0,1.0,4.5,8.0]
   refinada [2]        ==  [2.0]
   refinada []         ==  []

-}

refinada :: [Float] -> [Float]
refinada (x:y:xs) = x : (x + y)/2 :  refinada (y:xs)
refinada xs = xs


refinada2 :: [Float] -> [Float]
refinada2 [] =  []
refinada2 (x:xs) = x: concatMap (\(a,b) -> [(a+b)/2, b]) (zip (x:xs) xs)


refinada3 :: [Float] -> [Float]
refinada3 [] =  []
refinada3 (x:xs) = (x:) . concat $ zipWith (\a b -> [(a+b)/2, b]) (x:xs) xs


refinada4 :: [Float] -> [Float]
refinada4 [] =  []
refinada4 (x:xs) = x : concat [[(a+b)/2, b] | (a,b) <- zip (x:xs) xs]
