module Ex_20151030 where

{-

Factoriales iguales a su número de dígitos
==========================================

Se dice que un número n tiene un factorial especial si el número de dígitos de 
n! es igual a n. Por ejemplo, 22 tiene factorial especial porque 22! es 
1124000727777607680000 que tiene 22 dígitos.

Definir la función

   factorialesEspeciales :: [Integer]

tal que su valor es la lista de los números que tienen factoriales especiales. 
Por ejemplo,

   take 2 factorialesEspeciales  ==  [1,22]

Nota: Si factorialesEspeciales es una lista finita, argumentar porqué no puede 
tener más elementos.

-}

import Data.List (genericLength)

factorialesEspeciales :: [Integer]
factorialesEspeciales = [ n | n <- [1..]
                            , n == (genericLength . show $ factorial n) ]

factorial :: Integer -> Integer
factorial n = product [1..n]