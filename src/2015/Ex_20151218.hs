module Ex_20151218 where

{-

Potencias perfectas
===================

Un número natural n es una potencia perfecta si existen dos números naturales 
m > 1 y k > 1 tales que n = m^k. Las primeras potencias perfectas son

   4 = 2², 8 = 2³, 9 = 3², 16 = 2⁴, 25 = 5², 27 = 3³, 32 = 2⁵, 
   36 = 6², 49 = 7², 64 = 2⁶, ...

Definir la sucesión

   potenciasPerfectas :: [Integer]

cuyos términos son las potencias perfectas. Por ejemplo,

   take 10 potenciasPerfectas  ==  [4,8,9,16,25,27,32,36,49,64]
   potenciasPerfectas !! 100   ==  6724

Definir el procedimiento

   grafica :: Int -> IO ()

tal que (grafica n) es la representación gráfica de las n primeras potencias 
perfectas. Por ejemplo, para (grafica 30) dibuja

[[Ex_20151218-Potencias_perfectas.png]]

-}

import Data.List (insert)
import Graphics.Gnuplot.Simple

potenciasPerfectas :: [Integer]
potenciasPerfectas = ordena potencias

potencias :: [[Integer]]
potencias = [[n^k | n <- [2..]] | k <- [2..]]

ordena :: Ord a => [[a]] -> [a]
ordena ((x:xs):xss) = x : mezcla xs (ordena xss)

mezcla :: Ord a => [a] -> [a] -> [a]
mezcla (x:xs) (y:ys) | x == y =     mezcla xs (y:ys)
                     | x < y  = x : mezcla xs (y:ys)
                     | x > y  = y : mezcla (x:xs) ys


potenciasPerfectas2 :: [Integer]
potenciasPerfectas2 = concatOrd potencias

concatOrd :: Ord a => [[a]] -> [a]
concatOrd ((x:xs):(y:ys):zss) | x == y      =    concatOrd ((x:xs): insert ys zss)
                              | x < y       = x: concatOrd (insert xs ((y:ys):zss))
                              | otherwise   = y: concatOrd (    xs: insert ys zss)


-- Representación gráfica
-- ======================
 
grafica :: Int -> IO ()
grafica n = 
    plotList [Key Nothing] (take n potenciasPerfectas)