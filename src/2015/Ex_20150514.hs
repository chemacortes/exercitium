module Ex_20150514 where

{-

Visibilidad de listas y matrices
================================

La visibilidad de una lista es el número de elementos que son estrictamente
mayores que todos los anteriores. Por ejemplo, la visibilidad de la lista
[1,2,5,2,3,6] es 4.

La visibilidad de una matriz P es el par formado por las visibilidades de las
filas de P y las visibilidades de las columnas de P. Por ejemplo, dada la matriz

       ( 4 2 1 )
   Q = ( 3 2 5 )
       ( 6 1 8 )

la visibilidad de Q es ([1,2,2],[2,1,3]).

Definir las funciones

   visibilidadLista  :: [Int] -> Int
   visibilidadMatriz :: Matrix Int -> ([Int],[Int])

tales que

    (visibilidadLista xs) es la visibilidad de la lista xs. Por ejemplo,

     visibilidadLista [1,2,5,2,3,6]   ==  4
     visibilidadLista [0,-2,5,1,6,6]  ==  3

    (visibilidadMatriz p) es la visibilidad de la matriz p. Por ejemplo,

     ghci> visibilidadMatriz (fromLists [[4,2,1],[3,2,5],[6,1,8]])
     ([1,2,2],[2,1,3])
     ghci> visibilidadMatriz (fromLists [[0,2,1],[0,2,5],[6,1,8]])
     ([2,3,2],[2,1,3])
-}

import           Data.List
import           Data.Matrix
import qualified Data.Vector as V (toList)


-- por comprensión
visibilidadLista  :: [Int] -> Int
visibilidadLista xs = length [ x | (ys,x) <- zip (inits xs) xs, all (<x) ys ]

-- definición recursiva
visibilidadLista2  :: [Int] -> Int
visibilidadLista2 []    = 0
visibilidadLista2 [_]   = 1
visibilidadLista2 (x:y:xs) | x < y     = 1 + visibilidadLista2 (y:xs)
                          | otherwise =      visibilidadLista2 (x:xs)

-- definición con función auxiliar
visibilidadLista3  :: [Int] -> Int
visibilidadLista3 []     = 0
visibilidadLista3 (x:xs) = 1 + aux xs x
    where aux [] _      = 0
          aux (y:ys) n
            | y > n     = 1 + aux ys y
            | otherwise = aux ys n


visibilidadMatriz :: Matrix Int -> ([Int],[Int])
visibilidadMatriz p =
    ( [ visibilidadLista $ V.toList (getRow i p) | i <- [1..nrows p] ],
      [ visibilidadLista $ V.toList (getCol j p) | j <- [1..ncols p] ]  )



