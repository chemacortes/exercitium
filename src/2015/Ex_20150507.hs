module Ex_20150507 where

{-

Múltiplos especiales
====================

Dado dos números n y m, decimos que m es un múltiplo especial de n si m es un
múltiplo de n y m no tiene ningún factor primo que sea congruente con 1 módulo 3.

Definir la función

   multiplosEspecialesCota :: Int -> Int -> [Int]

tal que (multiplosEspecialesCota n k) es la lista ordenada de todos los múltiplos
especiales de n que son menores o iguales que k. Por ejemplo,

   multiplosEspecialesCota 5 50  ==  [5,10,15,20,25,30,40,45,50]
   multiplosEspecialesCota 7 50  ==  []

-}

import Data.Numbers.Primes

multiplosEspecialesCota :: Int -> Int -> [Int]
multiplosEspecialesCota n k = [x | x <- [n,n*2..k], esEspecial x ]
     where
        esEspecial = all (\p -> p `mod` 3 /=1) . primeFactors