module Ex_20150428 where

{-
Colinealidad de una lista de puntos
===================================

Una colección de puntos son colineales si esxiste una línea recta tal que todos
están en dicha línea. Por ejemplo, los puntos (2,1), (5,7), (4,5) y (20,37) son
colineales porque pertenecen a la línea y = 2*x-3.

Definir la función

   colineales :: [(Int,Int)] -> Bool

tal que (colineales ps) se verifica si los puntos de la lista ps son colineales. Por ejemplo,

   colineales [(2,1),(5,7),(4,5),(20,37)]  ==  True
   colineales [(2,1),(5,7),(4,5),(21,37)]  ==  False

-}


colineales :: [(Int,Int)] -> Bool
colineales []       = False
colineales [_]      = False
colineales (x:y:xs) = all (testColineal x y) xs
    where
        testColineal (x0,y0) (x1,y1) (x2,y2) = (x1-x0) * (y2-y0) == (y1-y0) * (x2-x0)

