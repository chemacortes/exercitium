module Ex_20151225 where

{-

Inserciones por posición
========================

Definir la función

   inserta :: [a] -> [[a]] -> [[a]]

tal que (inserta xs yss) es la lista obtenida insertando

    el primer elemento de xs como primero en la primera lista de yss,
    el segundo elemento de xs como segundo en la segunda lista de yss (si la 
        segunda lista de yss tiene al menos un elemento),
    el tercer elemento de xs como tercero en la tercera lista de yss (si la 
        tercera lista de yss tiene al menos dos elementos),

y así sucesivamente. Por ejemplo,

   inserta [1,2,3] [[4,7],[6],[9,5,8]]  ==  [[1,4,7],[6,2],[9,5,3,8]]
   inserta [1,2,3] [[4,7],[] ,[9,5,8]]  ==  [[1,4,7],[],   [9,5,3,8]]
   inserta [1,2]   [[4,7],[6],[9,5,8]]  ==  [[1,4,7],[6,2],[9,5,8]]
   inserta [1,2,3] [[4,7],[6]]          ==  [[1,4,7],[6,2]]
   inserta [1,2,3] [[],[],[4]]          ==  [[1],[],[4]]
   inserta "tad"   ["odo","pra","naa"]  ==  ["todo","para","nada"]

Nota: Este ejercicio es parte del examen del grupo 2 del 4 de diciembre.

-}

inserta :: [a] -> [[a]] -> [[a]]
inserta xs yss =  [ ins n x ys | (n,x,ys) <- zip3 [0..] xs yss ]
               ++ drop (length xs) yss

ins :: Int -> a -> [a] -> [a]
ins n x ys | length ys < n  = ys
           | otherwise      = take n ys ++ x : drop n ys
