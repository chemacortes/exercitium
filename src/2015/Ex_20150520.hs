module Ex_20150520 where

{-
Parte par de un polinomio
=========================

La parte par de un polinomio de coeficientes enteros es el polinomio formado por
sus monomios cuyos coeficientes son números pares. Por ejemplo, la parte par de
4x^3+x^2-7x+6 es 4x^3+6.

Definir la función

   partePar :: Integral a => Polinomio a -> Polinomio a

tal que (partePar p) es la parte par de p. Por ejemplo,

   ghci> partePar (consPol 3 4 (consPol 2 1 (consPol 0 6 polCero)))
   4*x^3 + 6

Nota: Este ejercicio debe realizarse usando únicamente las funciones de la
librería I1M.Pol que se encuentra [aquí][1] y se describe [aquí][2].

[1]: http://www.cs.us.es/~jalonso/cursos/i1m/codigos/I1M2014.zip
[2]: http://www.cs.us.es/~jalonso/cursos/i1m/doc/Tipos_abstractos_de_datos.html#el-tad-de-los-polinomios-i1m.pol

-}

import           I1M.Pol

partePar :: Integral a => Polinomio a -> Polinomio a
partePar p
    | esPolCero p   = polCero
    | even b        = consPol n b $ (partePar . restoPol) p
    | otherwise     =               (partePar . restoPol) p
    where
        (n, b) = (grado p, coefLider p)

