module Ex_20151224 where

{-

Árbol de Navidad
================

Definir el procedimiento

   arbol :: Int -> IO ()

tal que (arbol n) dibuja el árbol de Navidad con una copa de altura n y un 
tronco de altura la mitad de n. Por ejemplo,

   λ> arbol 5
 
        X
       XXX
      XXXXX
     XXXXXXX
    XXXXXXXXX
        X
        X
 
   λ> arbol 6
 
         X
        XXX
       XXXXX
      XXXXXXX
     XXXXXXXXX
    XXXXXXXXXXX
         X
         X
         X
 
   λ> arbol 7
 
          X
         XXX
        XXXXX
       XXXXXXX
      XXXXXXXXX
     XXXXXXXXXXX
    XXXXXXXXXXXXX
          X
          X
          X

-}

arbol :: Int -> IO ()
arbol n = mapM_ putStrLn s 
    where s =  [replicate (n-i) ' ' ++ replicate (2*i-1) 'X' | i <- [1..n]]
            ++ replicate (n `div` 2) (replicate (n-1) ' ' ++ "X")
