module Ex_20150611 where

{-

Método de bisección para encontrar raíces de funciones
======================================================

El método de bisección para calcular un cero de una función en el intervalo
[a,b] se basa en el teorema de Bolzano:

    Si f(x) es una función continua en el intervalo [a, b], y si, además, en
    los extremos del intervalo la función f(x) toma valores de signo opuesto
    (f(a) * f(b) < 0), entonces existe al menos un valor c en (a, b) para el
    que f(c) = 0

El método para calcular un cero de la función f en el intervalo [a,b] con un
error menor que e consiste en tomar el punto medio del intervalo c = (a+b)/2 y
considerar los siguientes casos:

    Si |f(c)| < e, hemos encontrado una aproximación del punto que anula f en
      el intervalo con un error aceptable.
    Si f(c) tiene signo distinto de f(a), repetir el proceso en el intervalo [a,c].
    Si no, repetir el proceso en el intervalo [c,b].

Definir la función

   biseccion :: (Double -> Double) -> Double -> Double -> Double -> Double

tal que (biseccion f a b e) es una aproximación del punto del intervalo [a,b]
en el que se anula la función f, con un error menor que e, calculada mediante
el método de la bisección. Por ejemplo,

   biseccion (\x -> x^2 - 3) 0 5 0.01             ==  1.7333984375
   biseccion (\x -> x^3 - x - 2) 0 4 0.01         ==  1.521484375
   biseccion cos 0 2 0.01                         ==  1.5625
   biseccion (\x -> log (50-x) - 4) (-10) 3 0.01  ==  -5.125
-}

biseccion :: (Double -> Double) -> Double -> Double -> Double -> Double
biseccion f a b e
    | abs (f c) < e                 = c
    | signum (f a) /= signum (f c)  = biseccion f a c e
    | otherwise                     = biseccion f c b e
    where c = (a+b)/2