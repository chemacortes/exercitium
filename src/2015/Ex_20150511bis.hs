module Ex_20150511bis where

{-
Representación decimal de números racionales
============================================

Continuando con los ejercicios propuestos por los alumnos, Antonio García
Blázquez ha propuesto un ejercicio que usa el período de los números decimales.
El ejercicio de hoy, basado en su propuesta, trata de la representación decimal
de los números racionales.

Los números decimales se representan por ternas, donde el primer elemento es la
parte entera, el segundo es el anteperíodo y el tercero es el período. Por ejemplo,

    6/2  = 3                  se representa por (3,[],[])
    1/2  = 0.5                se representa por (0,[5],[])
    1/3  = 0.333333...        se representa por (0,[],[3])
   23/14 = 1.6428571428571... se representa por (1,[6],[4,2,8,5,7,1])

Su tipo es

   type Decimal = (Integer,[Integer],[Integer])

Los números racionales se representan por un par de enteros, donde el primer
elemento es el numerador y el segundo el denominador. Por ejemplo, el número 2/3
se representa por (2,3). Su tipo es

   type Racional = (Integer,Integer)

Definir las funciones

   decimal  :: Racional -> Decimal
   racional :: Decimal -> Racional

tales que

    (decimal r) es la representación decimal del número racional r. Por ejemplo,

        decimal (1,4)    ==  (0,[2,5],[])
        decimal (1,3)    ==  (0,[],[3])
        decimal (23,14)  ==  (1,[6],[4,2,8,5,7,1])

    (racional d) es el número racional cuya representación decimal es d. Por ejemplo,

        racional (0,[2,5],[])           ==  (1,4)
        racional (0,[],[3])             ==  (1,3)
        racional (1,[6],[4,2,8,5,7,1])  ==  (23,14)

Con la función decimal se puede calcular los períodos de los números racionales.
Por ejemplo,

   ghci> let (_,_,p) = decimal (23,14) in concatMap show p
   "428571"
   ghci> let (_,_,p) = decimal (1,47) in concatMap show p
   "0212765957446808510638297872340425531914893617"
   ghci> let (_,_,p) = decimal (1,541) in length (concatMap show p)
   540

Comprobar con QuickCheck si las funciones decimal y racional son inversas.

-}

import           Test.QuickCheck
import           Data.List

type Decimal = (Integer,[Integer],[Integer])
type Racional = (Integer,Integer)

-- simplifica fracciones
reduce :: (Integer, Integer) -> (Integer, Integer)
reduce (a,b) = (a `div` m, b `div` m) where m = gcd a b

-----------------------------------------------------------------------

-- secuencia de dígitos de un número fraccionario
--  si x == 0               --> fin de secuencia (fracción exacta)
--  si gcd (10*x) y /= 1    --> periódico mixto
--  si gcd (10*x) y == 1    --> periódico puro
--  el flag booleano separa anteperiodo y periodo
digitos :: Integer -> Integer -> [(Integer,Integer,Bool)]
digitos x y = (d, x', y /= y') : digitos x' y'
    where
        (xp,y') = reduce (10*x, y)
        (d, x') = divMod xp y'


decimal :: Racional -> Decimal
decimal (0, _)  = (0, [], [])
decimal (x, y)  = (e, anteperiodo, periodo)
    where
        (x', y') = reduce (x, y)
        (e,xe) = divMod x' y'

        -- separación en dos partes
        (p1,p2) = span (\(_,_,g) -> g) (digitos xe y')

        -- obtención del anteperiodo
        anteperiodo = [ q | (q,_,_) <- p1]

        -- obtención del periodo
        (d,xd,_) = head p2
        rango = takeWhile (\(_,xq,_) -> xq /= xd) (tail p2)
        periodo = if xd == 0 then []        -- no hay periodo
                  else d : [q | (q, _, _) <- rango]

--------------------------------------------------------------------------------

-- numerador: la diferencia entre la parte anterior al período seguida del
--            período menos la parte anterior al período.
-- denominador: tantos 9 como cifras tiene el período, seguidos de tantos 0
--              como cifras tiene la parte no periódica.
racional :: Decimal -> Racional
racional (e, xs, []) = reduce (junta (e:xs), 10^n) where n = length xs    -- fracional exacto
racional (e, xs, ps) = reduce (num, den)
    where
        num = junta (e:xs ++ ps) - junta (e:xs)
        den = junta (replicate (length ps) 9 ++ replicate (length xs) 0)

-- concatena los dígitos
junta :: [Integer] -> Integer
junta = read . concatMap show

--------------------------------------------------------------------------------

genRacional :: Gen Racional
genRacional = do a <- arbitrary
                 b <- arbitrary
                 return (reduce (abs a, 1 + abs b))

prop_racionalDecimal :: Property
prop_racionalDecimal = forAll genRacional
                            (\r -> (racional . decimal) r == r )


genDecimal :: Gen Decimal
genDecimal = do a <- arbitrary
                b <- arbitrary
                return (decimal (abs a, 1 + abs b))

prop_decimalRacional :: Property
prop_decimalRacional = forAll genDecimal
    (\d -> (decimal . racional) d == d)

-- En la anterior propiedad, se hace trampas. Se usa el constructor
-- "decimal" para generarl el número racional. Siendo estrictos, habría que
-- crear otro generador
prop_decimalRacional2 :: Decimal -> Property
prop_decimalRacional2 (e, xs, ps) =
        and [ null ps' || last ps' /= 0,
              (not.null) ps' || null xs' || last xs' /= 0,
              (not.null) ps' && (not.null) xs' && last ps' /= last xs',
              not (xs' `isInfixOf` ps'),
              not (ps' `isInfixOf` xs'),
              ps' /= reverse ps' ]
        ==> (decimal . racional) d == d
    where
        d = (abs e,map ((`mod`10).abs) xs,map ((`mod`10).abs) ps)
        (_,xs',ps') = d

runTest :: IO ()
runTest = do quickCheck prop_racionalDecimal
             quickCheck prop_decimalRacional

