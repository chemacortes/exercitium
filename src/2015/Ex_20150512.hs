module Ex_20150512 where

{-
El teorema de Midy
==================

El ejercicio de hoy, propuesto por Antonio García Blázquez, tiene como objetivo
comprobar la veracidad del Teorema de Midy, este teorema dice:

> Sea a/p una fracción, donde a < p y p > 5 es un número primo. Si esta
> fracción tiene una expansión decimal periódica, donde la cantidad de dígitos
> en el período es par, entonces podemos partir el período en dos mitades, cuya
> suma es un número formado únicamente por nueves.
>
> Por ejemplo, 2/7 = 0’285714285714… El período es 285714, cuya longitud es par
> (6). Lo partimos por la mitad y las sumamos: 285+714 = 999.

Definir la función

   teoremaMidy :: Integer -> Bool

tal que (teoremaMidy n) se verifica si para todo todo número primo p menor que
n y mayor que 5 y todo número natural a menor que p tales que la cantidad de
dígitos en el período de a/p es par, entonces podemos partir el período de a/p
en dos mitades, cuya suma es un número formado únicamente por nueves. Por ejemplo,

   teoremaMidy 200  ==  True

Además, comprobar el teorema de Midy usando QuickCheck.
-}

import Test.QuickCheck
import Data.Numbers.Primes

-- del ejercicio anterior
import Ex_20150511bis (Decimal, decimal)

teoremaMidy :: Integer -> Bool
teoremaMidy n = and [ comprobacionMidy d | p <- takeWhile (<n) primes,
                                           p > 5,
                                           a <- [1..p-1],
                                           let d = decimal (a,p),
                                           condicionMidy d ]

-- condición para comprobar el teorema
condicionMidy :: Decimal -> Bool
condicionMidy (_,_,ps) = (not.null) ps && (even.length) ps

-- comprobar el teorema
comprobacionMidy :: Decimal -> Bool
comprobacionMidy (_,_,ps) = all (==9) (zipWith (+) xs ys)
                            where (xs,ys) = splitAt (length ps `div` 2) ps

-------------------------------------------------------------------------------

-- primos mayores de 5 y menores de 100000 (por poner un límite)
genSmallPrime :: Gen Integer
genSmallPrime = elements (takeWhile (< 100000) primes) `suchThat` (> 5)

genDecimalMidy :: Gen Decimal
genDecimalMidy = do p <- genSmallPrime
                    a <- choose (1,p-1)
                    return (decimal (a,p))
                 `suchThat` condicionMidy

prop_Midy :: Property
prop_Midy = forAll genDecimalMidy comprobacionMidy

