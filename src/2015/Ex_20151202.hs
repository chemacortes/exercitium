module Ex_20151202 where

{-

Raíces enteras de los números primos
====================================

Definir la sucesión

   raicesEnterasDePrimos :: [Integer]

cuyos elementos son las partes enteras de las raíces cuadradas de los números 
primos. Por ejemplo,

   λ> take 30 raicesEnterasDePrimos
   [1,1,2,2,3,3,4,4,4,5,5,6,6,6,6,7,7,7,8,8,8,8,9,9,9,10,10,10,10,10]
   λ> raicesEnterasDePrimos !!  9963
   322
   λ> raicesEnterasDePrimos !!  9964
   323

Comprobar con QuickCheck que la diferencia entre dos términos consecutivos de 
la sucesión es como máximo igual a 1.

-}

import Data.Numbers.Primes
import Test.QuickCheck

raicesEnterasDePrimos1 :: [Integer]
raicesEnterasDePrimos1 = [ (floor . sqrt . fromIntegral) n | n <- primes ]

raicesEnterasDePrimos :: [Integer]
raicesEnterasDePrimos = aux primes [1..]
    where aux (p:ps) (x:xs) | p > x*x   = aux (p:ps) xs
                            | otherwise = (x-1) : aux ps (x:xs)

raicesEnterasDePrimos2 :: [Integer]
raicesEnterasDePrimos2 = map raizEntera2 primes
 
raizEntera2 :: Integer -> Integer
raizEntera2 n = aux 1
    where aux k | k*k > n   = k-1
                | otherwise = aux (k+1)


prop_dif :: Int -> Property
prop_dif n = n > 0 ==>
    abs(raicesEnterasDePrimos!!n - raicesEnterasDePrimos!!(n+1)) <= 1
 