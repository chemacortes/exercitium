module Ex_20151230 where

{-

Elementos maximales
===================

Definir la función

   maximales :: Eq a => (a -> a -> Bool) -> [a] -> [a]

tal que (maximales r xs) es la lista de los elementos de xs para los que no 
hay ningún otro elemento de xs mayor según la relación r. Por ejemplo,

   maximales (>) [2,3,4,6]                     ==  [6]
   maximales (<) [2,3,4,6]                     ==  [2]
   maximales (\x y -> mod x y == 0) [2,3,4,6]  ==  [4,6]
   maximales (\x y -> mod y x == 0) [2,3,4,6]  ==  [2,3]

-}

maximales1 :: Eq a => (a -> a -> Bool) -> [a] -> [a]
maximales1 r xs = [ x | x <- xs
                      , (not . any (`r` x)) (filter (/=x) xs) ]

                     
maximales :: Eq a => (a -> a -> Bool) -> [a] -> [a]
maximales r xs = [ x | x <- xs
                     , null [ y | y <- xs, y /= x, y `r` x ] ]
