module Ex_20150506 where

{-
Mínimo y máximo de un montículo
===============================

Definir la función

   minMax :: Ord a => Monticulo a -> Maybe (a,a)

tal que (minMax m) es justamente el par formado por el menor y el mayor elemento de m,
si el montículo m es no vacío. Por ejemplo,

   minMax (foldr inserta vacio [4,8,2,1,5])  ==  Just (1,8)
   minMax (foldr inserta vacio [4])          ==  Just (4,4)
   minMax vacio                              ==  Nothing

Nota: Este ejercicio debe realizarse usando únicamente las funciones de la librería de
montículo (I1M.Monticulo) que se describe [aquí](http://bit.ly/1R9qlpV) y se encuentra
[aquí](http://bit.ly/1AKmUQB].

-}

import I1M.Monticulo

minMax :: Ord a => Monticulo a -> Maybe (a,a)
minMax m | esVacio m     = Nothing
         | otherwise     = Just (menor m, mayor m)
            where mayor = menor . until (esVacio . resto) resto


-- definición por recursión
mayor2 :: Ord a => Monticulo a -> a
mayor2 m | (not.esVacio.resto) m = (mayor2.resto) m
         | otherwise             = menor m
