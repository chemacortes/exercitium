module Ex_20151211 where

{-

Primos con cubos
================

Un primo con cubo es un número primo p para el que existe algún entero positivo 
n tal que la expresión n³ + n²p es un cubo perfecto. Por ejemplo, 19 es un 
primo con cubo ya que 8³ + 8²×19 = 12³.

Definir la sucesión

   primosConCubos :: [Integer]

tal que sus elementos son los primos con cubo. Por ejemplo,

   λ> take 6 primosConCubos
   [7,19,37,61,127,271]
   λ> length (takeWhile (< 1000000) primosConCubos)
   173

-}

import Data.List
import Data.Numbers.Primes

primosConCubos1 :: [Integer]
primosConCubos1 = [ p | (n,x) <- [(i,i^3) | i<-[1..]]
                      , (p,r) <- [ (x-i^3) `divMod` (i^2) | i <- [1..n]]
                      , r == 0
                      , isPrime p]

esCuboPerfecto :: Integer -> Bool
esCuboPerfecto n = all (\x -> length x `mod` 3 == 0) (group $ primeFactors n)


{-

Demostración
------------

La expresión n^3+n^2*p se puede expresar como n*n*(n+p). Si tomamos n y n+p 
como coprimos, entonces la expresión será un cubo perfecto únicamente si tanto 
n como n+p lo son. Operando:

    n = x^3
    n+p = y^3
    
    p = y^3 - x^3 = (y-x) (y^2+x*y+x^2)

Como p es primo, sólo puede ser dividido por 1 y por sí mismo. El segundo 
término de la descomposición anterior no puede ser 1, por lo que se deduce 
que y-x = 1, o lo que es lo mismo, que p = (x+1)^3 - x^3, de donde sale la 
expresión usada en la solución: 3*x^2+3*x+1.

Queda el caso en que n y n+p no son coprimos. Es cuando podemos expresar 
n = m*p, llegando a que m^2*(m+1) debe ser un cubo perfecto. Como m y m+1 son 
coprimos, entonces m y m+1 son cubos perfectos, cosa que no es posible (no hay 
cubos perfectos que se distancien en una unidad).

-}

primosConCubos :: [Integer]
primosConCubos = filter isPrime [3*x^2+3*x+1 | x <- [1..]]

