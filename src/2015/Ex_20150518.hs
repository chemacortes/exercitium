module Ex_20150518 where

{-
Ciclos de un grafo
==================

Un ciclo en un grafo G es una secuencia [v(1),v(2),v(3),...,v(n)] de nodos de G
tal que:

(v(1),v(2)), (v(2),v(3)), (v(3),v(4)), …, (v(n-1),v(n)) son aristas de G,
v(1) = v(n), y
salvo v(1) = v(n), todos los v(i) son distintos entre sí.

Definir la función

   ciclos :: Grafo Int Int -> [[Int]]

tal que (ciclos g) es la lista de ciclos de g. Por ejemplo, si g1 y g2 son los
grafos definidos por

   g1, g2 :: Grafo Int Int
   g1 = creaGrafo D (1,4) [(1,2,0),(2,3,0),(2,4,0),(4,1,0)]
   g2 = creaGrafo D (1,4) [(1,2,0),(2,1,0),(2,4,0),(4,1,0)]

entonces

   ciclos g1  ==  [[1,2,4,1],[2,4,1,2],[4,1,2,4]]
   ciclos g2  ==  [[1,2,1],[1,2,4,1],[2,1,2],[2,4,1,2],[4,1,2,4]]

Nota: Este ejercicio debe realizarse usando únicamente las funciones de la
librería de grafos (I1M.Grafo) que se describe aquí y se encuentra aquí.

[1]: http://www.cs.us.es/~jalonso/cursos/i1m/doc/Tipos_abstractos_de_datos.html#el-tad-de-los-grafos-i1m.grafo
[2]: http://www.cs.us.es/~jalonso/cursos/i1m/codigos/I1M2014.zip

-}

import           I1M.Grafo

g1, g2 :: Grafo Int Int
g1 = creaGrafo D (1,4) [(1,2,0),(2,3,0),(2,4,0),(4,1,0)]
g2 = creaGrafo D (1,4) [(1,2,0),(2,1,0),(2,4,0),(4,1,0)]

ciclos :: Grafo Int Int -> [[Int]]
ciclos g = concat [ caminos a a [] | a <- nodos g ]
    where
        -- caminos posibles de b -> a, sin pasar dos veces por un mismo nodo
        caminos a b vs
            | a == b && (not.null) vs   = [[b]]
            | otherwise = [ b:xs | c <- adyacentes g b,
                                   c `notElem` vs,
                                   xs <- caminos a c (c:vs)]


ciclos2 :: Grafo Int Int -> [[Int]]
ciclos2 g = concat [aux [a] (adyacentes g a) | a <- nodos g]
    where aux _ [] = []
          aux xs (y:ys)
              | y `notElem` xs  = aux (xs ++ [y]) (adyacentes g y) ++
                                  aux xs ys
              | y == head xs    = (xs ++ [y]) : aux xs ys
              | otherwise       = aux xs ys







