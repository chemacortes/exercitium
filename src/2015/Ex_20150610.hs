module Ex_20150610 where

{-

Números para los que mcm(1,2,…n-1) = mcm(1,2,…,n)
=================================================

Un número n es especial si mcm(1,2,…,n-1) = mcm(1,2,…,n). Por ejemplo, el 6 es
especial ya que

   mcm(1,2,3,4,5) = 60 = mcm(1,2,3,4,5,6)

Definir la sucesión

   especiales :: [Integer]

cuyos términos son los números especiales. Por ejemplo,

   take 10 especiales     ==  [1,6,10,12,14,15,18,20,21,22]
   especiales !! 50       ==  84
   especiales !! 500      ==  638
   especiales !! 5000     ==  5806
   especiales !! 50000    ==  55746
-}

import Data.Maybe (catMaybes)
import Data.List (mapAccumL)

-- aplicando directamente la definición
especiales :: [Integer]
especiales = 1 : [ x | x <- [2..],
                       mcm [1..x-1] == mcm [1..x] ]
    where
        -- mínimo común múltiplo de un conjunto de enteros
        mcm = foldr1 lcm


-- definición recursiva
especiales2 :: [Integer]
especiales2 = aux 1 1
    where
        aux n m | m == m'   = n : aux (n+1) m'
                | otherwise =     aux (n+1) m'
            where m' = lcm n m


-- aplicando scanl
especiales5 :: [Integer]
especiales5 = [n | ((n,x),(_,y)) <- zip mcms (tail mcms)
                 , x == y]
    where
        mcms :: [(Integer,Integer)]
        mcms = zip [1..] (scanl1 lcm [1..])


-- aplicando 'catMaybes'
especiales3 :: [Integer]
especiales3 = (1:) . catMaybes
            $ zipWith3 aux [2..] multiplos (tail multiplos)
    where
        multiplos = scanl1 lcm [1..]
        aux a b c | c == b      = Just a
                  | otherwise   = Nothing

-- aplicando catMaybes con mapAccumL
especiales4 :: [Integer]
especiales4 = catMaybes . snd $ mapAccumL aux 1 [1..]
    where
        aux acc x | acc == acc' = (acc', Just x)
                  | otherwise   = (acc', Nothing)
            where acc' = lcm acc x




