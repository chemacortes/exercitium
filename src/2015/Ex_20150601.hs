module Ex_20150601 where

{-

Problema de las bolas de Dijkstra
=================================

En el juego de las bolas de Dijkstra se dispone de una bolsa con bolas blancas
y negras. El juego consiste en elegir al azar dos bolas de la bolsa y añadir
una bola negra si las dos bolas elegidas son del mismo color o una bola blanca
en caso contrario. El juego termina cuando queda sólo una bola en la bolsa.

Vamos a representar las bolas blancas por 0, las negras por 1 y la bolsa la
representaremos por una lista cuyos elementos son 0 ó 1.

Definir las funciones

   juego  :: [Int] -> [[Int]]
   ultima :: [Int] -> Int

tales que

    (juego xs) es la lista de los pasos aleatorios de un juego de Dijkstra a
    partir de la lista xs. Por ejemplo,

     juego [1,1,0,0,1]  ==  [[1,1,0,0,1],[1,1,0,0],[1,1,1],[1,1],[1]]
     juego [1,1,0,0,1]  ==  [[1,1,0,0,1],[0,1,1,0],[0,0,1],[1,1],[1]]
     juego [1,0,0,0,1]  ==  [[1,0,0,0,1],[0,0,0,1],[0,1,1],[1,0],[0]]
     juego [1,0,1,1,1]  ==  [[1,0,1,1,1],[1,1,0,1],[1,0,1],[0,1],[0]]

    (ultima xs) es la bola que queda en la bolsa al final del juego de Dijkstra
    a partir de xs. Por ejemplo,

     ultima [1,1,0,0,1]  ==  1
     ultima [1,0,0,0,1]  ==  0
     ultima [1,0,1,1,1]  ==  0

Comprobar con QuickCheck que la bola que queda en la bolsa al final del juego
de Dijkstra es blanca si, y sólo si, el número de bolas blancas en la bolsa
inicial es impar.
-}

import Test.QuickCheck  -- >= 2.8.1
import System.IO.Unsafe (unsafePerformIO)

-- tipo de dato para la bola
data Bola = Blanca|Negra deriving (Eq, Show)

-- generador aleatorio de bolas
instance Arbitrary Bola where
    arbitrary = elements [Blanca, Negra]

-- ejemplos para hacer pruebas
ej1, ej2, ej3 :: [Bola]
ej1 = [Negra, Negra,  Blanca, Blanca, Negra]
ej2 = [Negra, Blanca, Blanca, Blanca, Negra]
ej3 = [Negra, Blanca, Negra,  Negra,  Negra]


juego  :: [Bola] -> [[Bola]]
juego []    = []
juego [x]   = [[x]]
juego xs | x == y        = xs : juego (Negra:ys)
         | otherwise     = xs : juego (Blanca:ys)
    where
        x:y:ys = desordena xs

desordena :: [a] -> [a]
desordena = unsafePerformIO . generate . shuffle

ultima :: [Bola] -> Bola
ultima = head . last . juego

prop_dijkstra :: [Bola] -> Property
prop_dijkstra xs = (not.null) xs ==>
    (ultima xs == Blanca) == (odd . length . filter (Blanca==)) xs

runTests :: IO ()
runTests = quickCheck prop_dijkstra






