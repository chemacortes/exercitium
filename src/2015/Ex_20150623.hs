module Ex_20150623 where

{-

Partición por longitudes
========================

Definir la función

   particion :: [a] -> [Int] -> [[a]]

tal que (particion xs ns) es la partición de xs donde la longitud de cada parte está determinada por los elementos de ns. Por ejemplo,

   particion [1..10] [2,5,0,3]  ==  [[1,2],[3,4,5,6,7],[],[8,9,10]]
   particion [1..10] [1,4,2,3]  ==  [[1],[2,3,4,5],[6,7],[8,9,10]]

-}

import Control.Arrow (second, (>>>))


-- definición recursiva
particion :: [a] -> [Int] -> [[a]]
particion _ []      =  []
particion xs (n:ns) = take n xs : particion (drop n xs) ns


-- definición recursiva (usando splitAt)
particion2 :: [a] -> [Int] -> [[a]]
particion2 _ []      =  []
particion2 xs (n:ns) = xs1 : particion xs2 ns
    where (xs1, xs2) = splitAt n xs

-- definición usando Arrows
particion3 :: [a] -> [Int] -> [[a]]
particion3 xs ns = (f ns) xs
   where f []     =   const []
         f (r:rs) =   splitAt r
                  >>> second (f rs)
                  >>> uncurry (:)

