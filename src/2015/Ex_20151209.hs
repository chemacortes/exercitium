module Ex_20151209 where

{-

Clausura transitiva de una relación binaria
===========================================

La clausura transitiva de una relación binaria R es la menor relación 
transitiva que contiene a R. Se puede calcular usando la composición de 
relaciones. Veamos un ejemplo, en el que (R ∘ S) representa la composición de 
R y S (definida en el ejercicio del lunes): sea

   R = [(1,2),(2,5),(5,6)]

la relación R no es transitiva ya que (1,2) y (1,5) pertenecen a R pero (1,5) 
no pertenece; sea

   R1 = R ∪ (R ∘ R)
      = [(1,2),(2,5),(5,6),(1,5),(2,6)]

la relación R1 tampoco es transitiva ya que (1,2) y (2,6) pertenecen a R pero 
(1,6) no pertenece; sea

   R2 = R1 ∪ (R1 ∘ R1)
      = [(1,2),(2,5),(5,6),(1,5),(2,6),(1,6)]

La relación R2 es transitiva y contiene a R. Además, R2 es la clausura 
transitiva de R.

Definir la función

   clausuraTransitiva :: Eq a => Rel a -> Rel a

tal que (clausuraTransitiva r) es la clausura transitiva de r. Por ejemplo,

   λ> clausuraTransitiva [(1,2),(2,5),(5,6)]
   [(1,2),(2,5),(5,6),(1,5),(2,6),(1,6)]
   λ> clausuraTransitiva [(1,2),(2,5),(5,6),(6,3)]
   [(1,2),(2,5),(5,6),(6,3),(1,5),(2,6),(5,3),(1,6),(2,3),(1,3)]

-}

import Data.List (nub, union)

type Rel a = [(a,a)]

clausuraTransitiva :: Eq a => Rel a -> Rel a
clausuraTransitiva r | transitiva r     = r
                     | otherwise        = clausuraTransitiva
                                        $ nub (r ++ composicion r r)

clausuraTransitiva2 :: Eq a => [(a,a)] -> [(a,a)]
clausuraTransitiva2 r 
    | transitiva r = r
    | otherwise = clausuraTransitiva2 (union r (composicion r r))

-- Funciones definidas en los dos ejercicios anteriores.

composicion :: Eq a => Rel a -> Rel a -> Rel a
composicion r s = nub [(x,y) | (x,z) <- r, (w,y) <- s, z == w ]

transitiva :: Eq a => Rel a -> Bool
transitiva r = and [ (x,y) `elem` r | (x,a) <- r, (b,y) <- r, a == b ]
