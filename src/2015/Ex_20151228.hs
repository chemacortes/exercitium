module Ex_20151228 where

{-

Operación sobre todos los pares
===============================

Definir la función

   todosPares :: (a -> b -> c) -> [a] -> [b] -> [c]

tal que (todosPares f xs ys) es el resultado de aplicar la operación f a todos 
los pares de xs e ys. Por ejemplo,

   todosPares (*) [2,3,5] [7,11]            == [14,22,21,33,35,55]
   todosPares (\x y -> x:show y) "ab" [7,5] == ["a7","a5","b7","b5"]

-}

todosPares :: (a -> b -> c) -> [a] -> [b] -> [c]
todosPares f xs ys = [ f x y | x <- xs, y <- ys]
