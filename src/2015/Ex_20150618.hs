module Ex_20150618 where

{-

Menor n tal que el primo n-ésimo cumple una propiedad
=====================================================

Sea p(n) el n-ésimo primo y sea r el resto de dividir (p(n)-1)^n + (p(n)+1)^n
por p(n)^2. Por ejemplo,

   si n = 3, entonces p(3) =  5 y r = ( 4^3 +  6^3) mod  (5^2) =   5
   si n = 7, entonces p(7) = 17 y r = (16^7 + 18^7) mod (17^2) = 238

Definir la función

   menorPR :: Integer -> Integer

tal que (menorPR x) es el menor n tal que el resto de dividir
(p(n)-1)^n + (p(n)+1)^n por p(n)^2 es mayor que x.

Por ejemplo,

   menorPR 100     == 5
   menorPR 345     == 9
   menorPR 1000    == 13
   menorPR (10^9)  == 7037
   menorPR (10^10) == 21035
   menorPR (10^12) == 191041

-}

import Data.Numbers.Primes (primes)


menorPR :: Integer -> Integer
menorPR x = head [ i | (i, p) <- zip [1..] primes
                     , propiedad p i x]
    where
        propiedad p n x = ((p-1)^n + (p+1)^n) `mod` p^2 > x


{-

Aplicando la descomposición de (x-1)^n

$$\sum\limits_{k=0}^n \binom n k (-1)^{n-k} x^k$$

Todos los términos son divisibles por x^2 excepto los dos primeros términos:

$$\binom n 0 (-1)^n + \binom n 1 (-1)^{n-1} x$$

Con el mismo razonamiento, todos los términos de (x+1)^n son divisible por x^2
excepto:

$$\binom n 0 + \binom n 1 x$$

Considerando que x es primo, la propiedad buscada se cumple únicamente
cuando la suma de estos dos componentes sea divisible por x^2, o sea:

$$(1 + (-1)^n) + (1 + (-1)^{n-1}) nx \mod p^2 = 0$$

Tenemos dos casos, según sea `n` par o impar:

$$\begin{cases}
       2 \mod p^2  & \text{if even } n \\
       2 n x \mod p^2 & \text{if odd } n
     \end{cases}$$
     
Considerando que siempre será n > p, la última expresión será equivalente a:

$$p\ (2~n\mod p)$$


-}

menorPR2 :: Integer -> Integer
menorPR2 1 = 2
menorPR2 x = head [ i | (i, p) <- zip [1..] primes
                      , odd i
                      , propiedad p i]
    where
        propiedad p n = 2*n*p `mod` p^2 > x

menorPR3 :: Integer -> Integer
menorPR3 1 = 2
menorPR3 x = head [ n | (p, n) <- zip primes [1..]
                      , odd n
                      , propiedad p n ]
    where
        propiedad p n = p * (2*n `mod` p) > x





