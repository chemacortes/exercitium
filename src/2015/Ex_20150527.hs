module Ex_20150527 where

{-

Cálculo de aprobados
====================

La notas de un examen se pueden representar mediante un vector en el que los
valores son los pares formados por los nombres de los alumnos y sus notas.

Definir la función

   aprobados :: (Num a, Ord a) => Array Int (String,a) -> Maybe [String]

tal que (aprobados p) es la lista de los nombres de los alumnos que han
aprobado y Nothing si todos están suspensos. Por ejemplo,

   ghci> aprobados (listArray (1,3) [("Ana",5),("Pedro",3),("Lucia",6)])
   Just ["Ana","Lucia"]
   ghci> aprobados (listArray (1,3) [("Ana",4),("Pedro",3),("Lucia",4.9)])
   Nothing

-}

import Data.Array

aprobados :: (Num a, Ord a) => Array Int (String,a) -> Maybe [String]
aprobados p | null nombres  = Nothing
            | otherwise     = Just nombres
    where nombres = [nombre | (nombre, nota) <- elems p, nota >= 5]
