module Ex_20150427 where

{-

Distancia invierte y suma hasta capicúa
=======================================

Un número es capicúa si es igual leído de izquierda a derecha que de derecha a
izquierda; por ejemplo, el 4884.

El transformado “invierte y suma” de un número x es la suma de x y su número
invertido; es decir, el número resultante de la inversión del orden en el que
aparecen sus dígitos. Por ejemplo, el transformado de 124 es 124 + 421 = 545.

Se aplica la transformación “invierte y suma” hasta obtener un capicúa. Por
ejemplo, partiendo del número 87, el proceso es

     87 +   78 =  165
    165 +  561 =  726
    726 +  627 = 1353
   1353 + 3531 = 4884

El número de pasos de dicho proceso es la distancia capicúa del número; por
ejemplo, la distancia capicúa de 87 es 4.

Definir la función

   distanciaIS :: Integer -> Integer

tal que (distanciaIS x) es la distancia capicúa de x. Por ejemplo,

   distanciaIS 11                   ==    0
   distanciaIS 10                   ==    1
   distanciaIS 19                   ==    2
   distanciaIS 59                   ==    3
   distanciaIS 69                   ==    4
   distanciaIS 166                  ==    5
   distanciaIS 79                   ==    6
   distanciaIS 89                   ==   24
   distanciaIS 10911                ==   55
   distanciaIS 1000000079994144385  ==  259

-}

import           Data.List

esCapicua :: Integer -> Bool
esCapicua n = show n == (reverse . show) n

transformado :: Integer -> Integer
transformado n = n + (read . reverse . show) n

distanciaIS :: Integer -> Integer
distanciaIS n =
    genericLength $ takeWhile (not . esCapicua) (iterate transformado n)

-- por recursión
distanciaIS2 :: Integer -> Integer
distanciaIS2 = genericLength . cadena
    where cadena x
            | esCapicua x   = []
            | otherwise     = x : cadena (transformado x)










