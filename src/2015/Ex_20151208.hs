module Ex_20151208 where

{-

Transitividad de una relación
=============================

Una relación binaria R sobre un conjunto A es transitiva cuando se cumple que 
siempre que un elemento se relaciona con otro y éste último con un tercero, 
entonces el primero se relaciona con el

Definir la función

   transitiva :: Eq a => [(a,a)] -> Bool

tal que (transitiva r) se verifica si la relación r es transitiva. Por ejemplo,

   transitiva [(1,1),(1,3),(3,1),(3,3),(5,5)]  ==  True
   transitiva [(1,1),(1,3),(3,1),(5,5)]        ==  False

 
-}

transitiva :: Eq a => [(a,a)] -> Bool
transitiva r = and [ (x,y) `elem` r | (x,a) <- r, (b,y) <- r, a == b ]
