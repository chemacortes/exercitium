module Ex_20150617 where

{-

Polinomio cromático de un grafo
===============================

El polinomio cromático de un grafo calcula el número de maneras en las cuales
puede ser coloreado el grafo usando un número de colores dado, de forma que dos
vértices adyacentes no tengan el mismo color.

En el caso del grafo completo de n vértices, su polinomio cromático es

   P(n,x) = x(x-1)(x-2) ... (x-(n-1))

Por ejemplo,

   P(3,x) = x(x-1)(x-2)      = x^3 - 3*x^2 + 2*x
   P(4,x) = x(x-1)(x-2)(x-3) = x^4 - 6*x^3 + 11*x^2 - 6*x

Lo que significa que P(4)(x) es el número de formas de colorear el grafo
completo de 4 vértices con x colores. Por tanto,

   P(4,2) =  0 (no se puede colorear con 2 colores)
   P(4,4) = 24 (hay 24 formas de colorearlo con 4 colores)

Definir la función

   polGC:: Int -> Polinomio Int

tal que (polGC n) es el polinomio cromático del grafo completo de n vértices.
Por ejemplo,

   polGC 4  ==  x^4 + -6*x^3 + 11*x^2 + -6*x
   polGC 5  ==  x^5 + -10*x^4 + 35*x^3 + -50*x^2 + 24*x

Comprobar con QuickCheck que si el número de colores (x) coincide con el número
de vértices del grafo (n), el número de maneras de colorear el grafo es n!.

Nota 1. Al hacer la comprobación limitar el tamaño de las pruebas como se
indica a continuación

   ghci> quickCheckWith (stdArgs {maxSize=7}) prop_polGC
   +++ OK, passed 100 tests.

Nota 2: Este ejercicio debe realizarse usando únicamente las funciones de la
librería de polinomios (I1M.PolOperaciones) que se describe aquí[1] y se
encuentra aquí[2].

[1]: http://www.cs.us.es/~jalonso/cursos/i1m/doc/Tipos_abstractos_de_datos.html#operaciones-con-el-tad-de-polinomios-i1m.poloperaciones
[2]: http://www.cs.us.es/~jalonso/cursos/i1m/codigos/I1M2014.zip

-}

import Test.QuickCheck
import I1M.PolOperaciones

polGC:: Int -> Polinomio Int
polGC 0 = polUnidad
polGC n = consPol 1 1 (consPol 0 (1-n) polCero) `multPol` polGC (n-1)

polGC2:: Int -> Polinomio Int
polGC2 n = foldl multPol polUnidad
              [ consPol 1 1 (consPol 0 (-i) polCero) | i <- [0..n-1]]


prop_polGC :: Int -> Property
prop_polGC n = n > 0 ==>
    valor (polGC n) n == product [1..n]