module Ex_20150625 where

{-
Pandigitales primos
===================

Un número con n dígitos es pandigital si contiene todos los dígitos del 1 a n 
exactamente una vez. Por ejemplo, 2143 es un pandigital con 4 dígitos y, 
además, es primo.

Definir la constante

   pandigitalesPrimos :: [Int]

tal que sus elementos son los números pandigitales, ordenados de mayor a menor. 
Por ejemplo,

   take 3 pandigitalesPrimos       ==  [7652413,7642513,7641253]
   2143 `elem` pandigitalesPrimos  ==  True
   length pandigitalesPrimos       ==  538

-}

import Data.List
import Data.Numbers.Primes


-- Por fuerza bruta
pandigitalesPrimos :: [Int]
pandigitalesPrimos = sortBy (flip compare) $ filter pandigital primes'
    where primes' = takeWhile (<10^10) primes
          pandigital n = show n == (nub.show) n 
          
-- aplicando permutaciones
pandigitalesPrimos2 :: [Int]
pandigitalesPrimos2 = [ p | n <- [9,8..1]
                          , p <- pandigitales n
                          , isPrime p ]
                                                       
-- pandigitales de n cifras ordenados de mayor a menor
pandigitales :: Int -> [Int]
pandigitales n = map read . sortBy (flip compare) . permutations $ take n "123456789"

