module Ex_20150626 where

{-

Pandigitales tridivisibles
==========================

El número 4106357289 tiene la siguientes dos propiedades:

    es pandigital, porque tiene todos los dígitos del 0 al 9 exactamente una vez y
    es tridivisible, porque los sucesivos subnúmeros de tres dígitos (a partir
    del segundo) son divisibles por los sucesivos números primos; es decir,
    representado por d(i) el i-ésimo dígito, se tiene

     d(2)d(3)d(4)  = 106 es divisible por 2
     d(3)d(4)d(5)  = 063 es divisible por 3
     d(4)d(5)d(6)  = 635 es divisible por 5
     d(5)d(6)d(7)  = 357 es divisible por 7
     d(6)d(7)d(8)  = 572 es divisible por 11
     d(7)d(8)d(9)  = 728 es divisible por 13
     d(8)d(9)d(10) = 289 es divisible por 17

Definir la constante

   pandigitalesTridivisibles :: [Integer]

cuyos elementos son los números pandigitales tridivisibles. Por ejemplo,

   head pandigitalesTridivisibles  ==  4106357289
   sum pandigitalesTridivisibles   ==  16695334890

NOTA: hay un pequeño error en el enunciado. El primer número sería 1406357289

-}

import Data.List
import Data.Numbers.Primes

pandigitalesTridivisibles :: [Integer]
pandigitalesTridivisibles = [ read xs | xs <- pandigitales
                                      , tridivisible xs ]
    where pandigitales = permutations "0123456789"
          aux xs = [(read . take 3 .drop n) xs | n <- [1..7] ]
          tridivisible xs = and [ n `mod` p == 0 | (n,p) <- zip (aux xs) primes]

-- optimización aplicando reglas de divisibilidad sencillas para el 2 y el 5
pandigitalesTridivisibles2 :: [Integer]
pandigitalesTridivisibles2 = [ read xs | xs <- pandigitales'
                                       , tridivisible' xs ]
    where pandigitales = permutations "0123456789"
          pandigitales' = [xs | xs <- pandigitales
                              , xs!!3 `elem` "02468"
                              , xs!!5 `elem` "05" ]
          aux xs = [(read . take 3 .drop n) xs | n <- [1..7] ]
          tridivisible' xs = and [ n `mod` p == 0 | (n,p) <- zip (aux xs) primes
                                                 , p /= 2
                                                 , p /= 5 ]

