module Ex_20150612 where

{-

Mayor producto de n números adyacentes en una matriz
====================================================

Definir la función

   mayorProductoAdyacentes :: (Num a, Ord a) => Int -> Matrix a -> [[a]]

tal que (mayorProductoAdyacentes n p) es la lista de los segmentos formados por
n elementos adyacentes en la misma fila, columna o diagonal de la matriz p cuyo
productos son máximo. Por ejemplo,

   ghci> mayorProductoAdyacentes 3 (listArray ((1,1),(3,4)) [1..12])
   [[10,11,12]]
   ghci> mayorProductoAdyacentes 3 (listArray ((1,1),(3,4)) [1,3,4,5, 0,7,2,1, 3,9,2,1])
   [[3,7,9]]
   ghci> mayorProductoAdyacentes 2 (listArray ((1,1),(2,3)) [1,3,4, 0,3,2])
   [[3,4],[4,3]]
   ghci> mayorProductoAdyacentes 2 (listArray ((1,1),(2,3)) [1,2,1, 3,0,3])
   [[2,3],[2,3]]
   ghci> mayorProductoAdyacentes 2 (listArray ((1,1),(2,3)) [1,2,1, 3,4,3])
   [[3,4],[4,3]]
   ghci> mayorProductoAdyacentes 2 (listArray ((1,1),(2,3)) [1,5,1, 3,4,3])
   [[5,4]]
   ghci> mayorProductoAdyacentes 3 (listArray ((1,1),(3,4)) [1,3,4,5, 0,7,2,1, 3,9,2,1])
   [[3,7,9]]
-}

import Data.Array

type Matriz a = Array (Int, Int) a

mayorProductoAdyacentes :: (Num a, Ord a) => Int -> Matriz a -> [[a]]
mayorProductoAdyacentes n p = snd . foldr aux (0,[]) $ adyacentes n p
    where
        aux xs (maxProd,xss)
            | null xss        = (r, [xs])
            | r > maxProd     = (r, [xs])
            | r == maxProd    = (maxProd, xs:xss)
            | otherwise       = (maxProd, xss)
            where r = product xs


-- Lista de 'n' elementos adyacentes en horizontal, vertical y en diagonales
adyacentes :: (Num a, Ord a) => Int -> Matriz a -> [[a]]
adyacentes n p = concat [ f (i,j) | i <- [1..a], j <- [1..b] ]
    where
        ((_,_),(a,b)) = bounds p
        f (i,j) = filter ((n==).length) [
                            take n [p!(i,k) | k <- [j..b]],
                            take n [p!(k,j) | k <- [i..a]],
                            take n [p!(k,l) | (k,l) <- zip [i..a] [j..b]],
                            take n [p!(k,l) | (k,l) <- zip [i..a] [j,j-1..1]]
                        ]



