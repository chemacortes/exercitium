module Ex_20150429 where

{-
Agrupamiento según valores
==========================

Definir la función

   agrupa :: Ord c => (a -> c) -> [a] -> Map c [a]

tal que (agrupa f xs) es el diccionario obtenido agrupando los elementos de xs según
sus valores mediante la función f. Por ejemplo,

   ghci> agrupa length ["hoy", "ayer", "ana", "cosa"]
   fromList [(3,["hoy","ana"]),(4,["ayer","cosa"])]
   ghci> agrupa head ["claro", "ayer", "ana", "cosa"]
   fromList [('a',["ayer","ana"]),('c',["claro","cosa"])]
   ghci> agrupa length (words "suerte en el examen")
   fromList [(2,["en","el"]),(6,["suerte","examen"])]
-}

import Data.Map

agrupa :: Ord c => (a -> c) -> [a] -> Map c [a]
agrupa f = Prelude.foldr aux empty
    where aux x = insertWith (++) (f x) [x]

-- OJO: cambia el orden de los valores
agrupa2 :: Ord c => (a -> c) -> [a] -> Map c [a]
agrupa2 f xs = fromListWith (++) [(f x, [x]) | x <- xs]

agrupa3 :: Ord c => (a -> c) -> [a] -> Map c [a]
agrupa3 f [] = empty
agrupa3 f (x:xs) = insertWith (++) (f x) [x] (agrupa3 f xs)
