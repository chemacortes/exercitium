module Ex_20151106 where

{-

Primos gemelos próximos a múltiplos de 6
========================================

Un par de números primos (p,q) es un par de números primos gemelos si su 
distancia de 2; es decir, si q = p+2. Por ejemplo, (17,19) es una par de 
números primos gemelos.

Se dice que un par de números (x,y) está próximo a un múltiplo de 6 si es de 
la forma (6*n-1,6*n+1). Por ejemplo, (17,19) está cerca de un múltiplo de 6 
porque (17,19) = (6*3-1,6*3+1).

Definir las funciones

   primosGemelos :: Integer -> [(Integer,Integer)]
   primosGemelosNoProximosAmultiplosDe6 :: Integer -> [(Integer,Integer)]

tales que

    (primosGemelos n) es la lista de los primos gemelos menores que n. Por 
    ejemplo,

     primosGemelos 50  == [(3,5),(5,7),(11,13),(17,19),(29,31),(41,43)]
     primosGemelos 43  == [(3,5),(5,7),(11,13),(17,19),(29,31)]

    (primosGemelosNoProximosAmultiplosDe6 n) es la lista de los primos gemelos 
    menores que n que no están próximos a un múltiplo de 6. Por ejemplo,

     primosGemelosNoProximosAmultiplosDe6 50  ==  [(3,5)]

-}

import Data.Numbers.Primes

primosGemelos :: Integer -> [(Integer,Integer)]
primosGemelos n = filter (\(x,y) -> x + 2 == y) 
                $ zip primes' (tail primes')
    where primes' = takeWhile (<n) primes

primosGemelosNoProximosAmultiplosDe6 :: Integer -> [(Integer,Integer)]
primosGemelosNoProximosAmultiplosDe6 = 
    filter (\(x,_) -> (x+1) `mod` 6 /= 0) . primosGemelos 
