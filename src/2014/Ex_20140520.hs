module Ex_20140520 where

{-
Elemento más repetido de manera consecutiva
===========================================

Enunciado
---------

-- Definir la función
--    masRepetido :: Ord a => [a] -> (a,Int)
-- tal que (masRepetido xs) es el elemento de xs que aparece más veces
-- de manera consecutiva en la lista junto con el número de sus
-- apariciones consecutivas; en caso de empate, se devuelve el último de
-- dichos elementos. Por ejemplo,
--    masRepetido [1,1,4,4,1]  ==  (4,2)
--    masRepetido "aadda"      ==  ('d',2)
-}

import           Data.List (group)

-- por compresión de listas
masRepetido :: Ord a => [a] -> (a,Int)
masRepetido xs = last [(x,l)| (x,l) <- xs', l == m]
    where
        xs' = [(y,length ys) |  (y:ys) <- group xs]
        m = maximum [l| (_,l) <- xs']


-- por recursión
masRepetido2 :: Ord a => [a] -> (a,Int)
masRepetido2 [] = (undefined,0)
masRepetido2 [x] = (x,1)
masRepetido2 (x:xs) = if n>m then (x,n) else (y,m)
    where (xs',ys) = span (==x) xs
          n = 1 + length xs'
          (y,m) = masRepetido2 ys

