module Ex_20140519 where

{-
Regiones en el plano
====================

Enunciado
---------

-- En los siguientes dibujos se observa que el número máximo de regiones
-- en el plano generadas con 1, 2 ó 3 líneas son 2, 4 ó 7,
-- respectivamente.
--
--                       \  |
--                        \5|
--                         \|
--                          \
--                          |\
--                          | \
--                |         |  \
--     1        1 | 3     1 | 3 \  6
--    ------   ---|---   ---|----\---
--     2        2 | 4     2 | 4   \ 7
--                |         |      \
--
-- Definir la función
--    regiones :: Integer -> Integer
-- tal que (regiones n) es el número máximo de regiones en el plano
-- generadas con n líneas. Por ejemplo,
--    regiones 3    ==  7
--    regiones 100  ==  5051
-}

regiones :: Integer -> Integer
regiones 1 = 2
regiones n = n + regiones (n-1)

-- por la fórmula
regiones2 :: Integer -> Integer
regiones2 n = n*(n+1) `div` 2 + 1