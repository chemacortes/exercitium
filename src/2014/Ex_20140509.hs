module Ex_20140509 where

{-
Ramas de un árbol
=================

Enunciado
---------

-- Los árboles se pueden representar mediante el siguiente tipo de datos
--    data Arbol a = N a [Arbol a]
--                   deriving Show
-- Por ejemplo, los árboles
--      1               3
--     / \             /|\ 
--    2   3           / | \
--        |          5  4  7
--        4          |     /\ 
--                   6    2  1
-- se representan por
--    ej1, ej2 :: Arbol Int
--    ej1 = N 1 [N 2 [],N 3 [N 4 []]]
--    ej2 = N 3 [N 5 [N 6 []], N 4 [], N 7 [N 2 [], N 1 []]]
-- 
-- Definir la función 
--    ramas :: Arbol b -> [[b]]
-- tal que (ramas a) es la lista de las ramas del árbol a. Por ejemplo,
--    ramas ej1  ==  [[1,2],[1,3,4]]
--    ramas ej2  ==  [[3,5,6],[3,4],[3,7,2],[3,7,1]]

-}

data Arbol a = N a [Arbol a]
               deriving Show

ej1, ej2 :: Arbol Int
ej1 = N 1 [N 2 [],N 3 [N 4 []]]
ej2 = N 3 [N 5 [N 6 []], N 4 [], N 7 [N 2 [], N 1 []]]


-- Por concatenación
ramas :: Arbol b -> [[b]]
ramas (N n []) = [[n]]
ramas (N n xs) = map (n:) (concatMap ramas xs)

-- Por compresión de listas
ramas2 :: Arbol b -> [[b]]
ramas2 (N n []) = [[n]]
ramas2 (N n xs) = [ n:ns | arbol <- xs, ns <- ramas2 arbol]

