module Ex_20140521 where

{-
Número de pares de elementos adyacentes iguales en una matriz
=============================================================
 
Enunciado
---------

-- Definir la función
--    numeroParesAdyacentesIguales :: Eq a => [[a]] -> Int
-- tal que (numeroParesAdyacentesIguales xss) es el número de pares de
-- elementos adyacentes (en la misma fila o columna) iguales de la
-- matriz xss. Por ejemplo,
--    numeroParesAdyacentesIguales [[0,1],[0,2]]              ==  1
--    numeroParesAdyacentesIguales [[0,0],[1,2]]              ==  1
--    numeroParesAdyacentesIguales [[0,1],[0,0]]              ==  2
--    numeroParesAdyacentesIguales [[1,2],[1,4],[4,4]]        ==  3
--    numeroParesAdyacentesIguales ["ab","aa"]                ==  2
--    numeroParesAdyacentesIguales [[0,0,0],[0,0,0],[0,0,0]]  ==  12
--    numeroParesAdyacentesIguales [[0,0,0],[0,1,0],[0,0,0]]  ==  8

-}

import Data.List (transpose)

numeroParesAdyacentesIguales :: Eq a => [[a]] -> Int
numeroParesAdyacentesIguales xss =
     sum (map igualesEnLinea xss) + sum (map igualesEnLinea (transpose xss))

igualesEnLinea :: Eq a => [a] -> Int
igualesEnLinea xs = sum [ 1 | (x,y) <- zip xs (tail xs), x == y]

-- una definición recursiva
igualesEnLinea2 :: Eq a => [a] -> Int
igualesEnLinea2 [] = 0
igualesEnLinea2 [_] = 0
igualesEnLinea2 (x:y:xs)
    | x == y    = 1 + igualesEnLinea2 (y:xs)
    | otherwise = igualesEnLinea2 (y:xs)

