module Ex_20140505 where

{-
Máximos locales
===============

Enunciado
---------

-- Un máximo local de una lista es un elemento de la lista que es mayor
-- que su predecesor y que su sucesor en la lista. Por ejemplo, 5 es un
-- máximo local de [3,2,5,3,7,7,1,6,2] ya que es mayor que 2 (su
-- predecesor) y que 3 (su sucesor).
--
-- Definir la función
--    maximosLocales :: Ord a => [a] -> [a]
-- tal que (maximosLocales xs) es la lista de los máximos locales de la
-- lista xs. Por ejemplo,
--    maximosLocales [3,2,5,3,7,7,1,6,2]  ==  [5,6]
--    maximosLocales [1..100]             ==  []
--    maximosLocales "adbpmqexyz"         ==  "dpq"
-}


-- Definición por recursión
maximosLocales :: Ord a => [a] -> [a]
maximosLocales (a:b:c:xs)
    | a < b && b > c = b : maximosLocales (c:xs)
    | otherwise      = maximosLocales (b:c:xs)
maximosLocales _     = []

-- Definición por compresión (zip3)
maximosLocales2 :: Ord a => [a] -> [a]
maximosLocales2 xs = [b | (a,b,c) <- zip3 xs (tail xs) (drop 2 xs), a < b, b > c]