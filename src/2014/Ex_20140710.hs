module Ex_20140710 where

{-

Código Morse
============

El código Morse es un sistema de representación de letras y números mediante 
señales emitidas de forma intermitente.

A los signos (letras mayúsculas o dígitos) se le asigna un código como se 
muestra a continuación

    +---+-------+---+-------+---+-------+---+-------+
    | A | .-    | J | .---  | S | ...   | 1 | ..--- |
    | B | -...  | K | -.-   | T | -     | 2 | ...-- |
    | C | -.-.  | L | .-..  | U | ..-   | 3 | ....- |
    | D | -..   | M | --    | V | ...-  | 4 | ..... |
    | E | .     | N | -.    | W | .--   | 5 | -.... |
    | F | ..-.  | O | ---   | X | -..-  | 6 | --... |
    | G | --.   | P | .--.  | Y | -.--  | 7 | ---.. |
    | H | ....  | Q | --.-  | Z | --..  | 8 | ----. |
    | I | ..    | R | .-.   | 0 | .---- | 9 | ----- |
    +---+-------+---+-------+---+-------+---+-------+

El código Morse de las palabras se obtiene a partir del de sus caracteres 
insertando un espacio entre cada uno. Por ejemplo, el código de "todo" es 
"- --- -.. ---"

El código Morse de las frases se obtiene a partir del de sus palabras 
insertando dos espacios entre cada uno. Por ejemplo, el código de "todo o nada" 
es "- --- -.. ---  ---  -. .- -.. .-"

Enunciado
---------

-- Definir las funciones
--    fraseAmorse :: String -> String
--    morseAfrase :: String -> String
-- tales que
-- * (fraseAmorse cs) es la traducción de la frase cs a Morse. Por
--   ejemplo, 
--      ghci> fraseAmorse "En todo la medida"
--      ". -.  - --- -.. ---  .-.. .-  -- . -.. .. -.. .-"
-- * (morseAfrase cs) es la frase cuya traducción a Morse es cs. Por 
--   ejemplo, 
--      ghci> morseAfrase ". -.  - --- -.. ---  .-.. .-  -- . -.. .. -.. .-"
--      "EN TODO LA MEDIDA"
--
-- Nota: La lista de los códigos Morse de A, B, ..., Z, 0, 1, ..., 9 es
--    [".-","-...","-.-.","-..",".","..-.","--.","....","..",".---",
--     "-.-",".-..","--","-.","---",".--.","--.-",".-.","...","-",
--     "..-","...-",".--","-..-","-.--","--..",".----","..---","...--",
--     "....-",".....","-....","--...","---..","----.","-----"]

Ayuda: Se puede usar la función splitOn de la librería Data.List.Split.

-}

import Data.List
import Data.List.Split (splitOn)
import Data.Char (toUpper)

abecedario :: String
abecedario = ['A'..'Z'] ++ ['0'..'9']

-- Lista de los códigos Morse de A, B, ..., Z, 0, 1, ..., 9 
codigos :: [String]
codigos = 
    [".-","-...","-.-.","-..",".","..-.","--.","....","..",".---",
     "-.-",".-..","--","-.","---",".--.","--.-",".-.","...","-",
     "..-","...-",".--","-..-","-.--","--..",".----","..---","...--",
     "....-",".....","-....","--...","---..","----.","-----"]


-- Busca un elemento en una lista y devuelve el correspondiente
busca :: Eq a => [a] -> [b] -> a -> b
busca xs ys x = ys !! n
    where Just n = elemIndex x xs

buscaCodigo :: Char -> String
buscaCodigo = busca abecedario codigos

buscaLetra :: String -> Char
buscaLetra = busca codigos abecedario


fraseAmorse :: String -> String
fraseAmorse cs = intercalate "  "
               $ map palabraAmorse (words cs)
    where palabraAmorse xs = unwords [buscaCodigo (toUpper x) | x <- xs]


morseAfrase :: String -> String
morseAfrase cs = unwords [ morseApalabra xs | xs <- splitOn "  " cs ]
    where morseApalabra xs = [buscaLetra c | c <- words xs]

