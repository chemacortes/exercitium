module Ex_20140516 where

{-
Ampliación de matrices por columnas
===================================

Enunciado
---------

-- Las matrices enteras se pueden representar mediante tablas con
-- índices enteros: 
--    type Matriz = Array (Int,Int) Int
-- 
-- Definir la función
--    ampliaColumnas :: Matriz -> Matriz -> Matriz
-- tal que (ampliaColumnas p q) es la matriz construida añadiendo las
-- columnas de la matriz q a continuación de las de p (se supone que
-- tienen el mismo número de filas). Por ejemplo, si p y q representa
-- las dos primeras matrices, entonces (ampliaColumnas p q) es la
-- tercera  
--    |0 1|    |4 5 6|    |0 1 4 5 6| 
--    |2 3|    |7 8 9|    |2 3 7 8 9|
-- En Haskell,
--    ghci> :{
--    *Main| ampliaColumnas (listArray ((1,1),(2,2)) [0..3]) 
--    *Main|                (listArray ((1,1),(2,3)) [4..9])
--    *Main| :}
--    array ((1,1),(2,5)) 
--          [((1,1),0),((1,2),1),((1,3),4),((1,4),5),((1,5),6),
--           ((2,1),2),((2,2),3),((2,3),7),((2,4),8),((2,5),9)]

-}

import Data.Array

type Matriz = Array (Int,Int) Int

ampliaColumnas :: Matriz -> Matriz -> Matriz
ampliaColumnas p q = listArray ((1,1),(n,a+b)) (concat [[p!(l,i)|i<-[1..a]]++[q!(l,j)|j<-[1..b]]| l <- [1..n]])
    where ((_,_),(n,a)) = bounds p
          ((_,_),(_,b)) = bounds q

ampliaColumnas2 :: Matriz -> Matriz -> Matriz
ampliaColumnas2 p q = listArray ((1,1),(n,a+b)) (concat (zipWith (++) (porLineas p) (porLineas q))) 
    where ((_,_),(n,a)) = bounds p
          ((_,_),(_,b)) = bounds q

porLineas :: Matriz -> [[Int]]
porLineas p = [[p!(i,j) | j <- [b..d]] | i <- [a..c]]
    where ((a,b),(c,d)) = bounds p




          