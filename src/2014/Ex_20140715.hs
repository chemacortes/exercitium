module Ex_20140715 where

{-

Sucesiones pucelanas
=====================

Introducción
------------

En la Olimpiada de Matemática del 2010 se planteó el siguiente problema:

>   Una sucesión pucelana es una sucesión creciente de 16 números impares 
> positivos consecutivos, cuya suma es un cubo perfecto. ¿Cuántas sucesiones 
> pucelanas tienen solamente números de tres cifras? 

Para resolverlo se propone el siguiente ejercicio.
Enunciado

-- Definir la función
--    pucelanasConNcifras :: Int -> [[Int]]
-- tal que (pucelanasConNcifras n) es la lista de las sucesiones
-- pucelanas que tienen solamente números de n cifras. Por ejemplo,

--    ghci> pucelanasConNcifras 2
--    [[17,19,21,23,25,27,29,31,33,35,37,39,41,43,45,47]]

-- Calcular cuántas sucesiones pucelanas tienen solamente números de
-- tres cifras.

-}

pucelanasConNcifras :: Int -> [[Int]]
pucelanasConNcifras n = [ xs | xs <- sucesion16 xss
                             , cuboPerfecto (sum xs) ]
    where xss = [10^(n-1)+1,10^(n-1)+3..10^n-1]

cuboPerfecto :: Int -> Bool
cuboPerfecto x = y*y*y == x
    where y = floor $ (fromIntegral x::Double) ** (1 / 3)

-- Convierte una sucesión en listas de 16 elementos
sucesion16 :: [Int] -> [[Int]]
sucesion16 xs | length xs >= 16 = take 16 xs : sucesion16 (tail xs)
              | otherwise       = []
