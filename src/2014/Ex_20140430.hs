module Ex_20140430 where

{-
Primos equidistantes
====================

Enunciado

-- Definir la función
--    primosEquidistantes :: Integer -> [(Integer,Integer)]
-- tal que (primosEquidistantes k) es la lista de los pares de primos
-- consecutivos cuya diferencia es k. Por ejemplo,
--    take 3 (primosEquidistantes 2)  ==  [(3,5),(5,7),(11,13)]
--    take 3 (primosEquidistantes 4)  ==  [(7,11),(13,17),(19,23)]
--    take 3 (primosEquidistantes 6)  ==  [(23,29),(31,37),(47,53)]
--    take 3 (primosEquidistantes 8)  ==  [(89,97),(359,367),(389,397)]
-}

-- import Data.Numbers.Primes

primosEquidistantes :: Integer -> [(Integer,Integer)]
primosEquidistantes k = [(x,y)| (x,y) <- zip primos (tail primos), y-x == k]

primos :: [Integer]
primos = 2 : filter isPrimo [3,5..]

isPrimo :: Integer -> Bool
isPrimo n = all (n `noDivBy`) (primosMenoresDe n)
    where
        a `noDivBy` b =  a `rem` b /= 0
        primosMenoresDe x = takeWhile (\y -> y*y <= x) primos
