module Ex_20140428 where

{-

Primos consecutivos con media capic�a
=====================================

La pasada semana, Antonio Rold�n public� en Twitter la siguiente observaci�n:

    Los pares de primos consecutivos (97,101) y (109,113) son los m�s peque�os,
    con promedio capic�a con m�s de una cifra: (97+101)/2=99 y (109+113)/2=111.

A partir de ella, se propone el ejercicio de hoy.

Enunciado

-- Definir la constante
--    primosConsecutivosConMediaCapicua :: [(Int,Int,Int)]
-- tal que primosConsecutivosConMediaCapicua es la lista de las ternas
-- (x,y,z) tales que x e y son primos consecutivos tales que su media,
-- z, es capic�a. Por ejemplo,
--    ghci> take 5 primosConsecutivosConMediaCapicua
--    [(3,5,4),(5,7,6),(7,11,9),(97,101,99),(109,113,111)]
-- Calcular cu�ntos hay anteriores a 2014.
-}

-- import Data.Numbers.Primes

capicua :: Integer -> Bool
capicua n = show n == (reverse . show) n

primo :: Integer -> Bool
primo x = null [ y | y <- [3,5..x-1], x `rem` y == 0]

primos :: [Integer]
primos = [ x | x <- [3,5..], primo x]

primosConsecutivosConMediaCapicua :: [(Integer,Integer,Integer)]
primosConsecutivosConMediaCapicua =
    [ (x,y,p) | (x,y) <- zip primos (tail primos),
              let p = (x+y) `div` 2,
              capicua p]

-- anteriores a 2014
anteriores :: Int
anteriores = length $ takeWhile (\(_,y,_) -> y < 2014) primosConsecutivosConMediaCapicua
