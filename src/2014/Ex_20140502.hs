module Ex_20140502 where

{-
Matrices de Toepliz
===================

Enunciado
---------

-- Una matriz de Toeplitz es una matriz cuadrada que es constante a lo
-- largo de las diagonales paralelas a la diagonal principal. Por
-- ejemplo,
--    |2 5 1 6|       |2 5 1 6|      |2 5 1 6|
--    |4 2 5 1|       |4 2 6 1|      |4 2 5 1|
--    |7 4 2 5|       |7 4 2 5|      |7 4 2 5|
--    |9 7 4 2|       |9 7 4 2|
-- la primera es una matriz de Toeplitz y las otras no lo son.
--
-- Las anteriores matrices se pueden definir por
--    ej1, ej2, ej3 :: Array (Int,Int) Int
--    ej1 = listArray ((1,1),(4,4)) [2,5,1,6,4,2,5,1,7,4,2,5,9,7,4,2]
--    ej2 = listArray ((1,1),(4,4)) [2,5,1,6,4,2,6,1,7,4,2,5,9,7,4,2]
--    ej3 = listArray ((1,1),(3,4)) [2,5,1,6,4,2,5,1,7,4,2,5]
--
-- Definir la función
--    esToeplitz :: Eq a => Array (Int,Int) a -> Bool
-- tal que (esToeplitz p) se verifica si la matriz p es de Toeplitz. Por
-- ejemplo,
--    esToeplitz ej1  ==  True
--    esToeplitz ej2  ==  False
--    esToeplitz ej3  ==  False
-- Nota: Hay que usar la librería Data.Array.
-}

import Data.Array

ej1, ej2, ej3 :: Array (Int,Int) Int
ej1 = listArray ((1,1),(4,4)) [2,5,1,6,4,2,5,1,7,4,2,5,9,7,4,2]
ej2 = listArray ((1,1),(4,4)) [2,5,1,6,4,2,6,1,7,4,2,5,9,7,4,2]
ej3 = listArray ((1,1),(3,4)) [2,5,1,6,4,2,5,1,7,4,2,5]

esToeplitz :: Eq a => Array (Int,Int) a -> Bool
esToeplitz p = esCuadrada &&
                    and [ p!(i,j) == p!(i+1,j+1) |
                             i <- [a..(c-a-1)], j <- [b..(d-b-1)] ]
    where ((a,b),(c,d)) = bounds p
          esCuadrada = d-b == c-a
