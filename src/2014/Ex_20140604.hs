﻿module Ex_20140604 where
{-
Índices de valores verdaderos
=============================

Enunciado
---------

-- Definir la función
--    indicesVerdaderos :: [Int] -> [Bool]
-- tal que (indicesVerdaderos xs) es la lista infinita de booleanos tal
-- que sólo son verdaderos los elementos cuyos índices pertenecen a la lista estrictamente creciente xs.
-- Por ejemplo,
--    ghci> take 6 (indicesVerdaderos [1,4])
--    [False,True,False,False,True,False]
--    ghci> take 6 (indicesVerdaderos [0,2..])
--    [True,False,True,False,True,False]
--    ghci> take 3 (indicesVerdaderos [])
--    [False,False,False]
--    ghci> take 6 (indicesVerdaderos [1..])
--    [False,True,True,True,True,True]
-}


indicesVerdaderos :: [Int] -> [Bool]
indicesVerdaderos xs = map pertenece [0..]
    where pertenece x = x `elem` (takeWhile (<=x) xs)
