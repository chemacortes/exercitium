module Ex_20140611 where

{-
Eliminación de las ocurrencias unitarias
========================================
 
Enunciado
---------

-- Definir la función
--    eliminaUnitarias :: Char -> String -> String
-- tal que (eliminaUnitarias c cs) es la lista obtenida eliminando de la
-- cadena cs las ocurrencias unitarias del carácter c (es decir,
-- aquellas ocurrencias de c tales que su elemento anterior y posterior
-- es distinto de c). Por ejemplo,
--    eliminaUnitarias 'X ""                  == ""
--    eliminaUnitarias 'X "X"                 == ""
--    eliminaUnitarias 'X "XX"                == "XX"
--    eliminaUnitarias 'X "XXX"               == "XXX"
--    eliminaUnitarias 'X "abcd"              == "abcd"
--    eliminaUnitarias 'X "Xabcd"             == "abcd"
--    eliminaUnitarias 'X "XXabcd"            == "XXabcd"
--    eliminaUnitarias 'X "XXXabcd"           == "XXXabcd"
--    eliminaUnitarias 'X "abcdX"             == "abcd"
--    eliminaUnitarias 'X "abcdXX"            == "abcdXX"
--    eliminaUnitarias 'X "abcdXXX"           == "abcdXXX"
--    eliminaUnitarias 'X "abXcd"             == "abcd"
--    eliminaUnitarias 'X "abXXcd"            == "abXXcd"
--    eliminaUnitarias 'X "abXXXcd"           == "abXXXcd"
--    eliminaUnitarias 'X "XabXcdX"           == "abcd"
--    eliminaUnitarias 'X "XXabXXcdXX"        == "XXabXXcdXX"
--    eliminaUnitarias 'X "XXXabXXXcdXXX"     == "XXXabXXXcdXXX"
--    eliminaUnitarias 'X' "XabXXcdXeXXXfXx"  ==  "abXXcdeXXXfx"

-}

import Data.List

eliminaUnitarias :: Char -> String -> String
eliminaUnitarias c cs = concat [xs | xs <- group cs, xs /= [c]]