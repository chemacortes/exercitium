module Ex_20140603 where

{-
C�digo de las alergias
======================

Enunciado
---------

-- Para la determinaci�n de las alergia se utiliza los siguientes
-- c�digos para los al�rgenos:
--    Huevos ........   1
--    Cacahuetes ....   2
--    Mariscos ......   4
--    Fresas ........   8
--    Tomates .......  16
--    Chocolate .....  32
--    Polen .........  64
--    Gatos ......... 128
-- As�, si Juan es al�rgico a los cacahuetes y al chocolate, su
-- puntuaci�n es 34 (es decir, 2+32).
--
-- Los al�rgenos se representan mediante el siguiente tipo de dato
--   data Alergeno = Huevos
--                 | Cacahuetes
--                 | Mariscos
--                 | Fresas
--                 | Tomates
--                 | Chocolate
--                 | Polen
--                 | Gatos
--                 deriving (Enum, Eq, Show, Bounded)
--
-- Definir la funci�n
--    alergias :: Int -> [Alergeno]
-- tal que (alergias n) es la lista de alergias correspondiente a una
-- puntuaci�n n. Por ejemplo,
--    ghci> alergias 0
--    []
--    ghci> alergias 1
--    [Huevos]
--    ghci> alergias 2
--    [Cacahuetes]
--    ghci> alergias 8
--    [Fresas]
--    ghci> alergias 3
--    [Huevos,Cacahuetes]
--    ghci> alergias 5
--    [Huevos,Mariscos]
--    ghci> alergias 248
--    [Fresas,Tomates,Chocolate,Polen,Gatos]
--    ghci> alergias 255
--    [Huevos,Cacahuetes,Mariscos,Fresas,Tomates,Chocolate,Polen,Gatos]
--    ghci> alergias 509
--    [Huevos,Mariscos,Fresas,Tomates,Chocolate,Polen,Gatos]
-}

import           Data.Bits
import           Data.List (subsequences)

data Alergeno = Huevos
              | Cacahuetes
              | Mariscos
              | Fresas
              | Tomates
              | Chocolate
              | Polen
              | Gatos
              deriving (Enum, Eq, Show, Bounded)

alergenos :: [Alergeno]
alergenos = [Huevos,Cacahuetes,Mariscos,Fresas,Tomates,Chocolate,Polen,Gatos]

codigos :: [(Alergeno, Int)]
codigos = zip alergenos (iterate (*2) 1)

alergias :: Int -> [Alergeno]
alergias n = [c | (c,i) <- codigos, n .&. i /= 0]

-- por variaciones
alergias2 :: Int -> [Alergeno]
alergias2 n = head [xs | (xs,ys) <- map unzip (subsequences codigos),
                         sum ys == n ]
