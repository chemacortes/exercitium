module Ex_20140529 where

{-
Elementos de una matriz con algún vecino menor
==============================================

Enunciado
---------

-- Las matrices puede representarse mediante tablas cuyos índices son
-- pares de números naturales:     
--    type Matriz = Array (Int,Int) Int
-- Definir la función 
--    algunMenor :: Matriz -> [Int]
-- tal que (algunMenor p) es la lista de los elementos de p que tienen
-- algún vecino menor que él. Por ejemplo,  
--    algunMenor (listArray ((1,1),(3,4)) [9,4,6,5,8,1,7,3,4,2,5,4])
--    [9,4,6,5,8,7,4,2,5,4]          
-- pues sólo el 1 y el 3 no tienen ningún vecino menor en la matriz
--    |9 4 6 5|
--    |8 1 7 3|
--    |4 2 5 4|
-}

import Data.Array

type Matriz = Array (Int,Int) Int

algunMenor :: Matriz -> [Int]
algunMenor p  = [e | (i,e) <- assocs p, any (<e) [p!j | j <- vecinos i]]
    where
        dims = bounds p  
        vecinos (x,y) = filter (inRange dims)
                               [(x+i,y+j) | i <- [-1..1], j <- [-1..1],
                                            i/=0 || j/=0]
