module Ex_20140508 where

{-
Valores de polinomios representados mediante vectores
=====================================================

Enunciado
---------

-- Los polinomios se pueden representar mediante vectores usando la
-- librería Data.Array. En primer lugar, se define el tipo de los
-- polinomios (con coeficientes de tipo a) mediante
--    type Polinomio a = Array Int a
-- Como ejemplos, definimos el polinomio
--    ej_pol1 :: Array Int Int
--    ej_pol1 = array (0,4) [(1,2),(2,-5),(4,7),(0,6),(3,0)]
-- que representa a 2x - 5x^2 + 7x^4 + 6 y el polinomio
--    ej_pol2 :: Array Int Double
--    ej_pol2 = array (0,4) [(1,2),(2,-5.2),(4,7),(0,6.5),(3,0)]
-- que representa a 2x - 5.2x^2 + 7x^4 + 6.5
--
-- Definir la función
--    valor :: Num a => Polinomio a -> a -> a
-- tal que (valor p b) es el valor del polinomio p en el punto b. Por
-- ejemplo, 
--    valor ej_pol1 0  ==  6
--    valor ej_pol1 1  ==  10
--    valor ej_pol1 2  ==  102
--    valor ej_pol2 0  ==  6.5
--    valor ej_pol2 1  ==  10.3
--    valor ej_pol2 3  ==  532.7
-}

import Data.Array

type Polinomio a = Array Int a

-- 2x - 5x^2 + 7x^4 + 6
ej_pol1 :: Array Int Int
ej_pol1 = array (0,4) [(1,2),(2,-5),(4,7),(0,6),(3,0)]

-- 2x - 5.2x^2 + 7x^4 + 6.5
ej_pol2 :: Array Int Double
ej_pol2 = array (0,4) [(1,2),(2,-5.2),(4,7),(0,6.5),(3,0)]


valor :: Num a => Polinomio a -> a -> a
valor p b = sum [ c * b^n | (n,c) <- assocs p]




