module Ex_20140527 where

{-
Números triangulares con n cifras distintas
===========================================
 
Enunciado
---------

-- Los números triangulares se forman como sigue
-- 
--    *     *      * 
--         * *    * *
--               * * *
--    1     3      6
-- 
-- La sucesión de los números triangulares se obtiene sumando los
-- números naturales. Así, los 5 primeros números triangulares son
--     1 = 1
--     3 = 1+2
--     6 = 1+2+3
--    10 = 1+2+3+4
--    15 = 1+2+3+4+5
-- 
-- Definir la función
--    triangularesConCifras :: Int -> [Integer]
-- tal que (triangulares n) es la lista de los números triangulares con
-- n cifras distintas. Por  ejemplo, 
--    take 6 (triangularesConCifras 1)   ==  [1,3,6,55,66,666]
--    take 6 (triangularesConCifras 2)   ==  [10,15,21,28,36,45]
--    take 6 (triangularesConCifras 3)   ==  [105,120,136,153,190,210]
--    take 5 (triangularesConCifras 4)   ==  [1035,1275,1326,1378,1485]
--    take 2 (triangularesConCifras 10)  ==  [1062489753,1239845706]

-}

import Data.List (nub)

triangulares :: [Integer]
triangulares = scanl1 (+)  [1..] 

triangularesConCifras :: Int -> [Integer]
triangularesConCifras n = [i | i <- triangulares, (length . nub . show) i == n ]

