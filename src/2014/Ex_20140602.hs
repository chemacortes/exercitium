module Ex_20140602 where

{-
Pim, Pam, Pum y divisibilidad
=============================

Enunciado
---------

-- Definir la función
--    sonido :: Int -> String
-- tal que (sonido n) escribe "Pim" si n es divisible por 3, además
-- escribe "Pam" si n es divisible por 5 y también escribe "Pum" si n es
-- divisible por 7. Por ejemplo,
--    sonido   3  ==  "Pim"
--    sonido   5  ==  "Pam"
--    sonido   7  ==  "Pum"
--    sonido   8  ==  ""
--    sonido   9  ==  "Pim"
--    sonido  15  ==  "PimPam"
--    sonido  21  ==  "PimPum"
--    sonido  35  ==  "PamPum"
--    sonido 105  ==  "PimPamPum"
-- 
-- Nota: Buscar la definición con el menor número de palabras.
-}

sonido :: Int -> String
sonido n = concat [s | (s,i) <- [("Pim",3),("Pam",5),("Pum",7)], n `mod` i == 0]