module Ex_20140721 where

{-

Números con todos sus dígitos primos
====================================

-- La sucesión A046034 de la OEIS (The On-Line Encyclopedia of Integer
-- Sequences) está formada por los números tales que todos sus dígitos
-- son primos. Los primeros términos de A046034 son
--    2,3,5,7,22,23,25,27,32,33,35,37,52,53,55,57,72,73,75,77,222,223
--
-- Definir la constante
--    numerosDigitosPrimos :: [Int]
-- cuyos elementos son los términos de la sucesión A046034. Por ejemplo,
--    ghci> take 22 numerosDigitosPrimos
--    [2,3,5,7,22,23,25,27,32,33,35,37,52,53,55,57,72,73,75,77,222,223]

-}

import Data.Numbers.Primes
import Data.Char (digitToInt)


numerosDigitosPrimos1 :: [Int]
numerosDigitosPrimos1 = [ x | x <- [2..], all isPrime (digitos x) ]
    where digitos = map digitToInt . show

numerosDigitosPrimos2 :: [Int]
numerosDigitosPrimos2 = [ x | x <- [2..], all (`elem`"2357") (show x) ]

numerosDigitosPrimos3 :: [Int]
numerosDigitosPrimos3 = map read . concat . tail
                      $ iterate (\xss -> [x:xs | x <- "2357", xs <- xss]) [[]]
