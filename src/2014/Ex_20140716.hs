module Ex_20140716 where

{-

Producto de matrices como listas de listas
==========================================

-- Las matrices pueden representarse mediante una lista de listas donde
-- cada una de las lista representa una fila  de la matriz. Por ejemplo,
-- la matriz
--    |1 0 -2|
--    |0 3 -1|
-- puede representarse por [[1,0,-2],[0,3,-1]]. 
-- 
-- Definir la función
--    producto :: Num a => [[a]] -> [[a]] -> [[a]]
-- tal que (producto p q) es el producto de las matrices p y q. Por
-- ejemplo, 
--    ghci> producto [[1,0,-2],[0,3,-1]] [[0,3],[-2,-1],[0,4]]
--    [[0,-5],[-6,-7]]

-}

import Data.List

producto :: Num a => [[a]] -> [[a]] -> [[a]]
producto p q = [ [sum $ zipWith (*) xs ys | ys <- transpose q ] | xs <- p ]  


producto2 :: Num a => [[a]] -> [[a]] -> [[a]]
producto2 p q = [[ prod xs ys | xs <- p ] | ys <- transpose q ]
    where prod = (sum .) . zipWith (*)
