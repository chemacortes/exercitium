module Ex_20140630 where

{-

Eliminación de n elementos
==========================

-- Definir la función
--    elimina :: Int -> [a] -> [[a]]
-- tal que (elimina n xs) es la lista de las listas obtenidas eliminando
-- n elementos de xs. Por ejemplo,
--    elimina 0 "abcd"  ==  ["abcd"]
--    elimina 1 "abcd"  ==  ["abc","abd","acd","bcd"]
--    elimina 2 "abcd"  ==  ["ab","ac","ad","bc","bd","cd"]
--    elimina 3 "abcd"  ==  ["a","b","c","d"]
--    elimina 4 "abcd"  ==  [""]
--    elimina 5 "abcd"  ==  []
--    elimina 6 "abcd"  ==  []

-}

elimina :: Int -> [a] -> [[a]]
elimina 0 xs = [xs]
elimina _ [] = []
elimina n (x:xs) = map (x:) (elimina n xs) ++ elimina (n-1) xs