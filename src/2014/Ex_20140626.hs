module Ex_20140626 where

{-
Sopa de letras
==============
 
Enunciado
---------

-- Las matrices se puede representar mediante tablas cuyos índices son
-- pares de números naturales:  
--    type Matriz a = Array (Int,Int) a
-- 
-- Definir la función 
--    enLaSopa :: Eq a => [a] -> Matriz a -> Bool
-- tal que (enLaSopa c p) se verifica si c está en la matriz p en
-- horizontal o en vertical. Por ejemplo, si p es la matriz siguiente:
--    p :: Matriz Char
--    p = listaMatriz ["mjtholueq",
--                     "juhoolauh",
--                     "dariouhyj",
--                     "rngkploaa"]
-- entonces,
--    enLaSopa "dar"  p  ==  True   -- En horizontal a la derecha en la 3ª fila
--    enLaSopa "oir"  p  ==  True   -- En horizontal a la izquierda en la 3ª fila
--    enLaSopa "juan" p  ==  True   -- En vertical descendente en la 2ª columna
--    enLaSopa "kio"  p  ==  True   -- En vertical ascendente en la 3ª columna
--    enLaSopa "Juan" p  ==  False
--    enLaSopa "hola" p  ==  False
-- 
-- Nota. Para resolverlo, se puede usar la función isInfixOf.

-}

import Data.Array
import Data.List (isInfixOf)

type Matriz a = Array (Int,Int) a

listaMatriz :: [[a]] -> Matriz a
listaMatriz xss = listArray ((1,1),(m,n)) (concat xss)
    where m = length xss
          n = length (head xss)

p1 :: Matriz Char
p1 = listaMatriz ["mjtholueq",
                  "juhoolauh",
                  "dariouhyj",
                  "rngkploaa"]

enLaSopa :: Eq a => [a] -> Matriz a -> Bool           
enLaSopa c p = any (c `isInfixOf`) ( lineas ++ columnas) ||
               any ((reverse c) `isInfixOf`) ( lineas ++ columnas)
    where
        ((_,_),(a,b)) = bounds p
        lineas   = [ [ p!(i,j) | j <- [1..b] ] | i <- [1..a] ]
        columnas = [ [ p!(i,j) | i <- [1..a] ] | j <- [1..b] ]

