module Ex_20140506 where

{-
Lista cuadrada
==============

Enunciado
---------

-- Definir la función
--    listaCuadrada :: Int -> a -> [a] -> [[a]]
-- tal que (listaCuadrada n x xs) es una lista de n listas de longitud n
-- formadas con los elementos de xs completada con x, si no xs no tiene
-- suficientes elementos. Por ejemplo,
--    listaCuadrada 3 7 [0,3,5,2,4]  ==  [[0,3,5],[2,4,7],[7,7,7]]
--    listaCuadrada 3 7 [0..]        ==  [[0,1,2],[3,4,5],[6,7,8]]
--    listaCuadrada 2 'p' "eva"      ==  ["ev","ap"]
--    listaCuadrada 2 'p' ['a'..]    ==  ["ab","cd"]
-}

listaCuadrada :: Int -> a -> [a] -> [[a]]
listaCuadrada n x xs = take n $ trocea n (xs ++ repeat x)

trocea :: Int -> [a] -> [[a]]
trocea _ [] = []
trocea n xs = let (ys,zs) = splitAt n xs
              in ys : trocea n zs
