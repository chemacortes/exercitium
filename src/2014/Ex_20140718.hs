module Ex_20140718 where

{-

Matriz permutación
==================

Introducción
------------

Una matriz permutación es una matriz cuadrada con todos sus elementos iguales 
a 0, excepto uno cualquiera por cada fila y columna, el cual debe ser igual a 1.

Ejercicio
---------

-- En este ejercicio se usará el tipo de las matrices definido por
--    type Matriz a = Array (Int,Int) a
-- y los siguientes ejemplos de matrices
--    q1, q2, q3 :: Matriz Int
--    q1 = array ((1,1),(2,2)) [((1,1),1),((1,2),0),((2,1),0),((2,2),1)]
--    q2 = array ((1,1),(2,2)) [((1,1),0),((1,2),1),((2,1),0),((2,2),1)]
--    q3 = array ((1,1),(2,2)) [((1,1),3),((1,2),0),((2,1),0),((2,2),1)]
--
-- Definir la función
--    esMatrizPermutacion :: Num a => Matriz a -> Bool
-- tal que (esMatrizPermutacion p) se verifica si p es una matriz
-- permutación. Por ejemplo.
--    esMatrizPermutacion q1  ==  True
--    esMatrizPermutacion q2  ==  False
--    esMatrizPermutacion q3  ==  False

-}

import Data.Array

type Matriz a = Array (Int,Int) a

q1, q2, q3 :: Matriz Int
q1 = array ((1,1),(2,2)) [((1,1),1),((1,2),0),((2,1),0),((2,2),1)]
q2 = array ((1,1),(2,2)) [((1,1),0),((1,2),1),((2,1),0),((2,2),1)]
q3 = array ((1,1),(2,2)) [((1,1),3),((1,2),0),((2,1),0),((2,2),1)]

esMatrizPermutacion :: (Num a, Eq a) => Matriz a -> Bool
esMatrizPermutacion p = n == m -- Matriz cuadrada
                      && all esListaUnitaria filas
                      && all esListaUnitaria columnas
    where (_,(n,m)) = bounds p
          filas    = [[ p!(i,j) | j <- [1..m]] | i <- [1..n]]
          columnas = [[ p!(i,j) | i <- [1..n]] | j <- [1..m]]

esListaUnitaria :: (Num a, Eq a) => [a] -> Bool
esListaUnitaria xs = filter (/=0) xs == [1]
