module Ex_20140526 where

{-
Trenzado de listas
==================
 
Enunciado
---------

-- Definir la función 
--    trenza :: [a] -> [a] -> [a]
-- tal que (trenza xs ys) es la lista obtenida intercalando los
-- elementos de xs e ys. Por ejemplo,
--    trenza [5,1] [2,7,4]             ==  [5,2,1,7]
--    trenza [5,1,7] [2..]             ==  [5,2,1,3,7,4]
--    trenza [2..] [5,1,7]             ==  [2,5,3,1,4,7]
--    take 8 (trenza [2,4..] [1,5..])  ==  [2,1,4,5,6,9,8,13]

-}

trenza :: [a] -> [a] -> [a]
trenza = (concat .) .  zipWith (\x y -> [x,y])

trenza2 :: [a] -> [a] -> [a]
trenza2 = (concatMap (\(x,y) -> [x,y]) .) . zip 

trenza3 :: [a] -> [a] -> [a]
trenza3 = (.)(.)(.) (concatMap (\(x,y) -> [x,y])) zip


-- por recursión
trenza4 :: [a] -> [a] -> [a]
trenza4 [] _ = []
trenza4 (x:xs) ys = x : trenza4 ys xs

-- extraño
{-
    si tenemos [5,1] [2,7,4], en el primer paso, mapeamos las dos lista
    a una secuencia [[5:,1:],[2:,7:,4:]] que luego unimos con zipWith(.)
    en: [(5:).(2:), (1:).(7:)] y luego concatenamos con foldr:
    (5:).(2:) $ (1:).(7:) $ []  -> [5,2,1,7] 
-}
trenza5 :: [a] -> [a] -> [a]
trenza5 xs ys = foldr($)[] $ zipWith(.) (map(:) xs) (map(:) ys)

