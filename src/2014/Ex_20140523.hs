module Ex_20140523 where

{-
Biparticiones de una lista
==========================
 
Enunciado
---------

-- Definir la función
--    biparticiones :: [a] -> [([a],[a])]
-- tal que (biparticiones xs) es la lista de pares formados por un
-- prefijo de xs y el resto de xs. Por ejemplo,
--    ghci> biparticiones [3,2,5]
--    [([],[3,2,5]),([3],[2,5]),([3,2],[5]),([3,2,5],[])]
--    ghci> biparticiones "Roma"
--    [("","Roma"),("R","oma"),("Ro","ma"),("Rom","a"),("Roma","")]

-}

import Data.List

biparticiones :: [a] -> [([a],[a])]
biparticiones xs = [splitAt i xs | i <- [0..length xs]]

 -- con inits y tails
biparticiones2 :: [a] -> [([a],[a])]
biparticiones2 xs = zip (inits xs) (tails xs)