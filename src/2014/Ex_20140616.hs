module Ex_20140616 where

{-
Divisores de un número con final dado
=====================================
 
Enunciado
---------

-- Definir la función
--    divisoresConFinal :: Integer -> Integer -> [Integer]
-- tal que (divisoresConFinal n m) es la lista de los divisores de n
-- cuyos dígitos finales coincide con m. Por ejemplo,
--    divisoresConFinal 84 4    ==  [4,14,84]
--    divisoresConFinal 720 20  ==  [20,120,720]

-}

import Data.List

divisoresConFinal :: Integer -> Integer -> [Integer]
divisoresConFinal n m =
    [ x | x <- [1..n], n `mod` x == 0, show m `isSuffixOf` show x]
