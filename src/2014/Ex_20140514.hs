module Ex_20140514 where

{-
Ordenación de estructuras
=========================

Enunciado
---------

-- Las notas de los dos primeros exámenes se pueden representar mediante
-- el siguiente tipo de dato
--    data Notas = Notas String Int Int
--                 deriving (Read, Show, Eq)
-- Por ejemplo, (Notas "Juan" 6 5) representa las notas de un alumno
-- cuyo nombre es Juan, la nota del primer examen es 6 y la del segundo
-- es 5.
-- 
-- Definir la función
--    ordenadas :: [Notas] -> [Notas]
-- tal que (ordenadas ns) es la lista de las notas ns ordenadas
-- considerando primero la nota del examen 2, a continuación la del
-- examen 1 y finalmente el nombre. Por ejemplo,
--    ghci> ordenadas [Notas "Juan" 6 5, Notas "Luis" 3 7] 
--    [Notas "Juan" 6 5,Notas "Luis" 3 7]
--    ghci> ordenadas [Notas "Juan" 6 5, Notas "Luis" 3 4] 
--    [Notas "Luis" 3 4,Notas "Juan" 6 5]
--    ghci> ordenadas [Notas "Juan" 6 5, Notas "Luis" 7 4] 
--    [Notas "Luis" 7 4,Notas "Juan" 6 5]
--    ghci> ordenadas [Notas "Juan" 6 4, Notas "Luis" 7 4] 
--    [Notas "Juan" 6 4,Notas "Luis" 7 4]
--    ghci> ordenadas [Notas "Juan" 6 4, Notas "Luis" 5 4] 
--    [Notas "Luis" 5 4,Notas "Juan" 6 4]
--    ghci> ordenadas [Notas "Juan" 5 4, Notas "Luis" 5 4] 
--    [Notas "Juan" 5 4,Notas "Luis" 5 4]
--    ghci> ordenadas [Notas "Juan" 5 4, Notas "Eva" 5 4] 
--    [Notas "Eva" 5 4,Notas "Juan" 5 4]

-}

import Data.List

data Notas = Notas String Int Int
             deriving (Read, Show, Eq)
             
ordenadas :: [Notas] -> [Notas]
ordenadas ns = [Notas n a b| (b,a,n) <- camposOrdenados]
    where camposOrdenados = sort [(b,a,n) | Notas n a b <- ns] 

-- usando sortBy
ordenadas2 :: [Notas] -> [Notas]
ordenadas2 = sortBy cmpNotas
    where cmpNotas (Notas s1 a1 b1) (Notas s2 a2 b2) =
                 compare (b1,a1,s1) (b2,a2,s2)
    
    
