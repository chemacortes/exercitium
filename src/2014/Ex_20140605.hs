module Ex_20140605 where

{-
Descomposiciones triangulares
=============================
 
Enunciado
---------

-- Definir la función
--    descomposicionesTriangulares :: Int -> [(Int, Int, Int)] 
-- tal que (descomposicionesTriangulares n) es la lista de las
-- ternas correspondientes a las descomposiciones de n en tres sumandos,
-- como máximo, formados por números triangulares. Por ejemplo,
--    ghci> descomposicionesTriangulares 6
--    [(0,0,6),(0,3,3)]
--    ghci> descomposicionesTriangulares 26
--    [(1,10,15),(6,10,10)]
--    ghci> descomposicionesTriangulares 96
--    [(3,15,78),(6,45,45),(15,15,66),(15,36,45)]

-}


triangulares :: [Int]
triangulares = [ n*(n+1) `div` 2 | n <- [0..]]

descomposicionesTriangulares :: Int -> [(Int, Int, Int)]
descomposicionesTriangulares n = 
    [(i,j,k) | i <- xs, j <- dropWhile (<i) xs,
               let k = n - i - j,
               k `elem` dropWhile (<j) xs ]
    where xs = takeWhile (<=n) triangulares
               
               
               
-- triangulares es la lista de los números triangulares. Por ejemplo,
--    take 10 triangulares  ==  [0,1,3,6,10,15,21,28,36,45]
triangulares2 :: [Int]
triangulares2 = scanl (+) 0 [1..]