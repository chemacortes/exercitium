module Ex_20140530 where

{-
Reiteración de una función
==========================

Enunciado
---------

-- Definir la función 
--    reiteracion :: Int -> (a -> a) -> a -> a
-- tal que (reiteracion n f x) es el resultado de aplicar n veces la
-- función f a x. Por ejemplo,
--    reiteracion 10 (+1) 5  ==  15
--    reiteracion 10 (+5) 0  ==  50
--    reiteracion  4 (*2) 1  ==  16
--    reiteracion 4 (5:) [] ==  [5,5,5,5]
-- 
-- Comprobar con QuickCheck que se verifican las siguientes propiedades
--    reiteracion 10 (+1) x  == 10 + x 
--    reiteracion 10 (+x) 0  == 10 * x 
--    reiteracion 10 (x:) [] == replicate 10 x
-}

import Test.QuickCheck

-- definición recursiva
reiteracion :: Int -> (a -> a) -> a -> a
reiteracion 0 _ = id
reiteracion n f = reiteracion (n-1) f . f

-- mediante folding
reiteracion2 :: Int -> (a -> a) -> a -> a
reiteracion2 n f = foldl1 (.) (replicate n f)

-- mediante iterate
reiteracion3 :: Int -> (a -> a) -> a -> a
reiteracion3 n f x = iterate f x !! n


-- Tests

prop1 :: Int -> Bool
prop1 x = reiteracion 10 (+1) x  == 10 + x

prop2 :: Int -> Bool
prop2 x = reiteracion 10 (+x) 0  == 10 * x 

prop3 :: Int -> Bool
prop3 x = reiteracion 10 (x:) [] == replicate 10 x

runTest :: IO ()
runTest = do quickCheck prop1
             quickCheck prop2
             quickCheck prop3