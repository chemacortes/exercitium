module Ex_20140424 where

{-|

Elementos minimales
===================

-- Definir la función
--    minimales :: Eq a => [[a]] -> [[a]]
-- tal que (minimales xss) es la lista de los elementos de xss que no
-- están contenidos en otros elementos de xss. Por ejemplo,
--    minimales [[1,3],[2,3,1],[3,2,5]]        ==  [[2,3,1],[3,2,5]]
--    minimales [[1,3],[2,3,1],[3,2,5],[3,1]]  ==  [[2,3,1],[3,2,5]]

Caso especial:

    minimales [[1,3],[2,1],[3,2,5],[3,1]] == [[1,3],[2,1],[3,2,5],[3,1]]

Si dos conjuntos tienen los mismos elementos ([1,3],[3,1]) no se consideran
*Subconjuntos Propios* uno de otro y, por lo tanto, no deben ser filtrados.

-}

import           Data.List

{- Solución basada en operaciones set de diferencias de listas -}

minimales :: Eq a => [[a]] -> [[a]]
minimales xss = filter redundante xss
    where redundante xs = not $ any (subconjuntoPropio xs) xss

-- (xs in ys) but not (ys in xs)
subconjuntoPropio :: Eq a => [a] -> [a] -> Bool
subconjuntoPropio xs ys = null (xs \\ ys) && (not . null $ ys \\ xs)

