module Ex_20140624 where

{-
Entero positivo de la cadena
============================
 
Enunciado
---------

-- Definir la función
--    enteroPositivo :: String -> Maybe Int
-- tal que (enteroPositivo cs) es justo el contenido de la cadena cs, si
-- dicho contenido es un entero positivo, y Nothing en caso contrario. 
-- Por ejemplo, 
--    enteroPositivo "235"    ==  Just 235
--    enteroPositivo "-235"   ==  Nothing
--    enteroPositivo "23.5"   ==  Nothing
--    enteroPositivo "235 "   ==  Nothing
--    enteroPositivo "cinco"  ==  Nothing
--    enteroPositivo ""       ==  Nothing

-}

enteroPositivo :: String -> Maybe Int
enteroPositivo ""                     = Nothing
enteroPositivo cs | all esDigito cs   = Just (read cs)
                  | otherwise         = Nothing
                  where esDigito = (`elem` ['0'..'9'])
