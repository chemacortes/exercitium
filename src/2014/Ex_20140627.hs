module Ex_20140627 where

{-

Intercalación de n copias
=========================

-- Definir la función
--    intercala :: Int -> a -> [a] -> [[a]]
-- tal que (intercala n x ys) es la lista de la listas obtenidas
-- intercalando n copias de x en ys. Por ejemplo,
--    intercala 2 'a' "bc" == ["bcaa","baca","baac","abca","abac","aabc"]
--    intercala 2 'a' "c"  == ["caa","aca","aac"]
--    intercala 1 'a' "c"  == ["ca","ac"]
--    intercala 0 'a' "c"  == ["c"]

-- Nota: No importa el orden de los elementos.

-}

intercala :: Int -> a -> [a] -> [[a]]
intercala 0 _ xs = [xs]
intercala n x [] = [replicate n x]
intercala n x (y:ys) = 
    concat [[ replicate i x ++ (y:zs) | zs <- intercala (n-i) x ys] | i <- [0..n]]
