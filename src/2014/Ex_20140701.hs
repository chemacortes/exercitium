module Ex_20140701 where

{-

Límite de sucesiones
====================

Enunciado
---------

-- Ejercicio. Definir la función  
--    limite :: (Double -> Double) -> Double -> Double
-- tal que (limite f a) es el valor de f en el primer término x tal que, 
-- para todo y entre x+1 y x+100, el valor absoluto de la diferencia
-- entre f(y) y f(x) es menor que a. Por ejemplo,
--    limite (\n -> (2*n+1)/(n+5)) 0.001  ==  1.9900110987791344
--    limite (\n -> (1+1/n)**n) 0.001     ==  2.714072874546881

-}

limite :: (Double -> Double) -> Double -> Double
limite f a = head [f x | x <- [0..], 
                         and [ abs(f (x+i) - f x) < a | i <- [1..100]] ] 