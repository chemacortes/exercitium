module Ex_20140620 where

{-
Mayor sucesión del problema 3n+1
================================

Enunciado
---------

-- La sucesión 3n+1 generada por un número entero positivo x es la
-- sucesión generada por el siguiente algoritmo: Se empieza con el
-- número x. Si x es par, se divide entre 2. Si x es impar, se
-- multiplica por 3 y se le suma 1. El  proceso se repite con el número
-- obtenido hasta que se alcanza el valor 1. Por ejemplo, la sucesión de
-- números generadas cuando se empieza en 22 es
--    22 11 34 17 52 26 13 40 20 10 5 16 8 4 2 1
-- Se ha conjeturado (aunque no demostrado) que este algoritmo siempre
-- alcanza el 1 empezando en cualquier entero positivo.
--
-- Definir la función
--    mayorLongitud :: Integer -> Integer -> Integer
-- tal que (mayorLongitud i j) es el máximo de las longitudes de las
-- sucesiones 3n+1 para todos los números comprendidos entre i y j,
-- ambos inclusives. Por ejemplo,
--    mayorLongitud   1   10  ==  20
--    mayorLongitud 100  200  ==  125
--    mayorLongitud 201  210  ==  89
--    mayorLongitud 900 1000  ==  174
-}

import Data.List (genericLength)

mayorLongitud :: Integer -> Integer -> Integer
mayorLongitud i j = maximum [genericLength (sucesion n) | n <- [i..j]]

sucesion :: Integer -> [Integer]
sucesion 1      = [1]
sucesion n
    | even n    = n : sucesion (n `div` 2)
    | otherwise = n : sucesion (3*n+1)
