module Ex_20140606 where

{-
Número de inversiones
=====================
 
Enunciado
---------

-- Se dice que en una sucesión de números x(1), x(2), ..., x(n) hay una 
-- inversión cuando existe un par de números x(i) > x(j), siendo i < j.
-- Por ejemplo, en la permutación 2, 1, 4, 3 hay dos inversiones 
-- (2 antes que 1 y 4 antes que 3) y en la permutación 4, 3, 1, 2 hay 
-- cinco inversiones (4 antes 3, 4 antes 1, 4 antes 2, 3 antes 1, 
-- 3 antes 2).
-- 
-- Definir la función 
--    numeroInversiones :: Ord a => [a] -> Int  
-- tal que (numeroInversiones xs) es el número de inversiones de xs. Por
-- ejemplo, 
--    numeroInversiones [2,1,4,3]  ==  2
--    numeroInversiones [4,3,1,2]  ==  5

-}

numeroInversiones :: Ord a => [a] -> Int
numeroInversiones []    = 0
numeroInversiones (x:xs) =
    length (filter (<x) xs) + numeroInversiones xs
    
