module Ex_20140703 where

{-

Renombramiento de un árbol
==========================

-- Los árboles binarios se pueden representar mediante el tipo Arbol definido
-- por
--    data Arbol a = H a 
--                 | N a (Arbol a) (Arbol a)
--                 deriving Show
-- Por ejemplo, el árbol
--         "C"
--         / \ 
--        /   \
--       /     \
--     "B"     "A"
--     / \     / \
--   "A" "B" "B" "C" 
-- se puede definir por 
--    ej1 :: Arbol String
--    ej1 = N "C" (N "B" (H "A") (H "B")) (N "A" (H "B") (H "C"))
--
-- Definir la función
--    renombraArbol :: Arbol t -> Arbol Int
-- tal que (renombraArbol a) es el árbol obtenido sustituyendo el valor
-- de los nodos y hojas por números tales que tengan el mismo valor si y
-- sólo si coincide su contenido. Por ejemplo,
--    ghci> renombraArbol ej1
--    N 2 (N 1 (H 0) (H 1)) (N 0 (H 1) (H 2))
-- Gráficamente, 
--          2 
--         / \ 
--        /   \
--       /     \
--      1       0 
--     / \     / \
--    0   1   1   2

-}

import Data.List

data Arbol a = H a 
             | N a (Arbol a) (Arbol a)
             deriving (Show, Eq)

-- Ejemplo en el enunciado
ej1 :: Arbol String
ej1 = N "C" (N "B" (H "A") (H "B")) (N "A" (H "B") (H "C"))


renombraArbol :: Eq t => Arbol t -> Arbol Int
renombraArbol a = aux a (nombres a)
    where aux (H b) ns = H (pos b ns)
          aux (N b t1 t2) ns = N (pos b ns) (aux t1 ns) (aux t2 ns)

-- Extrae todos los nombres de los nodos y crea una lista única
nombres :: Eq t => Arbol t -> [t]
nombres (H a) = [a]
nombres (N a t1 t2) = nub (a : nombres t1 ++ nombres t2)

-- Devuelve la posición de un elemento en una lista
pos :: Eq t => t -> [t] -> Int
pos x xs = head [ i | (y,i) <- zip xs [0..], x == y ]

