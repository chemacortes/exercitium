module Ex_20140612 where

{-
Ordenada cíclicamente
=====================
 
Enunciado
---------

-- Se dice que una sucesión x(1), ..., x(n) está ordenada cíclicamente
-- si existe un índice i tal que la sucesión  
--    x(i), x(i+1), ..., x(n), x(1), ..., x(i-1)
-- está ordenada crecientemente. 
-- 
-- Definir la función 
--    ordenadaCiclicamente :: Ord a => [a] -> Int
-- tal que (ordenadaCiclicamente xs) es el índice (empezando en 1) a
-- partir del cual está ordenada, si el la lista está ordenado cíclicamente
-- y 0 en caso contrario. Por ejemplo,
--    ordenadaCiclicamente [1,2,3,4]      ==  1  
--    ordenadaCiclicamente [5,8,2,3]      ==  3 
--    ordenadaCiclicamente [4,6,7,5,4,3]  ==  0 
--    ordenadaCiclicamente [1,0,1,2]      ==  0
--    ordenadaCiclicamente [0,2,0]        ==  3
--    ordenadaCiclicamente "cdeab"        ==  4

-}

import Data.List (sort)

ordenadaCiclicamente :: Ord a => [a] -> Int
ordenadaCiclicamente []     = 0
ordenadaCiclicamente (x:xs) = if ciclico then res else 0
    where (ys,zs) = span (>x) xs
          ciclico = sort (x:xs) == zs++[x]++ys
          res = if null zs then 1 else 2 + length ys