module Ex_20140609 where

{-
Separación por posición
=======================
 
Enunciado
---------

-- Definir la función
--    particion :: [a] -> ([a],[a])
-- tal que (particion xs) es el par cuya primera componente son los
-- elementos de xs en posiciones pares y su segunda componente son los
-- restantes elementos. Por ejemplo,
--    particion [3,5,6,2]    ==  ([3,6],[5,2])
--    particion [3,5,6,2,7]  ==  ([3,6,7],[5,2])
--    particion "particion"  ==  ("priin","atco")

-}

import Data.List

-- definición por recursión
particion :: [a] -> ([a],[a])
particion []        = ([],[])
particion (x:xs)    = (x:zs,ys) where (ys,zs) = particion xs

-- por plegado
particion5 :: [a] -> ([a],[a])
particion5 = foldr f ([],[]) where f x (ys,zs) = (x:zs,ys)
