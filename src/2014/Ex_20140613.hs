module Ex_20140613 where

{-
Órbita prima
============
 
Enunciado
---------

-- La órbita prima de un número n es la sucesión construida de la
-- siguiente forma: 
--    * si n es compuesto su órbita no tiene elementos 
--    * si n es primo, entonces n está en su órbita; además, sumamos n y
--      sus dígitos, si el resultado es un número primo repetimos el
--      proceso hasta obtener un número compuesto. 
-- Por ejemplo, con el 11 podemos repetir el proceso dos veces
--    13 = 11+1+1
--    17 = 13+1+3
-- Así, la órbita prima de 11 es 11, 13, 17. 
-- 
-- Definir la función
--    orbita :: Integer -> [Integer]
-- tal que (orbita n) es la órbita prima de n. Por ejemplo,
--    orbita 11 == [11,13,17]
--    orbita 59 == [59,73,83]
-- Calcular el menor número cuya órbita prima tiene más de 3 elementos.

-}

import Data.Numbers.Primes

orbita :: Integer -> [Integer]
orbita n
    | (not.isPrime) n   = []
    | otherwise         = n : orbita (n+sum (cifras n))
        where cifras x = [read [c] | c <- show x]
       
orbita2 :: Integer -> [Integer]
orbita2 n = takeWhile isPrime (iterate f n)
    where
        f x = x + sum (cifras x)
        cifras x = [read [c] | c <- show x]
