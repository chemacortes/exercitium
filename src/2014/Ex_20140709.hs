module Ex_20140709 where

{-
Elemento más cercano que cumple una propiedad
=============================================

-- Definir la función
--    cercano :: (a -> Bool) -> Int -> [a] -> Maybe a
-- tal que (cercano p n xs) es el elemento de xs más cercano a n que
-- verifica la propiedad p. La búsqueda comienza en n y los elementos se
-- analizan en el siguiente orden: n, n+1, n-1, n+2, n-2,... Por ejemplo, 
--    cercano (`elem` "aeiou") 6 "Sevilla"     ==  Just 'a'
--    cercano (`elem` "aeiou") 1 "Sevilla"     ==  Just 'e'
--    cercano (`elem` "aeiou") 2 "Sevilla"     ==  Just 'i'
--    cercano (`elem` "aeiou") 5 "Sevilla"     ==  Just 'a'
--    cercano (`elem` "aeiou") 9 "Sevilla"     ==  Just 'a'
--    cercano (`elem` "aeiou") (-3) "Sevilla"  ==  Just 'e'
--    cercano (>100) 4 [200,1,150,2,4]         ==  Just 150
--    cercano even 5 [1,3..99]                 ==  Nothing

-}

import Data.List

cercano :: (a -> Bool) -> Int -> [a] -> Maybe a
cercano p n xs = find p xs'
    where (ys,zs) = splitAt (n+1) xs
          xs' = (intercala (reverse ys) zs)

intercala :: [a] -> [a] -> [a]
intercala [] ys = ys
intercala (x:xs) ys = x : intercala ys xs
