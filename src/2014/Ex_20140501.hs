module Ex_20140501 where


{-
Suma si todos los valores son justos
====================================

Enunciado
---------

-- Definir la función
--    sumaSiTodosJustos :: (Num a, Eq a) => [Maybe a] -> Maybe a
-- tal que (sumaSiTodosJustos xs) es justo la suma de todos los
-- elementos de xs si todos son justos (es decir, si Nothing no
-- pertenece a xs) y Nothing en caso contrario. Por ejemplo,
--    sumaSiTodosJustos [Just 2, Just 5]           == Just 7
--    sumaSiTodosJustos [Just 2, Just 5, Nothing]  == Nothing
-- Nota: Hay que usar la librería Data.Maybe.
-}

import Data.Maybe
import Control.Monad

-- Usando expresiones monádicas
sumaSiTodosJustos :: (Num a, Eq a) => [Maybe a] -> Maybe a
sumaSiTodosJustos = foldr1 (liftM2 (+))

-- Usando patrones
sumaSiTodosJustos2 :: (Num a, Eq a) => [Maybe a] -> Maybe a
sumaSiTodosJustos2 = foldr1 sumaJustos
    where sumaJustos (Just x) (Just y) = Just (x+y)
          sumaJustos Nothing _ = Nothing
          sumaJustos _ Nothing = Nothing
