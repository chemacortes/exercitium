module Ex_20140421 where

{-|

Iguales al siguiente
====================

Definir la función

```haskell
    igualesAlSiguiente :: Eq a => [a] -> [a]
```

tal que `igualesAlSiguiente xs` sea la lista de los elementos de `xs`
que son iguales a su siguiente. Por ejemplo,

```haskell
    igualesAlSiguiente [1,2,2,2,3,3,4]  ==  [2,2,3]
    igualesAlSiguiente [1..10]          ==  []
```

-}

import           Data.List (group)

test1, test2 :: Bool
test1 = igualesAlSiguiente [1,2,2,2,3,3,4]  ==  [2,2,3]
test2 = igualesAlSiguiente [1..10]          ==  []


-- Implementación recursiva
igualesAlSiguiente :: (Eq a, Show a) => [a] -> [a]
igualesAlSiguiente [] = []
igualesAlSiguiente [_] = []
igualesAlSiguiente (x:y:zs)
          | x == y = x : igualesAlSiguiente (y:zs)
          | otherwise = igualesAlSiguiente (y:zs)

-- Implementación con concatMap
igualesAlSiguiente2 :: (Eq a, Show a) => [a] -> [a]
igualesAlSiguiente2 xs = concatMap tail (group xs)

-- Implementación por compresión de listas
igualesAlSiguiente3 :: (Eq a, Show a) => [a] -> [a]
igualesAlSiguiente3 xs = [ x | (x,y) <- zip xs (tail xs), x==y]
