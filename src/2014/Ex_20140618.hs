module Ex_20140618 where

{-
Selección hasta el primero que falla inclusive
==============================================

Enunciado
---------

-- Definir la función
--    seleccionConFallo :: (a -> Bool) -> [a] -> [a]
-- tal que (seleccionConFallo p xs) es la lista de los elementos de xs
-- que cumplen el predicado p hasta el primero que no lo cumple
-- inclusive. Por ejemplo,
--    seleccionConFallo (<5) [3,2,5,7,1,0]  ==  [3,2,5]
--    seleccionConFallo odd [1..4]          ==  [1,2]
--    seleccionConFallo odd [1,3,5]         ==  [1,3,5]
--    seleccionConFallo (<5) [10..20]       ==  [10]
-}

seleccionConFallo :: (a -> Bool) -> [a] -> [a]
seleccionConFallo _ []  = []
seleccionConFallo p (x:xs)
    | p x        = x : seleccionConFallo p xs
    | otherwise  = [x]

seleccionConFallo2 :: (a -> Bool) -> [a] -> [a]
seleccionConFallo2 p xs = ys ++ take 1 zs
    where (ys,zs) = span p xs