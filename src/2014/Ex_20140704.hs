module Ex_20140704 where

{-

Divide si todos son múltiplos
=============================

Ejercicio. Definir la función

   divideSiTodosMultiplos :: Integral a => a -> [a] -> Maybe [a]

tal que (divideSiTodosMultiplos x ys) es justo la lista de los cocientes de los
elementos de ys entre x si todos son múltiplos de x y Nothing en caso contrario.

Por ejemplo,

   divideSiTodosMultiplos 2 [6,10,4]  ==  Just [3,5,2]
   divideSiTodosMultiplos 2 [6,10,5]  ==  Nothing

-}

divideSiTodosMultiplos :: Integral a => a -> [a] -> Maybe [a]
divideSiTodosMultiplos x ys | and [ i `mod` x == 0 | i <- ys] = Just [ i `div` x | i <- ys]
                            | otherwise = Nothing
                            
-- Implementación monádica
divideSiTodosMultiplos2 :: Integral a => a -> [a] -> Maybe [a]
divideSiTodosMultiplos2 x = mapM aux
    where aux i | i `mod` x == 0  = Just (i `div` x)
                | otherwise       = Nothing
