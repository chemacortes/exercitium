module Ex_20140507 where

{-
Segmentos maximales con elementos consecutivos
==============================================

Enunciado
---------

-- Definir la función
--    segmentos :: (Enum a, Eq a) => [a] -> [[a]]
-- tal que (segmentos xss) es la lista de los segmentos maximales de xss
-- formados por elementos consecutivos. Por ejemplo,
--    segmentos [1,2,5,6,4]     ==  [[1,2],[5,6],[4]]
--    segmentos [1,2,3,4,7,8,9] ==  [[1,2,3,4],[7,8,9]]
--    segmentos "abbccddeeebc"  ==  ["ab","bc","cd","de","e","e","bc"]
-- Nota: Se puede usar la función succ tal que (succ x) es el sucesor de
-- x. Por ejemplo,
--    succ 3    ==  4
--    succ 'c'  ==  'd'
-}

-- Por recursión
segmentos :: (Enum a, Eq a) => [a] -> [[a]]
segmentos [] = []
segmentos [x] = [[x]]
segmentos (x:xs)
    | succ x == y = (x:y:ys):yss
    | otherwise = [x]:(y:ys):yss
        where ((y:ys):yss) = segmentos xs

-- por foldr
segmentos2 :: (Enum a, Eq a) => [a] -> [[a]]
segmentos2 = foldr addElem []


addElem :: (Enum a, Eq a) => a -> [[a]] -> [[a]]
addElem x []      = [[x]]
addElem x ((y:ys):yss)
    | succ x == y = (x:y:ys):yss    -- 'x' se hace precursor de un grupo
    | otherwise   = [x]:(y:ys):yss  -- 'x' inicia nuevo grupo
addElem _ ([]:_)  = [] -- caso que nunca ocurrirá
