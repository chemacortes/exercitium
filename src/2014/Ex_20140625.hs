module Ex_20140625 where

{-
N gramas
========
 
Un n-grama de una sucesión es una subsucesión de n elementos.

Enunciado
---------

-- Definir la función
--    nGramas :: Int -> [a] -> [[a]]
-- tal que (nGramas k xs) es la lista de los n-gramas de xs de longitud
-- k. Por ejemplo,
--    nGramas 0 "abcd"  ==  [""]
--    nGramas 1 "abcd"  ==  ["a","b","c","d"]
--    nGramas 2 "abcd"  ==  ["ab","ac","ad","bc","bd","cd"]
--    nGramas 3 "abcd"  ==  ["abc","abd","acd","bcd"]
--    nGramas 4 "abcd"  ==  ["abcd"]
--    nGramas 5 "abcd"  ==  []

-}

import Data.List

-- por recursión
nGramas :: Int -> [a] -> [[a]]
nGramas 0 _      = [[]]
nGramas _ []     = []
nGramas k (x:xs) = [ x:ys | ys <- nGramas (k-1) xs ] ++
                   nGramas k xs

-- por fuerza bruta
nGramas2 :: Int -> [a] -> [[a]]
nGramas2 k xs = [ ys | ys <- subsequences xs, length ys == k ]

