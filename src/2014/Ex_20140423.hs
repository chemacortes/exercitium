module Ex_20140423 where

{-|

La bandera tricolor
===================


El problema de la bandera tricolor consiste en lo siguiente:

Dada un lista de objetos `xs` que pueden ser rojos, amarillos o morados, se pide
devolver una lista `ys` que contiene los elementos de `xs`, primero los rojos,
luego los amarillos y por último los morados.

-- Definir el tipo de dato Color para representar los colores con los
-- constructores `R`, `A` y `M` correspondientes al rojo, azul y morado y la
-- función
--    banderaTricolor :: [Color] -> [Color]
-- tal que `banderaTricolor xs` es la bandera tricolor formada con los
-- elementos de `xs`. Por ejemplo,
--    banderaTricolor [M,R,A,A,R,R,A,M,M]  ==  [R,R,R,A,A,A,M,M,M]
--    banderaTricolor [M,R,A,R,R,A]        ==  [R,R,R,A,A,M]

-}

import           Data.List

data Color = R | A | M
                deriving (Eq, Ord, Show)

-- Ordenación "natural"
banderaTricolor :: [Color] -> [Color]
banderaTricolor = sort


-- Compresiones de listas
banderaTricolor2 :: [Color] -> [Color]
banderaTricolor2 xs = [ R | R <- xs] ++ [ A | A <- xs] ++ [ M | M <- xs]

-- concat
banderaTricolor3 :: [Color] -> [Color]
banderaTricolor3 xs = concat [ [ x | x <- xs, x==c] | c<-[R,A,M]]
