module Ex_20140513 where

{-
Numeración de las ternas de números naturales
=============================================

Enunciado
---------

-- Las ternas de números naturales se pueden ordenar como sigue
--    (0,0,0), 
--    (0,0,1),(0,1,0),(1,0,0),
--    (0,0,2),(0,1,1),(0,2,0),(1,0,1),(1,1,0),(2,0,0),
--    (0,0,3),(0,1,2),(0,2,1),(0,3,0),(1,0,2),(1,1,1),(1,2,0),(2,0,1),(2,1,0),(3,0,0),
--    ...
-- 
-- Definir la función
--    posicion :: (Int,Int,Int) -> Int
-- tal que (posicion (x,y,z)) es la posición de la terna de números
-- naturales (x,y,z) en la ordenación anterior. Por ejemplo,
--    posicion (0,1,0)  ==  2
--    posicion (0,0,2)  ==  4
--    posicion (0,1,1)  ==  5
-}

import Data.List
import Data.Maybe


ternas :: [(Int,Int,Int)]
ternas = [(i,j,k)| n <- [0..], i <- [0..n], j <- [0..n-i], let k = n -i - j]

-- versión muy ineficiente
ternas2 :: [(Int,Int,Int)]
ternas2 = [(i,j,k)| n <- [0..], i <- [0..n], j <- [0..n], k <- [0..n],
                   i+j+k == n]


posicion :: (Int,Int,Int) -> Int
posicion (x,y,z) = length $ takeWhile (/=(x,y,z)) ternas

-- usando los módulos List y Maybe
posicion2 :: (Int,Int,Int) -> Int
posicion2 (x,y,z) = fromJust $ elemIndex (x,y,z) ternas