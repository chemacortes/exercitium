module Ex_20140515 where

{-
Emparejamiento binario
======================

Enunciado
---------

-- Definir la función
--    zipBinario :: [a -> b -> c] -> [a] -> [b] -> [c]
-- tal que (zipBinario fs xs ys) es la lista obtenida aplicando cada una
-- de las operaciones binarias de fs a los correspondientes elementos de
-- xs e ys. Por ejemplo, 
--    zipBinario [(+), (*), (*)] [2,2,2] [4,4,4]     ==  [6,8,8]
--    zipBinario [(+)] [2,2,2] [4,4,4]               ==  [6]
--    zipBinario (cycle [(+), (*)]) [1 .. 4] [2..5]  ==  [3,6,7,20]

-}

zipBinario :: [a -> b -> c] -> [a] -> [b] -> [c]
zipBinario fs xs ys = [f x y | (f,x,y) <- zip3 fs xs ys]

-- con zipWith3
zipBinario2 :: [a -> b -> c] -> [a] -> [b] -> [c]
zipBinario2 = zipWith3 id
