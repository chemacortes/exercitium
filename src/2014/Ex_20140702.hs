module Ex_20140702 where

{-

Empiezan con mayúscula
======================

Enunciado
---------

-- Ejercicio. Definir, por composición, la función
--    conMayuscula :: String -> Int
-- tal que (conMayuscula cs) es el número de palabras de cs que empiezan
-- con mayúscula. Por ejemplo.
--    conMayuscula "Juan vive en Sevilla o en Huelva"  ==  3

-}

import Data.Char

conMayuscula :: String -> Int
conMayuscula = length . filter (isUpper . head). words