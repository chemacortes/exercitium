module Ex_20140422 where

{-|

Ordenación por máximos
======================

Definir la función
    ordenadosPorMaximo :: Ord a => [[a]] -> [[a]]
tal que `ordenadosPorMaximo xss` es la lista de los elementos de `xss`
ordenada por sus máximos. Por ejemplo,

    ghci> ordenadosPorMaximo [[3,2],[6,7,5],[1,4]]
    [[3,2],[1,4],[6,7,5]]
    ghci> ordenadosPorMaximo ["este","es","el","primero"]
    ["el","primero","es","este"]

-}

import           Data.Function (on)
import           Data.List

ordenadosPorMaximo :: Ord a => [[a]] -> [[a]]
ordenadosPorMaximo  = sortBy cmp
    where cmp x y = compare (maximum x) (maximum y)


ordenadosPorMaximo' :: Ord a => [[a]] -> [[a]]
ordenadosPorMaximo'  = sortBy (compare `on` maximum)

{-|
Otras soluciones basadas en crear tuplas ordenadas
-}

-- Comprensión de listas
ordenadosPorMaximo2 :: Ord a => [[a]] -> [[a]]
ordenadosPorMaximo2 xss = [ ys | (_,ys) <- sort [(maximum xs,xs) | xs <- xss]]


