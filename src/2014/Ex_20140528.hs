module Ex_20140528 where

{-
Enumeración de árboles binarios
===============================
 
Enunciado
---------

-- Los árboles binarios se pueden representar mediante el tipo Arbol
-- definido por  
--    data Arbol a = H a 
--                 | N a (Arbol a) (Arbol a)
--                 deriving Show
-- Por ejemplo, el árbol
--         "B"
--         / \ 
--        /   \
--       /     \
--     "B"     "A"
--     / \     / \
--   "A" "B" "C" "C" 
-- se puede definir por 
--    ej1 :: Arbol String
--    ej1 = N "B" (N "B" (H "A") (H "B")) (N "A" (H "C") (H "C"))
--
-- Definir la función
--    enumeraArbol :: Arbol t -> Arbol Int
-- tal que (enumeraArbol a) es el árbol obtenido numerando las hojas y
-- los nodos de a desde la hoja izquierda hasta la raíz. Por ejemplo,
--    ghci> enumeraArbol ej1
--    N 6 (N 2 (H 0) (H 1)) (N 5 (H 3) (H 4))
-- Gráficamente, 
--          6 
--         / \ 
--        /   \
--       /     \
--      2       5 
--     / \     / \
--    0   1   3   4

-}


data Arbol a = H a 
             | N a (Arbol a) (Arbol a)
             deriving Show

ej1 :: Arbol String
ej1 = N "B" (N "B" (H "A") (H "B")) (N "A" (H "C") (H "C"))

enumeraArbol :: Arbol t -> Arbol Int
enumeraArbol a = snd $ enumeraDesde 0 a

enumeraDesde :: Int -> Arbol t -> (Int, Arbol Int)
enumeraDesde n (H _) = (n+1, H n)
enumeraDesde n (N _ izq der) = (n2+1, N n2 ai ad)
    where
        (n1, ai) = enumeraDesde n izq
        (n2, ad) = enumeraDesde n1 der


top :: Arbol t -> t
top (H a) = a
top (N a _ _) = a

enumeraDesde2 :: Int -> Arbol t -> Arbol Int
enumeraDesde2 n (H _) = H n
enumeraDesde2 n (N _ izq der) = N (top ad +1) ai ad
    where
        ai = enumeraDesde2 n izq
        ad = enumeraDesde2 (top ai +1) der




