module Ex_20140429 where

{-
Anagramas
=========

Una palabra es una anagrama de otra si se puede obtener permutando sus letras.
Por ejemplo, mora y roma son anagramas de amor.

Enunciado
---------

-- Definir la función
--    anagramas :: String -> [String] -> [String]
-- tal que (anagramas x ys) es la lista de los elementos de ys que son
-- anagramas de x. Por ejemplo,
--    anagramas "amor" ["Roma","mola","loma","moRa"] ==  ["Roma","moRa"]
-}

import Data.List
import Data.Char (toLower)

anagramas :: String -> [String] -> [String]
anagramas s = filter (anagrama s)

anagrama :: String -> String -> Bool
anagrama s r = let f = sort . map toLower in f s == f r
