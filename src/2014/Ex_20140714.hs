module Ex_20140714 where

{-
Todas tienen par
================

-- Definir el predicado
--    todasTienenPar :: [[Int]] -> Bool
-- tal que (todasTienenPar xss) se verifica si cada elemento de
-- la lista de listas xss contiene algún número par. Por ejemplo, 
--    todasTienenPar [[1,2],[3,4,5],[8]]  ==  True
--    todasTienenPar [[1,2],[3,5]]        ==  False
-}

todasTienenPar :: [[Int]] -> Bool
todasTienenPar = all (any even)


todasTienenPar2 :: [[Int]] -> Bool
todasTienenPar2 xss = and [ or [even x | x <- xs] | xs <- xss ]
